var NespressoModal={template:`
  <div class="nespresso-modal modal fade" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <a href="javascript:void(0)" class="close text-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title"></h4>
      </a>
     </div>
     <div class="modal-body">
     </div>
     <div class="modal-footer text-center">
      <button type="button" class="button alt" data-dismiss="modal">Close</button>
     </div>
    </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
 `,title:'',content:'',that:null,show:function(){var that=this;if($('.nespresso-modal').length==0){var template=template=this.template.replace('<h4 class="modal-title">','<h4 class="modal-title">'+this.title).replace('<div class="modal-body">','<div class="modal-body">'+this.content);$('body').append(template);$('.nespresso-modal').modal().on('hidden.bs.modal',function(){that.hide();});}},hide:function(){$('.nespresso-modal').remove();$('.modal-backdrop').remove();},};