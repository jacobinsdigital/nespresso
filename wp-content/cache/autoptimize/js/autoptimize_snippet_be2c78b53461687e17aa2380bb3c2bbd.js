$(document).ready(function(){$('#form-reset-password').on('submit',function(e){$validForm=$(this).validFormResetPassword();if(!$validForm){e.preventDefault();return false;}
return true;});});(function($){$.fn.validFormResetPassword=function(){$password=$(this).find('[name="password"]');$password_confirmation=$(this).find('[name="password_confirmation"]');$validation=new Validation();if(!$validation.validate_password($password))
return false;if(!$validation.validate_password_confirmation($password,$password_confirmation))
return false;return true;};})(jQuery);