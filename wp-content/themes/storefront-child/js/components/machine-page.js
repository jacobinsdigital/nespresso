
/**
 * machine-page.js
 */

$(document).ready(function() {

	/**
	 * adjust the review star span length dynamically by style
	 */
	$('[data-review-percentage]').each(function() {
		var percentage = $(this).attr('data-review-percentage');
		$('body').append('<!-- dynamic styling for review star lengths --><style>[data-review-percentage="'+ percentage +'"]:after { width: '+ percentage +'% !important; }</style>');
	});


	$('#write-review').on('click', function () {

		var is_owned = $(this).data( "owned" );
		if(is_owned === 'yes') {
			$("#review_form_wrapper").removeClass('hide');
		} else {
			sweetAlert('', 'Only logged in customers who have purchased this product may leave a review.', 'error');
		}
	});

	$("#cancel-comment").on('click',function () {
		$("#review_form_wrapper").addClass('hide');
	});
	
	MachinePage.init();
});

var MachinePage = {

	that: null,

	init : function() {

		this.content = $('main#content');
		this.btn_colors = this.content.find('.product__info__colours__list a');

		that = this;

		this.ready();
	}, // init()

	ready : function() {

		this.changePrice(this.btn_colors.first());

		this.btn_colors.on('click', function() {

			var data_image_id = $(this).attr('data-image-id');

			that.changeColorAndShowImage($(this));

			// change price
			that.changePrice($(this));

		});

	}, // init()

	changePrice : function(obj) {

		var variation_price = obj.attr('data-variation-price-html');

		if ( !variation_price )
			return false;

		$('.product__info__price').html(variation_price);

	}, //changePrice()

	/**
	 * change the active color and show the image that corresponds to that color
	 * 
	 * @param  {object} obj - current interacted object
	 */
	changeColorAndShowImage : function(obj) {

		this.btn_colors.removeClass('active');

		$('.product__slider__list').find('li').removeClass('active');

		obj.addClass('active');

		$('.product__slider__list li[data-image-id="'+ obj.attr('data-image-id') +'"]').addClass('active');

	}, // changeColorAndShowImage()


};
