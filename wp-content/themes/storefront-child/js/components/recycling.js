/**
 * master origins js
 */

$(document).ready(function() {
    $("html").addClass("g_mounted");

    if($(window).width() > '749') {
        $("html").addClass("desktop");
        $('.g_mobile').remove();
    } else {
        $('.g_desktop').remove();
        $("html").addClass("mobile");
    }

    $(window).on('scroll', function () {
        if($(window).width() > '749') {
            //play video
            if($('#video_1_checker').is(':within-viewport')) {
                $('#video_1').get(0).play();
            }

            if($('#video_2_checker').is(':within-viewport')) {
                $('#video_2').get(0).play();
            }
        }

        //fixed recycle add to cart
        if($('#recyclingBar').is(':within-viewport')) {
            $('#recyclingBar').addClass('vue_recyclingBar--fixed');
        }

        if ($('#section_10_checker').is(':within-viewport')) {
            $('#hand_image_10').addClass('fixed');
        } else {
            $('#hand_image_10').removeClass('fixed');
        }
    });

    $('#expand_section_8').on('click', function() {
        var collapsed_status   = $('#collapsed').data('label');
        if (collapsed_status == 'hidden') {
            $('#collapsed').addClass('v_opened');
             $('#collapsed').data('label', 'show');
        } else {
            $('#collapsed').removeClass('v_opened');
            $('#collapsed').data('label', 'hidden');
        }
    });

    $('.faq-btn').on('click', function () {
        var faq_num = $(this).data('faq');
        var exapanded = $(this).attr('aria-expanded');
        if (exapanded == 'false') {
            $(this).addClass('v_open');
            $(this).attr('aria-expanded','true');
            $('.faq-container-'+faq_num).show();
        } else {
            $(this).removeClass('v_open');
            $(this).attr('aria-expanded','false');
            $('.faq-container-'+faq_num).hide();
        }
    });

    var current_active_range = '';
    $('#secondLife_close').on('click', function () {
        $('#v_rangeSlider--'+current_active_range).click();
    });

    $('#v_rangeSlider--Alu, #v_link--Alu').on('click', function() {
        var slider_position = $('#range_slider').data('position');
        if (slider_position == 'middle') {
            $("html").css("overflow-y", "hidden");
            $('#aluminium').addClass('active');
            $('#secondLife').css('min-height', '100vh');
            $('#secondLife_close').addClass('active');
            $('#range_slider').css('left', '100%');
            $('#range_slider').data('position', 'right');
            $('.v_content').css('opacity', '0');
            $('.v_content').css('max-height', '0px');

            $('.v_sectionAluminium__video').addClass('active').css('opacity', '1');
            $('#ground').css('width', '0%');
            $('.v_sectionAluminium__header').addClass('onTop').css('margin-top','50px');
            $('#aluminium').css('width', '100%');
            $('.v_sectionAluminium__visual').hide();
            $('.v_sectionAluminium__video--1').get(0).play();
            $('.v_sectionAluminium__description').css('opacity', '1');
            current_active_range = 'Ground';

        } else if(slider_position == 'left') {
            $('#secondLife').css('min-height', '0vh');
            $('#secondLife_close').removeClass('active');
            $('#range_slider').css('left', '50%');
            $('#range_slider').data('position', 'middle');
            $('.v_content').css('opacity', '1');
            $('.v_content').css('max-height', '600px');

            $('.v_sectionGround__video').removeClass('active').css('opacity', '0');
            $('#aluminium').css('width', '50%');
            $('.v_sectionGround__header').removeClass('onTop').css('margin-top','0px');
            $('#ground').css('width', '50%');
            $('.v_sectionGround__visual').show();
            $('.v_sectionGround__video--0').get(0).pause();
            $('.v_sectionGround__video--0').get(0).load();
            $('.v_sectionGround__video--0').css('opacity','0');

            $("html").css("overflow-y", "auto");
            $('#ground').removeClass('active');
            $('.v_sectionGround__description').css('opacity', '0');
        }
        $('html, body').animate({
            scrollTop: $("#secondLife").offset().top
        }, 500);
        //reset scroll
        document.getElementById('ground').scrollTop = 0;
    });

    $('#v_rangeSlider--Ground, #v_link--Ground').on('click', function() {
        var slider_position = $('#range_slider').data('position');
        if (slider_position == 'middle') {
             $("html").css("overflow-y", "hidden");
            $('#ground').addClass('active');
            $('#secondLife').css('min-height', '100vh');
            $('#secondLife_close').addClass('active');
            $('#range_slider').css('left', '0%');
            $('#range_slider').data('position', 'left');
            $('.v_content').css('opacity', '0');
            $('.v_content').css('max-height', '0px');

            $('.v_sectionGround__video').addClass('active').css('opacity', '1');
            $('.v_sectionGround__video--0').css('opacity', '1');
            $('#aluminium').css('width', '0%');
            if($(window).width() > '749') {
                $('.v_sectionGround__header').addClass('onTop').css('margin-top','50px');
            }
            $('#ground').css('width', '100%');
            $('.v_sectionGround__visual').hide();
            $('.v_sectionGround__video--0').get(0).play();
            $('.v_sectionGround__description').css('opacity', '1');
            current_active_range = 'Alu';

        } else if(slider_position == 'right') {
            $('#secondLife').css('min-height', '0vh');
            $('#secondLife_close').removeClass('active');
            $('#range_slider').css('left', '50%');
            $('#range_slider').data('position', 'middle');
            $('.v_content').css('opacity', '1');
            $('.v_content').css('max-height', '600px');

            $('.v_sectionAluminium__video').removeClass('active').css('opacity', '0');
            $('#ground').css('width', '50%');
            $('.v_sectionAluminium__header').removeClass('onTop').css('margin-top','0px');
            $('#aluminium').css('width', '50%');
            $('.v_sectionAluminium__visual').show();
            $('.v_sectionAluminium__video--1').get(0).pause();
            $('.v_sectionAluminium__video--1').get(0).load();
            $('.v_sectionAluminium__video--0').get(0).pause();
            $('.v_sectionAluminium__video--0').get(0).load();
            $('.v_sectionAluminium__video--1').css('opacity','1');
            $('.v_sectionAluminium__video--0').css('opacity','0');
            $("html").css("overflow-y", "auto");
            $('#aluminium').removeClass('active');
            $('.v_sectionAluminium__description').css('opacity', '0');

        }
        $('html, body').animate({
            scrollTop: $("#secondLife").offset().top
        }, 500);
        //reset scroll
        document.getElementById('aluminium').scrollTop = 0;
    });

    $('.v_sectionAluminium__video--1').on('ended',function(){
        $('.v_sectionAluminium__video--0').get(0).play();
        $(this).css('opacity','0');
        $('.v_sectionAluminium__video--0').css('opacity','1');
        $(this).get(0).load();
    });
    $('.v_sectionAluminium__video--0').on('ended',function(){
        $('.v_sectionAluminium__video--1').get(0).play();
        $(this).css('opacity','0');
        $('.v_sectionAluminium__video--1').css('opacity','1');
        $(this).get(0).load();
    });

    $('a[href^="#"], .section_nav').on('click', function(event) {
        var target =  $(this).attr('href');
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 2000);
        }

    });

    $('#play_video').on('click', function () {
        $("html").addClass("v_scrollLock ");
        $('.vue').addClass('v_introductionPlaying');
        $('#video_container').removeClass('hide');
        $('#v_video_container')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
    });

    $('#pause_video').on('click', function () {
        $("html").removeClass("v_scrollLock ");
        $('.vue').removeClass('v_introductionPlaying');
        $('#video_container').addClass('hide');
        $('#v_video_container')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
    });
});