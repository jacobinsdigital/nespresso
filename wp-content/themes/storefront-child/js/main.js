$.cookie.json = true;
$.cookie.expires = 365;
var gwp_gift_status = $('#gwp_gift_status').val();
var gwp_gift_by_product_status = $('#gwp_gift_by_product_status').val(); //this is the new gwp by product

var VAT = 1; //10%
var VATREDUCE = 1;


var Products = {
    /*
    * [index]
    *  -> id
    *  -> name
    *  -> image_url
    *  -> price
    *  -> quantity
    *  -> type ENUM: coffee|machines|accessories
    */
    list:{},
    add_item : {},
    loader:true,

    add:function(item, callback){

        if ( typeof callback !== 'function')
            callback = null;

        if ( item.quantity < 1 ) {
            sweetAlert('Add to basket failed', 'Please add quantity greater than zero.', 'error');
            return false;
        }

        item.product_id = item.id;
        this.add_item = item;

        this.updateBack(null, callback);
        addToCartGTM(item);
    }, // add()

    get:function(item){
        var index = item.id;
        return this.list[index];
    },

    remove: function(cart_item_key){

        if ( typeof cart_item_key === 'undefined' || !cart_item_key ) {
            sweetAlert('Remove basket item failed', 'Invalid cart item key', 'error');
            return false;
        }

        var data = {
            action: 'remove_cart_item',
            cart_item_key: cart_item_key
        };

        if ( Object.keys(this.list).length < 2 )
            data.action = 'empty_cart';

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: ajaxurl,
            data: data,
            success: function (data) {
                // if ( data.status == 'failed') {
                //     sweetAlert('Remove basket item failed', data.message, 'error');
                //     return null;
                // } else {
                    updateBasketItems();
                // }
                $button_btn_qty = $('button[data-id="'+ cart_item_key +'"]').find('.btn-add-qty');
                $button_btn_qty.html(null);
                $button_btn_qty.removeClass('active');
                $button_btn_qty.siblings('.icon-Visit_on').addClass('icon-basket');
                $button_btn_qty.siblings('.icon-Visit_on').removeClass('icon-Visit_on');
            }
        });
        removeFromCartGTM(cart_item_key);

        // if (gwp_gift_status_by_product && gwp_gift_status_by_product == 'Active') {
        //     setTimeout(function () {
        //         addFreeGift2();
        //     }, 1000);
        // }
        if (gwp_gift_status && gwp_gift_status == 'Active') {
            setTimeout(function () {
                addFreeGift();
            }, 3000);
        }

        if (gwp_gift_by_product_status && gwp_gift_by_product_status == 'Active') {
            setTimeout(function () {
                addFreeGiftByProduct();
            }, 2000);
        }

    },

    reset: function(){
        this.list = {};
        this.list = this.updateBack('reset');
        return true;
    },

    updateBack: function(custom_action, callback){

        var data = {
            action: 'add_to_cart',
            products: [this.add_item]
        };

        if ( typeof custom_action !== 'undefined' && custom_action) {
            data.action = custom_action;
        }

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: ajaxurl,
            data: data,
            error: function (jqXHR, textStatus, errorThrown) {
                // console.log(errorThrown);
                // return false;
                console.log('Something went wrong. Please ask the administrator for more information.');
            },
            success: function (data) {

                if ( data.status == 'failed') {
                    sweetAlert('Add to basket failed', data.message, 'error');
                    return null;
                } else {
                    if ( typeof callback === 'function')
                        callback();
                }

                if (typeof generateShoppingBag == 'function'){
                    generateShoppingBag(data); //refresh shopping bag : shopping-bag.php
                }

                if (typeof generateMinibasket == 'function'){

                    generateMinibasket(); //refresh minibasket : header.php

                    if($(window).width()> 996) {
                        if(!$('.basket-dialog').is(":visible")){
                            $(".custom-dropdown.basket .btn-basket").click();
                            setTimeout(function () {
                                $('#mask-mini-basket').click();
                            }, 2000);
                        }
                    }

                } // endif

                this.list = data;
                if (gwp_gift_status && gwp_gift_status == 'Active') {
                    setTimeout(function () {
                        addFreeGift();
                    }, 3000);
                }

                if (gwp_gift_by_product_status && gwp_gift_by_product_status == 'Active') {
                    setTimeout(function () {
                        addFreeGiftByProduct();
                    }, 2000);
                }

                updateBasketItems();
                resetQtySelector();

            } // success()

        }); // ajax()

    } // updateBack()


}; // Products

$(document).ready(function(){

    updateBasketItems();

    $('[id=search-btn]').click(function(){
        $('[id=search-div]').toggleClass('hide-me');
        $('[class=search-field]').focus();
    });

    $(document).on('click', '.fa-search',function(){
        $('.searchbox').focus();
    });

    $(document).on('focus','.input-group input',function(){
        $(this).parents('.input-group').addClass('is-focused')
    });
    $(document).on('focusout','.input-group input',function(){
        $(this).parents('.input-group').removeClass('is-focused')
    });
    $(document).on('change','.input-group input',function(){
        checkInput($(this));
    });

    $('.input-group input').each(function(){
        checkInput($(this));
    });


    $(document).on('focusout','.input-group input',function(){
        $(this).parent('.input-group').removeClass('is-focused')
    });

    $(document).on('change','[data-cart] select',function(){

        var _nb = $(this).val(),
            _item = $(this).parent('[data-cart]'),
            _qtyItem = $('span.qty', _item);

        if(_nb != 0) {
            _qtyItem.text(_nb);
        } else {
            _qtyItem.text('').append('<i class="icon-plus"></i>');
        }

        Products.add({
            id:_item.data('id') || null,
            hqid:_item.data('hqid') || null,
            name:_item.data('name'),
            image_url:_item.data('image-url') || null,
            price:_item.data('price'),
            type:_item.data('type'),
            quantity:_nb,
            variation_id: $('.product__info__colours__list').find('a.active').data('variation-id'),
            range:item.data('range') || null,
            sku:item.data('sku') || null,
            technology:item.data('technology') || null
        });
        // launchToBasket(_item,_nb);
        // resetQtySelector();
        return false;

    }); $(document).on('change', 'select.mobi-sec',function(){

        var _nb = $(this).val(),
            _item = $(this),
            _qtyItem = $('span.qty', _item);

        if(_nb != 0) {
            _qtyItem.text(_nb);
        } else {
	        _qtyItem.text('').append('<i class="icon-plus"></i>');
        }

        Products.add({
            id:_item.data('id') || null,
            hqid:_item.data('hqid') || null,
            name:_item.data('name'),
            image_url:_item.data('image-url') || null,
            price:_item.data('price'),
            type:_item.data('type'),
            quantity:_nb,
            variation_id: $('.product__info__colours__list').find('a.active').data('variation-id'),
            range:_item.data('range') || null,
            sku:_item.data('sku') || null,
            technology:_item.data('technology') || null
        });
        // launchToBasket(_item,_nb);
        // resetQtySelector();
        return false;
    });

	$('#header').on('click', '.basket-product-close', function (e) {
		e.preventDefault();

        $(this).prev().find('select').val(0).change();

        //remove gift if discovery offer removed
        if ($(this).hasClass('discovery-offer-item')) {
            var gift_data = {
                action: 'get_discovery_gift_key',
            };

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: ajaxurl,
                data: gift_data,
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    // console.log(errorThrown);
                    // return false;
                    console.log('Something went wrong. Please ask the administrator for more information.');
                },
                success: function (data) {

                    if (data.cart_item_key) {
                        var data = {
                            action: 'remove_cart_item',
                            cart_item_key: data.cart_item_key
                        };

                        $.ajax({
                            dataType: 'json',
                            type: 'post',
                            url: ajaxurl,
                            data: data,
                            success: function (data) {
                                updateBasketItems();
                            }
                        });
                    }
                }
            });
        }
	});

    $('.activateDyslexia').click(function(){
        if($('body').hasClass('dyslexia')){
            $('body').removeClass('dyslexia');
            $.cookie('dyslexia',false);
            $('.dyslexia-desactivate').addClass('hidden');
            $('.dyslexia-activate').removeClass('hidden');
        }else{
            $('body').addClass('dyslexia');
            $.cookie('dyslexia',true);
            $('.dyslexia-desactivate').removeClass('hidden');
            $('.dyslexia-activate').addClass('hidden');
        }
    });

    if($.cookie('dyslexia')){
        $('body').addClass('dyslexia');
        $('.dyslexia-desactivate').removeClass('hidden');
        $('.dyslexia-activate').addClass('hidden');
    }

    $('.activateKeyboardNavigation').bind('focus',function(){
        $('body').addClass('keyboard-navigation');
    });

    // $(document).on('change','.mobi-sec',function(e) {
    //     $(this).parents('a[data-cart]').trigger('click');
    // });

    $(document).on('click','[data-cart]',function(e){

	    e.preventDefault();


        var item = $(this);

        if ( typeof item.data('single') != 'undefined' && item.data('single') == true ) {
            var variation_id = item.data('recommended') == true ? '' : $('.product__info__colours__list').find('a.active').data('variation-id');
            Products.add({
                id:item.data('id') || null,
                hqid:item.data('hqid') || null,
                name:item.data('name'),
                image_url:item.data('image-url') || null,
                price:item.data('price'),
                type:item.data('type'),
                quantity: 1,
                variation_id: variation_id,
                range:item.data('range') || null,
                sku:item.data('sku') || null,
                technology:item.data('technology') || null

            });
            return;
        }

        if((!$(this).hasClass('product-qty') || $(this).find('.product-qty-select').css('display') == 'none') && !$(this).hasClass('btn-disabled')) {
            resetQtySelector();

            var item = $(this);
            item.parent('.add-to-cart').addClass('click');

            if(item.data('unique') &&  !item.data('replace') ) {
                //this product is unique - no selector display

                Products.add({
                    id:item.data('id') || null,
                    hqid:item.data('hqid') || null,
                    name:item.data('name'),
                    image_url:item.data('image-url') || null,
                    price:item.data('price') || 0,
                    type:item.data('type'),
                    quantity:1,
                    variation_id: $('.product__info__colours__list').find('a.active').data('variation-id'),
                    range:item.data('range') || null,
                    sku:item.data('sku') || null,
                    technology:item.data('technology') || null
                });

                // item.find('.btn-add-qty').removeClass('icon-basket').html(1);
                // var _$textBtn = item.find('.btn-add-text');

                // if(_$textBtn.length > 0) {
                //     _$textBtn.html(_$textBtn.data('update'));
                // }
                // launchToBasket(item,1);
                return false;
            }

            //listener to leave/hide qty selector
            $('html, body').click(function(e){
                if($(e.target).hasClass('qty-selector') || $(e.target).hasClass('qty-input-custom') || $(e.target).hasClass('select-your-qty')) {
                    return false;
                }else{
                    resetQtySelector();
                }
            });

            if($('#mask-qty-selector').is(':visible')){
                var el = $('#mask-qty-selector');
                el.addClass('qty-activate');
            }else{
                var el = item.parent();
            }

            var direction = 'up';
            if(el.closest('.basket-dialog-product').length){//special direction for minibasket
                    //force left direction
                direction = 'left';
                //                if(el.closest('.basket-dialog-product').find('[data-cart]:first').is(el.find('[data-cart]'))){
                //                    direction = 'bottom'; //for first element in minibasket
                //                }else if(el.closest('.basket-dialog-product').find('[data-cart]:eq(1)').is(el.find('[data-cart]'))){
                //                    direction = 'bottom'; //for second element in minibasket
                //                }
            }

            el.prepend('<div id="qty-selector-content" class="direction-'+direction+'"></div>');

            el.find('#qty-selector-content')
                .loadTemplate(
                    $("#template-qty-selector") ,{
                        defaultValue:null
                    },{

                        overwriteCache: true,
                        success: function(){

                            // var tpl = $(this);
                            var tpl = $('#qty-selector-content');

                            // force to add step by 10 if coffee
                            if ( el.find('[data-cart]').attr('data-type') == 'Coffee' && el.find('[data-cart]').attr('data-aromatic-profile') != 'Varied') {
                                el.find('.qty-input-custom').attr('step', '10');
                                tpl.find('.machine-qty-select').addClass('hide');
                            } else if ( el.find('[data-cart]').attr('data-type') == 'Machine' || el.find('[data-cart]').attr('data-aromatic-profile') == 'Varied') {
                                //machine / varied
                                if($(window).width()> 500) {
                                    // if product is machine
                                    tpl.find('.qty-selection').addClass('hide');
                                    tpl.find('.machine-qty-select').removeClass('hide');
                                    tpl.css('top', tpl.position().top + 70 + 'px');
                                    el.find('.qty-input-custom').attr('step', '1');
                                    $('#select-qty-custom').addClass('hide');
                                    $('#input-qty-custom').removeClass('hide');
                                } else {
                                    $('.qty-selector').css('height', 90 +'px');
                                    $('.qty-selector').css('top',  -100 +'px');
                                    $('.qty-selection').addClass('hide');
                                    $('#input-qty-custom').addClass('hide');
                                }

                            } else {
                                //accessories
                                if($(window).width()> 500) {
                                    tpl.css('top', tpl.position().top + 120 + 'px');
                                    tpl.find('.qty-selection').addClass('hide');
                                    tpl.find('.machine-qty-select').addClass('hide');
                                    tpl.find('.qty-input').css('margin-top', 0);
                                    $('#select-qty-custom').addClass('hide');
                                    $('#input-qty-custom').removeClass('hide');
                                } else {
                                    $('.qty-selector').css('height', 90 +'px');
                                    $('.qty-selector').css('top',  -100 +'px');
                                    $('.qty-selection').addClass('hide');
                                    $('#input-qty-custom').addClass('hide');
                                }

                            }



                            tpl.find('.qty-selection').click(function(){

        	                    $(this).parent().find('.qty-selection').removeClass('active');
                            	$(this).addClass('active');
                                var nb = parseInt($(this).text());
                                tpl.find('.qty-input-custom').val(nb).trigger('change');
                                $('.qty-input-submit').click();

                                // Products.add({
                                //     id:item.data('id') || null,
                                //     name:item.data('name'),
                                //     image_url:item.data('image-url') || null,
                                //     price:item.data('price'),
                                //     type:item.data('type'),
                                //     quantity:nb
                                // });


                                // if(nb) {
        	                       //  item.find('.btn-add-qty').removeClass('icon-basket').html(nb).addClass('active').prev().removeClass('icon-basket').addClass('icon-Visit_on');
                                // } else {
        	                       //  item.find('.btn-add-qty').removeClass('icon-basket').html('').removeClass('active').prev().removeClass('icon-Visit_on').addClass('icon-basket');
                                // }
                                // var _$textBtn = item.find('.btn-add-text');

                                // if(_$textBtn.length > 0) {
                                //     _$textBtn.html(_$textBtn.data('update'));
                                // }

                                // launchToBasket(item,nb);
                                // resetQtySelector();
                                return true;
                            });

                            // tpl.find('.qty-input-submit').submit(function(){
                            //     var nb = $(this).find('.qty-input-custom').val();
                            //     Products.add({
                            //         id:item.data('id') || null,
                            //         name:item.data('name'),
                            //         image_url:item.data('image-url') || null,
                            //         price:item.data('price'),
                            //         type:item.data('type'),
                            //         quantity:nb
                            //     });
                            //     launchToBasket(item,nb);
                            //     resetQtySelector();
                            //     return false;
                            // });

                            $('#select-your-qty').find('.qty-input-submit').on('click', function(){

                                var nb = parseInt( $(this).parents('form#select-your-qty').find('.qty-input-custom').val() );

                                // disallow values less than zero
                                if ( nb < 0) {
                                    sweetAlert('Invalid Quantity', 'Please enter a quantity minimum of 1.', 'error');
                                    return false;
                                }

                                // allow only values multiple of 10, only for coffee
                                if ( el.find('[data-cart]').attr('data-type') == 'Coffee' &&
                                    nb % 50 != 0 && el.find('[data-cart]').attr('data-aromatic-profile') != 'Varied'
                                ) {
                                    // sweetAlert('Invalid Quantity', 'Nespresso coffee is packaged in sleeves which contains 50 capsules.', 'error');
                                    // return false;
                                }


                                Products.add({
                                    id:item.data('id') || null,
                                    hqid:item.data('hqid') || null,
                                    name:item.data('name'),
                                    image_url:item.data('image-url') || null,
                                    price:item.data('price'),
                                    type:item.data('type'),
                                    quantity:nb,
                                    variation_id: variation_id,
                                    range:item.data('range') || null,
                                    sku:item.data('sku') || null,
                                    technology:item.data('technology') || null
                                }, function() {

                                    // if(nb) {
                                    //     item.find('.btn-add-qty').removeClass('icon-basket').html(nb).addClass('active').prev().removeClass('icon-basket').addClass('icon-Visit_on');
                                    // } else {
                                    //     item.find('.btn-add-qty').removeClass('icon-basket').html('').removeClass('active').prev().removeClass('icon-Visit_on').addClass('icon-basket');
                                    // }
                                    // var _$textBtn = item.find('.btn-add-text');

                                    // if(_$textBtn.length > 0) {
                                    //     _$textBtn.html(_$textBtn.data('update'));
                                    // }

                                    // launchToBasket(item,nb);
                                    // resetQtySelector();

                                });

                                return false;
                            });
                        },
                        error: function(){
                            console.log('Error: Quantity selector for product basket is not loaded.');
                        }
                    }
                );


            return false;
        }
    });

    /**
     * remove a cart item
     */
    $(document).on('click','[data-cart-remove]',function(e){

        e.preventDefault();
        resetQtySelector();

        var cart_item_key =  $(this).attr('id');

        Products.remove(cart_item_key);
        // return false;
    });
    /**
    * Change Delivery Method : delivery-mode.php > step2.html
    */
    $('input[name="delivery_method"]:radio').change(function(){
        $('.delivery-method .bloc-delivery').removeClass('bloc-selected');
        $(this).parents('.bloc-delivery').addClass('bloc-selected');
    });

});

function checkInput($input) {
    if($input.val() != '') {
        $input.parents('.input-group').addClass('is-dirty');
    }
    else {
        $input.parents('.input-group').removeClass('is-dirty');
    }
}

function resetQtySelector(){
    $('#qty-selector-content').remove();
    $('#mask-qty-selector').removeClass('qty-activate');
    $('.add-to-cart').removeClass('click');
    // $('.qty-selection').off();
    $('#select-your-qty').off();
    $('html, body').off();
}

function launchToBasket(item,nb){
    if($('.basket-inner').length){
        item.parent('.add-to-cart').addClass('click');
        var cart = $('.basket-inner');

        if(nb && item.parent('.add-to-cart').length){
            var toDrag = item.parent('.add-to-cart');
            if(toDrag){
                var clone = toDrag.clone();
                clone.find('#qty-selector-content').remove();
                clone.find('[data-cart]').remove();

                clone.offset({
                    top: toDrag.offset().top,
                    left: toDrag.offset().left
                }).css({
                    'opacity': '0.8',
                    'position': 'absolute',
                    'height': '26px',
                    'width': '29px',
                    'z-index': '1000'
                }).appendTo($('body')).animate({
                    'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10
                }, 1000);

                clone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach();
                });
            }
        }

        if(nb){
            if(item.find('.qty-in-cart').length){
                item.find('.qty-in-cart').text(nb);
            }else{
                item.find('.fa').removeClass('fa-plus').removeClass('fa').addClass('qty-in-cart').text(nb);
            }
        }else{
            item.find('.qty-in-cart').removeClass('qty-in-cart').addClass('fa-plus').addClass('fa');
        }
        setTimeout(function(){
            item.parent('.add-to-cart').removeClass('click');
        },1000);
    }
}

function initPopover(){
	var settings = {
		trigger:'click',
		width: 265,
		multi:true,
		closeable:false,
		style:'',
		delay:300,
		padding:true,
		backdrop:false,
		placement:'top',
		animation:'pop'
	};

	$('.show-pop').webuiPopover('destroy').webuiPopover(settings);

	/*$('.form-control').on('focus',function(){
		$(this).webuiPopover('show');
	});*/

}

function saveLanguagePreference(lang) {
    // document.cookie = "nespresso-vn-language="+lang+"; expires=Fri, 01 Jan 2100 12:00:00 UTC";
    $.cookie('nespresso-vn-language',lang, { expires: 50*365 });
}

function languagePreference() {
    if ( typeof $.cookie('nespresso-vn-language') == 'undefined') {
        if (navigator.language == 'vi' || navigator.language == 'vn') {
            saveLanguagePreference('vn')

            window.location.href = 'vi'+window.location.pathname;
        } else {
            saveLanguagePreference('en')
            window.location.href = window.location.pathname.replace('vi/','');
        }
    }

}

$(document).ready(function() {
    languagePreference();
    //language
    var current_language = $('#language').val();
    current_language = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');
    // $('#lang_flag').removeClass();
    if (current_language == 'vi') {
        // $('#lang_flag').addClass('lang-vn');
        $('#lang_btn_flag').html('Vietnam');
        $('#language').val('vn');

        //override css
        overrideCssVn();
    } else {
        // $('#lang_flag').addClass('lang-en');
        $('#lang_btn_flag').html('English');
    }

    $('#language').on('change', function () {
        if (current_language != $(this).val()) {
            if ($(this).val() == 'en') {
                $('#lang_flag').addClass('lang-en');
                $('#lang_btn_flag').html('English');
                //save cookie
                saveLanguagePreference($(this).val());
                setTimeout(function(){
                    window.location.href = window.location.pathname.replace('vi/','');
                }, 400);
            } else {
                $('#lang_flag').addClass('lang-vn');
                $('#lang_btn_flag').html('Vietnam');
                //save cookie
                saveLanguagePreference($(this).val());

                setTimeout(function(){
                    window.location.href = 'https://'+window.location.host+'/vi'+window.location.pathname
                }, 400);
            }
        }
    });

    // Toggle content
    $(".js-toggle").on("click", function(e){
        e.preventDefault();
        var $content = $(this).data("target") !== undefined ? $(this).data("target") : $(this).next();
        $content.toggleClass("active");

        if( !$content.hasClass("active") ){
            $content.hide();
            $(this).removeClass('open');
            $(this).attr("data-toggle", "+");
        }else{
            $content.show();
            $(this).addClass('open');
            $(this).attr("data-toggle", "-");
        }
    });

    $('.btn-recommended-cart').on('click', function (e) {
        var screenwid = $(window).width();
        if (screenwid <= 767) {
            window.location = $(this).data('url');
            return;
        }
    });

    $('.btn-custom-add-to-cart').on('click', function (e) {
        //redirect if mobile
        var screenwid = $(window).width();
        if (screenwid <= 767) {

            if ( typeof $(this).data('page') === 'undefined') {
                window.location = $(this).data('product-url');
                return;
            }
        }

        e.preventDefault();
        var item = $(this);
        if (item.data('type') == 'Coffee')  {
            e.preventDefault();

            var item = $(this);

            if((!$(this).hasClass('product-qty') || $(this).find('.product-qty-select').css('display') == 'none') && !$(this).hasClass('btn-disabled')) {
                resetQtySelector();

                var item = $(this);
                item.parent('.add-to-cart').addClass('click');

                //listener to leave/hide qty selector
                $('html, body').click(function(e){
                    if($(e.target).hasClass('qty-selector') || $(e.target).hasClass('qty-input-custom') || $(e.target).hasClass('select-your-qty')) {
                        return false;
                    }else{
                        resetQtySelector();
                    }
                });

                if($('#mask-qty-selector').is(':visible')){
                    var el = $('#mask-qty-selector');
                    el.addClass('qty-activate');
                }else{
                    var el = item.parent();
                }

                var direction = 'up';
                if(el.closest('.basket-dialog-product').length){//special direction for minibasket
                    //force left direction
                    direction = 'left';
                }

                el.prepend('<div id="qty-selector-content" class="direction-'+direction+'"></div>');

                el.find('#qty-selector-content')
                    .loadTemplate(
                        $("#template-qty-selector") ,{
                            defaultValue:null
                        },{

                            overwriteCache: true,
                            success: function(){

                                // var tpl = $(this);
                                var tpl = $('#qty-selector-content');

                                // force to add step by 10 if coffee
                                el.find('.qty-input-custom').attr('step', '10');
                                tpl.find('.machine-qty-select').addClass('hide');


                                tpl.find('.qty-selection').click(function(){

                                    $(this).parent().find('.qty-selection').removeClass('active');
                                    $(this).addClass('active');
                                    var nb = parseInt($(this).text());
                                    tpl.find('.qty-input-custom').val(nb).trigger('change');
                                    $('.qty-input-submit').click();

                                    // Products.add({
                                    //     id:item.data('id') || null,
                                    //     name:item.data('name'),
                                    //     image_url:item.data('image-url') || null,
                                    //     price:item.data('price'),
                                    //     type:item.data('type'),
                                    //     quantity:nb
                                    // });


                                    // if(nb) {
                                       //  item.find('.btn-add-qty').removeClass('icon-basket').html(nb).addClass('active').prev().removeClass('icon-basket').addClass('icon-Visit_on');
                                    // } else {
                                       //  item.find('.btn-add-qty').removeClass('icon-basket').html('').removeClass('active').prev().removeClass('icon-Visit_on').addClass('icon-basket');
                                    // }
                                    // var _$textBtn = item.find('.btn-add-text');

                                    // if(_$textBtn.length > 0) {
                                    //     _$textBtn.html(_$textBtn.data('update'));
                                    // }

                                    // launchToBasket(item,nb);
                                    // resetQtySelector();
                                    return false;
                                });

                                // tpl.find('.qty-input-submit').submit(function(){
                                //     var nb = $(this).find('.qty-input-custom').val();
                                //     Products.add({
                                //         id:item.data('id') || null,
                                //         name:item.data('name'),
                                //         image_url:item.data('image-url') || null,
                                //         price:item.data('price'),
                                //         type:item.data('type'),
                                //         quantity:nb
                                //     });
                                //     launchToBasket(item,nb);
                                //     resetQtySelector();
                                //     return false;
                                // });

                                $('#select-your-qty').find('.qty-input-submit').on('click', function(){

                                    var nb = parseInt( $(this).parents('form#select-your-qty').find('.qty-input-custom').val() );

                                    // disallow values less than zero
                                    if ( nb < 0) {
                                        sweetAlert('Invalid Quantity', 'Please enter a quantity minimum of 1.', 'error');
                                        return false;
                                    }

                                    // allow only values multiple of 10, only for coffee
                                    if ( el.find('[data-cart]').attr('data-type') == 'Coffee' &&
                                        nb % 50 != 0 && el.find('[data-cart]').attr('data-aromatic-profile') != 'Varied'
                                    ) {
                                        // sweetAlert('Invalid Quantity', 'Nespresso coffee is packaged in sleeves which contains 50 capsules.', 'error');
                                        // return false;
                                    }


                                    Products.add({
                                        hqid:item.data('hqid') || null,
                                        id:item.data('id') || null,
                                        name:item.data('name'),
                                        image_url:item.data('image-url') || null,
                                        price:item.data('price'),
                                        type:item.data('type'),
                                        quantity:nb,
                                        variation_id: $('.product__info__colours__list').find('a.active').data('variation-id'),
                                        range:item.data('range') || null,
                                        sku:item.data('sku') || null,
                                        technology:item.data('technology') || null
                                    }, function() {

                                        // if(nb) {
                                        //     item.find('.btn-add-qty').removeClass('icon-basket').html(nb).addClass('active').prev().removeClass('icon-basket').addClass('icon-Visit_on');
                                        // } else {
                                        //     item.find('.btn-add-qty').removeClass('icon-basket').html('').removeClass('active').prev().removeClass('icon-Visit_on').addClass('icon-basket');
                                        // }
                                        // var _$textBtn = item.find('.btn-add-text');

                                        // if(_$textBtn.length > 0) {
                                        //     _$textBtn.html(_$textBtn.data('update'));
                                        // }

                                        // launchToBasket(item,nb);
                                        // resetQtySelector();

                                    });

                                    return false;
                                });
                            },
                            error: function(){
                                console.log('Error: Quantity selector for product basket is not loaded.');
                            }
                        }
                    );


                return false;
            }
        } else {
            Products.add({
                hqid:item.data('hqid') || null,
                id:item.data('id') || null,
                name:item.data('name'),
                image_url:item.data('image-url') || null,
                price:item.data('price'),
                type:item.data('type'),
                quantity: 1,
                variation_id: $('.product__info__colours__list').find('a.active').data('variation-id'),
                range:item.data('range') || null,
                sku:item.data('sku') || null,
                technology:item.data('technology') || null
            });
        }

    });

    $(document).on('click','[id=btn-reorder]',  function(){
        var data = {
            action: 'reorder_item',
        };
        var last_order = $(this).data('val');
        ajaxReorder(last_order, data);

    });

    $(document).on('click','[id=btn-reorder-co]',  function(){
        var id = $(this).data('id');
        var data = {
            action: 'reorder_item_co',
            id: id,
        };
        var last_order = $(this).data('val');
        ajaxReorder(last_order, data);
    });

    function ajaxReorder(last_order, data){
         if (last_order) {
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: ajaxurl,
                data: data,
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    // console.log(errorThrown);
                    // return false;
                    console.log('Something went wrong. Please ask the administrator for more information.');
                },
                success: function (data) {
                    var oos = '';
                    if (data.prod) {
                        if (data.prod.length) {
                            $.each(data.prod,function(value, index){
                                oos += index+"\n";
                            });
                            swal('Out of stock', oos);
                        }
                    }
                    updateBasketItems();
                    openBasket();
                } // success()

            });
        } else {
            swal('','There are no completed purchases to re-order.');
        }
    }
	function openBasket(){
        $basketDialog = $(".basket-dialog");
        $cartIcon = $(".custom-dropdown.basket .btn-basket");
        $mask = $('#mask-mini-basket');
        opened = false;
        var screenwid = $(window).width();
        var desktopScreen = 996;

        if (opened) {
            closeBasket($basketDialog, $cartIcon, $mask, function(){
                    opened = false;
            });
        }else{
            $basketDialog.show();

            opened = true;

            if (screenwid <= desktopScreen) {
                setTimeout(function(){
                    $cartIcon.closest(".basket").addClass("opened");
                }, 10);
            }
            $mask.fadeIn(200);
            setTimeout(function(){
                $basketDialog.addClass("opened");
            }, 10);
        }

    }

    function closeBasket($basketDialog, $cartIcon, $mask, cb){
            $mask.fadeOut(200);

            $basketDialog.removeClass("opened");
            $cartIcon
                .closest(".basket")
                .removeClass("opened");

            setTimeout(function(){
                $basketDialog.hide();

                if( cb !== undefined )
                    cb();
            }, 500);
    }

	//Popover (=ToolTip)
	initPopover();

	//init lightbox callback pour js dans lightbox
	$.featherlight.defaults.afterOpen = function(event){

		/**
 		* Pickup Boutiques
 		*/
		$('.pickup__shop__seeaddress').click(function(e)
			{
				//état actif du bouton
				var currentToggler = $(this);
				if(currentToggler.is('.active')){
					$('.pickup__shop__seeaddress').removeClass('active'); //init tous les boutons
					//currentToggler.addClass('active'); //laisse actif le bouton courant
				}else{
					currentToggler.addClass('active'); //desactive le bouton courant
				}

				//fermeture des panneaux et ouverture du panneau correspondant
				var currentToggle = $(this).parent().find(".pickup__shop__address");
				if(currentToggle.is('.active')){
					currentToggle.removeClass('active');
				}else{
					$('.pickup__shop__address').removeClass('active');
					currentToggle.addClass('active');
				}

				e.preventDefault();
				return false;
			});

		/**
 		* Pickup Points
 		*/
		var switchers = $('.tab-switcher a');

		switchers.on('click', function(e)
		{

			switchers
				.removeClass('active')
				.parent().removeClass('active');

			$(this)
				.addClass('active')
				.parent().addClass('active');

			var target = $(this).attr('href');
			$('.pickup__panel')
				.removeClass('active')
				.filter(target).addClass('active');

			e.preventDefault();
			return false;
		});

	};
});


/**
 * update the button counter
 * @return {[type]} [description]
 */
function updateProductButtonCounter() {

    if ( Products.list.length < 1 )
        return;

    $.each ( Products.list, function(i, product) {
        var productButton = $('button[data-id="'+ product.product_id +'"]');
        productButton.find('.icon-basket').addClass('icon-Visit_on').removeClass('icon-basket');
        productButton.find('.btn-add-qty').addClass('active').html(product.quantity);
    });
} // updateProductButtonCounter()

//gift
function addFreeGift() {

//gwp by price
    var data = {
        action: 'fetch_gwp_gifts',
    };
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: ajaxurl,
        data: data,
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            // console.log(errorThrown);
            // return false;
            console.log('Something went wrong. Please ask the administrator for more information.');
        },
        success: function (data) {
            if(data.status) {
                var has_previous_gift = data.has_previous;
                var gift_name = data.gwp_gift.name;
                var cart_amount = data.amount;
                var gift_item = {
                    id: data.gwp_gift.post_id,
                    name: data.gwp_gift.name,
                    image_url: data.gwp_gift.image,
                    price: data.gwp_gift.price,
                    type: data.gwp_gift.product_type,
                    quantity: 1
                };
                var data = {
                    action: 'add_to_cart',
                    products: [gift_item]
                };
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: ajaxurl,
                    data: data,
                    error: function (jqXHR, textStatus, errorThrown) {
                        // console.log(errorThrown);
                        // return false;
                        console.log('Something went wrong. Please ask the administrator for more information.');
                    },
                    success: function (data) {
                        if ( data.status != 'failed') {

                            if (typeof generateShoppingBag == 'function'){
                                generateShoppingBag(data); //refresh shopping bag : shopping-bag.php
                            }

                            if (typeof generateMinibasket == 'function'){

                                generateMinibasket(); //refresh minibasket : header.php

                            } // endif

                            updateBasketItems();
                            resetQtySelector();
                            if(!has_previous_gift) {
                                if ($.cookie('nespresso-vn-language') == 'vi' || $.cookie('nespresso-vn-language') == 'vn') {
                                    sweetAlert('Congratulation', 'You will receive a free gift with your order', 'success');
                                } else {
                                    sweetAlert('Chúc mừng', 'Bạn sẽ nhận được một món quà miễn phí với đơn đặt hàng của bạn', 'success');
                                }

                            }
                        }
                    } // success()

                }); // ajax()
            } else if(!data.status) {
                if(has_previous_gift) {

                }
                updateBasketItems();
                resetQtySelector();
            }

        } // success()

    });
}

function addFreeGiftByProduct() {

    //gwp by product
    var data = {
        action: 'fetch_gwp_gift_by_product',
    };
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: ajaxurl,
        data: data,
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            // console.log(errorThrown);
            // return false;
            console.log('Something went wrong. Please ask the administrator for more information.');
        },
        success: function (data) {
            if(data.status) {
                var gift_name = '';
                var has_previous_gift = data.has_previous;
                var coys_gift_product = [];
                $.each( data.qualified_gift, function( gift, args ) {
                    coys_gift_product.push(args);
                });
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: ajaxurl,
                    data: {
                        action: 'add_to_cart',
                        products: coys_gift_product,
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // console.log(errorThrown);
                        // return false;
                        console.log('Something went wrong. Please ask the administrator for more information.');
                    },
                    success: function (data) {
                        if ( data.status != 'failed') {

                            if (typeof generateShoppingBag == 'function'){
                                generateShoppingBag(data); //refresh shopping bag : shopping-bag.php
                            }

                            if (typeof generateMinibasket == 'function'){

                                generateMinibasket(); //refresh minibasket : header.php

                            } // endif

                            updateBasketItems();
                            resetQtySelector();

                            if(!has_previous_gift) {
                                if ($.cookie('nespresso-vn-language') == 'vi' || $.cookie('nespresso-vn-language') == 'vn') {
                                    sweetAlert('Congratulation', 'You will receive a free gift with your order', 'success');
                                } else {
                                    sweetAlert('Chúc mừng', 'Bạn sẽ nhận được một món quà miễn phí với đơn đặt hàng của bạn', 'success');
                                }
                            }

                        }
                    } // success()

                }); // ajax()
            } else {
                updateBasketItems();
                resetQtySelector();
            }
        } // success()

    });
}

function updateBasketItems() {
    /**
     * get cart products on load
     */
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: ajaxurl,
        data: {
            action: 'fetch_cart'
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // console.log(errorThrown);
            console.log('Something went wrong. Please ask the administrator for more information.');
        },
        success: function (data) {
            // if(Object.keys(data).length>0){
                Products.list = data;
            // }

            Products.loader = true;

            generateMinibasket();
            updateProductButtonCounter();
        }
    });
}

function overrideCssVn()
{
    $('.override-css').remove();
    $('.override-vn-css').show();
}

$(document).ready(function(){

    $("#billing_country").change(function(){
        var a = $(this).val();
        $.ajax({
            method : "GET",
            url : ajaxurl,
            dataType: 'json',
            data : {
                action : 'dropdown_change',
                country : a,
            },
    error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                // return false;
                // console.log(errorThrown);
                console.log('Something went wrong. Please ask the administrator for more information.');
            },
    success : function(c) {
        if (c != 0) {
            var k=0;
            // postsHtml = '';
            var statess = [];
            $.each(c, function(scode, sname) {
                // var sel = k == 0 ? scode :""
                // postsHtml += '<option value="'+scode+'">'+sname+'</option>';
                // k++;
                statess.push(sname);
            });
        }else{
                // postsHtml = '<option >No state listed</option>';

        }
            // $("#billing_state").html(postsHtml);

             $("#billing_state").autocomplete({
                source: statess
            });
            }
        });
         $("#billing_state > option:eq(0)").prop('selected',true);
    });

    // Modal Advisory

    if ($('.call-modal-advisory').length > 0) {

        // When the user clicks the button, open the modal
        $('.call-modal-advisory').on('click', function () {
            var advisroy_callback = $(this).data('callback');
            if (advisroy_callback == 'how-to-order') {
                var modal = $('#modal-advisory');
                modal.css('display','block');
                // When the user clicks on <span> (x), close the modal
                $('.close-modal-advisory, .dismiss-modal-advisory').on('click', function () {
                    modal.css('display','none');
                });

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal[0]) {
                        modal.css('display','none');
                    }
                }
            }
        });
    }

    // Modal Slider

    if ($('.call_pop_up_slider').length > 0) {

        // When the user clicks the button, open the modal
        $('.call_pop_up_slider').on('click', function () {
            var modal = $('#modal-slider');
            modal.css('display','block');
            // When the user clicks on <span> (x), close the modal
            $('.close-modal-slider, .dismiss-modal-slider').on('click', function () {
                modal.css('display','none');
            });

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal[0]) {
                    modal.css('display','none');
                }
            }

        });
    }

    //legal
    var current_url = window.location.href;
    if (current_url.includes('legal?q=privacy-policy')) {
        $('#hrf-entry-8663').find('h3').addClass('open-faq');
        $('#hrf-content-8663').show();
    }

    //re-order
    $('[id=btn-reorder-login]').on('click',function(){
        swal({
             title: "Re-Order",
             text: "Please Login to proceed.",
             confirmButtonText: "Login"
            },
            function(){
                window.location="/login"
        });
    })
});
