<?php
/**
 *
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with accessory products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * get all the accessory products, not trached
 * sorted by post id
 *
 * @return array
 */
function get_accessory_products() {

	global $wpdb;

	$posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='product_type' AND meta_value='%s' ", 'Accessory') );

	$product_count = 0;

	$products = [];

	if ( !empty($posts) && $posts ) :

	    foreach ( $posts as $post ) :

	        $product = wc_get_product($post->post_id);

	    	$product = $product->get_data();

	    	if ( !$product ||
	    		$product['status'] != 'publish' ||
	    		$product['stock_status'] != 'instock'
    		) {
	    		continue;
    		}

	        $product = include_accessory_product_meta_from_results($post);

	        if ( !$product )
	    		continue;

	        $products[$product->collection][] = $product;

	        $product_count++;

	    endforeach;

	    $products ? ksort($products) : '';
	endif;

	return $products;
} // get_accessory_products()

/**
 * include the accessory product meta(custom) data from the result given
 *
 * @param  object|null $product - product result
 * @return object
 */
function include_accessory_product_meta_from_results( $product=null, $single_product_page=false ) {

	if ( isset($product->post_id) )
		$product_id = $product->post_id;
	elseif ( isset($product->ID) )
		$product_id = $product->ID;
	elseif ( isset($product->id) )
		$product_id = $product->id;
	else
		$product_id = 0;

	if ( get_post_status($product_id) == 'trash' )
		return null;

	return (object) array_merge(
		(array) $product,
		(array) get_accessory_product_meta_data( $product_id, $single_product_page )
	);

} // include_accessory_product_meta_from_results()

/**
 * get the accesssory product meta(custom) data
 *
 * @param  integer $product_id
 * @param  boolean $single_product_page
 * @return object|null
 */
function get_accessory_product_meta_data( $product_id=0, $single_product_page=false ) {

	$product_id = $product_id ? $product_id : 0;

	$product = (object)null;

	$product->product_id = $product_id;

	$product->name = get_the_title( $product_id );

	$product->url = get_permalink( $product_id );

	$product->price = get_post_meta( $product_id, '_price', true );

	$product->description = get_field('description', $product_id );

	$product->category = get_field( 'accessory_category', $product_id );

	$product->collection = get_field( 'collection', $product_id );

	$product->video_url = get_field( 'video_url', $product_id );

	$product->product_type = get_field( 'product_type', $product_id );

	$product->recommended_products = $single_product_page ? get_recommended_accessory_products($product_id) : '';

	$product->image = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );

	return $product;

} // get_accessory_product_meta_data()

/**
 * get recommended accessory products from acf form
 *
 * @param  integer $product_id
 * @return array|string - string = no product
 */
function get_recommended_accessory_products($product_id=0) {

	if ( !$product_id )
		return '';

	$recommended_products = get_field( 'recommended_products', $product_id );

	if ( !$recommended_products )
		return '';

	$return = [];
	foreach ( $recommended_products as $recommended_product ) :
		$return[]  = (object) array_merge(
			(array) $recommended_product,
			(array) get_accessory_product_meta_data( $recommended_product->ID, $single_product_page=false )
		);
	endforeach;

	return $return;

} // single_product_page()
