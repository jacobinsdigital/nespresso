<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( isset($_POST['form-for']) && $_POST['form-for'] == 'express-checkout' ) :

    $validation = new Validation();

    global $nespresso_my_express_checkout_hook_script;
    $nespresso_my_express_checkout_hook_script = '';
    if ( !$validation->valid_shipping_mode() ) :
        $nespresso_my_express_checkout_hook_script = 'validation.set_valid(false).validate_shipping_mode();';

    elseif ( $_POST['shipping_mode'] == 'standard' && !$validation->valid_shipping_mode_standard_options() ) :
        $nespresso_my_express_checkout_hook_script = 'validation.set_valid(false).validate_shipping_mode_standard_options();';

    elseif ( !$validation->valid_payment_mode() ):
        $nespresso_my_express_checkout_hook_script = 'validation.set_valid(false).validate_payment_mode();';

    else :

        $user = nespresso_get_user_data();

        update_user_meta( $user->ID, 'shipping_mode', sanitize_text_field( $_POST['shipping_mode'] ) );
        update_user_meta( $user->ID, 'shipping_mode_standard_options', sanitize_text_field( isset($_POST['shipping_mode_standard_options']) ? $_POST['shipping_mode_standard_options'] : 0 ) );
        update_user_meta( $user->ID, 'payment_mode', sanitize_text_field( $_POST['payment_mode'] ) );
        update_user_meta( $user->ID, 'express_checkout_active', sanitize_text_field( $_POST['express_checkout_active'] ) );

        $nespresso_my_express_checkout_hook_script = 'validation.success_information_updated();';

    endif;

    // put some scripts on the bottom
    if ( $nespresso_my_express_checkout_hook_script ) :
        function nespresso_my_express_checkout_hook() {
            global $nespresso_my_express_checkout_hook_script;
        ?>
            <script type="text/javascript">
                var validation = new Validation();
                <?= $nespresso_my_express_checkout_hook_script ?>
            </script>
        <?php
        } // nespresso_my_express_checkout_hook()
        add_action('wp_footer', 'nespresso_my_express_checkout_hook');
    endif;

endif;


$my_account_url = get_permalink( get_option('woocommerce_myaccount_page_id') );

$user = nespresso_get_user_data();

$current_url = home_url(add_query_arg(null, null));

?>

<main id="content">
	<div class="container">
	    <div class="row">

	        <!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <form action="<?= $current_url ?>" method="post" id="form-express-checkout" role="form">

			    <input type="hidden" name="form-for" value="express-checkout">

			    <div class="col-xs-12 col-md-8-5 account-detail  my-expresscheckout">
			        <header>
			            <h2 class="mr-bottom-0"><?= get_the_title() ?></h2>
			        </header>
			        <div class="row hide">
			            <div class="col-xs-12 my-expresscheckout__description"></div>
			        </div>

			        <div class="row">
			        	<div class="col-xs-12">
			                <div class="my-expresscheckout-step">
			                    <div class="my-expresscheckout-step__header">
			                        <a class="my-expresscheckout-step__link toggler" href="#" title="Choose the first step : shipping information">
			                            <h3 class="my-expresscheckout-step__title">1 - SHIPPING INFORMATION</h3>
			                            <i class="fa fa-angle-down"></i>
			                        </a>
			                    </div>
			                    <div class="my-expresscheckout-step__content step-toggle">
			                    	<p>(available for your default shipping address)</p>

			                        <p class="address__preview">
			                            <?= $user->title ?> <?= $user->billing_first_name ?> <?= $user->billing_last_name ?>
			                            <br />
			                            <?= $user->billing_first_name ?>
			                            <br />
			                            <?= $user->billing_address_1 ?>
			                            <br />
			                            <?= $user->billing_postcode ?>
			                            <br />
			                            <?= $user->billing_city ?>
			                        </p>

			                        <a class="btn btn-primary" href="<?= $my_account_url ?>/my-addresses" title="Change your default address">Change your default address</a>


			                        <hr class="hide" />
			                        <div class="radio my-expresscheckout-step__radio hide">
			                            <label for="shipping-mode">
			                                <input type="radio"
			                                	value="none"
			                                	name="shipping_mode"
			                                	checked="checked"
			                                	<?= isset($user->shipping_mode) && $user->shipping_mode == 'none' ? 'checked="checked"' : '' ?>
		                                	>
			                                None
			                            </label>
			                        </div>

			                        <div class="my-expresscheckout-ship-option hide">
			                            <div class="radio">
			                                <label for="shipping-mode-standard">
			                                    <input type="radio"
			                                    	value="standard"
			                                    	name="shipping_mode"
			                                    	<?= isset($user->shipping_mode) && $user->shipping_mode == 'standard' ? 'checked="checked"' : '' ?>
		                                    	>
			                                    <i class="icon-delivery_box my-expresscheckout-ship-option__icon"></i>
			                                    <span>Standard Delivery</span>
			                                </label>
			                            </div>
			                            <div class="content">
			                                <p>Delivery within one day (Monday to Saturday)* for all orders placed before 7pm (Monday to Friday).<br />
			                                    *For a delivery on Saturday, please select the free “Saturday Delivery” option</p>
			                                <div class="shipping-options">
			                                    <span>Available options :</span>
			                                    <div class="checkbox">
			                                        <label>
			                                            <input type="checkbox"
			                                            	name="shipping_mode_standard_options"
			                                            	value="Saturday Delivery"
			                                            	<?= isset($user->shipping_mode_standard_options) && $user->shipping_mode_standard_options == 'Saturday Delivery' ? 'checked="checked"' : '' ?>
		                                            	>
			                                            <span>Saturday Delivery <a href="#" class="link-btn" title="Information about the Saturday Delivery"><i class="fa fa-info"></i></a></span>
			                                        </label>
			                                        <span class="price">(Free)</span>
			                                    </div>
			                                    <div class="checkbox">
			                                        <label>
			                                            <input type="checkbox"
			                                            	name="shipping_mode_standard_options"
			                                            	value="Recycling at home"
			                                            	<?= isset($user->shipping_mode_standard_options) && $user->shipping_mode_standard_options == 'Recycling at home' ? 'checked="checked"' : '' ?>
		                                            	>
			                                            <span>Recycling at home <a href="#" class="link-btn" title="Information about the Recycling at home"><i class="fa fa-info"></i></a></span>
			                                        </label>
			                                        <span class="price">(Free)</span>
			                                    </div>
			                                    <div class="checkbox">
			                                        <label>
			                                            <input type="checkbox"
			                                            	name="shipping_mode_standard_options"
			                                            	value="Signature"
			                                            	<?= isset($user->shipping_mode_standard_options) && $user->shipping_mode_standard_options == 'Signature' ? 'checked="checked"' : '' ?>
		                                            	>
			                                            <span>Signature <a href="#" class="link-btn" title="Information about the Signature"><i class="fa fa-info"></i></a></span>
			                                        </label>
			                                        <span class="price">(Free)</span>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>

			                    </div>

			                </div>
			             </div>
			        </div>

			        <div class="row">
			        	<div class="col-xs-12">
			                <div class="my-expresscheckout-step">
			                    <div class="my-expresscheckout-step__header">
			                        <a class="my-expresscheckout-step__link toggler" href="#" title="Choose the first step : shipping mode">
			                            <h3 class="my-expresscheckout-step__title">2 - PAYMENT OPTION</h3>
			                            <i class="fa fa-angle-down"></i>
			                        </a>
			                    </div>
			                    <div class="my-expresscheckout-step__content step-toggle">
			                    	<div class="radio my-expresscheckout-step__radio">
			                            <label for="payment-mode">
			                                <input type="radio"
			                                	value="none"
			                                	id="payment-mode-none"
			                                	name="payment_mode"
			                                	<?= isset($user->payment_mode) && $user->payment_mode == 'none' ? 'checked="checked"' : '' ?>
			                                >
			                                None
			                            </label>
			                        </div>
			                        <div class="radio my-expresscheckout-step__radio my-expresscheckout-step__creditcard">
			                            <label for="payment-mode-cb">
			                                <input type="radio"
			                                	value="Credit Card"
			                                	id="payment-mode-cb"
			                                	name="payment_mode"
			                                	<?= isset($user->payment_mode) && $user->payment_mode == 'Credit Card' ? 'checked="checked"' : '' ?>
		                                	>
			                                <i class="fa fa-credit-card my-expresscheckout-step__radio__icon"></i>
			                                Credit Card
			                                <br>
			                                <a class="hide link link--right my-expresscheckout-step__creditcard__link" href="#" title="Learn More about the Credit Card payment">Learn more</a>
			                            </label>

			                        </div>
			                        <div class="radio my-expresscheckout-step__radio my-expresscheckout-step__invoice">
			                            <label for="payment-mode-invoice">
			                                <input type="radio"
			                                	value="Bank Deposit"
			                                	id="payment-mode-invoice"
			                                	name="payment_mode"
			                                	<?= isset($user->payment_mode) && $user->payment_mode == 'Bank Deposit' ? 'checked="checked"' : '' ?>
		                                	>
			                                <i class="fa fa-envelope-o my-expresscheckout-step__radio__icon"></i>
			                                Bank Deposit
			                                <small class="hide my-expresscheckout-step__invoice__info clearfix">Payable upon 30 days</small>
			                            </label>
			                        </div>
			                        <div class="radio my-expresscheckout-step__radio my-expresscheckout-step__postfinancecard">
			                            <label for="payment-mode-postfinance">
			                                <input type="radio"
			                                	value="Cash on Delivery"
			                                	id="payment-mode-postfinance"
			                                	name="payment_mode"
			                                	<?= isset($user->payment_mode) && $user->payment_mode == 'Cash on Delivery' ? 'checked="checked"' : '' ?>
		                                	>
			                                <i class="fa fa-money my-expresscheckout-step__radio__icon"></i>
			                                <span class="my-expresscheckout-step__radio__shiftedlabel">
			                                	Cash on Delivery
		                                	</span>
			                                <small class="my-expresscheckout-step__invoice__info clearfix">
			                                	<i>
			                                		Cash on Delivery payment option is available on Metro Manila orders only
			                                	</i>
		                                	</small>
			                            </label>
			                        </div>

			                    </div>
			                </div>
			             </div>
			        </div>

			        <div class="row">
			            <div class="col-xs-12">
			                <div class="my-expresscheckout-step">
			                    <div class="my-expresscheckout-step__header">
			                        <h3 class="my-expresscheckout-step__title">3 - ACTIVATE EXPRESS CHECK OUT <a href="#" class="my-expresscheckout-step__info" title="Information about the Express Checkout"><i class="icon icon-Information"></i></a></h3>
			                    </div>

			                    <div class="my-expresscheckout-step__content active">
			                        <div class="radio">
			                        	<label>
			                        		<input type="checkbox"
			                        			value="1"
			                        			name="express_checkout_active"
			                        			<?= isset($user->payment_mode) && $user->payment_mode ? 'checked="checked"' : '' ?>
		                        			>
			                        		I want to activate express checkout
		                        		</label>
	                        		</div>
			                        <p class="mr-top-10 my-expresscheckout__description">
			                        	By checking this box, you will skip the Shipping Information and Payment Option step during the checkout process.
			                        </p>
			                        <p class="mr-top-5 my-expresscheckout__description">
			                        	Should you wish to change your Express Checkout preference, you may select a new Payment Option on this page or uncheck this box.
			                        </p>
			                    </div>

			                </div>
			            </div>
			        </div>

			        <div class="account-detail__footer">
			            <a href="#" class="btn btn-primary btn-icon" title="Back to online shop"><i class="icon-arrow_right"></i>Back</a>
			            <button type="submit" class="btn btn-primary pull-right" title="Save">Save</button>
			        </div>
			    </div><!-- .my-expresscheckout -->

			</form>

		</div>
	</div>
</main>

<?php
	global $scripts;
	$scripts[] = 'js/components/validation.js';
	$scripts[] = 'js/components/my-expresscheckout.js';
?>
