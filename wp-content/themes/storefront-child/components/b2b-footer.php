<?php
/**
 * Nespresso Theme developed by Ramon Christopher Morales
 *
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>

        
<!-- <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/js/jquery-2.1.4.min.js"></script> -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/assets/scroll-up/scroll-up.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/assets/slick/slick.js"></script>
<!-- <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/assets/bootstrap-3.3.7/js/bootstrap.min.js"></script> -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/js/global.js"></script>