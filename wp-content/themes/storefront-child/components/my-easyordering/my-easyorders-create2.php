<?php
$activeMenu = 1;
$activeStep = 2;
?>
<div class="container">
    <div class="row">

        <!-- Sidebar account -->
        <div class="col-xs-12 col-md-3 account-sidebar">
            <?php include('components/my-account/menu.php'); ?>
        </div>
        <!-- endof: Sidebar account -->

        <div class="col-xs-12 col-md-8-5 account-detail my-easyorders my-easyorders__step2">
            <header>
                <h2>CREATION</h2>
            </header>

            <?php include('components/my-easyordering/steps.php'); ?>

            <div class="my-easyordering-header clearfix">
                <h3 class="pull-left my-easyordering-header__title">Frequency</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php?create=1" title="Back" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Back</a>
                    <a href="easyorder.php?create=3" title="Validate" class="btn btn-icon-right btn-green"><i class="fa fa-angle-right"></i>Validate</a>
                </div>
            </div>

            <div class="row">
                <div class="easyordering-frequency-main col-xs-12 col-sm-7">
                    <p>Your order will be shipped at this date. The delivery date to your address depends on your delivery method, normally it should be made in one working day.</p>
                    <div class="input-group input-group-generic">
	                    <label class="desktop-label col-sm-5" for="title-freq" title="Order is sent">Order is sent</label>
	                    <div class="col-sm-7">
		                    <div class="dropdown dropdown--input dropdown--init">
			                    <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
				                    <span class="mobile-label">Text Label<span class="required">*</span></span>
				                    <span class="current">Please select</span>
			                    </button>
			                    <select tabindex="-1" name="title-freq" id="title-freq">
				                    <option value="0">Please select</option>
				                    <option value="1">Every month</option>
				                    <option value="2">Every week</option>
			                    </select>
		                    </div>
	                    </div>
                    </div>
                    <div class="input-group input-group-generic with-picto">
                        <label class="desktop-label col-sm-5" for="next-shipment" title="Next shipment on">Next shipment on</label>
                        <div class="col-sm-7">
                            <i class="fa fa-calendar"></i>
                            <input type="text" title="Next shipment on" id="next-shipment" name="next-shipment" placeholder="Next shipment on" class="form-control col-sm-12 col-md-9">
                            <span class="mobile-label">Next shipment on</span>
                        </div>
                    </div>
                    <div class="easyordering-info">
                        <i class="icon icon-Information"></i>
                        <p>
                            We indicate the date by when you order will be ready for shipment. To know the delivery date at your adress, please refer to the delay of your selected delivery method (standart, express 24h...)
                        </p>
                    </div>
                    <p class="text-center">Estimated shipping dates for your next easyorders :</p>
                    <ul class="easyordering-frequency--estimated__shipping">
                        <li>May 25Th 2015</li>
                        <li>June 25Th 2015</li>
                        <li>July 25Th 2015</li>
                        <li>...</li>
                    </ul>

                </div>
                <div class="easyordering-frequency-sidebar col-xs-12 col-sm-4 col-sm-offset-1">
                    <div class="easyordering-frequency-sidebar__title">Help me choose my shipping frequency</div>
                    <div class="input-group input-group-generic">
                        <label class="desktop-label" for="coffee-freq">On a daily base, how many coffees do you or your family drink at home?</label>
                        <div class="dropdown dropdown--input dropdown--init">
	                        <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
		                        <span class="mobile-label">Text Label<span class="required">*</span></span>
		                        <span class="current">Please select</span>
	                        </button>
                            <select tabindex="-1" name="coffee-freq" id="coffee-freq">
                                <option value="0">Please select</option>
                                <option value="1">Between 1 and 3</option>
                                <option value="2">Between 3 and 5</option>
                                <option value="3">Between 5 and 8</option>
                                <option value="4">More than 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="easyordering-info">
                        <p>
                            Based on your shopping bag and consumption, we recommand this frequency:
                        </p>
                        <span class="result-frequency">Every month</span>
                        <a href="#" title="Use this" class="btn btn-primary">Use this</a>
                    </div>
                </div>
            </div>

            <div class="row create-easyorder__header-footer create-easyorder__header-footer--footer">
                <div>
                    <a href="#" title="FAQ" class="link link--right">FAQ</a>
                </div>
                <div>
                    <a href="easyorder.php?create=1" title="Back" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Back</a>
                    <a href="easyorder.php?create=3" title="Validate" class="btn btn-icon-right btn-green pull-right"><i class="fa fa-angle-right"></i> Validate</a>
                </div>
            </div>


        </div>
    </div>
</div>