<div class="container">
    <div class="row">

        <!-- Sidebar account -->
        <div class="col-xs-12 col-md-3 account-sidebar">
            <?php include('components/my-account/menu.php'); ?>
        </div>
        <!-- endof: Sidebar account -->

        <div class="col-xs-12 col-md-8-5 account-detail my-easyorders">
            <header>
                <h2>MY EasyORDER</h2>
            </header>

            <div class="row">
                <div class="col-xs-12">
                    <h3>Discover the benefits of Easyorder</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 my-easyorders__visu">
                    <img src="images/myeasyordering.jpg" alt="Coffee" />
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="my-easyorders__benefits">
                        <ul>
                            <li><i class="fa fa-check-circle"></i> Free delivery</li>
                            <li><i class="fa fa-check-circle"></i> Recurring shipment</li>
                            <li><i class="fa fa-check-circle"></i> Modify or cancel it at any time</li>
                            <li><i class="fa fa-check-circle"></i> Easy to manage</li>
                        </ul>
                    </div>
                    <a href="contact-us.php" class="link--right link" title="Discover the service">Discover the service</a>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12">
                    <h3>How does Easy Ordering work?</h3>
                </div>
            </div>
            <div class="row my-easyordering-steps my-easyordering-steps--verticalm">
                <div class="col-xs-12 col-sm-4 my-easyordering-steps__item">
                    <div class="my-easyordering-steps__title">1 - SELECT</div>
                    <div class="my-easyordering-steps__description">your products from a post order or scratch</div>
                </div>
                <div class="col-xs-12 col-sm-4 my-easyordering-steps__item">
                    <div class="my-easyordering-steps__title">2 - DEFINE</div>
                    <div class="my-easyordering-steps__description">frequency, delivery address and payment method</div>
                </div>
                <div class="col-xs-12 col-sm-4 my-easyordering-steps__item">
                    <div class="my-easyordering-steps__title">3 - RECEIVE</div>
                    <div class="my-easyordering-steps__description">your order at the frequency selected. Enjoy!</div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12">
                    <h3>Let's get started:</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 my-easyordering-start">
                    <div class="row">
                        <div class="col-md-12 my-easyordering-start__container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-5 my-easyordering-start__block">
                                    <div class="my-easyordering-start__description">You can make<br />a past order recurrent:</div>
                                    <div class="my-easyordering-start__link"><a href="order-history.php?exist=1" class="btn btn-icon-right btn-green btn-block" title="Go to my orders history"><i class="fa fa-angle-right"></i>Go to my orders history</a></div>
                                </div>
                                <div class="col-xs-12 col-sm-2 my-easyordering-start__sep">
                                    <span>OR</span>
                                </div>
                                <div class="col-xs-12 col-sm-5 my-easyordering-start__block">
                                    <div class="my-easyordering-start__description">You can create<br />a new one from the catalog:</div>
                                    <div class="my-easyordering-start__link"><a href="easyorder.php?create=1" class="btn btn-icon-right btn-green btn-block" title="Create a new easyorder"><i class="fa fa-angle-right"></i>Create a new easyorder</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>