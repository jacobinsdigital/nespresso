<?php
$activeMenu = 1;
$activeStep = 5;
?>
<div class="container">
    <div class="row">

        <!-- Sidebar account -->
        <div class="col-xs-12 col-md-3 account-sidebar">
            <?php include('components/my-account/menu.php'); ?>
        </div>
        <!-- endof: Sidebar account -->

        <div class="col-xs-12 col-md-8-5 account-detail my-easyorders my-easyorders__step5">
            <header>
                <h2>CREATION</h2>
            </header>

            <?php include('components/my-easyordering/steps.php'); ?>
	        <div class="my-easyordering-header clearfix">
		        <h3 class="pull-left my-easyordering-header__title">Validation</h3>
		        <p>Please note that the actual delivery date is depending on your delivery method.</p>
	        </div>

            <div class="my-easyordering-header my-easyordering-header--grey clearfix">
                <h3 class="pull-left my-easyordering-header__title">Products</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php?create=1" class="btn btn-primary btn-noWidth" title="Edit">Edit</a>
                </div>
            </div>

            <div class="my-easyordering-content my-easyordering-content__products clearfix">
                <div class="table-row">
                    <div class="easyordering-resume-main col-xs-12 col-sm-8">
                        <div class="sbag-list">
	                        <div class="sbag-list-head">
		                        <span class="sbag-head-article">Coffee capsules (3)</span>
		                        <span class="sbag-head-qty">Quantity</span>
	                        </div>
	                        <div class="sbag-list-content sbag-list-content--transparent">

		                        <div class="sbag-list-inner col-xs-12">
			                        <div class="sbag-item">
				                        <div class="sbag-name">
					                        <img src="images/product-09.png" alt="Arpeggio">
					                        Arpeggio
				                        </div>
				                        <div class="sbag-qty">20</div>
			                        </div>

			                        <div class="sbag-item">
				                        <div class="sbag-name">
					                        <img src="images/kazaar2.png" alt="Kazaar">
					                        Kazaar
					                        <span class="hidden-sm hidden-md show-xs">$0.41 x 10</span>
				                        </div>
				                        <div class="sbag-qty">10</div>
			                        </div>
		                        </div>
	                        </div>
                        </div>
                        <div class="sbag-list">
	                        <div class="sbag-list-head">
		                        <span class="sbag-head-article">Sweets (4)</span>
		                        <span class="sbag-head-qty">Quantity</span>
	                        </div>
	                        <div class="sbag-list-content sbag-list-content--transparent">

		                        <div class="sbag-list-inner col-xs-12">
			                        <div class="sbag-item">
				                        <div class="sbag-name">
					                        <img src="images/product-09.png" alt="Arpeggio">
					                        Arpeggio
				                        </div>
				                        <div class="sbag-qty">20</div>
			                        </div>

			                        <div class="sbag-item">
				                        <div class="sbag-name">
					                        <img src="images/kazaar2.png" alt="Kazaar">
					                        Kazaar
				                        </div>
				                        <div class="sbag-qty">10</div>
			                        </div>
			                        <div class="sbag-item">
				                        <div class="sbag-name">
					                        <img src="images/darkhan.png" alt="Arpeggio">
					                        Darkhan
				                        </div>
				                        <div class="sbag-qty">20</div>
			                        </div>

			                        <div class="sbag-item">
				                        <div class="sbag-name">
					                        <img src="images/ristretto.png" alt="Kazaar">
					                        Ristretto
				                        </div>
				                        <div class="sbag-qty">10</div>
			                        </div>
		                        </div>
	                        </div>
                        </div>
                    </div>
                    <div class="easyordering-resume-sidebar col-xs-12 col-sm-4">
                        <div class="clearfix">
                            <span class="uppercase pull-left">Coffee capsules (40)</span>
                            <span class="price pull-right">$40.00</span>
                        </div>
                        <div class="clearfix">
                            <span class="uppercase pull-left">Sweet (9)</span>
                            <span class="price pull-right">19.55</span>
                        </div>
                        <div class="clearfix">
                            <span class="pull-left">Shipping cost</span>
                            <span class="price pull-right">$0.00</span>
                        </div>
                        <div class="clearfix">
                            <span class="pull-left">VAT</span>
                            <span class="price pull-right">$6.50</span>
                        </div>
                        <div class="total-products clearfix">
                            <span class="pull-left"><strong>TOTAL</strong><small>(incl. VTA)</small></span>
                            <span class="price pull-right">$66.05</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-easyordering-header my-easyordering-header--grey clearfix">
                <h3 class="pull-left my-easyordering-header__title">Frequency</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php?create=2" class="btn btn-primary btn-noWidth" title="Edit">Edit</a>
                </div>
            </div>
            <div class="my-easyordering-content my-easyordering-content__frequency clearfix">
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><span class="lh-btn">Order is sent:</span></div>
                    <div class="col-xs-12 col-sm-4"><span class="lh-btn uppercase"><strong>Every month</strong></span></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><span class="lh-btn">Next shipment on:</span></div>
                    <div class="col-xs-12 col-sm-4"><span class="lh-btn shipment-text">May 25th 2016 <i class="icon icon-Information"></i></span></div>
                    <div class="col-xs-12 col-sm-4">
                        <a href="easyorder.php?create=2" class="btn btn-primary pull-right" title="Change date">Change date</a>
                    </div>
                </div>
            </div>

            <div class="my-easyordering-header my-easyordering-header--grey clearfix">
                <h3 class="pull-left my-easyordering-header__title">Delivery</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php?create=3" class="btn btn-primary btn-noWidth" title="Edit">Edit</a>
                </div>
            </div>

            <div class="my-easyordering-content my-easyordering-content__delivery clearfix">
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><strong>Delivery address:</strong></div>
                    <div class="col-xs-12 col-sm-8">
                        <p>
                            Mr Name Second Name<br>
                            Route des Jeunes<br>
                            89654 Genève<br>
                            Suisse
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><strong>Delivery method:</strong></div>
                    <div class="col-xs-12 col-sm-8">
                        <i class="icon-Delivery_off pull-left"></i>
                        <p class="pull-left">
                            <strong>Standard delivery</strong>
                            (2 working days)
                        </p>
                    </div>
                </div>
            </div>

            <div class="my-easyordering-header my-easyordering-header--grey clearfix">
                <h3 class="pull-left my-easyordering-header__title">Payment</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php?create=4" class="btn btn-primary btn-noWidth" title="Edit">Edit</a>
                </div>
            </div>
            <div class="my-easyordering-content my-easyordering-content__payment clearfix">
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><strong>Billing address:</strong></div>
                    <div class="col-xs-12 col-sm-8">
                        <p>
                            Mr Name Second Name<br>
                            Route des Jeunes<br>
                            89654 Genève<br>
                            Suisse
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><strong>Payment method:</strong></div>
                    <div class="col-xs-12 col-sm-8">
                        <img class="pull-left" src="images/icon-mastercard.png" alt="Mastercard">
                        <p class="pull-left">
                            <strong>CREDIT CARD</strong>
                            Willy Wonka<br>
                            ********3872<br>
                            Expiration date 31/12/16
                        </p>
                    </div>
                </div>
            </div>

            <div class="bloc-terms clearfix">
                <span class="title">Terms of Sale confirmation</span>
                <div class="checkbox pull-left">
                    <input type="checkbox" name="form-accept-terms" id="form-accept-terms">
                    <label for="form-accept-terms">I confirm that I have read and accepted the <i>Nespresso</i> <a href="#" target="_blank" title="Terms of sale">Terms of Sale</a> and the <i>Nespresso</i> <a href="">Privacy Policy</a></label>
                </div>
            </div>

            <div class="row create-easyorder__header-footer create-easyorder__header-footer--footer">
                <div>
                    <a href="easyorder.php" title="Cancel" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Cancel</a>
                    <a href="easyorder.php?exist=1" title="Place order" class="btn btn-icon-right btn-green pull-right"><i class="fa fa-angle-right"></i>Place order</a>
                </div>
            </div>


        </div>
    </div>
</div>