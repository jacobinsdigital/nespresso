<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$user = nespresso_get_user_data();

$current_url = home_url(add_query_arg(array(),$wp->request));


$states = nespresso_get_states();

$cities = nespresso_get_cities();
$country_codes = get_country_codes();
/**
 * some message from backend processing
 */
if (isset($_SESSION['nespresso_my_addresses_hook_script'])) :

    function nespresso_my_info_hook() {
        global $nespresso_my_info_hook_script;
        $hook_script = $_SESSION['nespresso_my_addresses_hook_script'];
        unset($_SESSION['nespresso_my_addresses_hook_script']);
    ?>
        <script type="text/javascript">
            var validation = new Validation();
            <?php echo $hook_script ?>
        </script>
    <?php
    } // nespresso_my_info_hook()
    add_action('wp_footer', 'nespresso_my_info_hook');

endif;

?>

<section class="form hide" id="form-addresses">

    <div class="col-xs-12 account-detail  my-address gray-bgcolor" >

        <div class="row">

            <div class="col-xs-12 my-infos__form">

                <form method="POST" action="<?= $current_url ?>" id="form-edit-addresses-billing" class="hide">

                    <header>
                        <h2>Edit Billing Information</h2>
                    </header>

                    <p>Please fill the form below, Fields marked with<span class="required">*</span> are required.</p>

                    <?php wp_nonce_field( 'nespresso_post_nonce', 'nespresso_post_nonce_field' ); ?>

                    <input type="hidden" name="form-for" value="my-addresses-billing">

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_first_name">Billing First Name<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"
                                required
                                title="Billing First Name"
                                id="billing_first_name"
                                name="billing_first_name"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->billing_first_name) && $user->billing_first_name ? $user->billing_first_name : '' ?>"
                            >
                            <span class="mobile-label">Billing First Name<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_last_name">Billing Last Name<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"
                                required
                                title="Billing Last Name"
                                id="billing_last_name"
                                name="billing_last_name"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->billing_last_name) && $user->billing_last_name ? $user->billing_last_name : '' ?>"
                            >
                            <span class="mobile-label">Billing Last Name<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_company">Billing Company</label>
                        <div class="col-sm-6">
                            <input type="text"
                                title="Billing Company"
                                id="billing_company"
                                name="billing_company"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->billing_company) && $user->billing_company ? $user->billing_company : '' ?>"
                            >
                            <span class="mobile-label">Billing Company<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="mobile">Billing Mobile<span class="required">*</span></label>
                        <div class="col-sm-2">
                            <select name="billing_country_code" id="billing_country_code" class="form-control" required>
                                <?php foreach ($country_codes as $c_code): ?>
                                   <option value="<?=$c_code?>" <?= $user->billing_country_code == $c_code ? 'selected="selected"' : '' ?> ><?=$c_code?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input type="text"
                                required
                                title="Billing Mobile"
                                id="billing_mobile"
                                name="billing_mobile"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= $user->billing_mobile; ?>"
                            >
                            <span class="mobile-label">Billing Mobile<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_address_1">Billing Address line 1<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"
                                required
                                title="Billing Address line 1"
                                id="billing_address_1"
                                name="billing_address_1"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->billing_address_1) && $user->billing_address_1 ? $user->billing_address_1 : '' ?>"
                            >
                            <span class="mobile-label">Billing Address line 1<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_address_2">Billing Address line 2</label>
                        <div class="col-sm-6">
                            <input type="text"
                                title="Billing Address line 2"
                                id="billing_address_2"
                                name="billing_address_2"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->billing_address_2) && $user->billing_address_2 ? $user->billing_address_2 : '' ?>"
                            >
                            <span class="mobile-label">Billing Address line 2</span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_state">Billing Province<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="mobile-label">Billing Province</span>
                                    <span class="current">Billing Province</span>
                                </button>
                                <select tabindex="-1" name="billing_state" class="billing_state custom-mobile-select" id="billing_state" required>
                                    <option value="">Billing Province</option>
                                    <?php foreach ( $states as $code => $name ) : ?>
                                    <option value="<?= $code ?>" <?= $user->billing_state == $code ? 'selected="selected"': '' ?>><?= $name ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_city">Billing District<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <!-- <input type="text"
                                title="Billing City"
                                id="billing_city"
                                name="billing_city"
                                placeholder="Billing District"
                                class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                value="<?= isset($user->billing_city) && $user->billing_city ? $user->billing_city : '' ?>"
                            > -->
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="mobile-label">Billing District</span>
                                    <span class="current current_billing_district">Shipping District</span>
                                </button>

                                <select tabindex="-1" id="billing_city" class="billing_state custom-mobile-select" name="billing_city" required>
                                    <option value="">Billing District</option>
                                    <?php foreach ( $cities as  $city ) : ?>
                                        <option value="<?= $city ?>" <?= $user->billing_city == $city ? 'selected="selected"': '' ?>><?= $city ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_postcode">Billing Post code<span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text"
                                title="Billing Post code"
                                id="billing_postcode"
                                name="billing_postcode"
                                placeholder="Ex: 1221"
                                class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                value="<?= isset($user->billing_postcode) && $user->billing_postcode ? $user->billing_postcode : '' ?>"
                            >
                            <span class="mobile-label">Billing Post code<span class="required">*</span></span>
                        </div>
                    </div> -->

                    <!-- country start -->
                    <?php $country = nespresso_get_country();?>
                    <input type="hidden" name="billing_country" value="<?= $country ?>">
                    <!-- <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_country">Billing Country<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="mobile-label">Billing Country</span>
                                    <span class="current">Billing Country</span>
                                </button>

                                <select tabindex="-1" name="billing_country" id="billing_country">
                                    <option value="">Billing Country</option>
                                    <?php foreach ( nespresso_get_countries() as $ccode => $cname ) : ?>
                                        <option value="<?= $ccode ?>" <?= $user->billing_country == $ccode ? 'selected="selected"': '' ?>><?= $cname ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    <!-- country end -->

                </form>

                <form method="POST" action="<?= $current_url ?>" id="form-edit-addresses-shipping" class="hide">

                    <?php wp_nonce_field( 'nespresso_post_nonce', 'nespresso_post_nonce_field' ); ?>

                    <header>
                        <h2>Edit Shipping Information</h2>
                    </header>

                    <p>Please fill the form below, Fields marked with<span class="required">*</span> are required.</p>

                    <input type="hidden" name="form-for" value="my-addresses-shipping">

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_first_name">Shipping First Name<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"
                                required
                                title="Shipping First Name"
                                id="shipping_first_name"
                                name="shipping_first_name"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->shipping_first_name) && $user->shipping_first_name ? $user->shipping_first_name : '' ?>"
                            >
                            <span class="mobile-label">Shipping First Name<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_last_name">Shipping Last Name<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"
                                required
                                title="Shipping Last Name"
                                id="shipping_last_name"
                                name="shipping_last_name"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->shipping_last_name) && $user->shipping_last_name ? $user->shipping_last_name : '' ?>"
                            >
                            <span class="mobile-label">Shipping Last Name<span class="required">*</span><span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_company">Shipping Company</label>
                        <div class="col-sm-6">
                            <input type="text"
                                title="Shipping Company"
                                id="shipping_company"
                                name="shipping_company"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->shipping_company) && $user->shipping_company ? $user->shipping_company : '' ?>"
                            >
                            <span class="mobile-label">Shipping Company</span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="mobile">Shipping Mobile<span class="required">*</span></label>
                        <div class="col-sm-2">
                            <select name="shipping_country_code" id="shipping_country_code" class="form-control" required>
                                <?php foreach ($country_codes as $c_code): ?>
                                   <option value="<?=$c_code?>" <?= $user->shipping_country_code == $c_code ? 'selected="selected"' : '' ?> ><?=$c_code?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input type="text"
                                title="Shipping Mobile"
                                id="shipping_mobile"
                                name="shipping_mobile"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= $user->shipping_mobile; ?>"
                            >
                            <span class="mobile-label">Shipping Mobile<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_address_1">Shipping Address line 1<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"
                                required
                                title="Shipping Address line 1"
                                id="shipping_address_1"
                                name="shipping_address_1"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->shipping_address_1) && $user->shipping_address_1 ? $user->shipping_address_1 : '' ?>"
                            >
                            <span class="mobile-label">Shipping Address line 1<span class="required">*</span></span>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_address_2">Shipping Address line 2</label>
                        <div class="col-sm-6">
                            <input type="text"
                                title="Shipping Address line 2"
                                id="shipping_address_2"
                                name="shipping_address_2"
                                class="form-control col-sm-12 col-md-9"
                                value="<?= isset($user->shipping_address_2) && $user->shipping_address_2 ? $user->shipping_address_2 : '' ?>"
                            >
                            <span class="mobile-label">Shipping Address line 2</span>
                        </div>
                    </div>

                     <!-- <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_city">Shipping District<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"
                                title="Shipping City"
                                id="shipping_city"
                                name="shipping_city"
                                placeholder="Shipping District"
                                class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                value="<?= isset($user->shipping_city) && $user->shipping_city ? $user->shipping_city : '' ?>"
                            >
                            <span class="mobile-label">Shipping City<span class="required">*</span></span>
                        </div>
                    </div> -->


                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="billing_state">Shipping Province<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="mobile-label">Shipping Province</span>
                                    <span class="current">Shipping Province</span>
                                </button>

                                <select tabindex="-1" name="shipping_state" class="billing_state custom-mobile-select"  id="shipping_state" required>
                                    <option value="">Shipping Province</option>
                                    <?php foreach ( $states as $code => $name ) : ?>
                                        <option value="<?= $code ?>" <?= $user->shipping_state == $code ? 'selected="selected"': '' ?>><?= $name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_city">Shipping District<span class="required">*</span></label>
                        <div class="col-sm-6">
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="mobile-label">Shipping District</span>
                                    <span class="current current_shipping_district">Shipping District</span>
                                </button>

                                <select tabindex="-1" class="billing_city custom-mobile-select" id="shipping_city" name="shipping_city" required>
                                    <option value="">Shipping District</option>
                                     <?php foreach ( $cities as $ship_city ) : ?>
                                        <option value="<?= $city ?>" <?= $user->shipping_city == $ship_city ? 'selected="selected"': '' ?>><?= $ship_city ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_postcode">Shipping Post code<span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text"
                                title="Shipping Post code"
                                id="shipping_postcode"
                                name="shipping_postcode"
                                placeholder="Ex: 1221"
                                class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                value="<?= isset($user->shipping_postcode) && $user->shipping_postcode ? $user->shipping_postcode : '' ?>"
                            >
                            <span class="mobile-label">Shipping Post code<span class="required">*</span></span>
                        </div>
                    </div> -->

                    <!-- <div class="input-group input-group-generic">
                        <label class="desktop-label col-sm-3 col-md-4" for="shipping_postcode">Shipping Post code<span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text"
                                title="Shipping Post code"
                                id="shipping_postcode"
                                name="shipping_postcode"
                                placeholder="Ex: 1221"
                                class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                value="<?= isset($user->shipping_postcode) && $user->shipping_postcode ? $user->shipping_postcode : '' ?>"
                            >
                            <span class="mobile-label">Shipping Post code<span class="required">*</span></span>
                        </div>
                    </div> -->

                    <input type="hidden" name="shipping_country" value="<?php echo key($country) ?>">

                <form>

            </div><!-- my-infos__form -->
        </div>
        <div class="account-detail__footer clearfix">
            <button type="button" class="btn btn-primary btn-icon" id="btn-cancel-edit-my-address" title="Cancel"><i class="icon-arrow_right"></i> Cancel</button>
            <button class="btn btn-primary pull-right btn-save-info btn-submit" data-address-type="">Save my information</button>
        </div>
    </div>

</section>

<?php
global $scripts;
$scripts[] = 'js/components/district.js';
?>
