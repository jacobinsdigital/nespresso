<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<main id="content">

	<div class="container">
	    <div class="row">

	        <!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <div class="pd-right-0 col-md-9 col-xs-12">

	        	<?php get_template_part('components/my-adresses/my-adresses'); ?>

				<?php get_template_part('components/my-adresses/my-adresses--edit'); ?>

			</div><!-- .col-md-9 -->

		</div><!-- .row -->
	</div><!-- .container -->

</main>

<?php
global $scripts;
$scripts[]='js/components/validation.js';
$scripts[]='js/components/my-addresses.js';
?>
