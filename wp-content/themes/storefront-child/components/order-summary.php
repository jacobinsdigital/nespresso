<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
?>

<!-- content -->
<main id="content" class="shopping-bag">
	<div class="container">
	    <header>

	        <?php $stepActive = 4; ?>

	        <?php get_template_part('components/wizard'); ?>

	    </header>
	    <section class="checkister step4 sbag-container" id="JumpContent" tabindex="-1">
	        <header>
	            <h2>Order summary</h2>
	        </header>
	        <section class="row">
	            <div class="col-md-4">
	                <aside>
	                    <div class="bloc-delivery">
	                        <h3 class="main-content__title">Delivery method</h3>
	                        <a class="btn btn-primary btn-noWidth" href="<?php bloginfo('url'); ?>/delivery-mode" title="Edit">Edit</a>
	                        <div class="content">
	                            <p class="address__preview"> Mr First Name Last Name <br>Address line <br>1234 City <br>Switzerland</p>
	                            <span class="bloc__summary"><i class="icon-Delivery_off"></i> Delivery mode: <strong>Standard</strong></span>
	                        </div>
	                    </div>
	                    <div class="bloc-delivery">
	                        <h3 class="main-content__title">Billing information</h3>
	                        <a class="btn btn-primary btn-noWidth" href="<?php bloginfo('url'); ?>/payment" title="Edit">Edit</a>
	                        <div class="content">
	                            <p class="address__preview"> Mr First Name Last Name <br>Address line <br>1234 City <br>Switzerland</p>
	                            <span class="bloc__summary"><i class="icon-invoice-off"></i> <strong>Invoice</strong></span>
	                        </div>
	                    </div>
	                    <section class="bloc-promotion">
	                        <h3 class="main-content__title main-content__title--promo">Promotion code</h3>
	                        <div class="content">
	                            <p>If you have a promotion code, please enter it on the <a href="<?php bloginfo('url'); ?>/shopping-bag">shopping bag</a> page</p>
	                        </div>
	                    </section>
	                </aside>
	            </div>
	            <div class="col-xs-12 col-md-8">
	                <div class="main-content clearfix">
	                    <section class="delivery-method">
	                        <h3 class="main-content__title">Shopping bag</h3>
	                        <a class="btn btn-primary btn-noWidth" href="<?php bloginfo('url'); ?>/shopping-bag" title="Edit">Edit</a>
	                        <h3 class="main-content__title main-content__title--save">Save time on your next orders</h3>
	                            <div class="bloc-delivery clearfix">
	                                <div class="input-group input-group-generic">
	                                    <div class="checkbox">
	                                        <label for="keep-me-informated">
	                                            <input value="none" id="keep-me-informated" name="keep-me-informated" type="checkbox">
	                                            Save your shipping and payment preferences (including your credit card details) and activate the <strong>Express Checkout option</strong>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
	                    </section>
	                    <section  class="delivery-method" style="background: #f9f9f9">
	                        <div class="sbag-list">
	                            <div class="sbag-list-content sbag-loading"></div>
	                            <div class="sbag-resume-content hidden">
	                                <div class="sbag-resume-head">
	                                    <span class="sbag-resume-head-top">Total: </span>
	                                    <ul>
	                                        <li>Capsules (<span>200</span>)</li>
	                                        <li>Accessories (<span>1</span>)</li>
	                                    </ul>
	                                </div>
	                                <div class="sbag-resume-item sbag-total">
	                                    <div class="sbag-total-key">Subtotal</div>
	                                    <div class="sbag-total-value sbag-padding-cross">$<span class="sbag-subtotal-price-int">0</span></div>
	                                </div>
	                                <div class="sbag-resume-item">
	                                    <div class="sbag-total-key">Shipping cost</div>
	                                    <div class="sbag-total-value sbag-padding-cross">FREE</div>
	                                </div>
	                                <div class="sbag-resume-item">
	                                    <div class="sbag-total-key">Vat (incl.)</div>
	                                    <div class="sbag-total-value sbag-padding-cross">$<span class="sbag-val-price-int">0</span></div>
	                                </div>
	                            </div>

	                            <div class="sbag-footer hidden">
	                                <div class="sbag-total-item">
	                                    <div class="sbag-total-title">Total</div>
	                                    <div class="sbag-total-price sbag-padding-cross">$<span class="sbag-total-price-int">0</span></div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="bloc-terms clearfix">
	                            <span class="title">Terms of Sale confirmation</span>
	                            <div class="checkbox pull-left">
	                                <input type="checkbox" name="form-accept-terms" id="form-accept-terms">
	                                <label for="form-accept-terms">I confirm that I have read and accepted the <i>Nespresso</i> <a href="<?php bloginfo('url'); ?>/#" target="_blank" class="link" title="Terms of sale">Terms of Sale</a> and the <i>Nespresso</i> <a href="<?php bloginfo('url'); ?>/#" class="link" title="Privacy Policy">Privacy Policy</a></label>
	                            </div>
	                        </div>
	                    </section>
		                <div class="link-wrapper" style="background: white">
			                <a class="btn btn-green btn-icon-right pull-right" href="<?php bloginfo('url'); ?>/order-confirmation" title="Confirm the order"><span>Confirm the order</span> <i class="icon icon-arrow_left"></i></a>
		                </div>
	                </div>
	            </div>

	        </section>
	    </section>
	</div>
</main>
<?php
global $scripts;
$scripts[] = 'js/components/shopping-bag.js';
?>

<!-- /content -->
