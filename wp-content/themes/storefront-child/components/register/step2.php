<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$states = nespresso_get_states();
asort($states);
// $cities = nespresso_get_cities();

?>
<div data-step="2" class="checkister hide">
    <header>
        <h2>Your address</h2>
    </header>
    <section>
        <div class="main-content clearfix">
            <h3 class="main-content__title">BILLING ADDRESS</h3>
            <!-- <form method="POST" action="#"> -->
                <section class="form private">
                    <div class="checkister-wrapper clearfix gray-bgcolor" >
                        <p>Please fill the form below, Fields marked with * are required.</p>

                        <div class="row">
                            <div class="col-md-8">
                                <!-- <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_address_type">Your delivery address is<span class="required">*</span></label>
                                    <div class="col-sm-6 radio-group">
                                        <div class="radio" role="group" aria-labelledby="delivery-type-private">
                                            <label for="billing_address_type">
                                                <input value="private"
                                                    id=""
                                                    name="billing_address_type"
                                                    type="radio"
                                                     <?php echo !isset($_SESSION['registration_data']) || !isset($_SESSION['registration_data']['billing_address_type']) || $_SESSION['registration_data']['billing_address_type'] == 'private' ? 'checked="checked"' : '' ?>
                                                >
                                                Private
                                            </label>
                                        </div>
                                        <div class="radio" role="group" aria-labelledby="delivery-type-company">
                                            <label for="billing_address_type">
                                                <input value="company"
                                                    id=""
                                                    name="billing_address_type"
                                                    type="radio"
                                                    <?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_address_type']) && $_SESSION['registration_data']['billing_address_type'] == 'company' ? 'checked="checked"' : '' ?>
                                                >
                                                Company
                                            </label>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_first_name">First Name</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="First name"
                                            id="billing_first_name"
                                            name="billing_first_name"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_first_name']) ? $_SESSION['registration_data']['billing_first_name'] : '' ?>"
                                        >
                                        <span class="mobile-label">First Name</span>
                                        <input type="hidden"
                                            name="shipping_first_name"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_first_name']) ? $_SESSION['registration_data']['billing_first_name'] : '' ?>"
                                        >
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_last_name">Last Name</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Last name"
                                            id="billing_last_name"
                                            name="billing_last_name"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_last_name']) ? $_SESSION['registration_data']['billing_last_name'] : '' ?>"
                                        >										
                                        <span class="mobile-label">Last Name</span>
                                        <input type="hidden"
                                            name="shipping_last_name"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_last_name']) ? $_SESSION['registration_data']['billing_last_name'] : '' ?>"
                                        >
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_company">Company</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Company"
                                            id="billing_company"
                                            name="billing_company"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_company']) ? $_SESSION['registration_data']['billing_company'] : '' ?>"
                                        >
                                        <input type="hidden"
                                            name="shipping_company"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_company']) ? $_SESSION['registration_data']['billing_company'] : '' ?>"
                                        >
                                        <span class="mobile-label">Company Name</span>
                                    </div>
                                </div>
                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_country_code">Mobile<span class="required">*</span></label>
                                    <div class="col-sm-2">
                                        <select name="billing_country_code" id="billing_country_code" class="form-control">
                                            <?php foreach ($country_codes as $c_code): ?>
                                               <option value="<?=$c_code?>"><?=$c_code?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text"
                                            title="Mobile"
                                            id="billing_mobile"
											data-regix="^(086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$"
											required
                                            name="billing_mobile"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_mobile']) ? $_SESSION['registration_data']['billing_mobile'] : '' ?>"
                                        >
										<small id="helpbilling_mobile" data-msgvi="Vui lòng nhập đúng số điện thoại của bạn (e.g. 0987654321)" data-msg="Please enter a valid phone number (e.g. 0987654321)" class="text-danger"></small>
                                        <span class="mobile-label">Mobile<span class="required">*</span></span>
                                        <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both; margin-left: -105px;"><small>Example: 0912345678</small></p>
                                    </div>
                                    <input type="hidden"
                                        name="shipping_country_code"
                                        value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_country_code']) ? $_SESSION['registration_data']['billing_country_code'] : '' ?>"
                                    >
                                    <input type="hidden"
                                        name="shipping_mobile"
                                        value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_mobile']) ? $_SESSION['registration_data']['billing_mobile'] : '' ?>"
                                    >
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_address_1">Address line 1<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Address line 1"
                                            id="billing_address_1"
                                            name="billing_address_1"
											required
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_address_1']) ? $_SESSION['registration_data']['billing_address_1'] : '' ?>"
                                        >
										<small id="helpbilling_address_1" data-msgvi="Vui lòng nhập địa chỉ của bạn" data-msg="Please provide a address" class="text-danger"></small>
                                        <input type="hidden"
                                            name="shipping_address_1"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_address_1']) ? $_SESSION['registration_data']['billing_address_1'] : '' ?>"
                                        >
                                        <span class="mobile-label">Address line 1<span class="required">*</span></span>
                                    </div>
                                </div>
                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_address_2">Address line 2</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Address line 2"
                                            id="billing_address_2"
                                            name="billing_address_2"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_address_2']) ? $_SESSION['registration_data']['billing_address_2'] : '' ?>"
                                        >
                                        <input type="hidden"
                                            name="shipping_address_2"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_address_2']) ? $_SESSION['registration_data']['billing_address_2'] : '' ?>"
                                        >
                                        <span class="mobile-label">Address line 2</span>
                                    </div>
                                </div>

                                <!-- <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_ward">Ward</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Ward"
                                            id="billing_ward"
                                            name="billing_ward"
                                            class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_ward']) ? $_SESSION['registration_data']['billing_ward'] : '' ?>"
                                        >
                                        <input type="hidden"
                                            name="shipping_ward"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_ward']) ? $_SESSION['registration_data']['billing_ward'] : '' ?>"
                                        >
                                        <span class="mobile-label">Ward</span>
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_ward">District</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="District"
                                            id="billing_district"
                                            name="billing_district"
                                            class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_district']) ? $_SESSION['registration_data']['billing_district'] : '' ?>"
                                        >
                                        <input type="hidden"
                                            name="shipping_district"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_district']) ? $_SESSION['registration_data']['billing_district'] : '' ?>"
                                        >
                                        <span class="mobile-label">District</span>
                                    </div>
                                </div> -->

                                <!-- SELECT STATE -->
                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_state">Province<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <div class="dropdown dropdown--input dropdown--init">
                                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                                <span class="mobile-label">Province<span class="required">*</span></span>
                                                <span class="current">Please select</span>
                                            </button>
                                             <?php
                                                $billing_state = isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_state']) ? $_SESSION['registration_data']['billing_state'] : ''
                                            ?>
                                            <select tabindex="-1" name="billing_state" id="billing_state" required>
                                                <option value="">Please select province</option>
                                                <?php foreach ( $states as $code => $name ) : ?>
                                                    <option value="<?= $code ?>" <?= $billing_state == $code ? 'selected="selected"': '' ?>><?= $name ?></option>
                                                <?php endforeach; ?>
                                            </select>
											<small id="helpbilling_state" data-msgvi="Vui lòng chọn tỉnh" data-msg="Please select province" class="text-danger"></small>
                                            <input type="hidden"
                                                name="shipping_state"
                                                value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_state']) ? $_SESSION['registration_data']['billing_state'] : '' ?>"
                                            >
                                        </div>
                                        <!-- <span class="mobile-label">Province<span class="required">*</span></span> -->
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4"></label>
                                    <div class="col-sm-6">
                                        <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both;"><small> Select Province to show districts</small></p>
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_city">District<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <div class="dropdown dropdown--input dropdown--init">
                                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                                <span class="mobile-label">District<span class="required">*</span></span>
                                                <span class="current">Please select</span>
                                            </button>
                                            <?php
                                                $billing_city = isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_city']) ? $_SESSION['registration_data']['billing_city'] : ''
                                            ?>
                                            <select tabindex="-1" name="billing_city" id="billing_city" required>
                                                <option value="">Please select district</option>
                                            </select>
											<small id="helpbilling_city" data-msgvi="Vui lòng chọn huyện" data-msg="Please select district" class="text-danger"></small>
                                            <input type="hidden"
                                                name="shipping_city"
                                                value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_city']) ? $_SESSION['registration_data']['billing_city'] : '' ?>"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <?php $country = nespresso_get_country();?>
                                <input type="hidden" name="billing_country" value="VN">
                                <input type="hidden" name="shipping_country" value="VN">
                                <!-- <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_country">Country<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <div class="dropdown dropdown--input dropdown--init">
                                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                                <span class="mobile-label">Country</span>
                                                <span class="current current_country">Please select</span>
                                            </button>
                                            <select tabindex="-1" name="billing_country"  name="shipping_country" id="billing_country">
                                                <?php foreach ( nespresso_get_countries() as $cc => $cname ) : ?>
                                                    <option value="<?= $cc ?>" <?= key($country) == $cc ? 'selected="selected"': '' ?>><?= $cname ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <span class="mobile-label">Country<span class="required">*</span></span>
                                    </div>
                                </div> -->

                                <!-- <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="billing_postcode">Post code<span class="required">*</span></label>
                                    <div class="col-sm-3">
                                        <input type="text"
                                            title="Postal code"
                                            id="billing_postcode"
                                            name="billing_postcode"
                                            class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_postcode']) ? $_SESSION['registration_data']['billing_postcode'] : '' ?>"
                                        >
                                        <input type="hidden"
                                            name="shipping_postcode"
                                            value="<?php echo  isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['billing_postcode']) ? $_SESSION['registration_data']['billing_postcode'] : '' ?>"
                                        >
                                        <span class="mobile-label">Postal code<span class="required">*</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <hr>
                        <span class="title">Newsletter</span>
                        <div class="input-group input-group-generic">
                            <div class="checkbox">
                                <label for="keep-me-informated">
                                    <input value="1"
                                        id="newsletter_subscription"
                                        name="newsletter_subscription"
                                        type="checkbox"
                                        checked="checked">
                                        <?php //echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['newsletter_subscription']) && $_SESSION['registration_data']['newsletter_subscription'] == '1' ? 'checked="checked"' : '' ?>
                                     Yes, I would like to receive special offers and information from <i>Nespresso</i> by email.
                                </label>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary btn-icon-right pull-left btn-previous-step"
                            href="javascript:void(0)"
                            title="Previous"
                            data-step="2"
                        ><i class="icon icon-arrow_right"></i>Previous</button>
                        <button type="button" class="btn btn-primary btn-icon-right pull-right btn-next-step"
                            href="javascript:void(0)"
                            title="Continue"
                            data-step="2"
                        ><i class="icon icon-arrow_left"></i>Continue</button>
                    </div>
                </section>
            <!-- </form> -->
        </div>
    </section>
</div>
