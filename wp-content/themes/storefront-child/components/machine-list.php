<?php

/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<!-- content -->

<?php

// Category Banner
$category_banner = get_nespresso_category_banner();

$machine_desktop_img = isset($category_banner['machine_category_banner_image_url']) && $category_banner['machine_category_banner_image_url'] ? $category_banner['machine_category_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 25/MachineList_Desktop.png";
$machine_mobile_img = isset($category_banner['machine_category_banner_image_url_mobile']) && $category_banner['machine_category_banner_image_url_mobile'] ? $category_banner['machine_category_banner_image_url_mobile'] : get_stylesheet_directory_uri() . "/images/banner/September 25/MachineList_Mobile.png";
$machine_sidebar_img = isset($category_banner['machine_sidebar_image_url']) && $category_banner['machine_sidebar_image_url'] ? $category_banner['machine_sidebar_image_url'] : '';
$machine_link = isset($category_banner['machine_category_banner_link']) && $category_banner['machine_category_banner_link'] ? $category_banner['machine_category_banner_link'] : "/machine-list/";
$machine_promo = isset($category_banner['machine_category_banner_promo_name']) && $category_banner['machine_category_banner_promo_name'] ? $category_banner['machine_category_banner_promo_name'] : "Machine Banner Promo";

/**
 * get the machine products
 */
$products = get_machine_products();

$product_count = count($products);

global $scripts;
$scripts[] = '/js/components/wNumb.js';
$scripts[] = 'js/components/productlist.js';
$scripts[] = 'js/libs/nouislider.min.js';
$arrays = get_nespresso_product_list();
if (!$arrays) {
    $machines = [];
    foreach ($products as $value) {
        $machines[] = $value->name;
    }
} else {
    $machines = $arrays['machine'];
}
// echo '<pre>',print_r($products),'</pre>'  ;
?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/nouislider.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/machine-list.css">

<!-- quantity selector for add to basket when clicked -->
<?php get_template_part('components/generics/qty-selector');?>

    <main id="content" class="fullPage fullPage__plp">


		<ul class="productlist-menu-shortcut">
			<li>
			    <a href="/coffee-list" title="Coffee Page"><i class="icon icon-capsule"></i>Coffee</a>
			</li>
			<li class="selected">
			    <a href="/machine-list" title="Machines Page"><i class="icon icon-machine"></i>Machines</a>
			</li>
			<li>
			    <a href="/accessory-list" title="Accessories Page"><i class="icon icon-accessories"></i>Accessories</a>
			</li>
		</ul>
		<!-- banner -->
        <div class="new-banner">
            <!-- web -->
            <a class="promo-banner web-banner-img nespress-promo-banner" data-promo-name="<?=$machine_promo;?>" data-promo-id="Machine_List_banner" data-promo-creative="Top Banner | Machine List Page" data-promo-position="Machine List Page | Top" href="<?php echo $machine_link; ?>"><img src="<?php echo $machine_desktop_img; ?>"  draggable="false"/></a>
            <!-- mobile -->
            <a class="promo-banner mobile-banner-img" data-promo-name="<?=$machine_promo;?>" data-promo-id="Machine_List_banner" data-promo-creative="Top Banner | Machine List Page" data-promo-position="Machine List Page | Top" href="<?php echo $machine_link; ?>"><img src="<?php echo $machine_mobile_img; ?>" draggable="false"/></a>
        </div>
        <!-- end banner -->

        <div class="container">
            <div class="row">

                <div class="productlist productlist-caps" id="JumpContent" tabindex="-1">

                    <div class="productlist-main clearfix">
                       <aside class="productlist-sidebar col-xs-12 col-md-3">
                        <div class="reorder">
                            <div class="title">Do you want to renew your last order?</div>
                            <?php if (is_user_logged_in()): ?>
                                <a title="RE-ORDER" id="btn-reorder" data-val="<?=check_last_order() ? 'true' : 'false';?>"  class="btn btn-green btn-block">RE-ORDER</a>
                            <?php else: ?>
                                <a href="javascript:void(0)" id="btn-reorder-login" class="btn btn-green">RE-ORDER</a>
                            <?php endif;?>
                        </div>
                        <?php /*
<a href="#" title="Ad 1">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/coffee1.jpg" alt="Ad 1">
</a>

<a href="#" class="ad" title="Ad 2">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/coffee2.jpg" alt="Ad 2">
</a>

 */;?>
                        <?php if ( $machine_sidebar_img != ''): ?>
                            <div class="sidebar-img">
                            <img src="<?php echo $machine_sidebar_img; ?>" alt="machine sidebar image" draggable="false"/>
                            </div>
                        <?php endif ?>
                        </aside>
                        <div class="productlist-panel col-xs-12 col-md-9">
                        <div class="productlist-contents">

                                <!-- original collection -->
                                <div class="productlist-content active" id="caps-original" style="padding-top: 0;">
	                                <!-- <div class="products-count">
                                        <strong><?=$product_count;?></strong> MACHINES
                                    </div> -->

                                    <?php if ($product_count > 0): ?>

                                        <button class="filters-toggler">Filter</button>
                                        <a href="javascript:void(0)" type="reset" class="filters-reset">Reset filters</a>
                                        <div class="filters">
                                            <?php include 'wp-content/themes/storefront-child/components/machine-list/filters.php';?>
                                        </div>

                                        <section class="product-category product-category--machine">
                                            <ul class="list">
                                                <?php $counter = 1;foreach ($machines as $key => $machine) {?>
                                                <?php foreach ($products as $k => $product): ?>
                                                    <?php if (html_entity_decode($machine) == html_entity_decode($product->name)): ?>
                                                        <?php include 'machine-list/product-line.php';?>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                                <?php $counter++;}?>
                                            </ul>
                                        </section>

                                    <?php endif;?>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </main>

<!-- /content -->
