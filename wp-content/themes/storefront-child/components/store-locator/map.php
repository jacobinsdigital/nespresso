
<div id="tabs-content">
    <div id="tab-1" class="tab" style="display:block;">
    	<div class="mapouter">
            <div id="getMap" style="height:100%;width:100%"></div>
        </div>
    	<div class="slocator-popup">
		    <div class="row" style="margin-top: -10px;">
		        <div class="slocator-popup__location col-md-4 store-reseller-rustan-shangrila">
		        	<div class="d-inline">
		        		<span class="slocator-stores__icon icon icon-Resellers_on footer-map-icon"></span>
		        		ÂAN NAM FINE FOOD CO., LTD
		        	</div>
		            <div class="slocator-popup__location-content">
		                322 Dien Bien Phu street,
						Ward 22, Binh Thanh District
						Ho Chi Minh City, VN
		            </div>
		        </div>
		    </div>
		</div>
    </div>
    <!-- Specific retailers -->

    <!-- rustan shangri la -->
    <div id="tab-2" class="tab">
        <div class="mapouter"><div class="gmap_canvas"><iframe style="width: 100%;" width="600" height="600" id="gmap_canvas" src="https://maps.google.com/maps?q=633%2F22%20%C4%90i%E1%BB%87n%20Bi%C3%AAn%20Ph%E1%BB%A7%2C%20Ph%C6%B0%E1%BB%9Dng%201%2C%20Qu%E1%BA%ADn%203%2C%20H%E1%BB%93%20Ch%C3%AD%20Minh%2C%20Vietnam&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net">crocothemes.net</a></div><style>.mapouter{text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div>
        <div class="slocator-popup">
		    <div class="row">
		        <div class="slocator-popup__location col-md-4">
		        	<div class="d-inline">
		        		<span class="slocator-stores__icon icon icon-Pick_up_point_on footer-map-icon"></span>
		        		 ÂAN NAM FINE FOOD CO., LTD
		        	</div>
		            <div class="slocator-popup__location-content">
		               	322 Dien Bien Phu street,
						Ward 22, Binh Thanh District
						Ho Chi Minh City, VN
		            </div>
		        </div>
		    </div>
		</div>
    </div>
</div>
