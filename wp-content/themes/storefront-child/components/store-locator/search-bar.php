<div class="slocator-search col-md-4 no-padding no-margin">
    <div class="slocator-search__fake-submit fa fa-search"></div>
    <div class="input-group slocator-search__group">
        <label class="desktop-label desktop-label-multiline col-xs-12 col-md-3" for="enteraddress">Enter
        a location</label>
        <input type="text" class="slocator-search__input" title="Enter Location"
        id="enteraddress" name="enteraddress" placeholder="Enter Location">
    </div>
</div>
