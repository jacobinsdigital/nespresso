<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>

<!-- content -->

<main id="content" class="shopping-bag">
	<div class="container">
    <header>

        <?php $stepActive = 1; ?>



        <?php
if(!isset($stepActive)) {
    $stepActive = 0;
}

$numSteps = 5;

$titles = array(
    'Shopping bag summary',
    'Delivery mode',
    'Payment',
    'Order summary',
    'Order confirmation'
);


$links = array(
    'shopping-bag.php',
    'delivery-mode.php',
    'payment.php',
    'order-summary.php',
    'order-confirmation.php'
);

if(isset($nameStep)){
    $titles = array(
            $nameStep,
            'Delivery mode',
            'Payment',
            'Order summary',
            'Order confirmation'
    );


    $links = array(
            $nameStepUrl,
            'delivery-mode.php',
            'payment.php',
            'order-summary.php',
            'order-confirmation.php'
    );
}

?>

<div class="wizard">
    <ol class="steps">
        <?php
            for($i=1; $i<= $numSteps; $i++) :

                $class = 'step';
                $activeLink = false;

                if($stepActive == $i) {
                    $class .= ' active';
                }
                if($stepActive > $i) {
                    $class .= ' completed';
                    $containerTop = '';
                    $activeLink = true;
                }
                if($stepActive + 1 == $i) {
                    $activeLink = true;
                }

        ?>
        <li class="<?php echo $class; ?>">
            <?php if($activeLink) :
                    echo '<a class="step-content" href="'.$links[$i-1].'">';
                else :
                    echo '<span class="step-content">';
                endif;
            ?>
                <strong><?php echo $i ?></strong>
                <span><?php echo $titles[$i-1] ?></span>
            <?php if($activeLink) :
                echo '</a>';
            else :
                echo '</span>';
            endif;
            ?>
        </li>
        <?php
            endfor;
        ?>
    </ol>
</div>


    </header>
    <section class="checkister step1 sbag-container" id="JumpContent" tabindex="-1">
        <header>
            <h2>Shopping bag</h2>
        </header>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="main-content clearfix">
                    <div class="sbag-list">
                        <div class="sbag-list-content sbag-loading"></div>
                        <a href="#" class="link link--left" title="Back to online shop">Back to online shop</a>
                    </div>
	                <section class="bloc-promotion">
		                <h3 class="main-content__title main-content__title--promo">Promotion code</h3>
		                <div class="bloc-delivery clearfix">
			                <div class="input-group input-group-generic">
				                <label class="desktop-label col-sm-3 col-md-4" for="membership-password">Enter your promo code<span class="required">*</span></label>
				                <div class="col-sm-6 col-xs-9">
					                <input type="text" title="My postal code" id="membership-password" name="membership-password" placeholder="Enter your promo code*" class="form-control col-sm-12 col-md-9">
					                <a href="#" title="Want to redeem a gift card ?" class="want-gift link">Want to redeem a gift card ? <i class="fa fa-question-circle"></i></a>
					                <span class="mobile-label">Enter your promo code<span class="required">*</span></span>
				                </div>
				                <button class="btn btn-primary btn-noWidth col-xs-3 col-md-2" type="button">Ok</button>
			                </div>
		                </div>
	                </section>

                    <div class="sbag-list clearfix" style="background: #F9F9F9">
                        <div class="sbag-resume-content clearfix hidden">
                            <div class="sbag-resume-head">
                                <span class="sbag-resume-head-top">Total: </span>
                                <ul>
                                    <li>Capsules (<span>200</span>)</li>
                                    <li>Accessories (<span>1</span>)</li>
                                </ul>
                            </div>
                            <div class="sbag-resume-item sbag-total">
                                <div class="sbag-total-key">Subtotal</div>
                                <div class="sbag-total-value">$<span class="sbag-subtotal-price-int">0</span></div>
                            </div>
                            <div class="sbag-resume-item">
                                <div class="sbag-total-key">Vat (incl.)</div>
                                <div class="sbag-total-value">$<span class="sbag-val-price-int">0</span></div>
                            </div>
                        </div>
                        <div class="sbag-footer hidden">
                            <div class="sbag-total-item">
                                <div class="sbag-total-title">Total</div>
                                <div class="sbag-total-price">$<span class="sbag-total-price-int">0</span></div>
                            </div>
                        </div>
                    </div>
	                <div class="link-wrapper">
		                <a class="btn btn-green btn-icon-right pull-right" href="<?php bloginfo('url');?>/delivery-mode" title="Proceed to checkout"><span>Proceed to checkout</span> <i class="icon icon-arrow_left"></i></a>
	                </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <aside class="sbag-recommends clearfix">
                    <div class="sbag-recommends-inner">
                        <h4>Nespresso recommends</h4>
                        <?php
                            $recommendedProduct = array(
                                'img' => 'images/product-box.png',
                                'title' => 'Welcome offer - 150 capsules',
                                'text' => 'The Welcome Offer is exclusive to New Club Members. Purchase our 150 pre-assorted Grand Cru selection and receive a complementary Cube to display and store your capsules. ',
                                'price' => '79.80',
                                'type' => 'capsules',
                                'url' => '#'
                            );
                            include('shopping-bag/product-recommended.php');
                        ?>
	                    <div class="sbag-recommended-product sbag-recommended-product--visual clearfix">
		                    <a href="#" title="">
			                    <img src="<?php echo get_stylesheet_directory_uri();?>/images/machines/M-0134_main_684x378.jpg" alt="">
		                    </a>
	                    </div>

                        <?php
                            $recommendedProduct = array(
                                'img' => 'images/product-06.png',
                                'title' => 'PIXIE ESPRESSO, DECAFFEINATO',
                                'price' => '21.00',
                                'text' => 'Set of 2 double-wall Espresso cups (80 ml) in stainless steel matching the Arpeggio Grand Cru color, with 2 stirrers (12.5 cm) in stainless steel.',
                                'type' => 'accessories'
                            );
                            include('shopping-bag/product-recommended.php');
                        ?>
                        <?php
                            $recommendedProduct = array(
                                'img' => 'images/touch-espresso_main.png',
                                'title' => 'TOUCH ESPRESSO - SET OF 2 ESPRESSO CUPS',
                                'price' => '16.00',
                                'text' => 'Set of 2 double-wall Espresso cups (80 ml) in stainless steel matching the Arpeggio Grand Cru color, with 2 stirrers (12.5 cm) in stainless steel.',
                                'type' => 'accessories',
                                'url' => 'accessory-page.php?product=touch-espresso'
                            );
                            include('shopping-bag/product-recommended.php');
                        ?></div>
                </aside>
            </div>
        </div>
    </section>
	</div>
</main>
<?php
global $scripts;
$scripts[] = 'js/components/shopping-bag.js';
?>

<!-- /content -->
