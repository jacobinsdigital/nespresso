<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * Template Name: Nespresso Home
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$sliders = get_nespresso_sliders();
$mobiles = get_nespresso_mobile_sliders();
if (empty($mobiles)) {
    $mobiles = $sliders;
}
$positive_cup = get_nespresso_positive_cup();
$recipies = get_nespresso_recipies();
$best_sellers = get_nespresso_best_sellers();

$best_seller_top_0 = isset($best_sellers->top) && isset($best_sellers->top[0]) ? $best_sellers->top[0] : null;
$best_seller_top_1 = isset($best_sellers->top) && isset($best_sellers->top[1]) ? $best_sellers->top[1] : null;
$best_seller_top_2 = isset($best_sellers->top) && isset($best_sellers->top[2]) ? $best_sellers->top[2] : null;

$best_seller_middle_0 = isset($best_sellers->middle) && isset($best_sellers->middle[0]) ? $best_sellers->middle[0] : null;
$best_seller_middle_1 = isset($best_sellers->middle) && isset($best_sellers->middle[1]) ? $best_sellers->middle[1] : null;
$best_seller_middle_2 = isset($best_sellers->middle) && isset($best_sellers->middle[2]) ? $best_sellers->middle[2] : null;

$best_seller_bottom_0 = isset($best_sellers->bottom) && isset($best_sellers->bottom[0]) ? $best_sellers->bottom[0] : null;
$best_seller_bottom_1 = isset($best_sellers->bottom) && isset($best_sellers->bottom[1]) ? $best_sellers->bottom[1] : null;
$best_seller_bottom_2 = isset($best_sellers->bottom) && isset($best_sellers->bottom[2]) ? $best_sellers->bottom[2] : null;

$left_image = get_nespresso_left_dynamic_banners();
$right_image = get_nespresso_right_dynamic_banners();

// slider pop up
$slider_pop_up = get_nespresso_slider_pop_up();
?>

<?php get_header(); ?>

<!-- quantity selector for add to basket when clicked -->
<?php get_template_part('components/generics/qty-selector'); ?>

<!-- content -->

<?php get_template_part('generics/shop-online'); ?>
<style type="text/css">
    .qty-selector::after {
        left: 85% !important;
    }
</style>
<main id="content">
    <div class="container">
        <div class="row">
            <div class="nrow">
                <?php //echo do_shortcode("[huge_it_slider id='1']"); ?>
                <?php if ( $sliders) : ?>
                    <div class="slider" id="slider-home">
                        <ul class="slider-list">
                            <?php $x = 0; foreach ($sliders as $key => $slider) : ?>
                                <li>
                                    <figure>
                                        <?php if (isset($slider->type) && $slider->type == 'call_pop_up_slider'): ?>
                                            <img src="<?= $slider->image_url ?>" width="996" height="410" draggable="false" class="slider-image <?= $slider->type ?> promo-banner promo-slider-<?=$x?>" data-promo-name="<?= $slider->content ?>" data-promo-id="web_slider_promotion_<?= $x ?>" data-promo-creative="Web_Hompage_Slider" data-promo-position="Home Main | Slot <?=$x+1?>"/>
                                            <figcaption style="top: 95% !important; "><a href="javascript:void(0)" class="<?= $slider->type ?>"><?= @$slider_pop_up['slider_pop_up_header'] ?></a></figcaption>
                                        <?php else: ?>
                                            <a href="<?= $slider->link ?>" <?= strpos($slider->link, $_SERVER['SERVER_NAME']) !== false  ? '' : 'target="_blank"'  ?>>
                                                <img src="<?= $slider->image_url ?>" width="996" height="410" draggable="false" class="slider-image promo-banner promo-slider-<?=$x?>" data-promo-name="<?= $slider->content ?>" data-promo-id="web_slider_promotion_<?= $x  ?>" data-promo-creative="Web_Hompage_Slider" data-promo-position="Home Main | Slot <?=$x+1?>"/>
                                            </a>
                                        <?php endif; ?>

                                    </figure>
                                </li>
                            <?php $x++; endforeach; ?>
                        </ul>
                    </div><!-- .slider -->
                <?php endif; ?>

                <?php if ( $mobiles) : ?>
                    <div class="slider" id="mobile-slider-home">
                        <ul class="slider-list">
                            <?php $x = 0; foreach ($mobiles as $count => $mobile) : ?>
                                <li>
                                    <figure>
                                        <?php if (isset($mobile->type) && $mobile->type == 'call_pop_up_slider'): ?>
                                            <img src="<?= $mobile->image_url ?>" width="996" height="410" draggable="false"  class="slider-image promo-banner <?= $mobile->type ?>" data-promo-name="<?=$mobile->content  ?>" data-promo-id="mobile_slider_promotion_<?= $x  ?>" data-promo-creative="Mobile_Hompage_Slider" data-promo-position="Home Main | Slot <?=$x+1?>"/>
                                            <figcaption style="left: -10px !important; top: -15px !important;"><a href="javascript:void(0)" class="<?= $mobile->type ?>"><?= @$slider_pop_up['slider_pop_up_header'] ?></a></figcaption>
                                        <?php else: ?>
                                            <a href="<?= $mobile->link ?>" <?= strpos($mobile->link, $_SERVER['SERVER_NAME']) !== false  ? '' : 'target="_blank"'  ?>>
                                                <img src="<?= $mobile->image_url ?>" width="996" height="410" draggable="false" class="slider-image promo-banner" data-promo-name="<?=$mobile->content  ?>" data-promo-id="mobile_slider_promotion_<?= $x  ?>" data-promo-creative="Mobile_Hompage_Slider" data-promo-position="Home Main | Slot <?=$x+1?>"/>
                                            </a>
                                        <?php endif; ?>
                                    </figure>
                                </li>
                            <?php $x++; endforeach; ?>
                        </ul>
                    </div><!-- .slider -->
                <?php endif; ?>
                <?php if ($left_image || $right_image) : ?>
                    <br>
                    <div class="row">
                        <?php if ($left_image): ?>
                            <div class="col-sm-<?= $right_image ? '6' : '12' ?> col-12">
                                <?php foreach ($left_image as $value): ?>
                                    <?php if ($value->link): ?>
                                    <a href="<?= $value->link ?>" target="_blank">
                                        <img class="dynamic-img" src="<?= $value->image_url ?>">
                                    </a>
                                    <?php else: ?>
                                        <img class="dynamic-img" src="<?= $value->image_url ?>">
                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>
                        <?php if ($right_image): ?>
                            <div class="col-sm-<?= $left_image ? '6' : '12' ?>  col-12">
                                <?php foreach ($right_image as $value): ?>
                                    <?php if ($value->link): ?>
                                    <a href="<?= $value->link ?>" target="_blank">
                                        <img class="dynamic-img" src="<?= $value->image_url ?>">
                                    </a>
                                    <?php else: ?>
                                    <img class="dynamic-img" src="<?= $value->image_url ?>">
                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>

                    </div>
                <?php endif; ?>
            </div>
            <div id="page-content" class="container-best-sellers">
                <div class="latest-product">
                    <!-- best Seller -->
                    <?php
                        $the_slug = 'best-sellers';
                        $args = array(
                          'name'        => $the_slug,
                          'post_type'   => 'page',
                          'post_status' => 'publish'
                        );
                        $best_seller_post = get_posts($args);
                        $best_seller_content = $best_seller_post[0]->post_content;
                        $data_list = @strip_tags($best_seller_content);
                        echo $best_seller_content;
                    ?>
                    <ul class="products-col-1">

                        <?php if ( $best_seller_top_0 ) :
                            $hq_data  = get_hq_data($best_seller_top_0->_sku[0]);
                        ?>
                            <li class="track-impression-product product-item"
                                data-product-item-id="<?= $best_seller_top_0->_sku[0] ?>"
                                data-product-section="Best Seller List"
                                data-product-position="1"
                            >
                                <div class="thumb-section">
                                    <figure>
                                        <a href="<?= get_permalink($best_seller_top_0->ID) ?>"
                                            class="nespresso-product-item"
                                            title="<?= $best_seller_top_0->name ?>"
                                            data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_top_0->ID ?>"
                                            data-name="<?=$best_seller_top_0->name?>"
                                            data-price="<?=$best_seller_top_0->_price[0]?>"
                                            data-type="<?=$best_seller_top_0->product_type?>"
                                            data-position="1"
                                            data-list="<?= $data_list ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-price="<?= $best_seller_top_0->_price[0] ?>"
                                            data-sku="<?= $best_seller_top_0->_sku[0] ?>"
                                        >
                                            <img src="<?= $best_seller_top_0->image_url ?>" alt="Go to <?= $best_seller_top_0->name ?>" width="98" height="102"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="content-info">
                                    <div class="pro-info">
                                        <span class="pro-title"><?= $best_seller_top_0->name ?></span>
                                        <span class="pro-price"><?= wc_price($best_seller_top_0->_price[0]) ?></span>
                                    </div>
                                    <div class="container-btn">
                                        <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_top_0->ID ?>"
                                            data-id="<?= $best_seller_top_0->ID ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-sku="<?= $best_seller_top_0->_sku[0] ?>"
                                            data-name="<?=$best_seller_top_0->name?>"
                                            data-price="<?=$best_seller_top_0->_price[0]?>"
                                            data-type="<?=$best_seller_top_0->product_type?>"
                                            data-image-url="<?=$best_seller_top_0->image_url ? $best_seller_top_0->image_url : '';?>"
                                            data-product-url="<?= get_permalink($best_seller_top_0->ID) ?>"
                                            href="javascript:void(0)"
                                        >
                                            <i class="icon-plus"></i>
                                            <span class="btn-add-qty"></span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if ( $best_seller_top_1 ) :
                            $hq_data  = get_hq_data($best_seller_top_1->_sku[0]);
                        ?>
                            <li class="track-impression-product product-item"
                                data-product-item-id="<?= $best_seller_top_1->_sku[0] ?>"
                                data-product-section="Best Seller List"
                                data-product-position="2"
                            >
                                <div class="thumb-section">
                                    <figure>
                                        <a href="<?= get_permalink($best_seller_top_1->ID) ?>"
                                            class="nespresso-product-item"
                                            title="<?= $best_seller_top_1->name ?>"
                                            data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_top_1->ID?>"
                                            data-name="<?=$best_seller_top_1->name?>"
                                            data-price="<?=$best_seller_top_1->_price[0]?>"
                                            data-type="<?=$best_seller_top_1->product_type?>"
                                            data-position="2"
                                            data-list="<?= $data_list ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-price="<?= $best_seller_top_1->_price[0] ?>"
                                            data-sku="<?= $best_seller_top_1->_sku[0] ?>"
                                        >
                                            <img src="<?= $best_seller_top_1->image_url ?>" alt="Go to <?= $best_seller_top_1->name ?>" width="98" height="102"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="content-info">
                                    <div class="pro-info">
                                        <span class="pro-title"><?= $best_seller_top_1->name ?></span>
                                        <span class="pro-price"><?= wc_price($best_seller_top_1->_price[0]) ?></span>
                                    </div>
                                    <div class="container-btn">
                                        <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_top_1->ID ?>"
                                            data-id="<?= $best_seller_top_1->ID ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-sku="<?= $best_seller_top_1->_sku[0] ?>"
                                            data-name="<?=$best_seller_top_1->name?>"
                                            data-price="<?=$best_seller_top_1->_price[0]?>"
                                            data-type="<?=$best_seller_top_1->product_type?>"
                                            data-image-url="<?=$best_seller_top_1->image_url ? $best_seller_top_1->image_url : '';?>"
                                            data-product-url="<?= get_permalink($best_seller_top_1->ID) ?>"
                                            href="javascript:void(0)"
                                        >
                                            <i class="icon-plus"></i>
                                            <span class="btn-add-qty"></span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>

                       <?php if ( $best_seller_middle_0 ) :
                            $hq_data  = get_hq_data($best_seller_middle_0->_sku[0]);
                        ?>
                            <li class="track-impression-product product-item"
                                data-product-item-id="<?= $best_seller_middle_0->_sku[0] ?>"
                                data-product-section="Best Seller List"
                                data-product-position="4"
                            >
                                <div class="thumb-section">
                                    <figure>
                                        <a href="<?= get_permalink($best_seller_middle_0->ID) ?>"
                                            class="nespresso-product-item"
                                            title="<?= $best_seller_middle_0->name ?>"
                                            data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_middle_0->ID?>"
                                            data-name="<?=$best_seller_middle_0->name?>"
                                            data-price="<?=$best_seller_middle_0->_price[0]?>"
                                            data-type="<?=$best_seller_middle_0->product_type?>"
                                            data-position="4"
                                            data-list="<?= $data_list ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-price="<?= $best_seller_middle_0->_price[0] ?>"
                                            data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
                                            data-sku="<?= $best_seller_middle_0->_sku[0] ?>"
                                        >
                                            <img src="<?= $best_seller_middle_0->image_url ?>" alt="Go to <?= $best_seller_middle_0->name ?>" width="98" height="102"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="content-info">
                                    <div class="pro-info">
                                        <span class="pro-title"><?= $best_seller_middle_0->name ?></span>
                                        <span class="pro-price"><?= wc_price($best_seller_middle_0->_price[0]) ?></span>
                                    </div>
                                    <div class="container-btn">
                                        <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_middle_0->ID ?>"
                                            data-id="<?= $best_seller_middle_0->ID ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-sku="<?= $best_seller_middle_0->_sku[0] ?>"
                                            data-name="<?=$best_seller_middle_0->name?>"
                                            data-price="<?=$best_seller_middle_0->_price[0]?>"
                                            data-type="<?=$best_seller_middle_0->product_type?>"
                                            data-image-url="<?=$best_seller_middle_0->image_url ? $best_seller_middle_0->image_url : '';?>"
                                            data-product-url="<?= get_permalink($best_seller_middle_0->ID) ?>"
                                            href="javascript:void(0)"
                                        >
                                            <i class="icon-plus"></i>
                                            <span class="btn-add-qty"></span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if ( $best_seller_bottom_0 ) :
                            $hq_data  = get_hq_data($best_seller_bottom_0->_sku[0]);
                        ?>
                            <li class="track-impression-product product-item"
                                data-product-item-id="<?= $best_seller_bottom_0->_sku[0] ?>"
                                data-product-section="Best Seller List"
                                data-product-position="7"
                            >
                                <div class="thumb-section">
                                    <figure>
                                        <a href="<?= get_permalink($best_seller_bottom_0->ID) ?>"
                                            class="nespresso-product-item"
                                            title="<?= $best_seller_bottom_0->name ?>"
                                            data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_bottom_0->ID?>"
                                            data-name="<?=$best_seller_bottom_0->name?>"
                                            data-price="<?=$best_seller_bottom_0->_price[0]?>"
                                            data-type="<?=$best_seller_bottom_0->product_type?>"
                                            data-position="7"
                                            data-list="<?= $data_list ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-price="<?= $best_seller_bottom_0->_price[0] ?>"
                                            data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
                                            data-sku="<?= $best_seller_bottom_0->_sku[0] ?>"
                                        >
                                            <img src="<?= $best_seller_bottom_0->image_url ?>" alt="Go to <?= $best_seller_bottom_0->name ?>" width="98" height="102"/>
                                        </a>
                                    </figure>
                                </div>
                                <div class="content-info">
                                    <div class="pro-info">
                                        <span class="pro-title"><?= $best_seller_bottom_0->name ?></span>
                                        <span class="pro-price"><?= wc_price($best_seller_bottom_0->_price[0]) ?></span>
                                    </div>
                                    <div class="container-btn">
                                        <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_bottom_0->ID ?>"
                                            data-id="<?= $best_seller_bottom_0->ID ?>"
                                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                            data-sku="<?= $best_seller_bottom_0->_sku[0] ?>"
                                            data-name="<?=$best_seller_bottom_0->name?>"
                                            data-price="<?=$best_seller_bottom_0->_price[0]?>"
                                            data-type="<?=$best_seller_bottom_0->product_type?>"
                                            data-image-url="<?=$best_seller_bottom_0->image_url ? $best_seller_bottom_0->image_url : '';?>"
                                            data-product-url="<?= get_permalink($best_seller_bottom_0->ID) ?>"
                                            href="javascript:void(0)"
                                        >
                                            <i class="icon-plus"></i>
                                            <span class="btn-add-qty"></span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>

                       </ul>

                        <ul class="products-col-2">

                            <?php if ( $best_seller_top_2 ) :
                                $hq_data  = get_hq_data($best_seller_top_2->_sku[0]);
                            ?>
                                <li class="track-impression-product product-item"
                                    data-product-item-id="<?= $best_seller_top_2->_sku[0] ?>"
                                    data-product-section="Best Seller List"
                                    data-product-position="3"
                                >
                                    <div class="thumb-section">
                                        <figure>
                                            <a href="<?= get_permalink($best_seller_top_2->ID) ?>"
                                                class="nespresso-product-item"
                                                title="<?= $best_seller_top_2->name ?>"
                                                data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_top_2->ID?>"
                                                data-name="<?=$best_seller_top_2->name?>"
                                                data-price="<?=$best_seller_top_2->_price[0]?>"
                                                data-type="<?=$best_seller_top_2->product_type?>"
                                                data-position="3"
                                                data-list="<?= $data_list ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-price="<?= $best_seller_top_2->_price[0] ?>"
                                                data-sku="<?= $best_seller_top_2->_sku[0] ?>"
                                            >
                                                <img src="<?= $best_seller_top_2->image_url ?>" alt="Go to <?= $best_seller_top_2->name ?>" width="98" height="102"/>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="content-info">
                                        <div class="pro-info">
                                            <span class="pro-title"><?= $best_seller_top_2->name ?></span>
                                            <span class="pro-price"><?= wc_price($best_seller_top_2->_price[0]) ?></span>
                                        </div>
                                        <div class="container-btn">
                                            <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                                data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_top_2->ID ?>"
                                                data-id="<?= $best_seller_top_2->ID ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-sku="<?= $best_seller_top_2->_sku[0] ?>"
                                                data-name="<?=$best_seller_top_2->name?>"
                                                data-price="<?=$best_seller_top_2->_price[0]?>"
                                                data-type="<?=$best_seller_top_2->product_type?>"
                                                data-image-url="<?=$best_seller_top_2->image_url ? $best_seller_top_2->image_url : '';?>"
                                                data-product-url="<?= get_permalink($best_seller_top_2->ID) ?>"
                                                href="javascript:void(0)"
                                            >
                                                <i class="icon-plus"></i>
                                                <span class="btn-add-qty"></span>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>

                            <?php if ( $best_seller_middle_1 ) :
                                $hq_data  = get_hq_data($best_seller_middle_1->_sku[0]);
                            ?>
                                <li class="track-impression-product product-item"
                                    data-product-item-id="<?= $best_seller_middle_1->_sku[0] ?>"
                                    data-product-section="Best Seller List"
                                    data-product-position="5"
                                >
                                    <div class="thumb-section">
                                        <figure>
                                            <a href="<?= get_permalink($best_seller_middle_1->ID) ?>"
                                                class="nespresso-product-item"
                                                title="<?= $best_seller_middle_1->name ?>"
                                                data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_middle_1->ID?>"
                                                data-name="<?=$best_seller_middle_1->name?>"
                                                data-price="<?=$best_seller_middle_1->_price[0]?>"
                                                data-type="<?=$best_seller_middle_1->product_type?>"
                                                data-position="5"
                                                data-list="<?= $data_list ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-price="<?= $best_seller_middle_1->_price[0] ?>"
                                                data-sku="<?= $best_seller_middle_1->_sku[0] ?>"
                                                >
                                                <img src="<?= $best_seller_middle_1->image_url ?>" alt="Go to <?= $best_seller_middle_1->name ?>" width="98" height="102"/>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="content-info">
                                        <div class="pro-info">
                                            <span class="pro-title"><?= $best_seller_middle_1->name ?></span>
                                            <span class="pro-price"><?= wc_price($best_seller_middle_1->_price[0]) ?></span>
                                        </div>
                                        <div class="container-btn">
                                            <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                                data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_middle_1->ID ?>"
                                                data-id="<?= $best_seller_middle_1->ID ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-sku="<?= $best_seller_middle_1->_sku[0] ?>"
                                                data-name="<?=$best_seller_middle_1->name?>"
                                                data-price="<?=$best_seller_middle_1->_price[0]?>"
                                                data-type="<?=$best_seller_middle_1->product_type?>"
                                                data-image-url="<?=$best_seller_middle_1->image_url ? $best_seller_middle_1->image_url : '';?>"
                                                data-product-url="<?= get_permalink($best_seller_middle_1->ID) ?>"
                                                href="javascript:void(0)"
                                            >
                                                <i class="icon-plus"></i>
                                                <span class="btn-add-qty"></span>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>

                            <?php if ( $best_seller_middle_2 ) :
                                $hq_data  = get_hq_data($best_seller_middle_2->_sku[0]);
                            ?>
                                <li class="track-impression-product product-item"
                                    data-product-item-id="<?= $best_seller_middle_2->_sku[0] ?>"
                                    data-product-section="Best Seller List"
                                    data-product-position="6"
                                >
                                    <div class="thumb-section">
                                        <figure>
                                            <a href="<?= get_permalink($best_seller_middle_2->ID) ?>"
                                                class="nespresso-product-item"
                                                title="<?= $best_seller_middle_2->name ?>"
                                                data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_middle_2->ID?>"
                                                data-name="<?=$best_seller_middle_2->name?>"
                                                data-price="<?=$best_seller_middle_2->_price[0]?>"
                                                data-type="<?=$best_seller_middle_2->product_type?>"
                                                data-position="6"
                                                data-list="<?= $data_list ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-price="<?= $best_seller_middle_2->_price[0] ?>"
                                                data-sku="<?= $best_seller_middle_2->_sku[0] ?>"
                                            >
                                                <img src="<?= $best_seller_middle_2->image_url ?>" alt="Go to <?= $best_seller_middle_2->name ?>" width="98" height="102"/>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="content-info">
                                        <div class="pro-info">
                                            <span class="pro-title"><?= $best_seller_middle_2->name ?></span>
                                            <span class="pro-price"><?= wc_price($best_seller_middle_2->_price[0]) ?></span>
                                        </div>
                                        <div class="container-btn">
                                            <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                                data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_middle_2->ID ?>"
                                                data-id="<?= $best_seller_middle_2->ID ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-sku="<?= $best_seller_middle_2->_sku[0] ?>"
                                                data-name="<?=$best_seller_middle_2->name?>"
                                                data-price="<?=$best_seller_middle_2->_price[0]?>"
                                                data-type="<?=$best_seller_middle_2->product_type?>"
                                                data-image-url="<?=$best_seller_middle_2->image_url ? $best_seller_middle_2->image_url : '';?>"
                                                data-product-url="<?= get_permalink($best_seller_middle_2->ID) ?>"
                                                href="javascript:void(0)"
                                            >
                                                <i class="icon-plus"></i>
                                                <span class="btn-add-qty"></span>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>

                          <?php if ( $best_seller_bottom_1 ) :
                                $hq_data  = get_hq_data($best_seller_bottom_1->_sku[0]);
                            ?>
                                <li class="track-impression-product product-item"
                                    data-product-item-id="<?= $best_seller_bottom_1->_sku[0] ?>"
                                    data-product-section="Best Seller List"
                                    data-product-position="8"
                                >
                                    <div class="thumb-section">
                                        <figure>
                                            <a href="<?= get_permalink($best_seller_bottom_1->ID) ?>"
                                                class="nespresso-product-item"
                                                title="<?= $best_seller_bottom_1->name ?>"
                                                data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_bottom_1->ID?>"
                                                data-name="<?=$best_seller_bottom_1->name?>"
                                                data-price="<?=$best_seller_bottom_1->_price[0]?>"
                                                data-type="<?=$best_seller_bottom_1->product_type?>"
                                                data-position="8"
                                                data-list="<?= $data_list ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-price="<?= $best_seller_bottom_1->_price[0] ?>"
                                                data-sku="<?= $best_seller_bottom_1->_sku[0] ?>"
                                            >
                                                <img src="<?= $best_seller_bottom_1->image_url ?>" alt="Go to <?= $best_seller_bottom_1->name ?>" width="98" height="102"/>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="content-info">
                                        <div class="pro-info">
                                            <span class="pro-title"><?= $best_seller_bottom_1->name ?></span>
                                            <span class="pro-price"><?= wc_price($best_seller_bottom_1->_price[0]) ?></span>
                                        </div>
                                        <div class="container-btn">
                                            <a class="btn btn-green btn-small add-to-cart btn-custom-add-to-cart"
                                                data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $best_seller_bottom_1->ID ?>"
                                                data-id="<?= $best_seller_bottom_1->ID ?>"
                                                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                                                data-sku="<?= $best_seller_bottom_1->_sku[0] ?>"
                                                data-name="<?=$best_seller_bottom_1->name?>"
                                                data-price="<?=$best_seller_bottom_1->_price[0]?>"
                                                data-type="<?=$best_seller_bottom_1->product_type?>"
                                                data-image-url="<?=$best_seller_bottom_1->image_url ? $best_seller_bottom_1->image_url : '';?>"
                                                data-product-url="<?= get_permalink($best_seller_bottom_1->ID) ?>"
                                                href="javascript:void(0)"
                                            >
                                                <i class="icon-plus"></i>
                                                <span class="btn-add-qty"></span>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                    </ul>
                </div>
            </div>
            <aside id="sidebar">
                <div class="sidebar-inner">
                    <?php if ( $positive_cup ) : ?>
                        <div class="sidebar-heading">
                            <div class="thumb-section">
                                <?php if ( $positive_cup->image_url ) : ?>
                                    <figure>
                                        <a href="<?= $positive_cup->link ?>" title="Positive cup Page" target="_blank">
                                            <img src="<?= $positive_cup->image_url ?>" alt="positive cup" width="332" height="200"/>
                                        </a>
                                    </figure>
                                <?php endif; ?>
                            </div>
                            <div class="content-info">
                                <?= str_replace('\\"', '"', $positive_cup->content) ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ( $recipies || !empty($recipies) ) : ?>
                        <div class="sidebar-wrapper clearfix">
                            <h2>Nespresso recipes</h2>
                            <div class="nrow">

                                <?php foreach ($recipies as $recipe ) : ?>

                                    <article class="product-item col-lg-6">
                                        <div class="directory-section">
                                            <div class="thumb-section">
                                                <figure>
                                                    <a href="<?= $recipe->link ?>" title="<?= $recipe->title ?>" target="_blank">
                                                        <img src="<?= $recipe->image_url ?>" alt="<?= $recipe->title ?>" width="128" height="107"/>
                                                    </a>
                                                </figure>
                                            </div>
                                            <div class="content-info no-padding">
                                                <h3><a href="<?= $recipe->link ?>" title="<?= $recipe->title ?>"><?= $recipe->title ?></a></h3>
                                                <p><?= $recipe->content ?></p>
                                                <!-- <a href="<?= $recipe->link ?>" class="link link--right" title="Get the recipes">Get the recipes</a> -->
                                                <a href="<?= $recipe->link ?>" class="" title="Get the recipe" target="_blank">Get the recipe</a>
                                            </div>
                                        </div>
                                    </article>

                                <?php endforeach; ?>

                            </div><!-- .nrow -->
                        </div><!-- .sidebar-wrapper -->
                    <?php endif; ?>

                </div>
            </aside>
        </div>
    </div>
</main>

<!-- /content -->

<!-- get_footer -->
<?php get_footer(); ?>
