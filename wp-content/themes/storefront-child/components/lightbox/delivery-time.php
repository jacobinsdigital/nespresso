<div class="pickup" id="JumpContent" tabindex="-1">
    <header>
    	<div class="row">
            <h2>Select your delivery time</h2>
         </div>
    </header>
    
    <div class="row pickup__deliveryoptions">
        <div class="col-xs-12">
            <div class="pickup__deliveryoptions__title">Available options</div>
            <div class="row checkbox pickup__deliveryoptions__option" role="group" aria-labelledby="delivery_options1">
                <label for="delivery_options1" class="col-xs-9 col-sm-3">
                    <input id="delivery_options1" type="checkbox" title="Recycling at home" name="delivery_options" aria-label="Recycling at home"  value="Recycling at home">
                    <span>Recycling at home</span>
                </label>
                <div class="pickup__deliveryoptions__info col-xs-3"><a title="Information" href="#"><i class="icon icon-Information"></i></a><span class="price">Free</span></div>
            </div>
            <div class="row checkbox pickup__deliveryoptions__option" role="group" aria-labelledby="delivery_options2">
                <label for="delivery_options2" class="col-xs-9 col-sm-3">
                    <input id="delivery_options2" type="checkbox" title="Signature" name="delivery_options" aria-label="Signature" value="Signature">
                    <span>Signature</span>
                </label>
                <div class="pickup__deliveryoptions__info col-xs-3"><a title="Information" href="#"><i class="icon icon-Information"></i></a><span class="price">Free</span></div>
            </div>
        </div>
   </div>
   
   <div class="row pickup__intro">
        <div class="col-xs-12">
              Choose the most convenient slot for you (expect from public holidays)
        </div>
   </div>
    
    <div class="row">
        <div class="col-xs-12 pickup__entete deliverytime">
              Friday 20.11
        </div>
   </div> 
   <div class="row">
        <div class="col-xs-12 pickup__horaires">
        	<div class="col-xs-6 pickup__horaires__heure">
            	17h00-20h00
            </div>
            <div class="col-xs-2 pickup__horaires__prix">
            	9.50&euro;
            </div>
            <div class="col-xs-4">
            	<button class="btn btn-primary btn-green btn-small hidden-lg hidden-md hidden-sm pull-right" title="Choose">+</button>
                <a href="#" class="btn btn-green btn-icon hidden-xs pull-right" title="Select this point"><i class="icon icon-check"></i> Select</a>
            </div>
        </div>
   </div>
   
   <div class="row">
        <div class="col-xs-12 pickup__entete deliverytime">
              Saturday 21.11
        </div>
   </div>
   <div class="row">
        <div class="col-xs-12 pickup__horaires">
            <div class="col-xs-6 pickup__horaires__heure">
                17h00-20h00
            </div>
            <div class="col-xs-2 pickup__horaires__prix">
                9.50&euro;
            </div>
            <div class="col-xs-4">
                <button class="btn btn-primary btn-green btn-small hidden-lg hidden-md hidden-sm pull-right" title="Choose">+</button>
                <a href="#" class="btn btn-green btn-icon hidden-xs pull-right" title="Select this point"><i class="icon icon-check"></i> Select</a>
            </div>
        </div>
        <div class="col-xs-12 pickup__horaires">
            <div class="col-xs-6 pickup__horaires__heure">
                17h00-20h00
            </div>
            <div class="col-xs-2 pickup__horaires__prix">
                9.50&euro;
            </div>
            <div class="col-xs-4">
                <button class="btn btn-primary btn-green btn-small hidden-lg hidden-md hidden-sm pull-right" title="Choose">+</button>
                <a href="#" class="btn btn-green btn-icon hidden-xs pull-right" title="Select this point"><i class="icon icon-check"></i> Select</a>
            </div>
        </div>
   </div>
   
   <div class="row">
        <div class="col-xs-12 pickup__entete deliverytime">
              Friday 20.11
        </div>
   </div> 
   <div class="row">
        <div class="col-xs-12 pickup__horaires">
            <div class="col-xs-6 pickup__horaires__heure">
                17h00-20h00
            </div>
            <div class="col-xs-2 pickup__horaires__prix">
                9.50&euro;
            </div>
            <div class="col-xs-4">
                <button class="btn btn-primary btn-green btn-small hidden-lg hidden-md hidden-sm pull-right" title="Choose">+</button>
                <a href="#" class="btn btn-green btn-icon hidden-xs pull-right" title="Select this point"><i class="icon icon-check"></i> Select</a>
            </div>
        </div>
        <div class="col-xs-12 pickup__horaires">
            <div class="col-xs-6 pickup__horaires__heure">
                17h00-20h00
            </div>
            <div class="col-xs-2 pickup__horaires__prix">
                9.50&euro;
            </div>
            <div class="col-xs-4">
                <button class="btn btn-primary btn-green btn-small hidden-lg hidden-md hidden-sm pull-right" title="Choose">+</button>
                <a href="#" class="btn btn-green btn-icon hidden-xs pull-right" title="Select this point"><i class="icon icon-check"></i> Select</a>
            </div>
        </div>
        <div class="col-xs-12 pickup__horaires">
            <div class="col-xs-6 pickup__horaires__heure">
                17h00-20h00
            </div>
            <div class="col-xs-2 pickup__horaires__prix">
                9.50&euro;
            </div>
            <div class="col-xs-4">
                <button class="btn btn-primary btn-green btn-small hidden-lg hidden-md hidden-sm pull-right" title="Choose">+</button>
                <a href="#" class="btn btn-green btn-icon hidden-xs pull-right" title="Select this point"><i class="icon icon-check"></i> Select</a>
            </div>
        </div>
   </div>
                      
</div>