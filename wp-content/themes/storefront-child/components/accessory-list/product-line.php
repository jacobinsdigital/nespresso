<?php
/**
 *
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with accessory products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$hq_data  = get_hq_data($product->sku);
//dd($product);
?>
<li class="track-impression-product product product--accessories"
    data-product-item-id="<?= $product->sku ?>"
    data-product-section="Accessory List"
    data-product-position="<?= $counter ?>"

    data-collection="<?= $product->collection ?>"
    data-category="<?= $product->category ?>"
>
    <div class="product-image"
        class="nespresso-product-item"
        title="<?= $product->name ?>"
        data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
        data-sku="<?= $product->sku ?>"
        data-name="<?=$product->name?>"
        data-type="<?=$product->product_type?>"
        data-position="<?= $counter ?>"
        data-list="<?= get_the_title() ?>"
        data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
        data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
        data-price="<?= $product->price ?>"

    ><?=get_nespresso_show_off($product->label_off);?>
        <a href="<?= $product->url ?>" title="<?= $product->name ?>"
            class="nespresso-product-item"
            title="<?= $product->name ?>"
            data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
            data-sku="<?= $product->sku ?>"
            data-name="<?=$product->name?>"
            data-type="<?=$product->product_type?>"
            data-position="<?= $counter ?>"
            data-list="<?= get_the_title() ?>"
            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
            data-price="<?= $product->price ?>"
            data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
        >
            <img src="<?= $product->image ? $product->image[0] : '' ?>" class="product-image" alt="<?= $product->name ?>">
			
        </a>
    </div>

    <div class="product-text">

        <?php if(isset($product->description)): ?>
          <div class="product-description">
        <?php else: ?>
          <div class="product-description small">
        <?php endif; ?>

            <h3>
                <a href="<?= $product->url ?>" title="<?= $product->name ?>">
                    <?= $product->name ?>
                </a>
            </h3>

        </div>

        <div class="product-price"><?= wc_price($product->price) ?>
		<?php if($product->regular_price && $product->regular_price>$product->price){ ?>
                <span class="product-price--old"><?=wc_price($product->regular_price) ?></span>
            <?php  } ?>
		</div>

    </div>

    <div class="product-add">
        <?php if ($product->stock > 0):
            $class = "btn-green";
            $label = "Add to basket";
        else:
            $class = "btn-disabled";
            $label = "Out of stock";
        endif; ?>
	    <button class="btn btn-icon btn-block <?= $class ?> btn-icon-right"
            data-single="true"
            data-id="<?= $product->post_id ?>"
            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
            data-sku="<?= $product->sku ?>"
            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
            data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
            data-cart="true"
            data-name="<?= $product->name ?>"
            data-price="<?= $product->price ?>"
            data-image-url="<?= $product->image ? $product->image[0] : '' ?>"
            data-type="<?= $product->product_type ?>"
            data-qty-step="1"
            data-vat="1"
            data-url="<?= $product->url ?>"
        >
		    <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text"><?= $label ?></span>&nbsp;<i class="icon-plus text"></i>
        </button>
        <div  class="mobi-select-cont" >
            <select class="mobi-sec" style="width: 100%;height: 100%;"
            data-id="<?=$product->post_id?>"
            data-cart="true"
            data-name="<?=$product->name?>"
            data-price="<?=$product->price?>"
            data-image-url="<?=$product->image ? $product->image[0] : ''?>"
            data-type="<?=$product->product_type?>"
            data-vat="1"
            data-qty-step="10"
            data-url="<?= $product->url ?>"
            data-aromatic-profile="<?= $product->aromatic_profile?>"
            >

                <?php
                $c=0;
                    while ( $c<= 100) {
                       echo "<option value='$c'>$c</option>";
                       $c+=1;
                    }
                 ?>
            </select>
        </div>

    </div>
</li>
