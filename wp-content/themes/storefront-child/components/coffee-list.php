<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<!-- content -->
<?php

// Category Banner
$category_banner = get_nespresso_category_banner();

$coffee_desktop_img = isset($category_banner['coffee_category_banner_image_url']) && $category_banner['coffee_category_banner_image_url'] ? $category_banner['coffee_category_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 12/Category Banners/CoffeeList_Desktop.png";
$coffee_mobile_img = isset($category_banner['coffee_category_banner_image_url_mobile']) && $category_banner['coffee_category_banner_image_url_mobile'] ? $category_banner['coffee_category_banner_image_url_mobile'] : get_stylesheet_directory_uri() . "/images/banner/September 12/Category Banners/CoffeeList_Mobile.png";
$coffee_sidebar_img = isset($category_banner['coffee_sidebar_image_url']) && $category_banner['coffee_sidebar_image_url'] ? $category_banner['coffee_sidebar_image_url'] : '';
$coffee_link = isset($category_banner['coffee_category_banner_link']) && $category_banner['coffee_category_banner_link'] ? $category_banner['coffee_category_banner_link'] : "/coffee-list/";
$coffee_promo = isset($category_banner['coffee_category_banner_promo_name']) && $category_banner['coffee_category_banner_promo_name'] ? $category_banner['coffee_category_banner_promo_name'] : "Coffee Banner Promo";

/**
 * get the coffee products
 */

$products = get_coffee_products();
krsort($products);
$product_count = 0;
foreach ($products as $product):
    foreach ($product as $product):
        $product_count++;
    endforeach;
endforeach;

// include the scripts
global $scripts;
$scripts[] = 'js/components/productlist.js';
$scripts[] = 'js/components/shopping-bag.js';
//$scripts[] = 'js/components/minibasket.js';

$arrays = get_nespresso_product_list();
if (!$arrays) {
    $categories = custom_acf_get_data('Product Details', 'category');
} else {
    $categories['choices'] = $arrays['coffee'];
}
?>

<!-- quantity selector for add to basket when clicked -->
<?php get_template_part('components/generics/qty-selector');?>

<main id="content" class="fullPage fullPage__plp coffee-list">

	<ul class="productlist-menu-shortcut">
		<li class="selected">
			<a href="/coffee-list" title="Coffee Page"><i class="icon icon-capsule"></i>Coffee</a>
		</li>
		<li>
			<a href="/machine-list" title="Machines Page"><i class="icon icon-machine"></i>Machines</a>
		</li>
		<li>
			<a href="/accessory-list" title="Accessories Page"><i class="icon icon-accessories"></i>Accessories</a>
		</li>
	</ul>
    <!-- banner -->
    <div class="new-banner">
        <!-- web -->
        <a class="promo-banner web-banner-img nespress-promo-banner" data-promo-name="<?=$coffee_promo;?>" data-promo-id="Coffee_List_banner" data-promo-creative="Top Banner | Coffee List Page" data-promo-position="Coffee List Page | Top" href="<?php echo $coffee_link; ?>"><img src="<?php echo $coffee_desktop_img; ?>"  draggable="false"/></a>
        <!-- mobile -->
        <a class="promo-banner mobile-banner-img" data-promo-name="<?=$coffee_promo;?>" data-promo-id="Coffee_List_banner" data-promo-creative="Top Banner | Coffee List Page" data-promo-position="Coffee List Page | Top" href="<?php echo $coffee_link; ?>"><img src="<?php echo $coffee_mobile_img; ?>" draggable="false"/></a>
    </div>

    <!-- end banner -->
    <div class="container">
        <div class="row">

            <div class="productlist productlist-caps" id="JumpContent" tabindex="-1">

                <div class="productlist-main clearfix">
                       <aside class="productlist-sidebar col-xs-12 col-md-3">
                        <div class="reorder">
                            <div class="title">Do you want to renew your last order?</div>
                            <?php if (is_user_logged_in()): ?>
                                <a title="RE-ORDER" id="btn-reorder" data-val="<?=check_last_order() ? 'true' : 'false';?>"  class="btn btn-green btn-block">RE-ORDER</a>
                            <?php else: ?>
                                <a href="javascript:void(0)" id="btn-reorder-login" class="btn btn-green">RE-ORDER</a>
                            <?php endif;?>
                        </div>
                        <?php /*
<a href="#" title="Ad 1">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/coffee1.jpg" alt="Ad 1">
</a>

<a href="#" class="ad" title="Ad 2">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/coffee2.jpg" alt="Ad 2">
</a>

 */;?>
                        <?php if ( $coffee_sidebar_img != ''): ?>
                            <div class="sidebar-img">
                            <img src="<?php echo $coffee_sidebar_img; ?>" alt="coffee sidebar image" draggable="false"/>
                            </div>
                        <?php endif ?>
                        </aside>
                        <div class="productlist-panel col-xs-12 col-md-9">
                        <div class="productlist-contents">
                            <!-- original collection -->
                            <div class="productlist-content active" id="caps-original" style="padding-top: 0;">
								<?php //dd($products); ?>
	                            <!-- <div class="products-count">
                                    <strong><?php echo $product_count; ?></strong> COFFEE<?php echo $product_count > 1 ? 'S' : ''; ?>
                                </div> -->

                                <?php if ($product_count > 0): ?>
                                    <button class="filters-toggler">FILTER</button>
                                    <a href="javascript:void(0)" type="reset" class="filters-reset">Reset filters</a>
                                    <div class="filters">
                                        <?php include 'wp-content/themes/storefront-child/components/coffee-list/filters.php';?>
                                    </div>
                                        <?php $counter = 1;foreach ($categories['choices'] as $key => $categ) {?>
                                            <?php foreach ($products as $category => $product): ?>
                                                 <?php if ($categ === $category) {?>
                                                <section class="product-category" id="<?php echo str_replace(' ', '', $category); ?>">
                                                    <h2><?php echo $category; ?></h2>
                                                    <!-- <a href="#" class="link-btn pull-right" title="Discover the range">Discover the range <span class="fa icon-Fleche"></span></a> -->
                                                    <ul class="list">
                                                        <?php foreach ($product as $k => $product): ?>
                                                            <?php include 'wp-content/themes/storefront-child/components/coffee-list/product-line.php';?>
                                                        <?php $counter++;endforeach;?>
                                                    </ul>
                                                </section>
                                                <?php }?>
                                            <?php endforeach;?>
                                        <?php }?>

                                <?php endif; // if ( $product_count > 0 ) ?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

</main><!-- #content -->

<!-- /content -->
