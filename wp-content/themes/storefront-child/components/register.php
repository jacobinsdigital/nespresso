<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php if ( isset($_SESSION['registration_errors']) ) : ?>
	<!-- show registration error message -->
	<script type="text/javascript">
		var registration_errors = <?= json_encode($_SESSION['registration_errors']) ?>;
	</script>
<?php unset($_SESSION['registration_errors']); endif; ?>

<!-- content -->

<main id="content">
	 <form action="<?= wp_registration_url() ?>" method="post" name="register-form" class="needs-validation" id="register-form" role="form">
	<!--<form action="<?= get_permalink( get_option('woocommerce_myaccount_page_id')) ?>" method="post" name="register-form" id="register-form" role="form"> -->

		<?php //do_action( 'woocommerce_register_form_start' ); ?>

		<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
		<?php //endif; ?>

		<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
		<?php //endif; ?>

		<div class="shopping-bag">

			<div class="container">

				<?php require 'register/breadcrumb.php'; ?>

				<?php
					require 'register/step1.php';
					require 'register/step2.php';
					require 'register/step3.php';
					require 'register/step4.php';
				?>

			</div><!-- .container -- >

		</div><!-- .shopping-bag -->

		<input type="hidden" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"

		<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>

		<?php do_action( 'woocommerce_register_form_end' ); ?>

	</form>

</main><!-- #content -->

<!-- /content -->

<?php
global $scripts;
$scripts[] = 'js/libs/bootstrap/bootstrap-datepicker.min.js';
$scripts[] = 'js/components/validation.js';
$scripts[] = 'js/components/shopping-bag-small.js';
$scripts[] = 'js/components/my-expresscheckout.js';
$scripts[] = 'js/components/register.js';
?>
