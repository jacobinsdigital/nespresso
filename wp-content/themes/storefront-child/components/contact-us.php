<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
?>

<!-- content -->
<div class="container">
    <div class="row">
        <div class="contactus">
            <div class="contactus-title">
                <h1><?= the_title() ?></h1>
            </div>
            <div class="contactus-main clearfix">
                <div class="col-xs-12">
                    <div class="contactus-heading col-xs-12 col-sm-8 col-md-7">
                        In order to provide you with the best possible service, please complete the following fields. Including a phone number will allow your request to be dealt with faster.
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="contactus-subtitle col-xs-12">
                        Personal Information
                    </div>
                </div>
                <div class="contactus-form col-xs-12 form">
                    <div class="col-xs-12 col-md-6">
                        <div class="input-group input-group-generic">
                            <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="title">Title<span class="required">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-6">
                                <div class="dropdown dropdown--input">
                                    <button type="button" class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current">Please make a choice</span></button>
                                    <select tabindex="-1" name="Title" id="title">
                                        <option value="1">Mr</option>
                                        <option value="2">Mrs</option>
                                        <option value="3">Ms</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-group input-group-generic">
                            <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="name">First Name<span class="required">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-6">
                                <input type="text" title="First name" id="name" name="name" placeholder="First name" class="form-control col-sm-12 col-md-9">
                                <span class="mobile-label">First Name</span>
                            </div>
                        </div>
                        <div class="input-group input-group-generic">
                            <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="lastname">Last Name<span class="required">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-6">
                                <input type="text" title="Last name" id="lastname" name="lastname" placeholder="Last name" class="form-control col-sm-12 col-md-9">
                                <span class="mobile-label">Last Name</span>
                            </div>
                        </div>
                        <div class="input-group input-group-generic">
                            <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="cnumber">Customer number<span class="required">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-6">
                                <input type="text" title="Customer number" id="cnumber" name="cnumber" placeholder="Customer number" class="form-control col-sm-12 col-md-9">
                                <span class="mobile-label">Customer number</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="input-group input-group-generic">
                            <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="emailadress">Email adress<span class="required">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-6">
                                <input type="text" title="Email adress" id="emailadress" name="emailadress" placeholder="Email adress" class="form-control col-sm-12 col-md-9">
                                <span class="mobile-label">Email adress</span>
                            </div>
                        </div>
                        <div class="input-group input-group-generic">
                            <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="phonenumber">Phone number<span class="required">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-6">
                                <input type="text" title="Phone number" id="phonenumber" name="phonenumber" placeholder="Phone number" class="form-control col-sm-12 col-md-9">
                                <span class="mobile-label">Phone number</span>
                            </div>
                        </div>
                        <div class="input-group input-group-generic">
                            <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="postalzip">Postal or ZIP<span class="required">*</span></label>
                            <div class="col-xs-12 col-sm-8 col-md-6">
                                <input type="text" title="Postal or ZIP" id="postalzip" name="postalzip" placeholder="Postal or ZIP" class="form-control col-sm-12 col-md-9">
                                <span class="mobile-label">Postal or ZIP</span>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row contactus-form-second-part">
                        <div class="col-xs-12 col-md-6">
                            <div class="input-group input-group-generic">
                                <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="Message category">Message category<span class="required">*</span></label>
                                <div class="col-xs-12 col-sm-8 col-md-6">
                                    <div class="dropdown dropdown--input">
                                        <button type="button" class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current">Please make a choice</span></button>
                                        <select tabindex="-1" name="Message category" id="msgcat">
                                            <option value="inquiry">Inquiry</option>
                                            <option value="comments">Comments</option>
                                            <option value="suggestions">Suggestions</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row contactus-form-second-part contactus-form-second-part-full">
                        <div class="col-xs-12">
                            <div class="input-group input-group-generic contactus-form-field">
                                <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="Subject">Subject<span class="required">*</span></label>
                                <div class="col-xs-12 col-sm-8 col-md-6">
                                    <input type="text" title="Subject" id="Subject" name="Subject" placeholder="Subject" class="form-control col-sm-12">
                                    <span class="mobile-label">Subject</span>
                                </div>
                            </div>
                            <div class="input-group input-group-generic contactus-form-field">
                                <label class="desktop-label col-xs-12 col-sm-4 col-md-5" for="msg">Message<span class="required">*</span></label>
                                <div class="col-xs-12 col-sm-8 col-md-6">
                                    <textarea type="text" line="5" title="Message" id="msg" name="msg" class="form-control col-sm-12 col-md-9"></textarea>
                                    <span class="mobile-label textarea-label">Message</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row contactus-form-sending">
                        <div class="col-xs-12">
                            <div class="contactus-form-footer col-xs-12 col-sm-6 col-md-8">
                                All informations will be kept strictly confidential for further details, please read the <i>Nespresso</i> <a href="/legal" class="link-btn" title="Privacy Policy">Privacy Policy</a>
                            </div>
                            <div class="contactus-form-button col-xs-12 col-sm-6 col-md-4">
                                <a href="#" class="btn btn-icon-right btn-green" title="Send"><i class="fa fa-angle-right"></i> SEND</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .container -->
<!-- /content -->
