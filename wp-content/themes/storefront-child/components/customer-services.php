<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>

<!-- content -->
<div class="container">
    <div class="row">
        <div class="customer-services">
            <div class="customer-services-title"><h1>CUSTOMER SERVICES</h1></div>
            <div class="customer-services-main clearfix">
                <div class="col-xs-12">
                    <div class="col-xs-12">
                        <div class="customer-services-heading col-xs-12">
                            Please find below an overview of the most frequently asked questions to Nespresso's Customer Relationship Centres.
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-Information icon-title"></i>
                                <h3>Product information</h3>
                                <span class="customer-services-bloc-description">
                                    Vintage Limited Editions Decaffeinated Grands Crus Variations
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about product information"><span class="fa fa-long-arrow-right"></span> Learn more</a>    
                            </div>
                        </div>
                    </div>

                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-bag icon-title"></i>
                                <h3>Order & Order follow up</h3>
                                <span class="customer-services-bloc-description">
                                    Registration and First Order<br>Where is my order ?<br>
                                    48 Delivery services with So Collissimo or Point Relais
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about order & order follow up"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>

                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-online_support icon-title"></i>
                                <h3>Online support</h3>
                                <span class="customer-services-bloc-description">
                                    Code 703<br>Code 743<br>Forgotten your password
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about the online support"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>

                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-Cup_size_espresso-lungo icon-title"></i>
                                <h3>Product Issue</h3>
                                <span class="customer-services-bloc-description">
                                    Damaged or unordered merchandise Coffee Taste
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about product issue"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>

                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-machine icon-title"></i>
                                <h3>Machine support</h3>
                                <span class="customer-services-bloc-description">
                                    Warranty<br>Maintenance<br>Descaling Kits
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about the machine support"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>

                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-Stick_a_label_on icon-title"></i>
                                <h3>Brand & Promotions</h3>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about the brand & promotions"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>

                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-Recycling_on icon-title"></i>
                                <h3>Recycling</h3>
                                <span class="customer-services-bloc-description">
                                    Recycling<br>Used capsules collection<br>Aluminium
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about the recycling"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-Re-shipping_on icon-title"></i>
                                <h3>Capsule purache from abroad</h3>
                                <span class="customer-services-bloc-description">
                                    <i>Nespresso</i> boutiques and retailers in the Dom-Tom<br>Dom-tom deliveries
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about the capsule purchase from abroad"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                        <div class="col-xs-12">
                            <div class="col-xs-12 customer-services-bloc-content">
                                <i class="icon icon-FAQ_on icon-title"></i>
                                <h3>Other</h3>
                                <span class="customer-services-bloc-description">
                                    Address of our Boutiques<br>Hiring
                                </span>
                                <a class="link-btn uppercase clearfix" href="#" title="Learn More about the machine support"><span class="fa fa-long-arrow-right"></span> Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .container -->
<!-- /content -->
