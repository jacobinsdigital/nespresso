<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$products = get_machine_products();
$arrays = get_nespresso_product_list();
$machines = [];
$cafetera_essenza_mini = 8843;

foreach ($products as $value) {
	if($cafetera_essenza_mini == $value->post_id) {
		$cafetera_essenza_mini = $value;
	}
    $machines[] = $value->name;
}

if ($arrays) {
	$machines = $arrays['machine'];
}

global $scripts;
$scripts[] = 'js/components/cafetera-essenza-mini.js';

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/cafetera-essenza-mini.css">

<main id="main" class="fullPage fullPage__plp coffee-list desktop">
	<div id="block-8809627407861" class="free-html" data-label="">
	    <div class="vue vue_essenzaMini vue_machines v_essenzaMini">
	        <div class="bg_sections" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/background_XL.jpg');">
			    <div class="bg_hideReflects" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/background_XL.jpg');"></div>
			</div>
	        <section class="vue_introduction v_introduction v_sectionLight" next-anchor=".v_diverting" id="introduction" data-label="Introduction">
	            <div class="v_sectionRestrict">
	                <div class="v_sectionContent">
	                    <header>
	                        <div class="v_cell">
	                            <div class="v_menu">
	                                <p class="v_visually_hidden"></p>
	                                <ul>
	                                    <li>
	                                        <a href=""></a>
	                                    </li>
	                                </ul>
	                            </div>
	                        </div>
	                    </header>
	                    <article>
	                        <div class="v_articleContent">
	                            <h2> <span><strong class="v_brand" term="essenza mini">Essenza Mini</strong></span> </h2>
	                            <p class="v_baseline">For big coffee moments</p>
	                            <div class="v_wysiwyg"></div>
	                        </div>
	                    </article>
	                </div>
	            </div>
	            <div class="v_video">
	                <button type="button" name="button" class="v_btnRoundSM v_btnClose" title="Press ESCAPE to exit the Essenza Mini video"> <i class="fn_close"></i> <span>Close video</span> </button>
	                <div></div>
	            </div>
	        </section>
	        <section class="vue_visualVariations vue_productConfigurator v_sectionLight v_toggleVariation0 v_toggle_cmodel v_key_visualVariations" id="visualVariations" data-label="Visual Variations">
	            <h2 class="v_visually_hidden">Customize and Buy your <strong class="v_brand" term="essenza mini">Essenza Mini</strong></h2>
	            <p class="v_visually_hidden">Use the buttons below to expand front or side view</p>
	            <div class="v_visualVariationsSlider essenza-main-product essenza-variation-1" style="touch-action: pan-y; -moz-user-select: none;">
	                <article class="v_product">
	                    <div class="v_restrict">
			                <button style="box-shadow: none !important;" class="v_activeView"><span class="v_visually_hidden">Press ENTER or SPACE to zoom on front view</span><span class="v_visually_hidden">Front</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/black_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/white_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/grey_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button><button style="box-shadow: none !important;"> <span class="v_visually_hidden">Press ENTER or SPACE to zoom on side view</span> <span class="v_visually_hidden">Side</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/black_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/white_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/grey_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button>
			            </div>
	                </article>
	            </div>
	            <div class="v_visualVariationsSlider hide essenza-main-product essenza-variation-2" style="touch-action: pan-y; -moz-user-select: none;">
	                <article class="v_product">
	                    <div class="v_restrict">
	                        <button style="box-shadow: none !important;" class="v_activeView"><span class="v_visually_hidden">Press ENTER or SPACE to zoom on front view</span><span class="v_visually_hidden">Front</span>
	                            <ul>
								    <li class="variation-color-2 variation-2-color-0 v_activeColor">
								        <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/red_front_XL.png');" lazy="loaded"></div>
								    </li>
								    <li class="variation-color-2 variation-2-color-1">
								        <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/lime_front_XL.png');" lazy="loaded"></div>
								    </li>
								    <li class="variation-color-2 variation-2-color-2">
								        <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/white_front_XL2.png');" lazy="loaded"></div>
								    </li>
								</ul>
	                        </button><button style="box-shadow: none !important;"> <span class="v_visually_hidden">Press ENTER or SPACE to zoom on side view</span> <span class="v_visually_hidden">Side</span>
	                            <ul>
								    <li class="variation-color-2 variation-2-color-0 v_activeColor">
								        <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/red_side_XL.png');" lazy="loaded"></div>
								    </li>
								    <li  class="variation-color-2 variation-2-color-1">
								        <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/lime_side_XL.png');" lazy="loaded"></div>
								    </li>
								    <li class="variation-color-2 variation-2-color-2">
								        <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/white_side_XL2.png');" lazy="loaded"></div>
								    </li>
								</ul>
	                        </button>
	                    </div>
	                </article>
	            </div>
	            <div class="v_sectionRestrict">
				    <div class="v_sectionContent"> <!-- dito -->
				        <div class="v_productConfiguratorPosition">
				            <div class="v_productConfigurator">
				                <p class="v_visually_hidden">Use the form below to choose your machine model, change color, and add to bag</p>
				                <form>
				                    <div class="v_cell v_productToggle">
				                        <fieldset>
				                            <legend>Choose between Essenza Mini model C or Essenza Mini model D</legend>
				                            <div class="v_rail" style="touch-action: pan-y; -moz-user-select: none;">
				                                <input name="productToggle" id="productToggle0" value="1" type="radio" checked="">
				                                <label class="v_label_productToggle0" for="productToggle0" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/black_front_thumb.png');">C Model</label>
				                                <input name="productToggle" id="productToggle1" value="2" type="radio">
				                                <label class="v_label_productToggle1" for="productToggle1" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/red_front_thumb.png');">D Model</label>
				                            </div>
				                        </fieldset>
				                    </div>
				                    <div class="v_cell v_toggleColor">
				                        <fieldset>
				                            <legend>Choose your color</legend>
				                            <div class="essenza-main-product-color essenza-main-product-color-1 v_bullets0">
				                                <p aria-hidden="true">Colors</p><div class="v_inputs">
				                                    <input name="colorToggle0" id="radio_colorToggle0_0" value="0" type="radio">
				                                    <label style="background-color: rgb(0, 0, 0);" for="radio_colorToggle0_0"> <b>Piano Black <span class="v_visually_hidden">color</span></b>
				                                    </label><input name="colorToggle0" id="radio_colorToggle0_1" value="1" type="radio"><label style="background-color: rgb(255, 255, 255);" for="radio_colorToggle0_1"> <b>Pure White <span class="v_visually_hidden">color</span></b> </label>
				                                    <!-- <input name="colorToggle0" id="radio_colorToggle0_2" value="2" type="radio"><label style="background-color: rgb(103, 107, 103);" for="radio_colorToggle0_2"> <b>Intense Grey <span class="v_visually_hidden">color</span></b>
				                                    </label> -->
				                                    <div class="v_bullet_selected"><span></span></div>
				                                </div>
				                            </div>
				                            <div class="essenza-main-product-color essenza-main-product-color-2 hide">
				                                <p aria-hidden="true">Colors</p><div class="v_inputs">
				                                    <input name="colorToggle1" id="radio_colorToggle1_0" value="0" type="radio">
				                                    <label style="background-color: rgb(186, 19, 29);" for="radio_colorToggle1_0"> <b>Ruby Red <span class="v_visually_hidden">color</span></b></label>
				                                    <!-- <input name="colorToggle1" id="radio_colorToggle1_1" value="1" type="radio"> -->
				                                    <!-- <label style="background-color: rgb(210, 227, 84);" for="radio_colorToggle1_1"> <b>Lime Green <span class="v_visually_hidden">color</span></b> </label> -->
				                                    <!-- <input name="colorToggle1" id="radio_colorToggle1_2" value="2" type="radio"><label style="background-color: rgb(255, 255, 255);" for="radio_colorToggle1_2"> <b>Pure White <span class="v_visually_hidden">color</span></b> </label> -->
				                                    <div class="v_bullet_selected"><span></span></div>
				                                </div>
				                            </div>
				                        </fieldset>
				                    </div>
				                    <div class="v_cell v_buyProduct">
				                        <fieldset>
				                            <legend>Review your product: D Model Lime Green color</legend>
				                            <div class="v_addToCart">
				                                <div class="v_priceAndButton">
				                                    <p class="v_productPrice"><?= wc_price(@$cafetera_essenza_mini->price) ?></p>
				                                    <div class="v_addToCartCustom">
				                                        <button type="button" class="view_buy" data-link="<?= @$cafetera_essenza_mini->url ?>" style="padding: 0.7em 1em .7em 1em;"><span class="v_label" aria-hidden="true">View Details & Buy</span> </button>
				                                    </div>
				                                </div>
				                            </div>
				                        </fieldset>
				                    </div>
				                </form>
				            </div>
				        </div>
				    </div>
				</div>
	        </section>
	        <section class="vue_benefits v_key_benefits showBenefits_dmodel" id="benefits" data-label="benefits">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover <strong class="v_brand" term="nespresso">Nespresso</strong>’s smallest machine</h2>
			            <div class="v_wysiwyg wow" data-wow="">
			                <p>With <strong class="v_brand" term="essenza mini">Essenza Mini</strong>, <strong class="v_brand" term="nespresso">Nespresso</strong> has delivered its most compact machine yet – without any compromise. Offering 2 programmable cup sizes, <strong class="v_brand" term="essenza mini">Essenza Mini</strong> machine creates perfect coffee just the way you like it. Choose from 2 distinctive contemporary shapes and 3 colours to fit your style and space. It’s the small machine that opens up the whole world of <strong class="v_brand" term="nespresso">Nespresso</strong> coffee.</p>
			            </div>
			            <ul class="v_row6">
			                <li data-wow="" class="v_icon_circle19Bar wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/circle19Bar.svg"></div>
			                    <div>
			                        <p>19 bar pression</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_circleCupSize2 wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/circleCupSize2.svg"></div>
			                    <div>
			                        <p>2 cup sizes</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_circleCompact wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/circleCompact.svg"></div>
			                    <div>
			                        <p>Compact size</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_circleEco wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/circleEco.svg"></div>
			                    <div>
			                        <p>Eco mode</p>
			                    </div>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>
			<section class="vue_videoOnScroll v_sectionCenter v_key_VideoOnScroll" id="demo1" data-label="Demo">
			    <div class="bg_video v_scene1">
			        <div class="v_video">
			            <video id="video_scene_1" preload="auto" muted="true" webkit-playsinline="true" playsinline="true" tabindex="-1" aria-hidden="true" class="v_scene" data-videoindex="1">
			                <source type="video/webm" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene1_forward.webm">
			                <source type="video/mp4" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene1_forward.mp4"> </video>
			        </div>
			        <div class="image_demo_1 v_sceneHD hide" aria-hidden="true" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene1.jpg');"> </div>
			    </div>
			    <div class="v_sectionRestrict description_demo_1" style="opacity: 0.01">
			        <div class="v_sectionContent">
			            <div class="v_emptyRow v_emptyRowTop"> </div>
			            <div class="v_text v_scene1">
			                <div class="v_textCell">
			                    <h2 class="description_demo" data-demo="1">Small machine for big coffee moments</h2>
			                    <div class="v_wysiwyg">
			                        <p>By concentrating its coffee knowhow and expertise into a brand new design, <strong class="v_brand" term="nespresso">Nespresso</strong> has delivered its most compact machine yet, without compromise.</p>
			                        <p>The new <strong class="v_brand" term="essenza mini">Essenza Mini</strong> machine combines ease-of-use, minimalist beauty and unrivalled <strong class="v_brand" term="nespresso">Nespresso</strong> quality to create the perfect cup every time.</p>
			                    </div>
			                </div>
			            </div>
			            <div class="v_emptyRow v_emptyRowBottom"></div>
			        </div>
			    </div>
			</section>
			<section class="vue_videoOnScroll v_sectionCenter v_key_VideoOnScroll" id="demo2" data-label="Demo">
			    <div class="bg_video v_scene2">
			        <div class="v_video">
			            <video id="video_scene_2" preload="auto" muted="true" webkit-playsinline="true" playsinline="true" tabindex="-1" aria-hidden="true" class="v_scene" data-videoindex="2">
			                <source type="video/webm" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene2_forward.webm">
			                <source type="video/mp4" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene2_forward.mp4"> </video>
			        </div>
			        <div class="image_demo_2 v_sceneHD hide" aria-hidden="true" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene2.jpg');"> </div>
			    </div>
			    <div class="v_sectionRestrict description_demo_2" style="opacity: 0.01">
			        <div class="v_sectionContent">
			            <div class="v_emptyRow v_emptyRowTop"> </div>
			            <div class="v_text v_scene2">
			                <div class="v_textCell">
			                    <h2 class="description_demo" data-demo="2">Minimalist design, maximum choice</h2>
			                    <div class="v_wysiwyg">
			                        <p>The compact design of the <strong class="v_brand" term="essenza mini">Essenza Mini</strong> machine comes in two distinctive shapes, and each shape is available in three different colours. Choose the machine that best fits your style and space.</p>
			                        <p>With purist aesthetics complemented by rounded corners or smooth lines, both models deliver a stylish take on the classic <strong class="v_brand" term="nespresso">Nespresso</strong> machine.</p>
			                    </div>
			                    <div class="v_logoAwards">
			                        <div class="v_logoAward v_ifAward" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/if-award_XL.png');"><span class="v_visually_hidden">IF Award 2017</span></div>
			                        <div class="v_logoAward v_redDot" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/red-dot-award_XL.png');"><span class="v_visually_hidden">Red Dot Design Award Winner 2017</span></div>
			                    </div>
			                </div>
			            </div>
			            <div class="v_emptyRow v_emptyRowBottom"></div>
			        </div>
			    </div>
			</section>
			<section class="vue_videoOnScroll v_sectionCenter v_key_VideoOnScroll" id="demo3" data-label="Demo">
			    <div class="bg_video v_scene3">
			        <div class="v_video">
			            <video id="video_scene_3" preload="auto" muted="true" webkit-playsinline="true" playsinline="true" tabindex="-1" aria-hidden="true" class="v_scene" data-videoindex="3">
			                <source type="video/webm" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene3_forward.webm">
			                <source type="video/mp4" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene3_forward.mp4"> </video>
			        </div>
			        <div class="image_demo_3 v_sceneHD hide" aria-hidden="true" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene3.jpg');"> </div>
			    </div>
			    <div class="v_sectionRestrict description_demo_3" style="opacity: 0.01">
			        <div class="v_sectionContent">
			            <div class="v_emptyRow v_emptyRowTop"> </div>
			            <div class="v_text v_scene3">
			                <div class="v_textCell">
			                    <h2 class="description_demo" data-demo="3">Enjoy more, with less</h2>
			                    <ul class="v_row3">
			                        <li> <img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/icn-size.svg">
			                            <h3>Optimal Size</h3>
			                            <p>Ultra-light and ultra-compact, the machine is easy to place and move around any kitchen or home.</p>
			                        </li><li> <img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/icn-look.svg">
			                            <h3>Unique look</h3>
			                            <p>The choice of shapes and colours has been developed to match contemporary trends and fit perfectly with any decor.</p>
			                        </li><li> <img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/icn-taste.svg">
			                            <h3>Endless recipes</h3>
			                            <p>The <strong class="v_brand" term="essenza mini">Essenza Mini</strong> &amp; <strong class="v_brand" term="aeroccino3">Aeroccino3</strong> bundle allows you to create an endless number of milk-based recipes.</p>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			            <div class="v_emptyRow v_emptyRowBottom"></div>
			        </div>
			    </div>
			</section>
			<section class="vue_videoOnScroll v_sectionCenter v_key_VideoOnScroll" id="demo4" data-label="Demo">
			    <div class="bg_video v_scene4">
			        <div class="v_video">
			            <video id="video_scene_4" preload="auto" muted="true" webkit-playsinline="true" playsinline="true" tabindex="-1" aria-hidden="true" class="v_scene" data-videoindex="4">
			                <source type="video/webm" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene4_forward.webm">
			                <source type="video/mp4" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene4_forward.mp4"> </video>
			        </div>
			        <div class="image_demo_4 v_sceneHD hide" aria-hidden="true" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/scene4.jpg');"> </div>
			    </div>
			    <div class="v_sectionRestrict description_demo_4" style="opacity: 0.01">
			        <div class="v_sectionContent">
			            <div class="v_emptyRow v_emptyRowTop"> </div>
			            <div class="v_text v_scene4">
			                <div class="v_textCell">
			                    <h2 class="description_demo" data-demo="4">With the ultra-compact <strong class="v_brand" term="essenza mini">Essenza Mini</strong> machine, you have full access to the world of <strong class="v_brand" term="nespresso">Nespresso</strong> coffee.</h2>
			                    <ul class="v_row3">
			                        <li> <img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/icn-capsule.svg">
			                            <h3>24 Coffees</h3>
			                            <p>Choose from our entire range of coffee and enjoy the smoothness and richness that suits you.</p>
			                        </li><li> <img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/icn-2CupSizes.svg">
			                            <h3>2 cups sizes</h3>
			                            <p>Enjoy Espresso and Lungo just the way you like thanks to two programmable buttons.</p>
			                        </li><li> <img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/icn-simple.svg">
			                            <h3>Simple &amp; Intuitive</h3>
			                            <p>Get your favourite coffee in an instant thanks to <strong class="v_brand" term="essenza mini">Essenza Mini</strong>’s ease of use and fast heat-up time.</p>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			            <div class="v_emptyRow v_emptyRowBottom"></div>
			        </div>
			    </div>
			</section>
			<section class="vue_faq v_sectionLeft v_key_faq" id="faq" data-label="Faq">
			    <div class="bg_normal bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/faq_XL.jpg');" lazy="loaded"></div>
			    <div class="bg_normal bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/faq_S.jpg');" lazy="loaded"></div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Frequently asked questions</h2>
			                <div class="vue_accordion">
			                    <ul role="accordion" class="v_isCollapsable">
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 1 of 6</span> What does 19 bar mean? </button>
			                            <div class="v_wysiwyg" aria-hidden="true" style="">
			                                <p>All the&nbsp;<strong class="v_brand" term="nespresso">Nespresso</strong>&nbsp;machines are equipped with a 19-bar pressure pump, which provides the power needed to pierce the film of the capsule and release the coffee's 900 or so different aromas. The shape of the&nbsp;<strong class="v_brand" term="nespresso">Nespresso</strong>&nbsp;capsule has been specially designed to ensure that the pressurized water flows evenly through the ground coffee during extraction. The temperature and flow time are also set to ensure that each precious aroma is expressed.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 2 of 6</span> How do I restore this machine to factory settings/reprogram this machine? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>You can restore both buttons to factory settings by following these easy steps: </p>
			                                <ol>
			                                    <li>Make sure your machine is turned off and plugged in</li>
			                                    <li>Turn the machine on while pressing and holding the Lungo button.</li>
			                                    <li>Release the Lungo button after 5 seconds.</li>
			                                    <li>LEDS will blink fast 3 times to confirm machine has been reset to factory settings.</li>
			                                    <li>Your machine has now been restored to approximately 40 ml for the Espresso cup and 110ml for the Lungo cup.</li>
			                                </ol>
			                                <p></p>
			                                <p>Additionally, the cup buttons can easily be programmed to your personal settings.</p>
			                                <ol>
			                                    <li>Fill your water tank</li>
			                                    <li>Make sure your machine is switched on and warmed up, with no capsule in the chamber</li>
			                                    <li>Place your preferred cup under the coffee outlet, then press and hold the cup button you would like to program. Water will begin to flow.</li>
			                                    <li>Release the cup button when the cup has been filled to your desired level. LED will blink fast 3 times to confirm new setting.</li>
			                                    <li>This button will now be programmed for that volume.</li>
			                                </ol>
			                                <p></p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 3 of 6</span> How do I take care of my machine? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>Check ‘how to descale’ video on: <a href="https://www.youtube.com/watch?v=kCsyMUM9HVM">How To Descale</a> and ‘how to clean’ video on <a href="https://www.youtube.com/watch?v=_wN-WO2Pwko">How To Clean</a></p>
			                                <ol>
			                                    <li>Descale your machine at least once a year if you use soft water
			                                        <br>Note: for hard water, descale twice a year. (If you don't know what type of water you have, check the water tank - hard water tends to leave white marks)
			                                        <br>The <strong class="v_brand" term="nespresso">Nespresso</strong> club offers you a descaling kit that is specially designed for your <strong class="v_brand" term="nespresso">Nespresso</strong> machine.
			                                        <br>
			                                    </li>
			                                    <li>Eject your capsules after each use</li>
			                                    <li>Change the water tank regularly, and refill with fresh drinking water</li>
			                                    <li>Empty and clean the capsule container and drip tray on a regular basis</li>
			                                    <li>For integrated milk devices, activate the cleaning procedure after each use</li>
			                                    <li>Before your first coffee, brew water without a capsule by pressing any coffee button.</li>
			                                </ol>
			                                <p>When you do this, you pre-heat your coffee cup while rinsing the extraction system for a better coffee experience.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 4 of 6</span> My machine has little or no coffee flow? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <ol>
			                                    <li>If there is no coffee flow, please check that:
			                                        <ul>
			                                            <li>the mains supply cord is not trapped between the water tank and the machine</li>
			                                            <li>the water tank is sufficiently full and correctly positioned</li>
			                                            <li>your machine is switched on (lights ON)</li>
			                                        </ul>
			                                    </li>
			                                    <li>If there is low coffee flow, it means you need to descale your machine</li>
			                                </ol>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 5 of 6</span> My capsules are not puncturing/my machine may have an air bubble </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>Your machine may actually have an air bubble. Air bubbles can block the flow of water through the spout. They usually occur if you run the machine without water in the tank or after a period of non-use.</p>
			                                <p>Fill the water tank, eject any capsule in the chamber, close the coffee lever and press the large cup button. Water should begin to flow from the coffee outlet.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 6 of 6</span> What does the blinking (or no light) on my machine mean? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <ol>
			                                    <li>If there is no coffee flow, please check that:
			                                        <ul>
			                                            <li>the mains supply cord is not trapped between the water tank and the machine</li>
			                                            <li>the water tank is sufficiently full and correctly positioned </li>
			                                            <li>your machine is turned on (lights ON)</li>
			                                        </ul>
			                                    </li>
			                                    <li>If there is low coffee flow, it means you need to descale your machine</li>
			                                </ol>
			                            </div>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_relativeProducts v_sectionLight v_key_relativeProducts" id="relativeProducts" data-label="Relative Products">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover all the <strong class="v_brand" term="nespresso">Nespresso</strong> machines</h2>
			            <div class="v_slider v_sliderFlex">
			                <button class="v_sliderArrow v_sliderPrev" tabindex="-1" aria-hidden="true"> <i class="fn_angleLeft"></i> </button>
			                <div class="v_slideContainer" style="touch-action: pan-y; -moz-user-select: none;">
			                    <div class="v_slide" style="transform: translateX(0); transition: all 1.3s ease 0.15s;">
			                        <ul aria-hidden="true">
			                        	<?php foreach ($machines as $key => $machine):?>
	                                        <?php foreach($products as $k => $product): ?>
	                                            <?php if (html_entity_decode($machine) == html_entity_decode($product->name)): ?>
	                                                <li class="nepresso-machine-product" style="width: 199.2px;">
						                                <div data-product-item-id="<?= $product->post_id ?>">
						                                    <div class="v_imageContainer">
						                                        <a class="v_cell" href="<?= $product->url ?>" tabindex="-1" style="background-image: url('<?= $product->image ? $product->image[0] : '' ?>');" lazy="loading"> <span class="v_visually_hidden"><strong class="v_brand" term="<?= $product->name ?>"><?= $product->name ?></strong></span> </a>
						                                    </div>
						                                    <div class="v_sliderItemText">
						                                        <h3 aria-hidden="true"><span><strong class="v_brand" term="prodigio"><?= $product->name ?></strong></span></h3>

						                                    </div>
						                                </div>
						                            </li>
	                                            <?php endif ?>
	                                        <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </ul>
			                    </div>
			                </div>
			                <button class="v_sliderArrow v_sliderNext" tabindex="-1" aria-hidden="true"> <i class="fn_angleRight"></i> </button>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_corporate v_sectionLeft v_sectionTop v_key_corporate" id="corporate" data-label="Corporate">
			    <div class="bg_parallax skrollable skrollable-between bg-xl" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_XL.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="bg_parallax skrollable skrollable-between bg-small" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_S.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Why <strong class="v_brand" term="nespresso">Nespresso</strong>?</h2>
			                <p data-wow="" class="wow">A cup of coffee is much more than a break. It is your small ritual. Make it an unparalleled experience.</p>
			                <div class="v_wysiwyg wow" data-wow="">
			                    <p>Choose <strong class="v_brand" term="nespresso">Nespresso</strong>, do not settle for less: strictly-selected coffee coming in a matchless range of exclusive varieties, coffee machines combining smart design with simple state-of-the-art technology, refined accessories, indulgent sweet treats and services anticipating your every desire. What else?</p>
			                </div>
			                <!-- <a class="v_link v_iconLeft" href="https://www.nespresso.com/es/en/discover-le-club"> <i class="fn_arrowLink"></i> Read more about <strong class="v_brand" term="nespresso">Nespresso</strong> </a> -->
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_services v_sectionDark v_key_services" id="services" data-label="Services">
			    <div class="v_sectionRestrict" style="max-width: 1140px !important">
			        <div class="v_sectionContent">
			            <h2 class="v_visually_hidden"><strong class="v_brand" term="nespresso">Nespresso</strong> Services</h2>
			            <ul class="v_row5">
			                <li data-wow="" class="wow"> <img alt="" class="freeDelivery" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/freeDelivery.svg">
			                    <h3>Order online and we&lsquo;ll deliver to the address of your choice</h3>
			                    <p>Free delivery for a minimum of ₫3,000 spent.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="pickup" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/pickup.svg">
			                    <h3>Visit us</h3>
			                    <p>Drop by our Nespresso Boutique, or any partner machine re-seller near you.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="assistance247" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/assistance247.svg">
			                    <h3>Are you a new machine owner?</h3>
			                    <p>Our experts are here for you. Call our Coffee Specialist at 1900 633 474</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="payment" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/payment.svg">
			                    <h3>Secured payment transactions</h3>
			                    <p>SSL encryption means sensitive information is always transmitted securely.</p>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>
	    </div>
	</div>
</main>
