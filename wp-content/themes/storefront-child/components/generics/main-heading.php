<div class="container">
    <div class="main-heading top-center">
        <p>WE DELIVER ALL YOUR ORDERS IN 24h* free of charge</p>
        <span>* Orders placed before 7 pm, from Monday to Friday (Nespresso Your Time excluded).</span>
    </div>
</div>