<div class="review-content">
    <div class="review-content__headerfooter row">
        <div class="col-xs-12 col-sm-6 review-content__headerfooter__l">
            <div class="pagination">
                <a href="#" class="pagination__nextprev pagination__nextprev--prev link-btn uppercase"><span class="fa fa-long-arrow-left"></span> Previous</a>
                <a href="#" class="active pagination__number">1</a>
                <a href="#" class="pagination__number">2</a>
                <a href="#" class="pagination__number">3</a>
                <a href="#" class="pagination__number">4</a>
                <a href="#" class="pagination__number">5</a>
                <a href="#" class="pagination__number">6</a>
                <a href="#" class="pagination__number">7</a>
                <a href="#" class="pagination__number">...</a>
                <a href="#" class="pagination__number last">14</a>
                <a href="#" class="pagination__nextprev pagination__nextprev--next link-btn uppercase">Next <span class="fa fa-long-arrow-right"></span></a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 review-content__headerfooter__r">
            <form action="#" method="GET">
                <label for="review-sort">Sort by : </label>
                <div class="dropdown">
                    <button type="button" class="btn btn-grey btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current">Featured</span></button>
                    <select id="review-sort" name="review-sort">
                        <option value="1">Featured</option>
                    </select>
                </div>
                <input type="submit" class="hidden" value="Valider" name="submit" />
            </form>
        </div>
    </div>

    <div class="review-content__item review-content-item">
        <div class="review-content-item__header clearfix">
            <div class="rate-widget pull-left">
                <div class="star-1 ratings-stars fa ratings-vote"></div>
                <div class="star-2 ratings-stars fa ratings-vote"></div>
                <div class="star-3 ratings-stars fa ratings-vote"></div>
                <div class="star-4 ratings-stars fa ratings-vote"></div>
                <div class="star-5 ratings-stars fa"></div>
            </div>
            <div class="pull-left">Posted by Marlene / 12/05/2015</div>
        </div>
        <div class="review-content-item__title">"Broke my Starbucks habit!"</div>
        
        <div class="review-content-item__content">I received the CITIZ bundle for Christmas in 2012. Purchased from William Sonoma. This little machine is wonderful! Fast, efficient, small footprint on counter and the coffee is delicious! Under a dollar per latte beats the $4.50 elsewhere!! Cannot imagine being without it!</div>
        <div class="review-content-item__content review-content-item__content--response">
            <span class="posted"><i class="icon icon-Nespresso_Monogram"></i> Posted by <i>Nespresso</i> Club May 27, 2016</span>
            I received the CITIZ bundle for Christmas in 2012. Purchased from William Sonoma. This little machine is wonderful! Fast, efficient, small footprint on counter and the coffee is delicious! Under a dollar per latte beats the $4.50 elsewhere!! Cannot imagine being without it!</div>
        <div class="review-content-item__question">Was this review helpful ? <button class="btn btn-grey btn-short btn-icon-right btn-inner-arrow"><i>4</i> YES</button></div>
    </div>

    <div class="review-content__item review-content-item">
        <div class="review-content-item__header clearfix">
            <div class="rate-widget pull-left">
                <div class="star-1 ratings-stars fa ratings-vote"></div>
                <div class="star-2 ratings-stars fa ratings-vote"></div>
                <div class="star-3 ratings-stars fa ratings-vote"></div>
                <div class="star-4 ratings-stars fa ratings-vote"></div>
                <div class="star-5 ratings-stars fa"></div>
            </div>
            <div class="pull-left">Posted by Marlene / 12/05/2015</div>
        </div>
        <div class="review-content-item__title">"Broke my Starbucks habit!"</div>
        <div class="review-content-item__content">I received the CITIZ bundle for Christmas in 2012. Purchased from William Sonoma. This little machine is wonderful! Fast, efficient, small footprint on counter and the coffee is delicious! Under a dollar per latte beats the $4.50 elsewhere!! Cannot imagine being without it!</div>
        <div class="review-content-item__question">Was this review helpful ? <button class="btn btn-grey btn-short  btn-icon-right btn-inner-arrow"><i>4</i> YES</button></div>
    </div>

    <div class="review-content__item review-content-item">
        <div class="review-content-item__header clearfix">
            <div class="rate-widget pull-left">
                <div class="star-1 ratings-stars fa ratings-vote"></div>
                <div class="star-2 ratings-stars fa ratings-vote"></div>
                <div class="star-3 ratings-stars fa ratings-vote"></div>
                <div class="star-4 ratings-stars fa ratings-vote"></div>
                <div class="star-5 ratings-stars fa"></div>
            </div>
            <div class="pull-left">Posted by Marlene / 12/05/2015</div>
        </div>
        <div class="review-content-item__title">"Broke my Starbucks habit!"</div>
        <div class="review-content-item__content">I received the CITIZ bundle for Christmas in 2012. Purchased from William Sonoma. This little machine is wonderful! Fast, efficient, small footprint on counter and the coffee is delicious! Under a dollar per latte beats the $4.50 elsewhere!! Cannot imagine being without it!</div>
        <div class="review-content-item__question">Was this review helpful ? <button class="btn btn-grey btn-short btn-icon-right btn-inner-arrow"><i>4</i> YES</button></div>
    </div>

    <div class="review-content__headerfooter row">
        <div class="col-xs-12 col-sm-6 review-content__headerfooter__l">
            <div class="pagination">
                <a href="#" class="pagination__nextprev pagination__nextprev--prev link-btn uppercase"><span class="fa fa-long-arrow-left"></span> Previous</a>
                <a href="#" class="active pagination__number">1</a>
                <a href="#" class="pagination__number">2</a>
                <a href="#" class="pagination__number">3</a>
                <a href="#" class="pagination__number">4</a>
                <a href="#" class="pagination__number">5</a>
                <a href="#" class="pagination__number">6</a>
                <a href="#" class="pagination__number">7</a>
                <a href="#" class="pagination__number">...</a>
                <a href="#" class="pagination__number last">14</a>
                <a href="#" class="pagination__nextprev pagination__nextprev--next link-btn uppercase">Next <span class="fa fa-long-arrow-right"></span></a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 review-content__headerfooter__r">
            <a href="#" class="btn btn-icon btn-grey no-margin" title="Write a review"><i class="fa fa-pencil"></i>Write a review</a>
        </div>
    </div>

</div>
<div class="review__recommand">
    <h3>Nespresso club members recommand</h3>
    <div class="row">
        <div class="col-sm-3 col-md-4 col-xs-12">
            <img src="images/recommand.jpg" alt="Aeroccino Red">
        </div>
        <div class="col-sm-4 col-md-3 col-xs-12">
            <div class="review__recommand__product">
                <span class="title">Aeroccino Red</span>
                <div class="rate-widget pull-left">
                    <div class="star-1 ratings-stars fa ratings-vote"></div>
                    <div class="star-2 ratings-stars fa ratings-vote"></div>
                    <div class="star-3 ratings-stars fa ratings-vote"></div>
                    <div class="star-4 ratings-stars fa ratings-vote"></div>
                    <div class="star-5 ratings-stars fa"></div>
                    <div class="total-votes">4.3</div>
                </div>
                <p class="rates__summary"><strong>78<sup>%</sup></strong> of recommandations </p>
                <a href="#" title="150 reviews">150 reviews</a>
                <span class="price">$ 99.00</span>
                <div class="container-btn">
                    <button class="btn btn-icon btn-green" data-cart="true" data-name="Lattissima+ Intense Black" data-price="399.00" data-picture="images/machines/MACHINES_B2C_DELONGHI_LATTISSIMA_LATTISSIMA+_ICESILVER_010520121200.png" data-type="machines" data-unique="true" data-qty-step="1">
                        <i class="icon-nespresso"></i>
                        <span>Add to basket</span>
                    </button>
                </div>
                <a href="#" class="link-btn link-btn-black uppercase" title="More details"><span class="fa fa-long-arrow-right"></span> More details</a>
            </div>
        </div>
        <div class="col-sm-5 col-xs-12">
            <div class="review__comments--small">
                <ul>
                    <li>
                        <span class="review__comments__quote">" The Best <i>Nespresso</i> machine I know "</span>
                        <span class="review__comments__author">Posted by <strong>Harry Potter</strong> | 11/12/2015</span>
                    </li>
                    <li>
                        <span class="review__comments__quote">" Amazing and Sleek "</span>
                        <span class="review__comments__author">Posted by <strong>Harry Potter</strong> | 11/12/2015</span>
                    </li>
                    <li>
                        <span class="review__comments__quote">" Love the ristretto button! "</span>
                        <span class="review__comments__author">Posted by <strong>Harry Potter</strong> | 11/12/2015</span>
                    </li>
                </ul>
                <a href="#" class="link-btn link-btn-black uppercase" title="See all reviews"><span class="fa fa-long-arrow-right"></span> See all reviews</a>
            </div>
        </div>
    </div>
</div>