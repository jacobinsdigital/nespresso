<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * Template Name: Machine Page 1
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

global $product_review_count_per_page;
$product_review_count_per_page = 5;

global $scripts;
$scripts[] = 'js/libs/simplePagination.js';
$scripts[] = 'js/components/review-content.js';

/**
 * show the pagination
 */
function show_review_content_pagination() {

	$current_review_page = sanitize_text_field( isset($_GET['review-page']) ? $_GET['review-page']  : '' );

	$pagination_count = count($product->reviews) / product_review_count_per_page;

	echo '
		<div class="col-xs-12 review-content__headerfooter__l">
	        <div class="pagination">
	            <a href="#" class="pagination__nextprev pagination__nextprev--prev link link--left">Previous</a>
    ';

    foreach ( $product->reviews as $k => $reivew ) :

    	$pagination_no = $k+1;

    	$active = '';
    	if ( $pagination_no == (int)$current_review_page )
    		$active = 'active';

    	if ( $pagination_no < 8 ) :
	    	echo '<a href="?review-page='.$pagination_no.'" class="pagination__number '.$active.'">'.$pagination_no.'</a>';

	    elseif ( $pagination_no == count($product->reviews) ) :
	    	echo '<a href="?review-page='.$pagination_no.'" class="pagination__number">'.$pagination_no.'</a>';

    	else :
    		echo '<a href="javascript:void(0)" class="pagination__number">...</a>';
		endif;

	endforeach;

    echo '
	            <a href="#" class="pagination__nextprev pagination__nextprev--next link link--right">Next</a>
	        </div>
	    </div>
	';
}

?>
<div class="review-content">

	<div class="container">

	   	<div class="review-content__headerfooter row">
	   		<?php //show_review_content_pagination(); ?>
	   			<div class="col-xs-12 review-content__headerfooter__l">
	        		<div class="pagination"></div>
				</div>
	   		<script>

	   		</script>
   		</div>

	    <div class="review-content__item review-content-item">
	        <div class="review-content-item__header clearfix">
	            <div class="rate-widget pull-left">
	                <div class="star-1 ratings-stars fa ratings-vote"></div>
	                <div class="star-2 ratings-stars fa ratings-vote"></div>
	                <div class="star-3 ratings-stars fa ratings-vote"></div>
	                <div class="star-4 ratings-stars fa ratings-vote"></div>
	                <div class="star-5 ratings-stars fa"></div>
	            </div>
	            <div class="pull-left">Posted by Marlene / 12/05/2015</div>
	        </div>
	        <div class="review-content-item__title">"Broke my Starbucks habit!"</div>
	        <div class="review-content-item__content">I received the CITIZ bundle for Christmas in 2012. Purchased from William Sonoma. This little machine is wonderful! Fast, efficient, small footprint on counter and the coffee is delicious! Under a dollar per latte beats the $4.50 elsewhere!! Cannot imagine being without it!</div>
		    <div class="review-content-item__question">Was this review helpful ? <button class="btn btn-primary btn-noWidth">YES</button></div>
	    </div>

	    <div class="review-content__headerfooter row">

	        <?php //show_review_content_pagination(); ?>

	        <div class="col-xs-12 review-content__headerfooter__r">
	            <a href="#" class="btn btn-primary" title="Write a review">Write a review</a>
	        </div>
	    </div>
    </div><!-- .container -->

</div><!-- .review-content -->
