
<script type="text/html" id="template-qty-selector">
    <div class="qty-selector">
        <span class="qty-selection machine-qty-select qty-nopad">0</span>
        <span class="qty-selection machine-qty-select">1</span>
        <span class="qty-selection machine-qty-select">2</span>
        <span class="qty-selection machine-qty-select">3</span>
        <span class="qty-selection machine-qty-select">4</span>
        <span class="qty-selection qty-nopad">10</span>
        <span class="qty-selection">20</span>
        <span class="qty-selection">30</span>
        <span class="qty-selection">40</span>
        <span class="qty-selection">50</span>
        <span class="qty-selection qty-nopad">60</span>
        <span class="qty-selection">70</span>
        <span class="qty-selection">80</span>
        <span class="qty-selection">90</span>
        <span class="qty-selection hide-sm">100</span>
        <span class="qty-selection qty-nopad hide-sm">150</span>
        <span class="qty-selection hide-sm">200</span>
        <span class="qty-selection hide-sm">300</span>
        <span class="qty-selection hide-sm">400</span>
        <span class="qty-selection hide-sm">500</span>
        <div class="qty-input" style="display: none;">
            <form id="select-your-qty" name="select-your-qty">
                <input class="qty-input-custom" data-value="defaultValue" name="qty-input-custom" type="number" step="1" placeholder="Other Quantity">
                <input class="qty-input-submit" type="submit" value="OK">
            </form>
        </div>
    </div>
</script>
