
<ul class="recommended-products col-xs-12">

    <?php $recommended_counter = 1; foreach ( $product->recommended_products as $recommended_product ) :
		$pro= wc_get_product( $recommended_product->ID );
		if($pro->is_type( 'variable' ))
		{
			$sub  = $pro->get_available_variations();
			//echo '<pre>',print_r($sub),'</pre>';
			//	echo $pro->variation_id; echo $pro->variation_sku;exit ;
			$recommended_product->sku = $sub[0]['sku'];
			$recommended_product->price = $sub[0]['display_price'];
			$recommended_product->ID = $sub[0]['variation_id'];
		}
        $hq_data  = get_hq_data($recommended_product->sku);
    ?>

        <li class="track-impression-product product-item col-xs-6 col-sm-3"
            data-product-item-id="<?= $recommended_product->sku ?>"
            data-product-section="Recommended Product List"
            data-product-position="<?= $recommended_counter ?>"
        >
            <div class="thumb-section">
                <figure>
                    <a href="<?= $recommended_product->url ?>" title="<?= $recommended_product->name ?> Page"
                        class="nespresso-product-item"
                        data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $recommended_product->ID?>"
                        data-sku="<?= $recommended_product->sku ?>"
                        data-name="<?=$recommended_product->name?>"
                        data-type="<?=$recommended_product->product_type?>"
                        data-position="<?= $recommended_counter ?>"
                        data-list="Recommended Products"
                        data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                        data-price="<?=$recommended_product->price?>"
                        data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
                    >
                        <img src="<?= $recommended_product->image ? $recommended_product->image[0] : '' ?>" alt="<?= $recommended_product->name ?>"/>
                    </a>
                </figure>
            </div>
            <div class="content-info">
                <div class="pro-info">
                    <span class="pro-title"><strong><?= $recommended_product->name ?></strong></span>
                    <span class="pro-price"><?= wc_price($recommended_product->price) ?></span>
                </div>
    	        <div class="container-btn">
                    <?php $single_entry = $recommended_product->product_type == 'Coffee' ? 'false' : 'true'?>
    	            <button class="btn btn-green btn-small add-to-cart"
                        data-id="<?= $recommended_product->ID?>"
                        data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $recommended_product->ID ?>"
                        data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                        data-sku="<?= $recommended_product->sku ?>"
                        data-cart="true"
                        data-single="<?= $single_entry ?>"
                        data-recommended="true"
                        data-type="<?= $recommended_product->product_type ?>"
                        data-name="<?= $recommended_product->name ?>"
                        data-price="<?= $recommended_product->price ?>"
                        data-image-url="<?= $recommended_product->image[0] ?>"
                        data-qty-step="1"
                        data-vat="1"
                        data-url="<?= $recommended_product->url ?>"
                    >
    	                <i class="icon-plus"></i>
    	                <span class="btn-add-qty"></span>
    				</button>
                    <div  class="mobi-select-cont btn-recommended-cart" data-url="<?= $recommended_product->url ?>">
                        <!-- <select class="mobi-sec" style="width: 100%;height: 100%;"
                            data-id="<?= $product->product_id ?>"
                            data-cart="true"
                            data-type="<?= $product->product_type ?>"
                            data-name="<?= $product->name ?>"
                            data-price="<?= $product->price ?>"
                            data-image-url="<?= $product->image ? $product->image[0] : '' ?>"
                            data-picture="<?= $product->image ? $product->image[0] : '' ?>"
                            data-qty-step="10"
                            data-vat="1"
                            data-aromatic-profile="<?= $product->aromatic_profile?>"
                        >

                           <?php
                            $c=0;
                            $quantity = 500;
                            $quantity_increment = 10;
                                if($product->aromatic_profile == 'Varied') {
                                    $quantity = 100;
                                    $quantity_increment = 1;
                                }
                                while ( $c<= $quantity) {
                                   if ($c != '0') {
                                        echo "<option value='$c'>$c</option>";
                                   }
                                   if ($c == '100') {
                                        $quantity_increment = 50;
                                   } else if ($c > '150') {
                                        $quantity_increment = 100;
                                   }

                                   $c+=$quantity_increment;
                                }
                             ?>
                        </select> -->
                    </div>
                </div>
            </div>
        </li>

    <?php $recommended_counter++; endforeach; ?>

</ul>

<!-- quantity selector for add to basket when clicked -->
<?php get_template_part('components/generics/qty-selector'); ?>
