<?php

$quantities = array(0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 250, 300);

if(!isset($productQty)) {
    $productQty = 0;
}
?>
<select class="product-qty-select" title="Select your quantity" tabindex="0">
    <?php foreach($quantities as $quantity): ?>
        <option<?php if($productQty == $quantity) { echo ' selected="selected"'; } ?>><?php echo $quantity; ?></option>
    <?php endforeach; ?>
</select>