<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
;?>

<?php get_header();?>
<div class="container" id="search-result">
	<div id="primary" class="content-area">
		<?php if (have_posts()): ?>

			<header class="page-header">
				<h2 class="page-title"><?php printf(esc_attr__('Search Results for: %s', 'storefront'), '<span>' . get_search_query() . '</span>');?></h2>
			</header><!-- .page-header <-->
		<?php endif;?>
		<main id="main" class="site-main customer-services-main search-content" role="main">

		<?php if (have_posts()): ?>

			<?php get_template_part('loop');
	else:

    get_template_part('content', 'none');

endif;?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();
