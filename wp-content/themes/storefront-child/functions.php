<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * included scripts in static pages in components
 * displayed on footer
 * for front-end only
 *
 * @var array
 */
global $scripts;

/**
 * my account url of woocommerce
 */
global $my_account_url;
$my_account_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
$my_account_url = rtrim($my_account_url, '/') . '/'; // add trailing slash

/**
 * require anything to do with customized function that can be used throughout the application
 */
require_once( __DIR__ . '/functions/common-functions.php');

/**
 * require anything to do with form validation
 */
require_once( __DIR__ . '/functions/validation.php');

/**
 * require anything to do with users
 */
require_once( __DIR__ . '/functions/user-functions.php');

/**
 * require anything to do with admin (backend)
 */
require_once( __DIR__ . '/functions/admin-functions.php');

/**
 * require anything to do with customized function that can be used throughout the application
 */
require_once( __DIR__ . '/functions/shortcodes-functions.php');

/**
 * require anything to do with hooking wopcoomerce
 */
require_once( __DIR__ . '/functions/woocommerce-functions.php');

/**
 * require anything to do with coffee products
 */
require_once( __DIR__ . '/functions/coffee-functions.php');

/**
 * require anything to do with machines products
 */
require_once( __DIR__ . '/functions/machines-functions.php');

/**
 * require anything to do with machines products
 */
require_once( __DIR__ . '/functions/accessories-functions.php');

/**
 * require anything to do with machines products
 */
require_once( __DIR__ . '/functions/store-locator-functions.php');

/**
 * require anything to do with cart
 */
require_once( __DIR__ . '/functions/cart-functions.php');

/**
 * require anything to do with my accounts
 */
require_once( __DIR__ . '/functions/my-accounts-functions.php');

/**
 * require anything to do with gtm
 */
require_once( __DIR__ . '/functions/gtm-functions.php');

/**
 * require anything to do with other products
 */
require_once( __DIR__ . '/functions/other-products-functions.php');

/**
 * require anything to do with other products
 */
require_once( __DIR__ . '/functions/custom_acf_get_data.php');

require_once( __DIR__ . '/functions/wp-rest-custom-api-base.php');
require_once( __DIR__ . '/functions/wp-rest-custom-api-func.php');

/**
 * display and die
 * used for debugging
 */
function ddd($var) {
	echo "<pre style=' width: 180vh;'>"; var_dump($var);die;
}

function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('post','product'));
    }

	return $query;
}

add_filter('pre_get_posts','searchfilter');
