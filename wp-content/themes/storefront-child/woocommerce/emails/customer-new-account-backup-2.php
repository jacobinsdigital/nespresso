<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <!doctype html>
    <html  xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>

      <style type="text/css">
        /* Client-specific Styles */
#outlook a {
  padding: 0;
}

.ReadMsgBody {
  width: 100%;
}

.ExternalClass {
  width: 100%;
}

body {
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  -webkit-font-smoothing: antialiased;
  font-family: Trebuchet MS, Helvetica, arial, sans-serif;
}

.yshortcuts,
.yshortcuts a,
.yshortcuts a:link,
.yshortcuts a:visited,
.yshortcuts a:hover,
.yshortcuts a span {
  text-decoration: none !important;
  border-bottom: none !important;
  background: none !important;
}

/*link style*/

td {
  font-family: Trebuchet MS, Helvetica, arial, sans-serif;
}

td[class~=fullCol] {
  padding: 40px 50px;
}

td[class~=pre] {
  padding: 10px 50px;
}

p {
  margin: 0;
  padding: 0;
}

a {
  color: #666666;
  text-decoration: none;
  outline: none;
}

a:hover {
  text-decoration: none
}

@media only screen and (max-width: 599px) {
  td[class~=module-td] {
    padding: 20px !important;
  }

  td[class~=fullCol],
  td[class~=pre] {
    padding: 30px !important;
  }

  table[class~=wrap] {
    width: 100% !important;
  }

  table[class~=row] {
    width: 440px !important;
  }

  img {
    height: auto !important;
  }

  img[class~=line] {
    height: 4px !important;
  }

  img[class~=img] {
    width: 100% !important;
    height: auto !important;
    max-width: 100% !important;
    display: block !important;
  }

  img[class~=title-border-img] {
    height: 1px !important;
  }

  table[class~=logo] {
    width: 100% !important;
    margin-bottom: 10px !important;
  }

  table[class~=menu-tb] {
    float: none !important;
    margin: 0 auto !important;
  }

  td[class~=menu-box] {
    height: 40px !important;
  }

  table[class~=menu] .content {
    text-align: center !important;
  }

  td[class~=header] {
    padding-bottom: 10px !important;
  }

  td[class~=top-bar] {
    padding: 5px 10px !important;
  }

  table[class=footer-left] {
    width: 100% !important;
  }

  table[class=footer-left] td {
    text-align: center !important;
  }

  table[class=footer-right] {
    width: 100% !important;
  }

  table[class=footer-right] td {
    text-align: center !important;
  }

  td[class~=col-1-3] {
    width: 50% !important;
  }

  td[class~=col-2-3] {
    width: 50% !important;
  }

  table[class~=col3] {
    width: 33% !important;
  }

  table[class~=col3_5] {
    width: 40% !important;
  }

  table[class~=col4] {
    width: 50% !important;
  }

  table[class~=col4double] {
    width: 100% !important;
  }

  table[class~=col3] .general-img-td {
    padding-right: 10px !important;
  }

  table[class~=col3] .general-td {
    padding-right: 10px !important;
  }

  table[class~=col4] .general-img-td {
    padding-right: 10px !important;
  }

  table[class~=col4] .general-td {
    padding-right: 10px !important;
  }

  td[class~=general-img-td][class~=p-l-r-20] {

    padding-left: 20px !important;
    padding-right: 20px !important;
  }

  td[class~=full] {
    width: 100% !important;
    float: left !important;
  }

  table[class~=full] {
    width: 100% !important;
    float: left !important;
  }

  td[class~=full] .center {
    text-align: center !important;
    padding: 10px !important;
  }

  td[class~=center] {
    text-align: center !important;
    padding: 10px !important;

  }

  table[class~=banner] td {
    text-align: center !important;
  }

  td[class~=hide480] {
    display: none !important;
  }

  span[class~=hide480] {
    display: none !important;
  }

  img[class~=hide-pc] {
    display: inline !important;
    width: 19px !important;
    height: auto !important;
    visibility: visible !important;

  }

  table[class~=menu2] .content {
    padding-right: 0 !important;
  }

  table[class~=col2_5] {
    width: 49% !important;
  }
}

@media only screen and (max-width: 439px) {
  td.responsive_row, td[class~=responsive_row] {
    width: 100% !important;
    float: left !important;
    display: block !important;
    padding: 0 !important;
    margin: 0 !important;
  }
  table[class~=col2] {
    width: 100% !important;
  }

  .noBlueLinks a {
    color: #999999 !important;
    text-decoration: none !important;
  }

  table[class~=col3_5] {
    width: 100% !important;
  }

  table[class~=col3fix] {
    width: 100% !important;
  }

  td [class~=footer] {
    text-align: center !important;
  }

  td [class~=footer] div {
    text-align: center !important;
  }

  td[class=coffeTable] {
    text-align: center !important;
  }

  table[class~=zebra] > tbody > tr {
    height: auto !important;
  }

  table[class~=zebra] td {
    padding: 0 10px !important;
  }

  td[class~=module-td] {
    padding: 20px !important;
  }

  table[class~=row] {
    width: 100% !important;
  }

  table[class~=logo] img {
    max-width: 100% !important;
  }

  table[class~=menu-tb] {
    width: 100% !important;
  }

  table[class~=menu] .content {
    padding: 4px 5px !important;
  }

  table[class~=top-bar] {
    margin-bottom: 0 !important;
  }

  td[class~=col-1-3] {
    width: 100% !important;
    padding-bottom: 0 !important;
    float: left !important;
    display: block !important;

  }

  td[class~=p-d0] {
    padding-bottom: 0 !important;
  }

  td[class~=col-2-3] {
    width: 100% !important;
  }

  table[class~=col3] {
    width: 100% !important;
  }

  td[class~=p-l-r-20] {

    padding-left: 10px !important;
    padding-right: 10px !important;
  }

  table[class~=col2_5] {
    width: 49% !important;

  }
}


@media only screen and (max-width: 339px) {
  td.responsive_row, td[class~=responsive_row] {
    width: 100% !important;
    float: left !important;
    display: block !important;
    padding: 0 !important;
    margin: 0 !important;
  }
  .noBlueLinks a {
    color: #999999 !important;
    text-decoration: none !important;
  }

  img[class~=capsule] {
    display: none !important;
  }

  td[class~=module-td] {
    padding: 20px !important;
  }

  table[class~=menu] .h4 {
    font-size: 14px !important;
    line-height: 21px !important;

  }

  td[class~=top-bar] {
    padding: 5px 10px !important;
  }

  table[class~=menu-tb] {
    width: 100% !important;
  }

  table[class~=logo] img {
    max-width: 260px !important;
  }
}
      </style>
      </head>
      <body style="margin:0;padding:0;width:100%;height:100%;">
        <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" class="BGtable" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #000000;height: 100%;width: 100%;">
          <tr>
            <td valign="top" class="BGtable-inner">

    <!-- Row -->
    <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
      <tr>
        <td>
          <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
           <tr>
             <td class="module-td noT_D" style="background-color: #000000; color: #ffffff; ">
               <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
                 <caption style="display:none!important;mso-hide: all;max-height: 0;font-size: 0;line-height: 0;"> </caption>
                 <tr>

            <!-- wrap -->
            <td valign="top" class="responsive_row" style="width: 100%;">

      <!-- Block -->
      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tr>
          <td style=" undefined">
      <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
        <tbody>
          <tr>
            <td valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
              <table role="presentation" width="50%" class="col2" border="0" cellpadding="0" align="left" cellspacing="0" style="border-collapse: collapse; width: 50%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border: none;">
                <tbody>
                  <tr>
                    <td class="footer" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 20px 9px 10px 10px;">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="left" class="logo">
                        <tbody>
                          <tr>
                            <td width="140" height="30" align="left" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                              <div class="imgpop" align="center" style="margin: 0; padding: 0;">
                                <a target="_blank" href="https://www.nespresso.com/nl/en" style="color: #666666; text-decoration: none; outline: none;">
                                  <span style="">
                                    <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/nespressoLogo2.png" alt="" border="0" style="display: block; outline: none; text-decoration: none; border: none;">
                                  </span>
                                </a>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
              <table role="presentation" width="49%" class="col2" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 49%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border: none;">
                <tbody>
                <tr>
                  <td class="footer" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 20px 9px 10px 10px;">
                    <div class="t1" style="font-size: 13px; font-weight: bold; color: #ffffff; text-align: right; line-height: 24px; text-transform: uppercase; margin: 0; padding: 0;" align="right">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" class="button" style="border-collapse: collapse; display: inline-block; border-radius: 4px; background-clip: padding-box; background: #308C11;" bgcolor="#308C11">
                        <tbody>
                          <tr>

      <td style="font-size: 12px; line-height: 25px; font-weight: 400; letter-spacing: -0.2px; -webkit-font-smoothing: antialiased; display: block; font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 3px 18px;">
        <a href="http://www.nespresso.com/order/capsules?m=NL&l=EN" title="Order coffee" style="text-decoration: none !important; color: #666666; outline: none;">
          <span style="color: #ffffff; text-align: center;">Order coffee</span>
        </a>
      </td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
        </tr>
      </table>

            </td>

                 </tr>
               </table>
             </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>


    <!-- Row -->
    <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
      <tr>
        <td>
          <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
           <tr>
             <td class="module-td noT_D" style="background-color: #000000; color: #ffffff; ">
               <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
                 <caption style="display:none!important;mso-hide: all;max-height: 0;font-size: 0;line-height: 0;"> </caption>
                 <tr>

            <!-- wrap -->
            <td valign="top" class="responsive_row" style="width: 100%;">

      <!-- Block -->
      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tr>
          <td style=" undefined"><img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/registration_confirmation.jpg" alt="Registration confirmation" title="Registration confirmation" hspace="0" vspace="0" border="0" style="border: 0; display: block; max-width: 100%;  border-radius: 0;"/></td>
        </tr>
      </table>

            </td>

                 </tr>
               </table>
             </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>


    <!-- Row -->
    <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
      <tr>
        <td>
          <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
           <tr>
             <td class="module-td noT_D" style="background-color: #f3f5f7; color: #000000; padding-left: 40px; padding-right: 40px; ">
               <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
                 <caption style="display:none!important;mso-hide: all;max-height: 0;font-size: 0;line-height: 0;">Information about the registration</caption>
                 <tr>

            <!-- wrap -->
            <td valign="top" class="responsive_row" style="width: 100%;">

      <!-- Block -->
      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tr>
          <td style=" undefined">
    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:40px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="40" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">

          </td>
        </tr>
      </tbody>
    </table>
    <p class="classes" aria-level="1" role="heading" style=" font-size: 22px; font-weight: bold; text-align:center; line-height: 32px;text-transform: uppercase;color: #2c2c2c;">REGISTRATION CONFIRMATION</p>
    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:43px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="43" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">
          <img src="https://www.nespresso.com/shared_res/newsletter/transactional/img/hr_light_560_triangle.png" role="presentation" alt="" hspace="0" vspace="0" border="0" width="501" style="border: 0; display: block; max-width: 100%; width: 100%; border-radius: 0;">
          </td>
        </tr>
      </tbody>
    </table>
    <p class="classes"  style="margin-bottom: 30px; font-size: 18px; color: #2c2c2c; text-align:left;color: #2c2c2c;"><?= esc_html( $user_login ) ?>,</p><p class="classes"  style="margin-bottom: 30px; font-size: 18px; color: #2c2c2c; text-align:left;color: #2c2c2c;">Thank you for registering with <span style="font-style:italic;">Nespresso</span>.</p><p class="classes"  style="margin-bottom: 30px; font-size: 18px; color: #2c2c2c; text-align:left;color: #2c2c2c;">You can now login using your email address as your username.</p><p class="classes"  style="margin-bottom: 30px; font-size: 18px; color: #2c2c2c; text-align:left;color: #2c2c2c;">You can access your account area to view your orders and change your password:<br/><span aria-hidden="true" style="font-size: 16px; font-weight: bold; color: #cc9d49; text-align: left;">→&nbsp;</span><a href="<?= esc_url( wc_get_page_permalink( 'myaccount' ) ) ?>" style="font-size: 16px; text-align: left; text-decoration: underline; color: #1a1a1a; outline: none;"><span style="font-size: 16px; text-align: left; color: #1a1a1a; text-decoration: underline;">Click here</span></a>.
    </p>
    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:23px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="23" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">

          </td>
        </tr>
      </tbody>
    </table>
    </td>
        </tr>
      </table>

            </td>

                 </tr>
               </table>
             </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>


    <!-- Row -->
    <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
      <tr>
        <td>
          <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
           <tr>
             <td class="module-td noT_D" style="background-color: #1a1a1a; color: #ffffff; padding-left: 40px; padding-right: 40px; ">
               <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">

                 <tr>

            <!-- wrap -->
            <td valign="top" class="responsive_row" style="width: 100%;">

      <!-- Block -->
      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tr>
          <td style=" undefined">
    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:40px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="40" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">

          </td>
        </tr>
      </tbody>
    </table>
    <p class="classes" aria-level="1" role="heading" style=" font-size: 22px; font-weight: bold; text-align:center; line-height: 32px;text-transform: uppercase;color: #ffffff;">Nespresso recommends</p>
    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:23px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="23" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">
          <img src="https://www.nespresso.com/shared_res/newsletter/transactional/img/hr_dark_560_triangle.png" role="presentation" alt="" hspace="0" vspace="0" border="0" width="501" style="border: 0; display: block; max-width: 100%; width: 100%; border-radius: 0;">
          </td>
        </tr>
      </tbody>
    </table>
    </td>
        </tr>
      </table>

            </td>

                 </tr>
               </table>
             </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>


    <!-- Row -->
    <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
      <tr>
        <td>
          <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
           <tr>
             <td class="module-td noT_D" style="background-color: #1a1a1a; color: #ffffff; padding-left: 40px; padding-right: 40px; ">
               <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
                 <caption style="display:none!important;mso-hide: all;max-height: 0;font-size: 0;line-height: 0;">Nespresso suggestions</caption>
                 <tr>

            <!-- wrap -->
            <td valign="top" class="responsive_row" style="width: 50%;">

      <!-- Block -->
      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tr>
          <td style=" undefined"><img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/grand-cru.png" alt="" title="" hspace="0" vspace="0" border="0" style="border: 0; display: block; max-width: 100%;  border-radius: 0;"/><a href="https://www.nespresso.com/au/en/grands-crus-coffee-range" style="color: #cccccc" style="font-size: 12px; text-align:left;line-height:16px;text-decoration:none;"><span style="font-size: 15px; text-align:left;line-height:16px;" aria-hidden="true">▸&nbsp;</span><span style="font-size: 12px; text-align:left;line-height:16px; text-decoration:none;color: #cccccc">Discover our 24 Grands Crus</span></a>
    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:23px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="23" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">

          </td>
        </tr>
      </tbody>
    </table>
    </td>
        </tr>
      </table>

            </td>

            <!-- wrap -->
            <td valign="top" class="responsive_row" style="width: 50%;">

      <!-- Block -->
      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tr>
          <td style=" undefined"><img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/recycle.jpg" alt="" title="" hspace="0" vspace="0" border="0" style="border: 0; display: block; max-width: 100%;  border-radius: 0;"/><a href="https://www.nespresso.com/au/en/order/accessories/nespresso-recycling-bag" style="color: #cccccc" style="font-size: 12px; text-align:left;line-height:16px;text-decoration:none;"><span style="font-size: 15px; text-align:left;line-height:16px;" aria-hidden="true">▸&nbsp;</span><span style="font-size: 12px; text-align:left;line-height:16px; text-decoration:none;color: #cccccc">Recycle your capsules</span></a>
    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:23px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="23" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">

          </td>
        </tr>
      </tbody>
    </table>
    </td>
        </tr>
      </table>

            </td>

                 </tr>
               </table>
             </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>


    <!-- Row -->
    <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
      <tr>
        <td>
          <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
           <tr>
             <td class="module-td noT_D" style="background-color: #000000; color: #ffffff; ">
               <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
                 <caption style="display:none!important;mso-hide: all;max-height: 0;font-size: 0;line-height: 0;">Legal Information and social media</caption>
                 <tr>

            <!-- wrap -->
            <td valign="top" class="responsive_row" style="width: 100%;">

      <!-- Block -->
      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tr>
          <td style=" undefined">
      <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">

        <tbody>
          <tr>
            <td valign="top" class="col2" style="width: 50%; font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
              <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                <tbody>
                  <tr>
                    <td class="twoColumns" style="text-align: left; font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 30px 9px 10px 10px;" align="left">
                      <table role="presentation" width="112" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>

            <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
              <a href="http://www.facebook.com/nespresso" title="Facebook Nespresso" style="color: #666666; text-decoration: none; outline: none;">
                <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/fb.png" width="28" height="28" alt="Facebook Nespresso" border="0" class="img3" style="display: block;">
              </a>
            </td>

            <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
              <a href="https://twitter.com/nespresso" title="Twitter Nespresso" style="color: #666666; text-decoration: none; outline: none;">
                <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/tw.png" width="28" height="28" alt="Twitter" border="0" class="img3" style="display: block;">
              </a>
            </td>

            <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
              <a href="https://www.pinterest.com/nespresso/" title="Pinterest Nespresso" style="color: #666666; text-decoration: none; outline: none;">
                <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/pn.png" width="28" height="28" alt="Pinterest" border="0" class="img3" style="display: block;">
              </a>
            </td>

            <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
              <a href="https://plus.google.com/+nespresso/" title="Google+" style="color: #666666; text-decoration: none; outline: none;">
                <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/gplus.png" width="28" height="28" alt="Google Plus" border="0" class="img3" style="display: block;">
              </a>
            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td valign="top" class="col2" style="width: 50%; font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
              <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                <tbody>
                  <tr>
                    <td class="twoColumns" style="text-align: right; font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 30px 9px 10px 10px;" align="right">
                      <a href="https://www.nespresso.com/nl/en" style="color: #666666; text-decoration: none; outline: none;">
                        <span style="">
                          <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/nLogo2.png" alt="Nespresso" width="35" height="34" hspace="0" vspace="0" border="0" class="style=&quot;border:" max-width:="" width:="" border-radius:="">
                        </span>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
      <table role="contentinfo"  width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">

        <tbody>
          <tr>
            <td class="1c" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 0 9px 0 10px;">
              <div class="appleLinkGrey" style="font-size: 11px; color: #a3a6a8; text-align: justify; margin: 0; padding: 0;" align="justify">You have been sent this email because you recently registered on the <span style="font-style:italic;">Nespresso</span> website. Please do not respond to this email. If you wish to ask a question, please contact <span style="font-style:italic;">Nespresso</span> on 1800 623 033 or use the form available on our website at <a href="https://www.nespresso.com/au/en/contactus" style="color: #a3a6a8; text-decoration: underline; outline: none;"><span style="color: #a3a6a8; text-decoration: underline;">Nespresso.com</span></a>. <br/> All details are held in accordance with our <a href="https://www.nespresso.com/au/en/legal" style="color: #a3a6a8; text-decoration: underline; outline: none;"><span style="color: #a3a6a8; text-decoration: underline;">Privacy Policy</span></a>.</div>
               <div class="appleLinkGrey" style="font-size: 11px; color: #a3a6a8; text-align: justify; margin: 0; padding: 0;" align="justify"></div>
              <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <tbody>
                  <tr>
                    <td valign="top" width="100%" height="10" style="font-size: 1px; line-height: 10px; font-family: Trebuchet MS, Helvetica, arial, sans-serif;" dir="ltr">&nbsp;</td>
                  </tr>
                </tbody>
              </table>
              <div class="appleLinkGrey" style="font-size: 11px; color: #a3a6a8; text-align: justify; margin: 0; padding: 0;" align="justify">Nestlé Australia Ltd trading as Nespresso Australia<br/>ABN 77 000 011 316 of Building D,<br/>1 Homebush Bay Drive,Rhodes,<br/>NSW 2138</div>
            </td>
          </tr>
        </tbody>
      </table>

    <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:24px;padding:0;text-align:left;vertical-align:top;width:100%">
      <tbody>
        <tr style="padding:0;text-align:left;vertical-align:middle">
          <td height="24" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto;mso-line-height-rule:exactly;padding:0;text-align:center;vertical-align:middle;word-wrap:break-word">

          </td>
        </tr>
      </tbody>
    </table>
    </td>
        </tr>
      </table>

            </td>

                 </tr>
               </table>
             </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
          </tr>
        </table>
      </body>
    </html>
