<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$user = nespresso_get_user_data();
$shipping_cities = nespresso_get_cities($user->shipping_state);
$states = nespresso_get_states();
$country_codes = get_country_codes();
?>
<div class="woocommerce-shipping-fields">
    <?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

        <!-- <h3 id="ship-to-different-address"> -->
        <h3 id="ship-to-different-address" class="c-header">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox hide" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" />
                <!-- <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox hide" checked="checked" type="checkbox" name="ship_to_different_address" value="1" />  -->
                <span><?php _e( 'Shipping address', 'woocommerce' ); ?></span>
            </label>
        </h3>

        <div class="shipping_address">

            <?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

            <div class="woocommerce-shipping-fields__field-wrapper">
			<style>
		.box-pupop {
			position: fixed;
			width: 100%;
			height: 100%;
			background: rgba(0, 0, 0, 0.4);
			top: 0;
			left: 0;
			display:none;
			z-index: 9999;
		}
		.box-pupop-content {
			max-width: 800px;
			border: 1px solid #fff;
			margin: 5% auto 0;
			background: #fff;

			position:relative;
		}
		.box-pupop-content span.box-close:hover {
				background: red;
				color: #fff;
			}
			.box-pupop-content span.box-close {
				    border: 1px solid red;
					display: inline-block;
					position: absolute;
					padding: 5px 10px;
					cursor: pointer;
					right: 0;
					top: 0;
			}
			.box-pupop-content{
				overflow-y: auto;
				height:100%;
			}
			.box-content {
    padding: 0 15px 15px;
}
			.item-option {
				display: flex;
				border: 1px solid #ddd;
				padding: 15px;
				margin-bottom: 10px;
				font-size: 16px;
				cursor:pointer;
			}
			.item-option.active,.item-option:hover{
				border:1px solid #3D8705;
				position: relative;
				background: #dedede;
			}
			.selectlocal{
				display:none;
			}
			.item-option.active > .selectlocal{
				display:block;
				position: absolute;
				right: 45%;
				top:45%
			}
			.item-option div  *{
				display:block;
			}
			.item-option div .item-title {
				font-size: 18px;
				font-weight: bold;
				text-decoration: underline;
			}
			.item-option> div {
				width: 50%;
			}
			.item-option> div img{
				float:right
			}
			
			.box-title {
				font-size: 20px;
				text-transform: uppercase;
				text-align: center;
				padding-bottom: 15px;
				border-bottom: 1px solid #daeacd;
				margin-bottom: 15px;
				background: #D5D5D5;
			}
			.r-pick{
				width:25px !important;
			}
			input.button.alt.yith-wcms-button.next {
				background: #3D8705;
				border-color: #3D8705;
			}
			@media(max-width:768px)
			{
				.item-option.active > .selectlocal
				{
					right: 35%;
					top:35%
				}
			}
		</style>
	  <div class="box-pupop">
			<div class="box-pupop-content">
				<div class="box-title">
					Select a boutique
				</div>
				<div class="box-content">
					
				</div>
				<div class="box-footer" style="text-align: right;display:none">
					<button id="selectlocal">Select</button>
				</div>
			</div>
	  </div>
		<div class=" validate-required" id="shipping_pickup_field" style="clear:both;overflow: hidden;" data-priority="">
			<span class="woocommerce-input-wrapper">
				<ul>
					<li class="option-stand" style="list-style-type: none;">
						<div>
							<span class="option-title">Standard Delivery</span>
							<label>
								<span class="r-pick"><input type="radio" name="delivery_option" data-index="0" id="stand_option"  value="customer_address" checked="checked"> </span>
								<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAqCAYAAADFw8lbAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAKzSURBVFhH7Zg9SGphGMfdcjRDB6GhyY9BocEtAg3BuanFQUQkEUwai6AmIbBCBzENEVxEXJx0cYhSB4OmQghDHIQEESGcnsv/vXjw2CmvXE/3jevww/d9nuccf7zn6zlHMRqN6CewFF00ItH393dqtVpUKpXo6upKVpLJJN3e3tLb25tI6DNEos/Pz2QwGEihUFC5XJaVQqFA4XCY9Ho9HR8fi6SkEIk+Pj6yjc7Ozphss9kUFctBp9Ohvb09crvdkvkxkqIY49BAtlgsCnk58Xq95HA4JHPgU1Fwf39P6+vrFI/HhZichEIhstvtkrkvRcHFxQVbWZvNRh6Ph46OjuYC+5uH7e1ttt2kA5gp+vLywkR3d3c/SMgFZNvttshjpiiA6HRMTiALl8nYUvRvWIoumv9T9OHhgbLZLF1eXpLf7yez2UxWq5UikQh1u90P9fMg+4riD1BrsVgol8tJ1vwJsovGYjFBdmVlhXVJk3nknE6nKCaF7KKoy+fz5PP52HhjY4MajYaQ50oUTQyama2tLTbHb6/XY3muRMHOzg7d3d3R2toam7tcLpbnThQcHBzQ9fW1MD85OeFL9PDwUJBLpVJMeDznShRXOlpCjDUaDdVqNXYqYI5TgSvR19dX2tzcZHP0ltVqlbRaLZtzJYpxpVKh1dVVFtvf36d0Os2nKMhkMiwGotEonZ6e8iGKwzz9NBq/cwH0BoFAQJSXQnbRzzg/PyeVSsX2k0gkJGsm+WeioF6vMwGdTsfuAsFgkN1nscrTtTNFcbXi8E3GwCJE52Gm6HA4JJPJJCoA3IkCFE2vKpeiAB/JjEYj9fv930W8igK8YqjVanp6euJbFNzc3JBSqWSi3/EJcgzaQizQZOxLUYCmF7cQPA5x7n4HWJjBYCDymCnKC0vRRfNDREf0C+lTViBVrWFYAAAAAElFTkSuQmCC"/> 
								<span class="option-content">
									<span>Ho Chi Minh Area: 1-2 working days</span>
									<span>Other Areas: 3-5 working days</span>
									<span>From Monday to Saturday</span>
								</span>
							</label>
						</div>
					</li>
					<li class="option-pickup" style="list-style-type: none;"> 
						<div>
							<span class="option-title">Boutique pick up</span>
							<label><span class="r-pick"><input   type="radio" name="delivery_option" id="pick_option" data-index="0" value="pick_up"></span>
								<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAvCAYAAACG2RgcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAH6SURBVFhH7Zcri0JBGIb3ZxhFmwaTeAkGb2DSIoigRUQQi13QICKCFsGgxSKIwaDJ5gWDIHgJgkHEoP4FTe/yHRDccfacWXDZDROecL73G+Y5c2YGzsf9fsd/QIqwSBEWYZHdbodqtYp4PI5AIIBQKIR8Po/FYsHt/ylCIu12G06nE/V6HcvlEtfrFfv9Xqn7/X6Uy2XuuJ+gKTIcDpW3PxwO3Px2uyGbzaJSqXBzUVRFaJJgMIjpdMrNiWKxiMFgAKvVivP5zO0RQVVkMpkgkUhwswck6XA44PP5vl01EVRF6LNEo1Fu9gxtWJPJhH6/z81FUBU5Ho9wu93cjGW1WkGn08Fut3NzLTQ3K52IUqnEzR7Q56HVWK/Xikyn0+H2qaEpstls4PV6MRqNuDmRSqWg1+sRiUQQDocRi8W4fWpoihCPI7zdbr/U6dnj8WA2m8HlcmE8HiviVHvuE0FIhOj1espRPp1OKBQKyt65XC4wGAxIJpOYz+dKHx1huuTY8VoIixB0pZvNZuRyORiNRmUVarXal563iXS73W+h29NmsyGTyaDVaqHRaLz0NJtNWCyWl/oztKnZeV9E0uk0d/C7YeflirC1dyJFWKQIixRhkSIsUoRFirBIERYpwiJFWKQIixRhERahvzmS+U2ERP4KKcLyT0Tu+ARkQUjmdThHRAAAAABJRU5ErkJggg=="/> 
								<span class="option-content">
									<span>Pick up the order in one of our partner stores.</span>									
									<span>&nbsp;</span>
								</span>
							</label>
						</div>
					</li>
				</ul>
				<?php if((new Express_Shipping_Method)->settings['enabled']=='yes'){ ?>
				<ul>
					<li class="option-stand " id="express-option" style="list-style-type: none;height: auto;<?=$user->shipping_state!='HO-CHI-MINH'?'display:none':''?>">
						<div>
							<span class="option-title"> Express Delivery</span>
							<label>
								<span class="r-pick"><input type="radio" name="delivery_option" data-index="0" id="express_option" value="express" > </span>
								<img src="data:image/svg+xml;utf8;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiIgdmlld0JveD0iMCAwIDMyIDMyIj48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjAxIC0xNDk1LjM2MikiPjxjaXJjbGUgc3R5bGU9Imlzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbCIgY3g9IjIxNyIgY3k9IjE1MTEuMzYyIiByPSIxNS41IiBmaWxsPSJub25lIiBzdHJva2U9IiMzNzM3NDgiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgY29sb3I9IiMwMDAiIG92ZXJmbG93PSJ2aXNpYmxlIi8+PHBhdGggZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMzczNzQ4IiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIGQ9Ik0yMTcuMDAxNTEgMTUwMS44NDZMMjE3IDE1MTEuMzYyMk0yMjQuNjEyNiAxNTExLjM2MjhsLTcuNTk3NjUgME0yMTcuMDAwMDEgMTUyNC44NTU0bDAtMk0yMDMuNTA3MTIgMTUxMS4zNjIybDIgME0yMTcuMDA5MzUgMTQ5Ny44NzUzbDAgMk0yMzAuNDk2MjUgMTUxMS4zNzc1bC0yIDBNMjA1LjMyMjAzIDE1MTguMTExMmwxLjczMjA1LTFNMjEwLjI2MTAzIDE0OTkuNjc5NGwxIDEuNzMyTTIyOC42OTIxNSAxNTA0LjYyOTJsLTEuNzMyMDUgMU0yMjMuNzQyMzUgMTUyMy4wNjAzbC0xLTEuNzMyMU0yMDUuMzIzNjggMTUwNC42MjU2bDEuNzMyMDUgMU0yMjMuNzU1NTkgMTQ5OS42ODdsLS45OTk5NiAxLjczMjFNMjI4LjY4NDUgMTUxOC4xMjM3bC0xLjczMjA1LTFNMjEwLjI0NzggMTUyMy4wNTI2bDEuMDAwMDQtMS43MzIiLz48L2c+PC9zdmc+"/> 
								<span class="option-content">
									<span>Available only for orders in Ho Chi Minh City</span>
									<span>Booking Time: 9AM - 4PM</span>
									<span>Delivery Time: 11AM - 6PM</span>
									<span>Orders outside booking time will be scheduled for delivery the next business day</span>
								</span>
							</label>
						</div>
					</li>
				</ul>
				<?php } ?>
			</span>			
		</div>
		<div class="input-group col-sm-12" style="    padding: 20px 0;">
					<label class="desktop-label col-sm-12"> <input type="checkbox" value="1" name="recycling_pick_up" id="recycling_pick_up"/> Would you like to return your used capsules?<br><span>Our couriers will pick up your used capsules at home and deliver to Nespresso recyling partners</span></label>
				</div>
		<div style="padding: 10px 0;">
			<a id="editaddress" style="    border: 1px solid;
    padding: 5px;
    cursor: pointer;">Edit address</a>
		</div>
		<div id="pickselect">
		</div>
                <?php
                    $fields = $checkout->get_checkout_fields( 'shipping' );

                    foreach ( $fields as $key => $field ) {

                        if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
                            $field['country'] = $checkout->get_value( $field['country_field'] );
                        }
						if ($key == 'shipping_first_name'):?>
                            <p class="form-row form-row-first validate-required disabled" id="shipping_first_name_field" data-priority="10">
                                <label for="shipping_first_name" class="">First name <abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" readonly class="input-text " name="shipping_first_name" id="shipping_first_name" 
									placeholder="First name" value="<?= $checkout->get_value( $key ) ?>"
									required
									>
									<small id="helpfirst_name" data-msgvi="Vui lòng nhập tên của bạn" data-msg="Please provide a first name" class="text-danger"></small>
                                </span>
                            </p>
						<?php elseif ($key == 'shipping_last_name'):?>
                            <p class="form-row form-row-last validate-required disabled" id="shipping_last_name_field" data-priority="20">
                                <label for="shipping_last_name" class="">Last name <abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" readonly class="input-text " name="shipping_last_name" id="shipping_last_name" placeholder="Last name" value="<?= $checkout->get_value( $key ) ?>"
									required
									>
									<small id="helplast_name" data-msgvi="Vui lòng nhập họ của bạn" data-msg="Please provide a last name" class="text-danger"></small>
                                </span>
                            </p>
							<p class="form-row form-row-first address-field disabled" id="shipping_company_field" data-priority="50">
                                <label for="shipping_company" class="">Company</label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" readonly class="input-text " name="shipping_company" id="shipping_company" placeholder="Company" value="<?= $checkout->get_value( $key ) ?>">
                                </span>
                            </p>
                        <?php elseif ($key == 'shipping_mobile'):?>
                            <div class="form-row validate-required form-row-last disabled" id="shipping_mobile_field" data-priority="">
                                <label for="shipping_mobile" class="">
                                    Mobile&nbsp;<abbr class="required" title="required">*</abbr>
                                </label>
                                <span class="woocommerce-input-wrapper">
                                    <div class="row">
                                        <div class="col-sm-2" style="display:none">
                                            <?php $country_code = $checkout->get_value( 'shipping_country_code' ) ?>
                                            <select name="shipping_country_code" id="shipping_country_code" style="width: 100%;">
                                                <?php foreach ($country_codes as $c_code): ?>
                                                   <option value="<?=$c_code?>" <?= $country_code == $c_code ? 'selected="selected"' : '' ?> ><?=$c_code?></option>
                                                <?php endforeach?>
                                            </select>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" readonly class="input-text " name="shipping_mobile" id="shipping_mobile" placeholder="" value="<?= $checkout->get_value( $key ) ?>" style="width: 100%;"
											data-regix="^(086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$"                                            
											required
											>
											<small id="helpmobile" data-msgvi="Vui lòng nhập đúng số điện thoại của bạn (e.g. 0987654321)" data-msg="Please enter a valid phone number (e.g. 0987654321)" class="text-danger"></small>
                                        </div>
                                    </div>
                                </span>
                                <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both;"><small>Example: 0912345678</small></p>
                            </div>

						<?php elseif ($key == 'shipping_address_1'): 
						 $customer_address_1 = $checkout->get_value( $key );
						?>
                            <p class="form-row form-row-first disabled" id="shipping_address_1_field" data-priority="50">
                                <label for="shipping_address_1" class="">Address line 1 <span class="required">*</span></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" readonly class="input-text " name="shipping_address_1" id="shipping_address_1" placeholder="Address line 1" value="<?= $checkout->get_value( $key ) ?>"
									required>
									<small id="helpshipping_address_1" data-msgvi="Vui lòng nhập địa chỉ của bạn" data-msg="Please provide a address" class="text-danger"></small>
                                </span>
                            </p>
                        <?php elseif ($key == 'shipping_address_2'):?>
                            <p class="form-row form-row-last disabled" id="shipping_address_2_field" data-priority="60">
                                <label for="shipping_address_2" class="">Address line 2</label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" readonly class="input-text " name="shipping_address_2" id="shipping_address_2" placeholder="Address line 2" value="<?= $checkout->get_value( $key ) ?>">
                                </span>
                            </p>
                        <?php elseif ($key == 'shipping_city'):
                            $customer_city = $user->shipping_city;
                            $valid_city = false;
                            if (!in_array($customer_city, $shipping_cities))
                                $customer_city = $shipping_cities[0];
                            ?>
                            <p class="form-row form-row-last  address-field validate-required validate-required disabled" id="shipping_city_field">
                                <label for="shipping_city" class="">District <abbr class="required" title="required">*</abbr></label>
                                <select name="shipping_city" readonly id="shipping_city" class="city_select " autocomplete="address-level2" placeholder="District">
                                    <?php foreach ( $shipping_cities as  $ctyk=>$shipping_city ) : ?>
                                    <option value="<?= $ctyk ?>" <?= $customer_city == $shipping_city ? 'selected="selected"': '' ?>><?= $shipping_city ?></option>
                                <?php endforeach; ?>
                                </select>
                            </p>
							<script>
							jQuery(function(){
								var $t = setInterval( function(){
									jQuery('#shipping_city').trigger('change');
									if(jQuery('#select2-shipping_city-container').text()!='')
									{
										clearInterval($t);
									}
								},1000)								
							})
							</script>
                        <?php
                        elseif ($key == 'shipping_state'):
                            $customer_province = $user->shipping_state;?>
                            <p class="form-row form-row-first  address-field validate-required validate-required disabled" id="shipping_state_field">
                                <label for="shipping_state" class="">Province <abbr class="required" title="required">*</abbr></label>
                                <select name="shipping_state" readonly id="shipping_state" class="state_select " autocomplete="address-level2" placeholder="Province">
                                <?php foreach ( $states as $code => $name ) : ?>
                                    <option value="<?= $code ?>" <?= $customer_province == $code ? 'selected="selected"': '' ?>><?= $name ?></option>
                                <?php endforeach; ?>
                                </select>
                            </p>
                        <?php
                        else:
                            woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
							if ($key == 'shipping_address_1') {
                               $customer_address_1 = $checkout->$checkout->get_value( $key );
                            }
                        endif;

                    }
                ?>

            </div>

            <?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

        </div>

    <?php endif; ?>
</div>
<div class="woocommerce-additional-fields">
    <?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

    <?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

        <?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

            <h3><?php _e( 'Additional information', 'woocommerce' ); ?></h3>

        <?php endif; ?>

        <div class="woocommerce-additional-fields__field-wrapper">
        <p class="form-row notes"
            id="order_comments_field"
            data-priority=""
        >
            <label for="order_comments" class="">
                Additional information for delivery
            </label>
            <textarea name="order_comments"
                class="input-text "
                id="order_comments"
                placeholder="Notes about your order, e.g. special notes for delivery."
                rows="2"
                cols="5"
            ></textarea>
        </p>
            <?php //foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
                <?php //woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
            <?php //endforeach; ?>
        </div>

    <?php endif; ?>
	
    <?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>
<script type="text/javascript">
    window.customer_address_1 = '<?= $customer_address_1 ?>';
    window.customer_province = '<?= $customer_province ?>';
    window.customer_city = '<?= $customer_city ?>';
</script>
