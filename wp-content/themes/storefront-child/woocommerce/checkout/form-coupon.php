<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! wc_coupons_enabled() ) {
	return;
}

if ( empty( WC()->cart->applied_coupons ) ) {
	$info_message = apply_filters( 'woocommerce_checkout_coupon_message', __( 'Have a coupon?', 'woocommerce' ) . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>' );
	wc_print_notice( $info_message, 'notice' );
}
?>
<style>
#checkout_coupon .woocommerce-info
{
	display:none !important;
}
#checkout_coupon .checkout_coupon
{
	display:block !important;
}
.coupon-body {
    background: #F9F9F9;
    padding: 20px;
}
h3.coupon-title {
    background: #D5D5D5;
    font-size: 18px;
    padding: 12px;
    margin-bottom: 0;
}
.checkout_coupon{
	margin-bottom:0 !important;
}
.woocommerce-error{
	margin:0;
}
</style>
<form class="checkout_coupon" method="post" style="display:block">
	<h3 class="coupon-title">Coupon code</h3>
	<div class="coupon-body">
	<p class="form-row form-row-first" style="text-align:right">
		<input type="text" name="coupon_code" class="input-text" style="width:80%" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
	</p>

	<p class="form-row form-row-last" style="text-align:center">
		<input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>" />
	</p>

	<div class="clear"></div>
	</div>
</form>
