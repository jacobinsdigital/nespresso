<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$user = nespresso_get_user_data();
$shipping_cities = nespresso_get_cities($user->shipping_state);
$states = nespresso_get_states();
$country_codes = get_country_codes();
?>
<div class="woocommerce-shipping-fields">
    <?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

        <!-- <h3 id="ship-to-different-address"> -->
        <h3 id="ship-to-different-address">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox hide" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" />
                <!-- <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox hide" checked="checked" type="checkbox" name="ship_to_different_address" value="1" />  -->
                <span><?php _e( 'Shipping details', 'woocommerce' ); ?></span>
            </label>
        </h3>

        <div class="shipping_address">

            <?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

            <div class="woocommerce-shipping-fields__field-wrapper">
                <?php
                    $fields = $checkout->get_checkout_fields( 'shipping' );

                    foreach ( $fields as $key => $field ) {

                        if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
                            $field['country'] = $checkout->get_value( $field['country_field'] );
                        }
                        if ($key == 'shipping_mobile'):?>
                            <p class="form-row validate-required" id="shipping_mobile_field" data-priority="">
                                <label for="shipping_mobile" class="">
                                    Mobile&nbsp;<abbr class="required" title="required">*</abbr>
                                </label>
                                <span class="woocommerce-input-wrapper">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <?php $country_code = $checkout->get_value( 'shipping_country_code' ) ?>
                                            <select name="shipping_country_code" id="shipping_country_code" style="width: 100%;">
                                                <?php foreach ($country_codes as $c_code): ?>
                                                   <option value="<?=$c_code?>" <?= $country_code == $c_code ? 'selected="selected"' : '' ?> ><?=$c_code?></option>
                                                <?php endforeach?>
                                            </select>
                                        </div>
                                        <div class="col-sm-10">
                                            <input type="text" class="input-text " name="shipping_mobile" id="shipping_mobile" placeholder="" value="<?= $checkout->get_value( $key ) ?>" style="width: 100%;">

                                        </div>
                                    </div>
                                </span>
                                <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both;"><small>Example: 0912345678</small></p>
                            </p>
                            <p class="form-row validate-required" id="shipping_mobile_field" data-priority="">
                                <label>
                                    Delivery Option&nbsp;<abbr class="required" title="required">*</abbr>
                                </label>
                                <span class="woocommerce-input-wrapper">
                                    <ul>
                                        <li style="list-style-type: none;"> <label><input type="radio" name="delivery_option" data-index="0" value="customer_address" checked="checked"> Customer address</label></li>
                                        <li style="list-style-type: none;"> <label><input type="radio" name="delivery_option" data-index="0" value="pick_up"> Pick up store</label></li>
                                    </ul>
                                </span>
                            </p>

                        <?php elseif ($key == 'shipping_address_2'):?>
                            <p class="form-row" id="shipping_address_2_field" data-priority="60">
                                <label for="shipping_address_2" class="">Address line 2</label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" class="input-text " name="shipping_address_2" id="shipping_address_2" placeholder="Address line 2" value="<?= $checkout->get_value( $key ) ?>">
                                </span>
                            </p>
                        <?php elseif ($key == 'shipping_city'):
                            $customer_city = $user->shipping_city;
                            $valid_city = false;
                            if (!in_array($customer_city, $shipping_cities))
                                $customer_city = $shipping_cities[0];
                            ?>
                            <p class="form-row form-row-wide address-field validate-required validate-required" id="shipping_city_field">
                                <label for="shipping_city" class="">District <abbr class="required" title="required">*</abbr></label>
                                <select name="shipping_city" id="shipping_city" class="city_select " autocomplete="address-level2" placeholder="District">
                                    <?php foreach ( $shipping_cities as  $shipping_city ) : ?>
                                    <option value="<?= $shipping_city ?>" <?= $customer_city == $shipping_city ? 'selected="selected"': '' ?>><?= $shipping_city ?></option>
                                <?php endforeach; ?>
                                </select>
                            </p>
                        <?php
                        elseif ($key == 'shipping_state'):
                            $customer_province = $user->shipping_state;?>
                            <p class="form-row form-row-wide address-field validate-required validate-required" id="shipping_state_field">
                                <label for="shipping_state" class="">Province <abbr class="required" title="required">*</abbr></label>
                                <select name="shipping_state" id="shipping_state" class="state_select " autocomplete="address-level2" placeholder="Province">
                                <?php foreach ( $states as $code => $name ) : ?>
                                    <option value="<?= $code ?>" <?= $customer_province == $code ? 'selected="selected"': '' ?>><?= $name ?></option>
                                <?php endforeach; ?>
                                </select>
                            </p>
                        <?php
                        else:
                            woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                            if ($key == "shipping_last_name") {
                                   $data["shipping_company"] = [
                                    "label" => "Company",
                                    "placeholder" => "Company",
                                    "required" => false,
                                    "class" => [
                                      0 => "form-row-wide",
                                      1 => "address-field",
                                    ],
                                    "priority" => 50,
                                  ];
                                woocommerce_form_field( 'shipping_company', $data["shipping_company"], $checkout->get_value( 'shipping_company' ) );
                            } elseif ($key == 'shipping_address_1') {
                                $customer_address_1 = $checkout->get_value( $key );
                            }
                        endif;

                    }
                ?>

            </div>

            <?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

        </div>

    <?php endif; ?>
</div>
<div class="woocommerce-additional-fields">
    <?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

    <?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

        <?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

            <h3><?php _e( 'Additional information', 'woocommerce' ); ?></h3>

        <?php endif; ?>

        <div class="woocommerce-additional-fields__field-wrapper">
        <p class="form-row notes"
            id="order_comments_field"
            data-priority=""
        >
            <label for="order_comments" class="">
                Additional information for delivery
            </label>
            <textarea name="order_comments"
                class="input-text "
                id="order_comments"
                placeholder="Notes about your order, e.g. special notes for delivery."
                rows="2"
                cols="5"
            ></textarea>
        </p>
            <?php //foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
                <?php //woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
            <?php //endforeach; ?>
        </div>

    <?php endif; ?>

    <?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>
<script type="text/javascript">
    window.customer_address_1 = '<?= $customer_address_1 ?>';
    window.customer_province = '<?= $customer_province ?>';
    window.customer_city = '<?= $customer_city ?>';
</script>
