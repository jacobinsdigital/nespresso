<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<div class="woocommerce-order woocommerce-order-recieved" style="margin-left: auto !important; max-width: 995px;
  margin-right: auto !important; padding-bottom: 40px; padding-top: 40px; overflow: hidden; top: 0;">

	<?php if ($order): ?>
		<!-- for credit card -->
		<?php if ($order->get_payment_method() == 'onepayus' || $order->get_payment_method() == 'onepayvn'): ?>
			<div class="checkister step5 temporary-failed" style="display: none;">
				<div id="JumpContent" class="main-content failed-transaction clearfix p-t-65" tabindex="-1">
					<div class="checkister-wrapper">
						<h2>Something went wrong with your payment</h2>
						<p></p>
						<p>Please choose another payment method or try again.</p>
						<p>Having trouble ordering? </p>
						<p>Call our coffee specialists :  1900 633 474 from monday to saturday 10am - 6pm.</p>
						<p>
							<a class="btn btn-green btn-icon font-color-white" title="Proceed to payment" href="<?php echo esc_url($order->get_checkout_payment_url()); ?>"><?php _e('Pay', 'woocommerce')?></a>
							<?php if (is_user_logged_in()): ?>
								<a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="btn btn-green btn-icon font-color-white font-color-white"><?php _e('My account', 'woocommerce');?></a>
							<?php endif;?>
						</p>

					</div>
				</div>
			</div>

			<div class="temporary-success" style="display: none;">
				<div class="checkister step5">
					<div id="JumpContent" class="main-content clearfix p-t-65" tabindex="-1">
						<div class="checkister-wrapper">
							<h2>Thank you for your order</h2>
							<p></p>
							<p>You order has been succesfully sent. Thank you for shopping with Nespresso.</p>
							<p><a class="link" title="Track your order" href="/my-account/">Track your order status in the my account section</a></p>
							<p><a class="btn btn-green btn-icon font-color-white" title="Proceed to payment" href="/coffee-list"><i class="icon icon-arrow_right"></i> Back to shop</a></p>
						</div>
					</div>
				</div>

				<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details" style="color: black">

					<li class="woocommerce-order-overview__order order">
						<?php _e('Order number:', 'woocommerce');?>
						<strong><?php echo $order->get_order_number(); ?></strong>
					</li>

					<li class="woocommerce-order-overview__date date">
						<?php _e('Date:', 'woocommerce');?>
						<strong><?php echo wc_format_datetime($order->get_date_created()); ?></strong>
					</li>

					<li class="woocommerce-order-overview__total total">
						<?php _e('Total:', 'woocommerce');?>
						<strong><?php echo $order->get_formatted_order_total(); ?></strong>
					</li>

					<?php if ($order->get_payment_method_title()): ?>

					<li class="woocommerce-order-overview__payment-method method">
						<?php _e('Payment method:', 'woocommerce');?>
						<strong><?php echo wp_kses_post($order->get_payment_method_title()); ?></strong>
					</li>

					<?php endif;?>

				</ul>
			</div>
		<?php elseif ($order->has_status('failed')):?>
			<div class="checkister step5">
				<div id="JumpContent" class="main-content failed-transaction clearfix p-t-65" tabindex="-1">
					<div class="checkister-wrapper">
						<h2>Something went wrong with your payment</h2>
						<p></p>
						<p>Please choose another payment method or try again.</p>
						<p>Having trouble ordering? </p>
						<p>Call our coffee specialists :  1900 633 474 from monday to saturday 10am - 6pm.</p>
						<p>
							<a class="btn btn-green btn-icon font-color-white" title="Proceed to payment" href="<?php echo esc_url($order->get_checkout_payment_url()); ?>"><?php _e('Pay', 'woocommerce')?></a>
							<?php if (is_user_logged_in()): ?>
								<a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="btn btn-green btn-icon font-color-white font-color-white"><?php _e('My account', 'woocommerce');?></a>
							<?php endif;?>
						</p>

					</div>
				</div>
			</div>
		<?php else: ?>
			<div class="checkister step5">
				<div id="JumpContent" class="main-content clearfix p-t-65" tabindex="-1">
					<div class="checkister-wrapper">
						<h2>Thank you for your order</h2>
						<p></p>
						<p>You order has been succesfully sent. Thank you for shopping with Nespresso.</p>
						<p><a class="link" title="Track your order" href="/my-account/">Track your order status in the my account section</a></p>
						<p><a class="btn btn-green btn-icon font-color-white" title="Proceed to payment" href="/coffee-list"><i class="icon icon-arrow_right"></i> Back to shop</a></p>
					</div>
				</div>
			</div>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details" style="color: black">

				<li class="woocommerce-order-overview__order order">
					<?php _e('Order number:', 'woocommerce');?>
					<strong><?php echo $order->get_order_number(); ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php _e('Date:', 'woocommerce');?>
					<strong><?php echo wc_format_datetime($order->get_date_created()); ?></strong>
				</li>

				<li class="woocommerce-order-overview__total total">
					<?php _e('Total:', 'woocommerce');?>
					<strong><?php echo $order->get_formatted_order_total(); ?></strong>
				</li>

				<?php if ($order->get_payment_method_title()): ?>

				<li class="woocommerce-order-overview__payment-method method">
					<?php _e('Payment method:', 'woocommerce');?>
					<strong><?php echo wp_kses_post($order->get_payment_method_title()); ?></strong>
				</li>

				<?php endif;?>

			</ul>

		<?php endif;?>

		<?php do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id());?>
		<?php do_action('woocommerce_thankyou', $order->get_id());?>

	<?php else: ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Thank you. Your order has been received.', 'woocommerce'), null); ?></p>

	<?php endif;?>

</div>
<?php
	$order_new = wc_get_order($order->get_id());
?>
<script type="text/javascript">
	var onepay = ['onepayvn', 'onepayus'];
	var order_status = "<?php echo $order_new->get_status() ?>";
	var order_method = "<?php echo $order_new->get_payment_method()?>";
	var onepay_index = onepay.indexOf(order_method);
	if (onepay_index != '-1' && (order_status == 'pending' || order_status == 'failed')) {
		jQuery('.temporary-failed').css('display', 'block');
	} else {
		jQuery('.temporary-success').css('display', 'block');
	}


</script>
