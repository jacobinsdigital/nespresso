<?php

abstract class ProductList
{

    /**
     * @var array
     *
     * collection
     *   - category
     *     - product
     *       - value : name
     *       - value : name
     *       - ...
     */
    protected static $data = [];


    /**
     * Get all data
     * @return array
     */
    public static function data()
    {
        return static::$data;
    }


    /**
     * Get collection
     * @param $collection
     * @return array
     */
    public static function collection($collection)
    {
        return isset(static::$data[$collection])
            ? static::$data[$collection]
            : [];
    }


    /**
     * Count collection items
     * @param $collection
     * @return int
     */
    public static function count($collection)
    {
        return isset(static::$data[$collection])
            ? array_sum(array_map('count', static::$data[$collection]))
            : 0;
    }


    /**
     * Get random product
     * @return array
     */
    public static function random()
    {
        $collection = array_rand(static::$data);
        $category = array_rand(static::$data[$collection]);
        $slug = array_rand(static::$data[$collection][$category]);
        $product = static::$data[$collection][$category][$slug];

        return [$slug, $product];
    }


    /**
     * Find product by slug
     * @param $search
     * @return array
     */
    public static function find($search)
    {
        foreach(static::$data as $collection => $categories) {
            foreach ($categories as $category) {
                foreach ($category as $slug => $product) {
                    if ($slug == $search) {
                        return [$slug, $product];
                    }
                }
            }
        }
    }
}