<?php

require_once 'ProductList.php';

abstract class MachineList extends ProductList
{

    /**
     * @var array
     *
     * collection
     *   - category
     *     - product
     *       - value : name
     *       - value : name
     *       - ...
     */
    protected static $data = [
        'original' => [
            'machine' => [
                'lattissima-black' => [
                    'name' => 'Lattissima+ Intense Black',
                    'desc' => 'With its advanced technology, this generation of Lattissima machines delivers asimpler and more intuitive operation thanks to its one touch system',
                    'image' => 'images/machines/MACHINES_B2C_DELONGHI_LATTISSIMA_LATTISSIMA__ICESILVER_010520121200.png',
                    'slideshow' => ['images/machines/machine-slideshow-lattissima.jpg'],
                    'color' => ['white'],
                    'price' => '399.00',
                    'technology' => 'Original',
                    'type' => 'machines',
                    'text' => '
                    <p>With its advanced technology, this generation of Lattissima machines delivers asimpler and more intuitive operation thanks to its one touch system. Exceptional cappuccinos and lattes are prepared with thick milk froth with the simple touch of a button.</p>
                    <p>Dimensions with lever closed: (W x D x H) 6.5 x 12.6 x 10 in.
                    <br>Dimensions with lever open: (W x D x H) 6.5 x 12.6 x 13.5 in.</p>
                    <p>Machine parts can be ordered via the Nespresso Club at 800.562.1465.</p>
                    <!--p><a href="#" class="link link--right">Discover the range</a></p-->',
                    'specification' => '
                    <p>
                    - Weight: 9.9 Pounds<br>
                    - Height: 10 Inches<br>
                    - Removable water tank: 0.9 Litre<br>
                    - Removable fresh milk container: 0.35 Litre<br>
                    - Used capsule container capacity: 12<br>
                    - Dimensions (WxDxH): 6.5 in x 12.6 in x 10 in<br>
                    - Warranty: 2 Year<br>
                    - Cable storage<br>
                    - Automatic OFF mode after 9 mins<br>
                    - Fast heat-up<br>
                    </p>
                    <p><a href="#" title="Download User Guide" class="link link--right">Download User Guide</a></p>
                    '
                ],
                'pixie' => [
                    'name' => 'PIXIE ELECTRIC',
                    'desc' => 'Pixie is one of our fastest single cup coffee makers with only a 25 second heat up time.  This machine has a range of innovative, advanced features in a surprisingly small machine.',
                    'image' => 'images/machines/MACHINES_B2C_MAGIMIX_PIXIE_ELECTRICLIME_010520121537.png',
                    'slideshow' => ['images/machines/M-0134_main_684x378.jpg', 'images/machines/M-0134_main_684x378.jpg', 'images/machines/M-0134_main_684x378.jpg'],
                    'color' => ['yellow', 'red', 'blue', 'green'],
                    'price' => '229.00',
                    'technology' => 'Original',
                    'type' => 'machines',
					'text' => '
					<p>The new restylable Pixie Clips machine on which you can easily change the side panels according to your tastes or the latest design trends. Express your personality by giving it a new look with a wide range of clip-on side panels, playing with colours and textures. Your style in a clip!
Pixie Clips, Style it as you want!</p>
					'
                ],
                'latissima-pro' => [
                    'name' => 'LATISSIMA PRO',
                    'desc' => 'The new Lattissima Pro is inspired by the quality of professional machines with a high level of simplicity.',
                    'image' => 'images/machines/MACHINES_B2C_DELONGHI_LATTISSIMA_LATTISSIMAPRO_F456_111820131221.png',
                    'slideshow' => ['images/machines/M-0245_main_684x378.jpg', 'images/machines/M-0245_main_684x378.jpg', 'images/machines/M-0245_main_684x378.jpg'],
                    'color' => ['grey'],
                    'price' => '599.00',
                    'technology' => 'Original',
                    'type' => 'machines'
                ],
                'kitchenaid' => [
                    'name' => 'KITCHENAID FROSTED PEARL',
                    'desc' => 'Nespresso by KitchenAid offers an exceptional combination of an iconic silhouette, high quality materials and an array of functions to help you easily prepare your favorite Grands Crus',
                    'image' => 'images/machines/MACHINES_B2C_ARTISAN_KITCHENAID_KITCHENAID_FROSTEDPEARL_110320141643.png',
                    'slideshow' => ['images/machines/KitchenAid_FrostedPearl_main_684x378.jpg', 'images/machines/KitchenAid_CandyAppleRed_main_684x378.jpg', 'images/machines/KitchenAid_OnyxBlack_main_684x378.jpg'],
                    'color' => ['white','red','black'],
                    'price' => '399.00',
                    'technology' => 'Original',
                    'type' => 'machines'
                ]
            ]
        ],
        'vertuo' => [
            'machine' => [
                'galileo-chrome' => [
                    'name' => 'CHROME VERTUO',
                    'desc' => 'Freshly brewed Coffee and authentic Espresso with crema at the touch of a button, thanks to the revolutionary Centrifusion™ and code reading technologies.',
                    'image' => 'images/machines/Galileo_Chrome_3Quarts_1.png',
                    'slideshow' => ['images/machines/M-0238_VertuoLine-Nespresso-Chrome_main_684x378.jpg', 'images/machines/M-0238_VertuoLine-Nespresso-Chrome_main_684x378.jpg', 'images/machines/M-0238_VertuoLine-Nespresso-Chrome_main_684x378.jpg'],
                    'color' => ['black', 'white'],
                    'price' => '199.00',
                    'technology' => 'Vertuo',
                    'type' => 'machines'
                ],
                'galileo-black' => [
                    'name' => 'BLACK BUNDLE VERTUO',
                    'desc' => 'The Aeroccino, included with the new VertuoLine machine, is ideal for preparing cold/hot foam or hot milk.',
                    'image' => 'images/machines/Galileo_Noir_3Quarts_1.png',
                    'slideshow' => ['images/machines/M-0242_main_684x378.jpg', 'images/machines/M-0242_main_684x378.jpg', 'images/machines/M-0242_main_684x378.jpg'],
                    'color' => ['black'],
                    'price' => '249.00',
                    'technology' => 'Vertuo',
                    'type' => 'machines'
                ]
            ]
        ]
    ];

}