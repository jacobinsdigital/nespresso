<?php

require_once 'ProductList.php';

abstract class AccessoryList extends ProductList
{

    /**
     * @var array
     *
     * collection
     *   - category
     *     - product
     *       - value : name
     *       - value : name
     *       - ...
     */
    protected static $data = [
        'original' => [
            'touch collection' => [
                'touch-espresso' => [
                    'name' => 'Touch Lungo cups',
                    'image' => 'images/touch-espresso_main.png',
                    'price' => '16.00',
                    'type' => 'accessories',
                    'video' => 'sz66vEImaT4'
                ],
                'touch-lungo' => [
                    'name' => 'Touch Lungo - Set of 2 lungo cups',
                    'image' => 'images/touch-lungo_main.png',
                    'price' => '16.00',
                    'type' => 'accessories',
                    'video' => 'sz66vEImaT4'
                ],
                'touch-espresso-lungo' => [
                    'name' => 'Touch Espresso Lungo kit - Set of 2 espresso cups & 2 lungo cups',
                    'image' => 'images/touch-lungo-espresso_main.png',
                    'price' => '32.00',
                    'type' => 'accessories',
                    'video' => 'sz66vEImaT4'
                ]
            ],
            'pure collection' => [
                'pure-espresso' => [
                    'name' => '2 pure espresso cups & 2 saucers',
                    'image' => 'images/pure-espresso.png',
                    'price' => '21.00',
                    'type' => 'accessories',
                    'video' => 'Sw0WtnTi6mo'
                ],
                'pure-lungo' => [
                    'name' => '2 pure lungo cups & 2 saucers',
                    'image' => 'images/pure-lungo.png',
                    'price' => '21.00',
                    'type' => 'accessories',
                    'video' => 'Sw0WtnTi6mo'
                ],
                'pure-espresso-lungo' => [
                    'name' => 'Pure Espresso Lungo kit - Set of 2 espresso cups & 2 lungo cups',
                    'image' => 'images/pure-espresso-lungo.png',
                    'price' => '21.00',
                    'type' => 'accessories',
                    'video' => 'Sw0WtnTi6mo'
                ]
            ],
            'pixie collection' => [
                'pixie-espresso-arpeggio' => [
                    'name' => 'Pixie espresso, Arpeggio',
                    'image' => 'images/pixie-arpeggio.png',
                    'price' => '21.00',
                    'type' => 'accessories',
                    'video' => 'RXy2BMzWNi4'
                ],
                'pixie-espresso-linizio' => [
                    'name' => 'Pixie espresso, Linizio',
                    'image' => 'images/pixie-linizio.png',
                    'price' => '21.00',
                    'type' => 'accessories',
                    'video' => 'RXy2BMzWNi4'
                ],
                'pixie-espresso-decaffeinato' => [
                    'name' => 'Pixie espresso, Decaffeinato',
                    'image' => 'images/product-06.png',
                    'price' => '21.00',
                    'type' => 'accessories',
                    'video' => 'RXy2BMzWNi4'
                ]
            ]
        ]
    ];

}