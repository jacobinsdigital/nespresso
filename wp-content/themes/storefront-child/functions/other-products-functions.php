<?php
/**
 *
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with other products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * get all the other products, not trached
 * sorted by post id
 *
 * @return array
 */
function get_other_products() {

    global $wpdb;

    $posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='product_type' AND meta_value='%s' ", 'Others') );

    $product_count = 0;

    $products = [];

    if ( !empty($posts) && $posts ) :

        foreach ( $posts as $post ) :

            $product = wc_get_product($post->post_id);
            $product = $product->get_data();
            if ( !$product ||
                $product['status'] != 'publish' ||
                $product['stock_status'] != 'instock'
            ) {
                continue;
            }

            $product = include_other_product_meta_from_results($post);

            if ( !$product )
                continue;

            $products[] = $product;

            $product_count++;

        endforeach;

        $products ? ksort($products) : '';
    endif;
    return $products;
} // get_other_products()

/**
 * include the other product meta(custom) data from the result given
 *
 * @param  object|null $product - product result
 * @return object
 */
function include_other_product_meta_from_results( $product=null, $single_product_page=false ) {

    if ( isset($product->post_id) )
        $product_id = $product->post_id;
    elseif ( isset($product->ID) )
        $product_id = $product->ID;
    elseif ( isset($product->id) )
        $product_id = $product->id;
    else
        $product_id = 0;

    if ( get_post_status($product_id) == 'trash' )
        return null;

    return (object) array_merge(
        (array) $product,
        (array) get_other_product_meta_data( $product_id, $single_product_page )
    );

} // include_other_product_meta_from_results()

/**
 * get the other product meta(custom) data
 *
 * @param  integer $product_id
 * @param  boolean $single_product_page
 * @return object|null
 */
function get_other_product_meta_data( $product_id=0, $single_product_page=false ) {

    $product_id = $product_id ? $product_id : 0;

    $product = (object)null;

    $product->product_id = $product_id;

    $product->name = get_the_title( $product_id );

    $product->url = get_permalink( $product_id );

    $product->price = get_post_meta( $product_id, '_price', true );

    $product->description = get_field('description', $product_id );

    $product->product_type = get_field( 'product_type', $product_id );

    $product->gwp_tier = get_post_meta( $product_id, 'gwp_tier', true );

    $product->image = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );


    return $product;

} // get_other_product_meta_data()


/**
 * chect product stock
 *
 */
add_action('wp_ajax_check_product_stock', 'check_product_stock');
add_action('wp_ajax_nopriv_check_product_stock', 'check_product_stock');
function check_product_stock() {
    $product_data  = wc_get_product( $_POST['product_id']);
    $product_data = $product_data->get_data();
    if ( $product_data['stock_status'] != 'instock' ) :
        $message =  'Product is out of stock';
        $status = false;
    else :
        $message = 'Product stock has stock';
        $status = true;
    endif;
    wp_send_json(['status' => $status, 'message' => $message]);
}

function get_gwp_gift_by_tier($amount, $gwp_tiers) {

    global $wpdb;

    $tier = 0;

    foreach ($gwp_tiers as $promo_tier) {
        if ($amount >= $promo_tier) {
            $tier = $promo_tier;
            break;
        }
    }

    $posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='gwp_tier' AND meta_value!='%s' ", 'other' ));

    $product_count = 0;

    $products = [];

    $product_ids = [];

    if ( !empty($posts) && $posts ) :

        foreach ( $posts as $post ) :

            $product_ids[]  = $post->post_id;
            $product        = wc_get_product($post->post_id);

            if ( !$product )
                continue;


            $product = $product->get_data();

            if ( !$product ||
                $product['status'] != 'publish'
            ) {
                continue;
            }

            $product = include_other_product_meta_from_results($post);

            if  ($product->gwp_tier == $tier) {
                return $product;
                break;
            }

            $product_count++;

        endforeach;

        // $products ? ksort($products) : [];

    endif;

    return $products;


}

function get_gwp_gift_id() {

    global $wpdb;

    $posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='gwp_tier' AND meta_value!='%s' ", 'other' ));

    $product_ids = [];

    if ( !empty($posts) && $posts ) :

        foreach ( $posts as $post ) :

            $product_ids[]  = $post->post_id;

        endforeach;

    endif;

    return $product_ids;
}

function get_gwp_other_gift_id() {

    global $wpdb;

    $posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='gwp_tier' AND meta_value='%s' ", 'other' ));

    $product_ids = [];

    if ( !empty($posts) && $posts ) :

        foreach ( $posts as $post ) :

            $product_ids[]  = $post->post_id;

        endforeach;

    endif;

    return $product_ids;
}

/**
 * get gwp gift by subtotal
 *
 */
add_action('wp_ajax_fetch_gwp_gifts', 'fetch_gwp_gifts');
add_action('wp_ajax_nopriv_fetch_gwp_gifts', 'fetch_gwp_gifts');
function fetch_gwp_gifts() {
    global $woocommerce;
    $other_settings     = get_nespresso_other_settings();
    $product_with_gwp   = @$other_settings['product_with_gwp'];
    $gwp_tiers          = @$other_settings['subtotal_tier'];


    if (!$product_with_gwp )
        return false;

    if (!$gwp_tiers )
        return false;

    $product_with_gwp   = preg_replace('/\s+/', '', $product_with_gwp);
    $product_with_gwp   = explode(',',$product_with_gwp);


    $gwp_tiers   = preg_replace('/\s+/', '', $gwp_tiers);
    $gwp_tiers   = explode(',',$gwp_tiers);
    rsort($gwp_tiers);

    $amount = 0;
    $gwp_gift_ids = get_gwp_gift_id();
    $has_previous = false;
    $qualified = false;
    foreach( WC()->cart->get_cart() as $key => $cart_item ){

        if($product_with_gwp && in_array($cart_item['data']->get_sku(), $product_with_gwp)) {
            // $amount = $amount + $cart_item['data']->get_price();
            $qualified = true;
        }

        if (in_array($cart_item['product_id'], $gwp_gift_ids)) {
            $has_previous = true;
            $cart_item_key = wc_clean( $key );
            $item_deleted = wc()->cart->remove_cart_item( $cart_item_key );
            break;
        }

    }

    $amount = WC()->cart->subtotal;
    $gwp_gift = $qualified ? get_gwp_gift_by_tier($amount, $gwp_tiers) : false;

    if ($gwp_gift)
        $status = true;
    else
        $status = false;
    wp_send_json(compact('gwp_gift', 'status', 'has_previous', 'amount'));
}

/**
 * get gwp gift by product
 *
 */
add_action('wp_ajax_fetch_gwp_gift_by_product', 'fetch_gwp_gift_by_product');
add_action('wp_ajax_nopriv_fetch_gwp_gift_by_product', 'fetch_gwp_gift_by_product');

function fetch_gwp_gift_by_product() {
    global $woocommerce;
    $coys_promo = get_nespresso_coys_promo();
    $qualified_gift = [];
    $result = [];
    $has_previous = false;
    if (isset($coys_promo['main_product'])) {
        $combine_array = [];
        $main_product = json_decode($coys_promo['main_product']);
        $conditional_products = json_decode($coys_promo['conditional_product']);
        $conditional_products_quantity = json_decode($coys_promo['conditional_product_quantity']);
        $gift = json_decode($coys_promo['gift']);
        if ($main_product) {
            foreach ($main_product as $mk => $value) {
                $main_product_conditional_product = preg_replace('/\s+/', '', $conditional_products[$mk]); //remove spaces
                $main_product_conditional_product = explode(',',$main_product_conditional_product);
                $combine_array = array_merge($combine_array, $main_product_conditional_product);
            }
            $combine_array = array_merge($combine_array, $main_product);
            //get current cart data
            $cart_current_item = [];
            foreach( WC()->cart->get_cart() as $key => $cart_item ){
                $product_sku = $cart_item['data']->get_sku();
                //remove gift first
                if (in_array($product_sku, $gift)) {
                    $has_previous = true;
                    $cart_item_key = wc_clean( $key );
                    $item_deleted = wc()->cart->remove_cart_item( $cart_item_key );
                } else if (in_array($product_sku, $combine_array)) {
                    $cart_current_item[$product_sku]['cart_key'] = wc_clean( $key );
                    $cart_current_item[$product_sku]['product_id'] = $cart_item['product_id'];
                    $cart_current_item[$product_sku]['quantity'] = $cart_item['quantity'];
                    $cart_current_item[$product_sku]['sku'] = $product_sku;
                }

            }
            //coys gift base in current cart
            $qualified_gift = getQualifiedGift($cart_current_item, $main_product, $conditional_products, $conditional_products_quantity, $gift, $qualified_gift);
            $status = $qualified_gift ? true : false;
        }
    }
    wp_send_json(compact('qualified_gift', 'has_previous','status'));
}

function getQualifiedGift($cart_current_item, $main_product, $conditional_products, $conditional_products_quantity, $gift, $qualified_gift ) {
    foreach ($main_product as $mk => $mv) {
        if (isset($cart_current_item[$mv])) {


            $main_product_conditional_product = preg_replace('/\s+/', '', $conditional_products[$mk]); //remove spaces
            $main_product_conditional_product = explode(',',$main_product_conditional_product);

            $qualified_count    = $cart_current_item[$mv]['quantity'];
            $main_product_gift  = $gift[$mk];
            $id                 = wc_get_product_id_by_sku($main_product_gift);

            if ($id == 0)
                continue;

            $product = wc_get_product($id);
            if ($product->get_status() == 'draft')
                continue;

            if ($product->get_stock_quantity() < 1)
                continue;


            if (!isset($qualified_gift[$main_product_gift])) {
                $qualified_gift[$main_product_gift]['quantity'] = 0;
                $qualified_gift[$main_product_gift]['id']       = $id;
                $qualified_gift[$main_product_gift]['price']    = 0;
                $qualified_gift[$main_product_gift]['type']     = 'GWP Gift';
                $qualified_gift[$main_product_gift]['name']     = $product->get_title();
            }

            foreach ($main_product_conditional_product as $cv) {
                if (isset($cart_current_item[$cv])) {
                    if (isset($conditional_products_quantity[$mk]) && ($conditional_products_quantity[$mk] != 0 || $conditional_products_quantity[$mk] != '')) {
                        if ($cart_current_item[$cv]['quantity'] == $conditional_products_quantity[$mk]) {
                            $qualified_gift[$main_product_gift]['quantity']++;
                        } else if ($cart_current_item[$cv]['quantity'] > $conditional_products_quantity[$mk]) {
                            $need_main_product = floor($cart_current_item[$cv]['quantity'] / $conditional_products_quantity[$mk]);
                            if ($need_main_product >= $qualified_count) {
                                $qualified_gift[$main_product_gift]['quantity'] = $qualified_gift[$main_product_gift]['quantity']+ $qualified_count;
                            }
                        }
                        break;
                    } else {
                        if ($qualified_count > 0) {
                            if ($qualified_count >=  $cart_current_item[$cv]['quantity']) {
                                $qualified_gift[$main_product_gift]['quantity'] = $qualified_gift[$main_product_gift]['quantity']+$cart_current_item[$cv]['quantity'];
                                $qualified_count = $qualified_count - $cart_current_item[$cv]['quantity'];
                                unset($cart_current_item[$cv]);
                            } else {
                                $qualified_gift[$main_product_gift]['quantity'] = $qualified_gift[$main_product_gift]['quantity']+ $qualified_count;
                                $cart_current_item[$cv]['quantity'] = $cart_current_item[$cv]['quantity'] - $qualified_count;
                                break;
                            }
                        }
                    }
                }

            }

            if ($qualified_gift[$main_product_gift]['quantity'] < 1){
                unset($qualified_gift[$main_product_gift]);
            }

            unset($cart_current_item[$mv]);
        }
    }
    return $qualified_gift;
}

function get_coffee_category_with_product($meta_key, $meta_value)
{

    global $wpdb;
    $posts = $wpdb->get_results( $wpdb->prepare( "SELECT DISTINCT post_id FROM $wpdb->postmeta WHERE meta_key='%s' AND meta_value='%s' ", $meta_key, $meta_value) );

    if ($posts) {
        foreach ($posts as $key => $value) {
            $product = wc_get_product($value->post_id);
            if ($product->status == 'publish') {
                return true;
            }
        }
    }
    return false;
}


