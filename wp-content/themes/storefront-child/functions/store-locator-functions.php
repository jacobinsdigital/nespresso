<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with machine products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

add_action('wp_ajax_nopriv_fetch_stores', 'fetch_stores');
add_action('wp_ajax_fetch_stores', 'fetch_stores');

function fetch_stores() {
    $args = array(
        'post_type' => 'wpsl_stores',
    );
    $posts = query_posts($args);
    $stores = array();
    foreach ($posts as $post) :
        $stores[] = array(
            'id' => $post->ID,
            'store_title' => $post->post_title
        );
    endforeach;
    wp_send_json($stores);
}
