<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with customized function that can be used throughout the application
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * get the acf woocommerce field data
 * usually used in filters
 *
 * @param  string $field_name
 * @return array
 */
function get_acf_field($field_name='') {

    if ( !$field_name )
        return [];

    global $wpdb;

    $sql = $wpdb->prepare( "select post_id from " . $wpdb->prefix . "postmeta where meta_key = %s limit 0,1 ", $field_name);
    $post = $wpdb->get_results( $sql );

    return get_field_object( $field_name, $post[0]->post_id );
}

/**
 * get the product data without knowing the product type
 *
 * @param  integer $product_id
 * @return array|null
 */
function get_product_metadata($product_id=0) {

    $product_type = get_field( 'product_type', $product_id );

    if ( !$product_type )
        return null;

    $product = null;

    switch ($product_type) {
        case 'Accessory':
            $product = get_accessory_product_meta_data( $product_id );
            break;
        case 'Coffee':
            $product = get_coffee_product_meta_data( $product_id );
            break;
        case 'Machine':
            $product = get_machines_product_meta_data( $product_id );
            break;
    }

    return $product;
} // get_product_metadata()

/**
 * get the product reviews
 *
 * @param  integer $product_id = post_id
 * @return object|string
 */
function get_product_reviews($product_id=0) {

	if ( !$product_id )
		return '';

	$comments = get_comments(['post_id' => $product_id]);

	$newComments = [];

	foreach ( $comments as $k => $comment ) :

		$comment_meta = get_comment_meta($comment->comment_ID);

		if ( $comment_meta ) :
			$newComments[$k] = (object) array_merge( (array)$comment, $comment_meta);
		else :
			$newComments[$k] = comment;
		endif;

	endforeach;

	return (object)$newComments;

} // get_product_reviews()

function add_javascript_urls() {
   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
           var login_url = "'.get_permalink( get_option('woocommerce_myaccount_page_id') ).'";
         </script>';
}
add_action('wp_head', 'add_javascript_urls');

/**
 * check if the url_string passed contains in the url
 * not the best way, but still sorta effetive
 *
 * @param  string  $url_string
 * @return boolean
 */
function nespresso_is_current_url($url_string) {

    $urlserver = $_SERVER['REQUEST_URI'];
    $arrayUrl = preg_split('/\//',$urlserver);

    if ( empty($arrayUrl) || !$arrayUrl )
        return false;

    $return = false;
    foreach ( $arrayUrl as $url ) {
        if ( $url == $url_string ) {
            $return = true;
            break;
        }
    }
    return $return;
} // nespresso_is_current_url()

/**
 * generate simple hashing with md5 and wp native password hasher
 * @param  string $string
 * @return string
 */
function nespresso_hash_generate($string='') {
    return md5(wp_hash_password($string));
} // nespresso_hash_generate()

/**
 * get the nespresso country default
 *
 * @return array ['country_code' => 'country_name']
 */
function nespresso_get_country($country_code=0) {

    $countries_obj   = new WC_Countries();

    $countries  = $countries_obj->__get('countries');

    $default_country = $countries_obj->get_base_country();

    if ( isset( $countries[$country_code] ))
        return [ $country_code => $countries[$country_code] ];
    elseif ( $country_code != 0 )
        return null;

    return isset( $countries[$default_country] ) ? [ $default_country => $countries[$default_country] ] : null;

} // nespresso_get_country()
function nespresso_get_countries() {
$countries_obj   = new WC_Countries();

$countries  = $countries_obj->__get('countries');

return $countries;
}
/**
 * get the state codes or the state name by state code
 *
 * @param  string $state_code
 * @return array
 */
function nespresso_get_states($state_code='VN') {

    $countries_obj   = new WC_Countries();


    if ($state_code != '') {
        return $countries_obj->get_states( $state_code );
    }else{
        $default_country = $countries_obj->get_base_country();
        return $countries_obj->get_states( $default_country );
    }

} // nespresso_get_states()

/**
 * get the state codes or the state name by state code
 *
 * @param  string $state_code
 * @return array
 */
function nespresso_get_cities($state_code = null) {
    $cities_obj   = new WC_City_Select();
    $state_cities = $cities_obj->get_cities('VN');
    if ($state_code) {
        return $state_cities[$state_code];
    } else {
        $all_cities = [];
        foreach ($state_cities as $state => $cities) {
            $all_cities = array_merge($all_cities,$cities);
            $all_cities[] = $state;
        }
        return $all_cities;
    }

} // nespresso_get_cities()

add_action('wp_ajax_nopriv_dropdown_change', 'dropdown_change');
add_action('wp_ajax_dropdown_change', 'dropdown_change');

function dropdown_change(){
    $c = nespresso_get_states($_GET['country']);
    if (count($c) != 0) {
        # code...
    wp_send_json($c);
    }else{
        return 0;
    }
}

function nespresso_get_state($state_code,$country) {
    $countries_obj   = new WC_Countries();
    $states = $countries_obj->get_states( $country );

    if ( isset( $states[$state_code] ) )
        return $states[$state_code];

    return null;
} // nespresso_get_state()

add_action('wp_ajax_get_province_district', 'get_province_district');
add_action('wp_ajax_nopriv_get_province_district', 'get_province_district');
function get_province_district() {
    if ( !isset($_POST['province_code']) && $_POST['province_code'] != '' ) {
        wp_send_json([ 'status' => 'failed', 'data' => 0, 'message' => 'Invalid provice code']);
        exit;
    }

    $state_code   = $_POST['province_code'];
    $cities_obj   = new WC_City_Select();
    $state_cities = $cities_obj->get_cities('VN');
    $districts = [];
    if ($state_code) {
        $districts =  $state_cities[$state_code];
    } else {
        // $all_cities = [];
        // foreach ($state_cities as $state => $cities) {
        //     $all_cities = array_merge($all_cities,$cities);
        //     $all_cities[] = $state;
        // }
        // $districts = $all_cities;
    }
    wp_send_json([ 'status' => 'success', 'districts' => $districts,]);
}

function get_nespresso_order_statuses() {
    $order_statuses = wc_get_order_statuses();
    $new_order_statuses = [];
    foreach ($order_statuses as $key => $value) {
        $new_order_statuses[] = str_replace('wc-', '', $key);
    }
    return $new_order_statuses;
}

function check_email_exist() {
    $email = $_POST['email'];
    wp_send_json(email_exists($email));
} // check_email_exist()

add_action('wp_ajax_nopriv_check_email_exist', 'check_email_exist');
add_action('wp_ajax_check_email_exist', 'check_email_exist');


function dhl_get_states() {
    return [
    "AN-GIANG" => "An Giang",
    "BA-RIA-VUNG-TAU" => "Bà Rịa - Vũng Tàu",
    "BAC-LIEU" => "Bạc Liêu",
    "BAC-KAN" => "Bắc Kạn",
    "BAC-GIANG" => "Bắc Giang",
    "BAC-NINH" => "Bắc Ninh",
    "BEN-TRE" => "Bến Tre",
    "BINH-DUONG" => "Bình Dương",
    "BINH-DINH" => "Bình Định",
    "BINH-PHUOC" => "Bình Phước",
    "BINH-THUAN" => "Bình Thuận",
    "CA-MAU" => "Cà Mau",
    "CAO-BANG" => "Cao Bằng",
    "CAN-THO" => "Cần Thơ",
    "DA-NANG" => "Đà Nẵng",
    "DAK-LAK" => "Đắk Lắk",
    "DAK-NONG" => "Đắk Nông",
    "DONG-NAI" => "Đồng Nai",
    "DONG-THAP" => "Đồng Tháp",
    "DIEN-BIEN" => "Điện Biên",
    "GIA-LAI" => "Gia Lai",
    "HA-GIANG" => "Hà Giang",
    "HA-NAM" => "Hà Nam",
    "HA-NOI" => "Hà Nội",
    "HA-TINH" => "Hà Tĩnh",
    "HAI-DUONG" => "Hải Dương",
    "HAI-PHONG" => "Hải Phòng",
    "HOA-BINH" => "Hòa Bình",
    "HAU-GIANG" => "Hậu Giang",
    "HUNG-YEN" => "Hưng Yên",
    "HO-CHI-MINH" => "Hồ Chí Minh",
    "KHANH-HOA" => "Khánh Hòa",
    "KIEN-GIANG" => "Kiên Giang",
    "KON-TUM" => "Kon Tum",
    "LAI-CHAU" => "Lai Châu",
    "LAO-CAI" => "Lào Cai",
    "LANG-SON" => "Lạng Sơn",
    "LAM-DONG" => "Lâm Đồng",
    "LONG-AN" => "Long An",
    "NAM-DINH" => "Nam Định",
    "NGHE-AN" => "Nghệ An",
    "NINH-BINH" => "Ninh Bình",
    "NINH-THUAN" => "Ninh Thuận",
    "PHU-THO" => "Phú Thọ",
    "PHU-YEN" => "Phú Yên",
    "QUANG-BINH" => "Quảng Bình",
    "QUANG-NAM" => "Quảng Nam",
    "QUANG-NGAI" => "Quảng Ngãi",
    "QUANG-NINH" => "Quảng Ninh",
    "QUANG-TRI" => "Quảng Trị",
    "SOC-TRANG" => "Sóc Trăng",
    "SON-LA" => "Sơn La",
    "TAY-NINH" => "Tây Ninh",
    "THAI-BINH" => "Thái Bình",
    "THAI-NGUYEN" => "Thái Nguyên",
    "THANH-HOA" => "Thanh Hóa",
    "THUA-THIEN-HUE" => "Thừa Thiên Huế",
    "TIEN-GIANG" => "Tiền Giang",
    "TRA-VINH" => "Trà Vinh",
    "TUYEN-QUANG" => "Tuyên Quang",
    "VINH-LONG" => "Vĩnh Long",
    "VINH-PHUC" => "Vĩnh Phúc",
    "YEN-BAI" => "Yên Bái",
    ];
} // dhl_get_states()

function get_country_codes()
{
    return ['+1','+7','+20','+27','+30','+31','+32','+33','+34','+36','+39','+40','+41','+43','+44','+45','+46','+47','+48','+49','+51','+52','+53','+54','+55','+56','+57','+58','+60','+61','+62','+63','+64','+65','+66','+81','+82','+84','+86','+90','+91','+92','+93','+94','+95','+98','+211','+212','+213','+216','+218','+220','+221','+222','+223','+224','+225','+226','+227','+228','+229','+230','+231','+232','+233','+234','+235','+236','+237','+238','+239','+240','+241','+242','+243','+244','+245','+246','+248','+249','+250','+251','+252','+253','+254','+255','+256','+257','+258','+260','+261','+262','+263','+264','+265','+266','+267','+268','+269','+290','+291','+297','+298','+299','+350','+351','+352','+353','+354','+355','+356','+357','+358','+359','+370','+371','+372','+373','+374','+375','+376','+377','+378','+379','+380','+381','+382','+383','+385','+386','+387','+389','+420','+421','+423','+500','+501','+502','+503','+504','+505','+506','+507','+508','+509','+590','+591','+592','+593','+595','+597','+598','+599','+670','+672','+673','+674','+675','+676','+677','+678','+679','+680','+681','+682','+683','+685','+686','+687','+688','+689','+690','+691','+692','+850','+852','+853','+855','+856','+880','+886','+960','+961','+962','+963','+964','+965','+966','+967','+968','+970','+971','+972','+973','+974','+975','+976','+977','+992','+993','+994','+995','+996','+998'];
}

function get_hq_data($sku)
{   $data_layer = [
        'EN119474'  => [
            'id'            => '3694-EU-BK',
            'product_range' => 'aeroccino milk frother|create your iced coffee experience',
            'technology'    =>  'Orignal|||Pro',
            'name'          =>  'Aerocino3 Milk Frother Black'
        ],
        'B121875'   => [
            'id'            => '7431.4',
            'product_range' => 'standing-orders|intensely roasted|cocoa|ristretto|espresso|milk|intense|intenso',
            'technology'    => 'Original',
            'name'          => 'Arpeggio'
        ],
        'B121880'   => [
            'id'            => '7644.4',
            'product_range' => 'standing-orders|intensely roasted|cocoa|ristretto|espresso|milk|intense|decaffeinato',
            'technology'    => 'Original',
            'name'          => 'Arpeggio Decaffeinato'
        ],
        'B121874'   => [
            'id'            => '7413.4',
            'product_range' => 'espresso|balanced|cereal|milk|standing-orders|espresso',
            'technology'    => 'Original',
            'name'          => 'Capriccio'
        ],
        'B130387'   => [
            'id'            => '7628.4',
            'product_range' => 'standing-orders|espresso|milk|caramel|variation',
            'technology'    => 'Original',
            'name'          => 'Caramelito'
        ],
        'B130385'   => [
            'id'            => '7626.4',
            'product_range' => 'standing-orders|chocolate|espresso|milk|variation',
            'technology'    => 'Original',
            'name'          => 'Ciocattino'
        ],
        'B130392'   => [
            'id'            => '7713',
            'product_range' => 'espresso|lungo|master origin|fruity-winey|standing-orders|milk',
            'technology'    => 'Original',
            'name'          => 'Colombia'
        ],
        'B130383'   => [
            'id'            => '7642.4',
            'product_range' => 'espresso|fruity|cereal|fruity|milk|standing-orders|espresso',
            'technology'    => 'Original',
            'name'          => 'Cosi'
        ],
        'PMB122277' => [
            'id'            => 'DKB2C5',
            'product_range' => 'standing-orders|best sellers|other accessories|standing-orders',
            'technology'    =>  'Orignal|||Pro',
            'name'          =>  'Descling Kit (for all machines)'
        ],
        'B130361'   => [
            'id'            => '7695.4',
            'product_range' => 'standing-orders|ristretto|espresso|milk|long roasted|hints of bitter cocoa powder and cereals|intenso',
            'technology'    => 'Original',
            'name'          => 'Dharkan'
        ],
        'B130384'   => [
            'id'            => '7654.4',
            'product_range' => 'lungo|intense|caramelized|woody|milk|standing-orders|lungo',
            'technology'    => 'Original',
            'name'          => 'Envivo Lungo'
        ],
        'EN119473'  => [
            'id'            => 'C30-EU2-BK-NE',
            'product_range' => '19 bar pressure|heat|off|cup|lungo|espresso|black|essenza mini',
            'technology'    => 'Original',
            'name'          => 'Essenza mini Black Color'
        ],
        'EN119471'  => [
            'id'            => 'C30-EU2-WH-NE',
            'product_range' => '19 bar pressure|heat|off|cup|lungo|espresso|white|essenza mini',
            'technology'    => 'Original',
            'name'          => 'Essenza Mini Pure White'
        ],
        'EN119472'  => [
            'id'            => 'D30-EU2-RE-NE',
            'product_range' => '19 bar pressure|heat|off|cup|lungo|espresso|red|essenza mini',
            'technology'    => 'Original',
            'name'          => 'Essenza Mini Ruby Red'
        ],
        'B130391'   => [
            'id'            => '7715',
            'product_range' => 'espresso|lungo|master origin|fruity|flowery|fruity|standing-orders|milk',
            'technology'    => 'Original',
            'name'          => 'Ethiopia'
        ],
        'B121883'   => [
            'id'            => '7834.4',
            'product_range' => 'lungo|intense|malty toasted|sweet cereal|milk|standing-orders|lungo',
            'technology'    => 'Original',
            'name'          => 'Fortissio Lungo'
        ],
        'B130389'   => [
            'id'            => '7711',
            'product_range' => 'spicy|espresso|lungo|intense|master origin|intense|standing-orders|milk',
            'technology'    => 'Original',
            'name'          => 'India'
        ],
        'B130388'   => [
            'id'            => '7712',
            'product_range' => 'cereal|woody|espresso|lungo|intense|master origin|standing-orders|milk',
            'technology'    => 'Original',
            'name'          => 'Indonesia'
        ],
        'EN119470'  => [
            'id'            => 'D40-EU2-BK-NE',
            'product_range' => 'black|heat|cup|off|compact|lungo|espresso|inissia',
            'technology'    => 'Original',
            'name'          => 'Inissia Black'
        ],
        'EN119469'  => [
            'id'            => 'C40-EU2-RE-NE',
            'product_range' => 'red|heat|cup|off|compact|lungo|espresso|inissia',
            'technology'    => 'Original',
            'name'          => 'Inissia Ruby Red'
        ],
        'B130360'   => [
            'id'            => '7694.4',
            'product_range' => 'standing-orders|spicy|woody|ristretto|espresso|milk|intense|intenso',
            'technology'    => 'Original',
            'name'          => 'Kazaar'
        ],
        'B121878'   => [
            'id'            => '7443.4',
            'product_range' => 'standing-orders|espresso|milk|balanced|caramel|espresso',
            'technology'    => 'Original',
            'name'          => 'Livanto'
        ],
        'B130390'   => [
            'id'            => '7714',
            'product_range' => 'sweet|espresso|lungo|master origin|fruity-winey|standing-orders|milk',
            'technology'    => 'Original',
            'name'          => 'Nicaragua'
        ],
        'EN119467'  => [
            'id'            => 'C60-EU-RE-NE',
            'product_range' => 'red|cup|heat|off|fast|ergonomic|eco|lungo|espresso|pixie',
            'technology'    => 'Original',
            'name'          => 'Pixie Electric Red'
        ],
        'EN119468'  => [
            'id'            => 'C60-EU-TI-NE',
            'product_range' => 'grey|heat|cup|off|fast|ergonomic|eco|lungo|espresso|pixie',
            'technology'    => 'Original',
            'name'          => 'Pixie Electric Titan'
        ],
        'B121879'   => [
            'id'            => '7615.4',
            'product_range' => 'standing-orders|intensely roasted|ristretto|espresso|milk|intense|hint of fruity|intenso',
            'technology'    => 'Original',
            'name'          => 'Ristretto'
        ],
        'B130363'   => [
            'id'            => '7702.4',
            'product_range' => 'standing-orders|ristretto|espresso|milk|decaffeinato',
            'technology'    => 'Original',
            'name'          => 'Ristretto Decaffeinato'
        ],
        'B121877'   => [
            'id'            => '7439.4',
            'product_range' => 'ristretto|espresso|intense|cereal|woody|milk|standing-orders|intenso',
            'technology'    => 'Original',
            'name'          => 'Roma'
        ],
        'B130386'   => [
            'id'            => '7627.4',
            'product_range' => 'standing-orders|vanilla|espresso|milk|variation',
            'technology'    => 'Original',
            'name'          => 'Vanilio'
        ],
        'B121882'   => [
            'id'            => '7810.4',
            'product_range' => 'fruity|lungo|floral|roasted|milk|standing-orders|lungo',
            'technology'    => 'Original',
            'name'          => 'Vivalto Lungo'
        ],
        'B121881'   => [
            'id'            => '7646.4',
            'product_range' => 'lungo|fruity|floral|roasted|milk|standing-orders|decaffeinato',
            'technology'    => 'Original',
            'name'          => 'Vivalto Lungo Decaffeinato'
        ],
        'B121876'   => [
            'id'            => '7435.4',
            'product_range' => 'espresso|balanced|fruity|cereal|fruity|milk|standing-orders|espresso',
            'technology'    => 'Original',
            'name'          => 'Volluto'
        ],
        'B130362'   => [
            'id'            => '7645.4',
            'product_range' => 'espresso|fruity|balanced|cereal|fruity|milk|standing-orders|decaffeinato',
            'technology'    => 'Original',
            'name'          => 'Volluto Decaffeinato'
        ],
        'PMB122277' => [
            'id'            => 'DKB2C5',
            'product_range' => 'Classic',
            'technology'    => 'Original',
            'name'          => 'Descaling Kit'
        ]
    ];
    return isset($data_layer[$sku]) ? $data_layer[$sku] : '';
}
