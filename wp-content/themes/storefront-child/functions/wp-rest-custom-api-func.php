<?php

class CustomApiDefaultController extends CustomApiBaseController
{
    public $method;
    public $response;
    public $user;
    public $request;

    public function __construct($method)
    {
        $this->method = $method;
        $this->response = array(
            'code' => 0,
            'message' => 'Default',
            'data' => ''
        );
    }

    private $status_codes = array(
        'success' => true,
        'failure' => 0,
        'missing_param' => 150,
    );

    public function init(WP_REST_Request $request)
    {
        try {
            if (!method_exists($this, $this->method)) {
                throw new Exception('No method exists', 500);
            }

            $consumer_key = $request->get_param('oauth_consumer_key');
            $this->user = $this->get_user_data_by_consumer_key( $consumer_key );
            $params = $this->get_oauth_parameters();
            $signature = $this->check_oauth_signature( $this->user, $params );

            if ( is_wp_error( $signature ) ) {
                return $this->response = array(
                    'code' => $signature->get_error_code(),
                    'message' => $signature->get_error_message(),
                    'data' => $signature->get_error_data(),
                );
            }

            $data = $this->{$this->method}($request);

            $this->response = $data;


        } catch (Exception $e) {
            $this->response['code'] = 'woocommerce_rest_authentication_error';
            $this->response['message'] = $e->getMessage();
            $this->response['data'] = ['status' => 500];
        }

        return $this->response;
    }

    public function update_order_status($request)
    {
        $data_param = $request->get_json_params();

        if (!isset($data_param['status']) || !is_array($data_param['status']))
            return ['ResponseStatus' => 'failed', 'FailedReason' => 'Missing status data.'];

        $total_update_order = count($data_param['status']);
        $updated_order = 0;
        $message = '';
        $order_statuses = get_nespresso_order_statuses();
        foreach ($data_param['status'] as $key =>  $order_data) {
            $item_index = $key+1;
            if (!isset($order_data['OrderId'])) {
                $message .= "Missing OrderId in Item $item_index ";
                continue;
            }

            if (!isset($order_data['UpdatedStatus'])) {
                $message .= "Missing UpdatedStatus in Item $item_index ";
                continue;
            }

            if (!in_array($order_data['UpdatedStatus'], $order_statuses)) {
                $message .= "Invalid Status in Item $item_index ";
                continue;
            }

            $order = wc_get_order(absint($order_data['OrderId']));

            if (!$order) {
                $message .= $order_data['OrderId'] . " does not exist. ";
                continue;
            }

            if(($order->get_status() != $order_data['UpdatedStatus']))
                $order->update_status($order_data['UpdatedStatus'], 'Triggered via ax api. ');

            $updated_order++;
        }

        $status = $message == '' ? 'success' : 'failed';

        return ['ResponseStatus' => $status, 'ResponseTotalUpdated' => "$updated_order out of $total_update_order", "ResponseMessage" => $message];

    }

    public function product_stockupdate($request)
    {
        $data_param = $request->get_json_params();

        if (!isset($data_param['ActionNumber']))
            return ['ActionNumber' => null, 'ResponseStatus' => 'failed', 'FailedReason' => 'Missing ActionNumber'];

        if (!isset($data_param['Quantity']) || !is_array($data_param['Quantity']))
            return ['ActionNumber' => $data_param['ActionNumber'], 'ResponseStatus' => 'failed', 'FailedReason' => 'Missing quantity data'];

        $total_update_product = count($data_param['Quantity']);
        $updated_product = 0;
        $message = '';
        foreach ($data_param['Quantity'] as $key =>  $product_data) {
            $item_index = $key+1;
            if (!isset($product_data['ItemId'])) {
                $message .= "Missing ItemId in Item $item_index ";
                continue;
            }

            if (!isset($product_data['Qty'])) {
                $message .= "Missing Qty in Item $item_index ";
                continue;
            }

            $product_id = wc_get_product_id_by_sku($product_data['ItemId']);
            $product = wc_get_product($product_id);

            if (!$product) {
                $message .= $product_data['ItemId'] . " does not exist. ";
                continue;
            }

            $product_category = get_post_meta($product_id, 'product_type', true);
            if ($product_category == "Coffee") {
                $product_data['Qty'] = $product_data['Qty']*10;
            }

            $current_stock = $product->get_stock_quantity();
            $product->set_stock_quantity($current_stock+$product_data['Qty']);
            $data = $this->get_formatted_product_data($product);
            $product->save();
            $updated_product++;
        }

        $status = $message == '' ? 'success' : 'failed';

        return ['ActionNumber' => $data_param['ActionNumber'], 'ResponseStatus' => $status, 'ResponseTotalUpdated' => "$updated_product out of $total_update_product", 'ResponseMessage' => $message];
    }

    public function get_order_by_modified_date($request)
    {
        $this->request = $request;
        $args = [
            'post_type' => 'shop_order',
            'post_status' => 'any',
            'nopaging' => true,
        ];

        $date_query = [];
        if ($request->get_param('after') ) {

            $after = [
                [
                    'column' => 'post_modified_gmt',
                    'after'  => $request->get_param('after')
                ]
            ];

            array_push($date_query , $after);
        }

        if ($request->get_param('before') ) {
            $before = [
                [
                    'column' => 'post_modified_gmt',
                    'before'  => $request->get_param('before')
                ]
            ];

            array_push($date_query , $before);
        }

        if ($date_query) {
            $args['date_query'] = $date_query;
        }

        $order = new WP_Query( $args );
        $order_post = $order->get_posts();

        if (!$order_post) {
            return [];

        } else {
            return $this->get_orders_object($order_post);
        }

    }

    private function get_orders_object($object)
    {
        $orders = [];
        foreach ($object as $order) {
            $order_object = wc_get_order($order->ID);
            $orders[] = $this->get_formatted_order_data($order_object);
        }
        return $orders;
    }

    public function get_formatted_order_data( $object ) {
        $data              = $object->get_data();
        $format_decimal    = array( 'discount_total', 'discount_tax', 'shipping_total', 'shipping_tax', 'shipping_total', 'shipping_tax', 'cart_tax', 'total', 'total_tax' );
        $format_date       = array( 'date_created', 'date_modified', 'date_completed', 'date_paid' );
        $format_line_items = array( 'line_items', 'tax_lines', 'shipping_lines', 'fee_lines', 'coupon_lines' );

        // Format decimal values.
        foreach ( $format_decimal as $key ) {
            $data[ $key ] = wc_format_decimal( $data[ $key ], $this->request['dp'] );
        }

        // Format date values.
        foreach ( $format_date as $key ) {
            $datetime              = $data[ $key ];
            $data[ $key ]          = wc_rest_prepare_date_response( $datetime, false );
            $data[ $key . '_gmt' ] = wc_rest_prepare_date_response( $datetime );
        }

        // Format the order status.
        $data['status'] = 'wc-' === substr( $data['status'], 0, 3 ) ? substr( $data['status'], 3 ) : $data['status'];

        // Format line items.
        foreach ( $format_line_items as $key ) {
            $data[ $key ] = array_values( array_map( array( $this, 'get_order_item_data' ), $data[ $key ] ) );
        }

        // Refunds.
        $data['refunds'] = array();
        foreach ( $object->get_refunds() as $refund ) {
            $data['refunds'][] = array(
                'id'     => $refund->get_id(),
                'reason' => $refund->get_reason() ? $refund->get_reason() : '',
                'total'  => '-' . wc_format_decimal( $refund->get_amount(), $this->request['dp'] ),
            );
        }

        $delivery_region = ( $value = get_post_meta($object->get_id(), 'delivery_region', true) ) ? $value : $data['shipping']['state'];
        $delivery_city = ( $value = get_post_meta($object->get_id(), 'delivery_city', true) ) ? $value : $data['shipping']['city'];
        $delivery_address = ( $value = get_post_meta($object->get_id(), 'delivery_address', true) ) ? $value : $data['shipping']['address_1'] . ' ' . $data['shipping']['address_2'];
        $delivery_option = ( $value = get_post_meta($object->get_id(), 'delivery_option', true) ) ? $value : '';
        $delivery_contact = ( $value = get_post_meta($object->get_id(), 'delivery_contact', true) ) ? $value : get_user_meta($data['customer_id'],'shipping_country_code', true) . ' ' .get_user_meta($data['customer_id'],'shipping_mobile', true);

        $data['billing']['billing_country_code'] = get_user_meta($data['customer_id'],'billing_country_code', true);
        $data['billing']['billing_mobile'] = get_user_meta($data['customer_id'],'billing_mobile', true);
        $data['billing']['country'] = $data['shipping']['country'];

        $data['shipping']['shipping_country_code'] = $object->get_meta('_shipping_country_code');
        $data['shipping']['shipping_mobile'] = $object->get_meta('_shipping_mobile');

        return array(
            'id'                   => $object->get_id(),
            'parent_id'            => $data['parent_id'],
            'number'               => $data['number'],
            'order_key'            => $data['order_key'],
            'created_via'          => $data['created_via'],
            'version'              => $data['version'],
            'status'               => $data['status'],
            'currency'             => $data['currency'],
            'date_created'         => $data['date_created'],
            'date_created_gmt'     => $data['date_created_gmt'],
            'date_modified'        => $data['date_modified'],
            'date_modified_gmt'    => $data['date_modified_gmt'],
            'discount_total'       => $data['discount_total'],
            'discount_tax'         => $data['discount_tax'],
            'shipping_total'       => $data['shipping_total'],
            'shipping_tax'         => $data['shipping_tax'],
            'cart_tax'             => $data['cart_tax'],
            'total'                => $data['total'],
            'total_tax'            => $data['total_tax'],
            'prices_include_tax'   => $data['prices_include_tax'],
            'customer_id'          => $data['customer_id'],
            'customer_ip_address'  => $data['customer_ip_address'],
            'customer_user_agent'  => $data['customer_user_agent'],
            'customer_note'        => $data['customer_note'],
            'billing'              => $data['billing'],
            'shipping'             => $data['shipping'],
            'payment_method'       => $data['payment_method'],
            'payment_method_title' => $data['payment_method_title'],
            'transaction_id'       => $data['transaction_id'],
            'date_paid'            => $data['date_paid'],
            'date_paid_gmt'        => $data['date_paid_gmt'],
            'date_completed'       => $data['date_completed'],
            'date_completed_gmt'   => $data['date_completed_gmt'],
            'cart_hash'            => $data['cart_hash'],
            'meta_data'            => $data['meta_data'],
            'line_items'           => $data['line_items'],
            'tax_lines'            => $data['tax_lines'],
            'shipping_lines'       => $data['shipping_lines'],
            'fee_lines'            => $data['fee_lines'],
            'coupon_lines'         => $data['coupon_lines'],
            'refunds'              => $data['refunds'],
            'delivery_address'     => $delivery_address,
            'delivery_city'        => $delivery_city,
            'delivery_region'      => $delivery_region,
            'delivery_option'      => $delivery_option,
            'delivery_contact'     => $delivery_contact,
        );
    }


    public function location_based_notify($request)
    {
        $data = array();
        return $data;
    }

    private function get_user_data_by_consumer_key( $consumer_key ) {
        global $wpdb;

        $consumer_key = wc_api_hash( sanitize_text_field( $consumer_key ) );
        $user         = $wpdb->get_row(
            $wpdb->prepare(
                "
            SELECT key_id, user_id, permissions, consumer_key, consumer_secret, nonces
            FROM {$wpdb->prefix}woocommerce_api_keys
            WHERE consumer_key = %s
        ",
                $consumer_key
            )
        );

        return $user;
    }

    private function check_oauth_signature( $user, $params ) {
        $http_method  = isset( $_SERVER['REQUEST_METHOD'] ) ? strtoupper( $_SERVER['REQUEST_METHOD'] ) : ''; // WPCS: sanitization ok.
        $request_path = isset( $_SERVER['REQUEST_URI'] ) ? wp_parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH ) : ''; // WPCS: sanitization ok.
        $wp_base      = get_home_url( null, '/', 'relative' );

        if ( substr( $request_path, 0, strlen( $wp_base ) ) === $wp_base ) {
            $request_path = substr( $request_path, strlen( $wp_base ) );
        }

        $base_request_uri = rawurlencode( get_home_url( null, $request_path, is_ssl() ? 'https' : 'http' ) );
        // Get the signature provided by the consumer and remove it from the parameters prior to checking the signature.
        $consumer_signature = rawurldecode( str_replace( ' ', '+', $params['oauth_signature'] ) );
        unset( $params['oauth_signature'] );

        // Sort parameters.
        if ( ! uksort( $params, 'strcmp' ) ) {
            return new WP_Error( 'woocommerce_rest_authentication_error', __( 'Invalid signature - failed to sort parameters.', 'woocommerce' ), array( 'status' => 401 ) );
        }

        // Normalize parameter key/values.
        $params         = $this->normalize_parameters( $params );
        $query_string   = implode( '%26', $this->join_with_equals_sign( $params ) ); // Join with ampersand.

        $string_to_sign = $http_method . '&' . $base_request_uri . '&' . $query_string;
        if ( 'HMAC-SHA1' !== $params['oauth_signature_method'] && 'HMAC-SHA256' !== $params['oauth_signature_method'] ) {
            return new WP_Error( 'woocommerce_rest_authentication_error', __( 'Invalid signature - signature method is invalid.', 'woocommerce' ), array( 'status' => 401 ) );
        }

        $hash_algorithm = strtolower( str_replace( 'HMAC-', '', $params['oauth_signature_method'] ) );
        $secret         = $user->consumer_secret . '&';
        $signature      = base64_encode( hash_hmac( $hash_algorithm, $string_to_sign, $secret, true ) );

        if ( ! hash_equals( $signature, $consumer_signature ) ) {
            return new WP_Error( 'woocommerce_rest_authentication_error', __( 'Invalid signature - provided signature does not match.', 'woocommerce' ), array( 'status' => 401 ) );
        }
        return true;
    }

    private function normalize_parameters( $parameters ) {
        $keys       = wc_rest_urlencode_rfc3986( array_keys( $parameters ) );
        $values     = wc_rest_urlencode_rfc3986( array_values( $parameters ) );
        $parameters = array_combine( $keys, $values );

        return $parameters;
    }

    private function join_with_equals_sign( $params, $query_params = array(), $key = '' ) {
        foreach ( $params as $param_key => $param_value ) {
            if ( $key ) {
                $param_key = $key . '%5B' . $param_key . '%5D'; // Handle multi-dimensional array.
            }

            if ( is_array( $param_value ) ) {
                $query_params = $this->join_with_equals_sign( $param_value, $query_params, $param_key );
            } else {
                $string         = $param_key . '=' . $param_value; // Join with equals sign.
                $query_params[] = wc_rest_urlencode_rfc3986( $string );
            }
        }

        return $query_params;
    }

    public function get_oauth_parameters() {
        $params = array_merge( $_GET, $_POST ); // WPCS: CSRF ok.
        $params = wp_unslash( $params );
        $header = $this->get_authorization_header();

        if ( ! empty( $header ) ) {
            // Trim leading spaces.
            $header        = trim( $header );
            $header_params = $this->parse_header( $header );

            if ( ! empty( $header_params ) ) {
                $params = array_merge( $params, $header_params );
            }
        }

        $param_names = array(
            'oauth_consumer_key',
            'oauth_timestamp',
            'oauth_nonce',
            'oauth_signature',
            'oauth_signature_method',
        );

        $errors   = array();
        $have_one = false;

        // Check for required OAuth parameters.
        foreach ( $param_names as $param_name ) {
            if ( empty( $params[ $param_name ] ) ) {
                $errors[] = $param_name;
            } else {
                $have_one = true;
            }
        }

        // All keys are missing, so we're probably not even trying to use OAuth.
        if ( ! $have_one ) {
            return array();
        }

        // If we have at least one supplied piece of data, and we have an error,
        // then it's a failed authentication.
        if ( ! empty( $errors ) ) {
            $message = sprintf(
                /* translators: %s: amount of errors */
                _n( 'Missing OAuth parameter %s', 'Missing OAuth parameters %s', count( $errors ), 'woocommerce' ),
                implode( ', ', $errors )
            );

            $this->set_error( new WP_Error( 'woocommerce_rest_authentication_missing_parameter', $message, array( 'status' => 401 ) ) );

            return array();
        }

        return $params;
    }

    public function get_authorization_header() {
        if ( ! empty( $_SERVER['HTTP_AUTHORIZATION'] ) ) {
            return wp_unslash( $_SERVER['HTTP_AUTHORIZATION'] ); // WPCS: sanitization ok.
        }

        if ( function_exists( 'getallheaders' ) ) {
            $headers = getallheaders();
            // Check for the authoization header case-insensitively.
            foreach ( $headers as $key => $value ) {
                if ( 'authorization' === strtolower( $key ) ) {
                    return $value;
                }
            }
        }

        return '';
    }

    public function parse_header( $header ) {
        if ( 'OAuth ' !== substr( $header, 0, 6 ) ) {
            return array();
        }

        // From OAuth PHP library, used under MIT license.
        $params = array();
        if ( preg_match_all( '/(oauth_[a-z_-]*)=(:?"([^"]*)"|([^,]*))/', $header, $matches ) ) {
            foreach ( $matches[1] as $i => $h ) {
                $params[ $h ] = urldecode( empty( $matches[3][ $i ] ) ? $matches[4][ $i ] : $matches[3][ $i ] );
            }
            if ( isset( $params['realm'] ) ) {
                unset( $params['realm'] );
            }
        }

        return $params;
    }

    public function get_formatted_product_data( $object ) {
        $data = array(
            'id'                    => $object->get_id(),
            'date_created'          => wc_rest_prepare_date_response( $object->get_date_created(), false ),
            'date_created_gmt'      => wc_rest_prepare_date_response( $object->get_date_created() ),
            'date_modified'         => wc_rest_prepare_date_response( $object->get_date_modified(), false ),
            'date_modified_gmt'     => wc_rest_prepare_date_response( $object->get_date_modified() ),
            'description'           => wc_format_content( $object->get_description() ),
            'permalink'             => $object->get_permalink(),
            'sku'                   => $object->get_sku(),
            'price'                 => $object->get_price(),
            'regular_price'         => $object->get_regular_price(),
            'sale_price'            => $object->get_sale_price(),
            'date_on_sale_from'     => wc_rest_prepare_date_response( $object->get_date_on_sale_from(), false ),
            'date_on_sale_from_gmt' => wc_rest_prepare_date_response( $object->get_date_on_sale_from() ),
            'date_on_sale_to'       => wc_rest_prepare_date_response( $object->get_date_on_sale_to(), false ),
            'date_on_sale_to_gmt'   => wc_rest_prepare_date_response( $object->get_date_on_sale_to() ),
            'on_sale'               => $object->is_on_sale(),
            'visible'               => $object->is_visible(),
            'purchasable'           => $object->is_purchasable(),
            'virtual'               => $object->is_virtual(),
            'downloadable'          => $object->is_downloadable(),
            'downloads'             => $this->get_downloads( $object ),
            'download_limit'        => '' !== $object->get_download_limit() ? (int) $object->get_download_limit() : -1,
            'download_expiry'       => '' !== $object->get_download_expiry() ? (int) $object->get_download_expiry() : -1,
            'tax_status'            => $object->get_tax_status(),
            'tax_class'             => $object->get_tax_class(),
            'manage_stock'          => $object->managing_stock(),
            'stock_quantity'        => $object->get_stock_quantity(),
            'in_stock'              => $object->is_in_stock(),
            'backorders'            => $object->get_backorders(),
            'backorders_allowed'    => $object->backorders_allowed(),
            'backordered'           => $object->is_on_backorder(),
            'weight'                => $object->get_weight(),
            'dimensions'            => array(
                'length' => $object->get_length(),
                'width'  => $object->get_width(),
                'height' => $object->get_height(),
            ),
            'shipping_class'        => $object->get_shipping_class(),
            'shipping_class_id'     => $object->get_shipping_class_id(),
            'image'                 => '',
            'attributes'            => $this->get_attributes( $object ),
            'menu_order'            => $object->get_menu_order(),
            'meta_data'             => $object->get_meta_data(),
        );
        return $data;
    }

    private function get_attributes( $product ) {

        $attributes = array();

        if ( $product->is_type( 'variation' ) ) {

            // variation attributes
            foreach ( $product->get_variation_attributes() as $attribute_name => $attribute ) {

                // taxonomy-based attributes are prefixed with `pa_`, otherwise simply `attribute_`
                $attributes[] = array(
                    'name'   => ucwords( str_replace( 'attribute_', '', str_replace( 'pa_', '', $attribute_name ) ) ),
                    'option' => $attribute,
                );
            }
        } else {

            foreach ( $product->get_attributes() as $attribute ) {
                $attributes[] = array(
                    'name'      => ucwords( str_replace( 'pa_', '', $attribute['name'] ) ),
                    'position'  => $attribute['position'],
                    'visible'   => (bool) $attribute['is_visible'],
                    'variation' => (bool) $attribute['is_variation'],
                    'options'   => $this->get_attribute_options( $product->get_id(), $attribute ),
                );
            }
        }

        return $attributes;
    }

    private function get_downloads( $product ) {

        $downloads = array();

        if ( $product->is_downloadable() ) {

            foreach ( $product->get_downloads() as $file_id => $file ) {

                $downloads[] = array(
                    'id'   => $file_id, // do not cast as int as this is a hash
                    'name' => $file['name'],
                    'file' => $file['file'],
                );
            }
        }

        return $downloads;
    }

    protected function get_order_item_data( $item ) {
        $data           = $item->get_data();
        $format_decimal = array( 'subtotal', 'subtotal_tax', 'total', 'total_tax', 'tax_total', 'shipping_tax_total' );

        // Format decimal values.
        foreach ( $format_decimal as $key ) {
            if ( isset( $data[ $key ] ) ) {
                $data[ $key ] = wc_format_decimal( $data[ $key ], $this->request['dp'] );
            }
        }

        // Add SKU and PRICE to products.
        if ( is_callable( array( $item, 'get_product' ) ) ) {
            $data['sku']   = $item->get_product() ? $item->get_product()->get_sku() : null;
            $data['price'] = $item->get_quantity() ? $item->get_total() / $item->get_quantity() : 0;
        }

        // Format taxes.
        if ( ! empty( $data['taxes']['total'] ) ) {
            $taxes = array();

            foreach ( $data['taxes']['total'] as $tax_rate_id => $tax ) {
                $taxes[] = array(
                    'id'       => $tax_rate_id,
                    'total'    => $tax,
                    'subtotal' => isset( $data['taxes']['subtotal'][ $tax_rate_id ] ) ? $data['taxes']['subtotal'][ $tax_rate_id ] : '',
                );
            }
            $data['taxes'] = $taxes;
        } elseif ( isset( $data['taxes'] ) ) {
            $data['taxes'] = array();
        }

        // Remove names for coupons, taxes and shipping.
        if ( isset( $data['code'] ) || isset( $data['rate_code'] ) || isset( $data['method_title'] ) ) {
            unset( $data['name'] );
        }

        // Remove props we don't want to expose.
        unset( $data['order_id'] );
        unset( $data['type'] );

        return $data;
    }
}
