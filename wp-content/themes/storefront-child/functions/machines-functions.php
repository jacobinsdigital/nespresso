<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with machine products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * 'm_' stands for coffee products
 */

/**
 * get all the machine products, not trached
 * sorted by post id
 *
 * @return array
 */
function get_machine_products() {

	global $wpdb;

	$posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='product_type' AND meta_value='%s' ", 'Machine') );

	$products = [];

	if ( !empty($posts) && $posts ) :

		foreach ( $posts as $post ) :

		   $product = wc_get_product($post->post_id);

	    	if ( !$product )
	    		continue;

	    	$product = $product->get_data();

	        if ( !$product ||
	        	$product['status'] != 'publish'
        	) {
	    		continue;
        	}

	    	$product = include_machine_product_meta_from_results($post);

	    	if ( !$product )
	    		continue;

	        $index = get_the_title($post->post_id);
		    if ( isset($products[$index]) )
		        $index .= $post->post_id;

		    $products[$index] = $product;

		endforeach;

		$products ? ksort($products) : [];

	endif;

	return $products;

} // get_machine_products()

/**
 * include the machine product meta(custom) data from the result given
 *
 * @param  object|null $product - product result
 * @return object|null
 */
function include_machine_product_meta_from_results( $product=null,  $single_product_page=false ) {

	$product = (object)$product;

	if ( isset($product->post_id) )
		$product_id = $product->post_id;
	elseif ( isset($product->ID) )
		$product_id = $product->ID;
	elseif ( isset($product->id) )
		$product_id = $product->id;
	else
		$product_id = 0;

	if ( get_post_status($product_id) == 'trash' )
		return null;

	return (object) array_merge(
		(array) $product,
		(array) get_machines_product_meta_data( $product_id, $single_product_page )
	);

} // include_machine_product_meta_from_results()

/**
 * get the machine product meta(custom) data
 * this function might be looped from other functions
 *
 * @param  integer $product_id
 * @param  boolean $single_product_page
 * @return object|null
 */
function get_machines_product_meta_data( $product_id=0, $single_product_page=false ) {

	$product_id = $product_id ? $product_id : 0;

	$product = (object)null;

	$product->product_id = $product_id;

	$product->post_id = $product_id;

	$product->name = get_the_title( $product_id );

	$product->url = get_permalink( $product_id );

	$product->price = get_post_meta( $product_id, '_price', true );
	$product->regular_price = get_post_meta( $product_id, '_regular_price', true );
	$product->description = get_field('description', $product_id );

	$product->short_description = get_field('short_description', $product_id );

	$product->product_color_variant = get_product_color_variant($product_id);

	$product->product_type = get_field( 'product_type', $product_id );
$product->label_off = get_field( 'label_off', $product_id );
	$product->specification = get_field( 'specification', $product_id );

	$product->recommended_products = $single_product_page ? get_recommended_machine_products($product_id) : '';

	$product->image = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );

	if ($product->price) {
		$wc_product = new WC_Product_Variable( $product_id );
		$product->variations = $wc_product->get_available_variations();
	} else {
		$product->variations = [];
	}


	$product->sku = get_post_meta( $product_id, '_sku', true );

	return $product;

} // get_machines_product_meta_data()

/**
 * get recommended machine products from acf form
 *
 * @param  integer $product_id
 * @return array|string - string = no product
 */
function get_recommended_machine_products($product_id=0) {

	if ( !$product_id )
		return '';

	$recommended_products = get_field( 'recommended_products', $product_id );

	if ( !$recommended_products )
		return '';

	$return = [];
	foreach ( $recommended_products as $recommended_product ) :
		$return[]  = (object) array_merge(
			(array) $recommended_product,
			(array) get_machines_product_meta_data( $recommended_product->ID, false )
		);
	endforeach;

	return $return;

} // single_product_page()

/**
 * get the product color variant of the machine
 *
 * @param  integer $post_id
 * @return array
 */
function get_product_color_variant( $post_id=0 ) {

	$vs = get_post_meta( $post_id, 'product_color_variant');
	$variants = $vs ? $vs[0] : [];

	foreach ( $variants as $k => $variant ) :
		$image = wp_get_attachment_image_src( $k, 'single-post-thumbnail' );
		if ( $image )
			$variants[$k]['image_url'] = $image[0];
	endforeach;

	return $variants;

} // get_product_color_variant()

function get_embedded_machine_video($product_id=0) {

	if ( !$product_id )
		return '';

	$embedded_machine_video = get_field( 'embedded_machine_video', $product_id );

	if ( !$embedded_machine_video )
		return '';

	return $embedded_machine_video;

} // get_embedded_machine_video()


function get_downloadable($product_id=0) {

	if ( !$product_id )
		return '';

	$downloadable = get_field( 'downloadable', $product_id );

	if ( !$downloadable )
		return '';

	return $downloadable;

} // get_embedded_machine_video()
