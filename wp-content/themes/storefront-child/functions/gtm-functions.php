<?php
/**
 * Nespresso Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

// add_action('woocommerce_before_checkout_billing_form', 'nespresso_gtm_checkout');
// add_action('woocommerce_thankyou', 'nespresso_gtm_checkout');
function nespresso_gtm_checkout() {

    global $wp, $woocommerce;

    $order = null;
    $order_data = null;
    $order_items = null;

    if ( isset( $wp->query_vars['order-received']) ) :

        $order_id = $wp->query_vars['order-received'];
        if ( $order_id ) :

            $order = new WC_Order( $order_id );
            $order_data =  $order->get_data();

            foreach ( $order->get_items() as $item_id => $item ) :
                $order_items[] = $item->get_data();
            endforeach;

        endif;
    endif;

    $user_id = get_current_user_id();
    $states     = dhl_get_states();
    $billing_city = get_user_meta($user_id, 'billing_city', false);
    $billingCity = get_val($billing_city);
    $billing_country = get_user_meta($user_id, 'billing_country', false);
    $billingCountry = get_val($billing_country);
    $billing_postcode = get_user_meta($user_id, 'billing_postcode', false);
    $billingPostCode = get_val($billing_postcode);
    $billing_state = get_user_meta($user_id, 'billing_state', false);
    $billingState = get_val($billing_state);
    $billingState = $states[$billingState];
    $shipping_city = get_user_meta($user_id, 'shipping_city', false);
    $shippingCity = get_val($shipping_city);
    $shipping_country = get_user_meta($user_id, 'shipping_country', false);
    $shippingCountry = get_val($shipping_country);
    $shipping_postcode = get_user_meta($user_id, 'shipping_postcode', false);
    $shippingPostCode = get_val($shipping_postcode);
    $shipping_state = get_user_meta($user_id, 'shipping_state', false);
    $shippingState = get_val($shipping_state);
    $shippingState = $states[$shippingState];

    //user info
    $clubMemberID = $user_id; //get_user_meta($user_id, 'club_membership_no', true);
    $club_status = 'True'; //get_user_meta($user_id, 'has_club_membership_no', true);

    $clubMemberStatus = 'True';//$club_status > 0 ? 'True' : 'False';
    $clubMemberTitle = get_user_meta($user_id, 'title', true);
    $clubMemberLoginStatus = 'True';
    // $machine_owner = get_user_meta($user_id, 'has_machine_registered', true);
    $machineOwner = $machine_owner > 0 ? 'Yes' : 'No';
    $my_machines = get_user_meta($user_id, 'my_machines', false);

    $user_total_spent = wc_get_customer_total_spent($user_id);
    if ($user_total_spent == 0) {
        $clubMemberLevel = 'Bronze';
        $clubMemberLevelID = '3';
    } else if ($user_total_spent >= 10000000) {
        $clubMemberLevel = 'Gold';
        $clubMemberLevelID = '1';
    } else if ($user_total_spent <= 9999999) {
        $clubMemberLevel = 'Silver';
        $clubMemberLevelID = '2';
    }

    $machineOwned = '';
    $machine_counter = 0;
    foreach ($my_machines as $k => $v) {
        foreach ($v as $m) {
            if (isset($m['product_sku']) && $m['product_sku'] != '0') {
                $machine_id = wc_get_product_id_by_sku($m['product_sku']);
                $machine_data = wc_get_product( $machine_id );
                $machineOwned .= $machine_data->get_name() . '|';
                $machine_counter++;
            } else if( $m['serial_no'] != '' ) {
                if( $m['product_sku'] == '0' || $m['product_sku'] == '') {
                    $machineOwned .=  'Others|';
                    $machine_counter++;
                }
            }

        }
    }

    $machineOwned = substr($machineOwned, 0, strlen($machineOwned) - 1) != false ? substr($machineOwned, 0, strlen($machineOwned) - 1) : '';
    $machineOwner = $machine_counter > 0 ? 'Yes' : 'No';

    $cart_subtotal = $woocommerce->cart->subtotal;

    if ( $order_data ) :
        $cart_subtotal = 0;
        foreach ( $order_items as $item ) :
            $cart_subtotal += isset($item['subtotal']) ? $item['subtotal'] : 0;
        endforeach;
    endif;

    $cart_taxtotal = isset($order_data['total_tax']) ? $order_data['total_tax'] : $woocommerce->cart->tax_total;
    $cart_taxtotal = number_format((float)$cart_taxtotal, 2, '.', '');

    $cart_shipping_total = isset($order_data['shipping_total']) ? $order_data['shipping_total'] : $woocommerce->cart->shipping_total;
    $cart_shipping_total = number_format((float)$cart_shipping_total, 2, '.', '');

    $cart_grand_total = $cart_subtotal + $cart_taxtotal + $cart_shipping_total;
    $cart_grand_total = number_format((float)$cart_grand_total, 2, '.', '');
    $persistentBasketLoaded = count(WC()->cart->get_cart()) > 0 ? 'True' : 'False';
    $current_url = home_url(add_query_arg(array(), $wp->request));
    $language = 'EN';
    $parsed = parse_url($current_url);
    if (isset($parsed['path'])) {
        $path = $parsed['path'];
        $path_parts = explode('/', $path);
        $segment = sizeof($path_parts) > 1 ? $path_parts[1] : '';

        if ($segment == 'vi' || $segment == 'vn') {
            $language = 'VN';
        }
    }

    ?>

        <script type="text/javascript">
            gtmDataObject = [{
                'isEnvironmentProd': 'Yes',
                'pageName': 'Billing Setup | Nespresso',
                'pageType': 'Checkout', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                'pageCategory': 'Checkout', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                'pageSubCategory': 'Checkout', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                'market': 'VN',
                'version': 'VN - WooCommerce V1',
                'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                'segmentBusiness': 'B2C',
                'currency': 'VND',
                'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                'preferredTechnology': 'Espresso',
                'event': 'event_pageView',
                'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                'language': '<?= $language ?>'

            }];
            gtmDataObject.push({
                'event': 'checkout',
                'currencyCode': 'VND',
                'discoveryOfferIncluded': 'FALSE',
                'ecommerce': {
                    'checkout': {
                        'actionField': {
                            'step': 1,
                            'checkoutStepName': 'Billing Setup'
                        },
                    },

                     'products': [
                        <?php

                            if ( !function_exists('get_product_category_by_id') ) :
                                function get_product_category_by_id($category_id) {
                                    $term = get_term_by('id', $category_id, 'product_cat', 'ARRAY_A');
                                    return $term['name'];
                                }
                            endif;


                            $items = $order_items ? $order_items : $woocommerce->cart->get_cart();
                            foreach ($items as $k => $v) {

                                if ( $order_items )
                                    $data = wc_get_product( $v['product_id'] );
                                else
                                    $data = $v['data'];

                                $product_variation_id = $v['variation_id'];

                                // Check if product has variation.
                                if ($product_variation_id) {
                                    $product = wc_get_product($v['variation_id']);
                                } else {
                                    $product = wc_get_product($v['product_id']);
                                }

                                // Get SKU
                                $sku      = $product->get_sku();
                                $hq_data  = get_hq_data($sku);

                                $item = $v;
                                $product_name = $data->get_name();
                                $product_id = $data->get_id();
                                $product_price = $data->get_price();
                                $product_qty = $item['quantity'];

                                $product_categories = $data->get_category_ids();
                                $product_category = null;
                                foreach ($product_categories as $product_category) {
                                    $product_category = get_product_category_by_id($product_category);
                                }
                                if (strtolower($product_category)=== "coffee") {
                                    $product_category = "capsule";
                                }elseif (strtolower($product_category)=== "machines") {
                                    $product_category = "machine";
                                }elseif (strtolower($product_category)=== "accessories") {
                                    $product_category = "accessory";

                                }

                                $bundle = get_post_meta($product_id, 'bundle', true);
                                if ($bundle == '1') {
                                    $dimension57 = 'Bundle Products';
                                } else {
                                    if ($product_category == 'machine') {
                                        $dimension57 = 'Variable Products';
                                    } else {
                                        $dimension57 = 'Single Products';
                                    }
                                }

                                $product_category = get_post_meta($product_id, 'category', true);
                                $dimension44 = 'False';
                                if ($product_category == 'Discovery Offer')
                                    $dimension44 = 'True';
                        ?>
                                    {
                                        'name': '<?= isset($hq_data['name']) ? $hq_data['name'] : $product_name; ?>',
                                        'id': '<?= isset($hq_data['id']) ? $hq_data['id'] : $product_id; ?>',
                                        'quantity': <?php echo $product_qty; ?>,
                                        'price': '<?php echo $product_price; ?>',
                                        'category': '<?php echo strtolower($product_category); ?>',
                                        'dimension56': '<?= isset($hq_data['technology']) ? $hq_data['technology'] : 'VirtuoLine'; ?>',
                                        'brand': 'Nespresso',
                                        'dimension59': "<?= $product_price > 0 ? 'True' : 'False' ?>",
                                        'dimension57': '<?= $dimension57 . ' - ' .strtolower($product_category); ?>',
                                        'dimension44': '<?= $dimension44 ?>',
                                        'dimension53': '<?= $sku ?>',
                                        'dimension54': '<?= $product_name ?>',
                                        'dimension55': '<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : ''; ?>'
                                    },
                        <?php
                            }
                        ?>
                    ]
                }
            });
        </script>
        <script type="text/javascript">
            function checkout_step_1() {
                gtmDataObject.push({
                    'isEnvironmentProd': 'Yes',
                    'pageName': 'Billing Setup | Nespresso',
                    'pageType': 'Checkout', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                    'pageCategory': 'Checkout', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageSubCategory': 'Checkout', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                    'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                    'market': 'VN',
                    'version': 'VN - WooCommerce V1',
                    'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                    'segmentBusiness': 'B2C',
                    'currency': 'VND',
                    'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                    'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                    'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                    'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                    'preferredTechnology': 'Espresso',
                    'event': 'event_pageView',
                    'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                    'language': '<?= $language ?>'

                });
                gtmDataObject.push({
                    'event': 'checkout',
                    'currencyCode': 'VND',
                    'discoveryOfferIncluded': 'FALSE',
                    'ecommerce': {
                        'checkout': {
                            'actionField': {
                                'step': 1,
                                'checkoutStepName': 'Billing Setup'
                            },
                        },

                         'products': [
                            <?php

                                if ( !function_exists('get_product_category_by_id') ) :
                                    function get_product_category_by_id($category_id) {
                                        $term = get_term_by('id', $category_id, 'product_cat', 'ARRAY_A');
                                        return $term['name'];
                                    }
                                endif;


                                $items = $order_items ? $order_items : $woocommerce->cart->get_cart();
                                foreach ($items as $k => $v) {

                                    if ( $order_items )
                                        $data = wc_get_product( $v['product_id'] );
                                    else
                                        $data = $v['data'];

                                    $product_variation_id = $v['variation_id'];

                                    // Check if product has variation.
                                    if ($product_variation_id) {
                                        $product = wc_get_product($v['variation_id']);
                                    } else {
                                        $product = wc_get_product($v['product_id']);
                                    }

                                    // Get SKU
                                    $sku      = $product->get_sku();
                                    $hq_data  = get_hq_data($sku);

                                    $item = $v;
                                    $product_name = $data->get_name();
                                    $product_id = $data->get_id();
                                    $product_price = $data->get_price();
                                    $product_qty = $item['quantity'];

                                    $product_categories = $data->get_category_ids();
                                    $product_category = null;
                                    foreach ($product_categories as $product_category) {
                                        $product_category = get_product_category_by_id($product_category);
                                    }
                                    if (strtolower($product_category)=== "coffee") {
                                        $product_category = "capsule";
                                    }elseif (strtolower($product_category)=== "machines") {
                                        $product_category = "machine";
                                    }elseif (strtolower($product_category)=== "accessories") {
                                        $product_category = "accessory";

                                    }

                                    $bundle = get_post_meta($product_id, 'bundle', true);
                                    if ($bundle == '1') {
                                        $dimension57 = 'Bundle Products';
                                    } else {
                                        if ($product_category == 'machine') {
                                            $dimension57 = 'Variable Products';
                                        } else {
                                            $dimension57 = 'Single Products';
                                        }
                                    }

                                    $product_category = get_post_meta($product_id, 'category', true);
                                    $dimension44 = 'False';
                                    if ($product_category == 'Discovery Offer')
                                        $dimension44 = 'True';
                            ?>
                                        {
                                        'name': '<?= isset($hq_data['name']) ? $hq_data['name'] : $product_name; ?>',
                                        'id': '<?= isset($hq_data['id']) ? $hq_data['id'] : $product_id; ?>',
                                        'quantity': <?php echo $product_qty; ?>,
                                        'price': '<?php echo $product_price; ?>',
                                        'category': '<?php echo strtolower($product_category); ?>',
                                        'dimension56': '<?= isset($hq_data['technology']) ? $hq_data['technology'] : 'VirtuoLine'; ?>',
                                        'brand': 'Nespresso',
                                        'dimension59': "<?= $product_price > 0 ? 'True' : 'False' ?>",
                                        'dimension57': '<?= $dimension57 . ' - ' .strtolower($product_category); ?>',
                                        'dimension44': '<?= $dimension44 ?>',
                                        'dimension53': '<?= $sku ?>',
                                        'dimension54': '<?= $product_name ?>',
                                        'dimension55': '<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : ''; ?>'
                                    },
                        <?php
                            }
                        ?>
                    ]
                }
            });
            }

            function checkout_step_2() {
                gtmDataObject.push({
                    'isEnvironmentProd': 'Yes',
                    'pageName': 'Delivery Setup | Nespresso',
                    'pageType': 'Checkout', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                    'pageCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                    'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                    'market': 'VN',
                    'version': 'VN - WooCommerce V1',
                    'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                    'segmentBusiness': 'B2C',
                    'currency': 'VND',
                    'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                    'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                    'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                    'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                    'preferredTechnology': 'Espresso',
                    'event': 'event_pageView',
                    'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                    'language': '<?= $language ?>'

                });
                gtmDataObject.push({
                    'event': 'checkout',
                    'currencyCode': 'VND',
                    'discoveryOfferIncluded': 'FALSE',
                    'ecommerce': {
                        'checkout': {
                            'actionField': {
                                'step': 1,
                                'checkoutStepName': 'Delivery Setup'
                            },
                        },

                         'products': [
                            <?php

                                if ( !function_exists('get_product_category_by_id') ) :
                                    function get_product_category_by_id($category_id) {
                                        $term = get_term_by('id', $category_id, 'product_cat', 'ARRAY_A');
                                        return $term['name'];
                                    }
                                endif;


                                $items = $order_items ? $order_items : $woocommerce->cart->get_cart();
                                foreach ($items as $k => $v) {

                                    if ( $order_items )
                                        $data = wc_get_product( $v['product_id'] );
                                    else
                                        $data = $v['data'];

                                    $product_variation_id = $v['variation_id'];

                                    // Check if product has variation.
                                    if ($product_variation_id) {
                                        $product = wc_get_product($v['variation_id']);
                                    } else {
                                        $product = wc_get_product($v['product_id']);
                                    }

                                    // Get SKU
                                    $sku      = $product->get_sku();
                                    $hq_data  = get_hq_data($sku);

                                    $item = $v;
                                    $product_name = $data->get_name();
                                    $product_id = $data->get_id();
                                    $product_price = $data->get_price();
                                    $product_qty = $item['quantity'];

                                    $product_categories = $data->get_category_ids();
                                    $product_category = null;
                                    foreach ($product_categories as $product_category) {
                                        $product_category = get_product_category_by_id($product_category);
                                    }
                                    if (strtolower($product_category)=== "coffee") {
                                        $product_category = "capsule";
                                    }elseif (strtolower($product_category)=== "machines") {
                                        $product_category = "machine";
                                    }elseif (strtolower($product_category)=== "accessories") {
                                        $product_category = "accessory";

                                    }
                                    $bundle = get_post_meta($product_id, 'bundle', true);
                                    if ($bundle == '1') {
                                        $dimension57 = 'Bundle Products';
                                    } else {
                                        if ($product_category == 'machine') {
                                            $dimension57 = 'Variable Products';
                                        } else {
                                            $dimension57 = 'Single Products';
                                        }
                                    }

                                    $product_category = get_post_meta($product_id, 'category', true);
                                    $dimension44 = 'False';
                                    if ($product_category == 'Discovery Offer')
                                        $dimension44 = 'True';
                            ?>
                                        {
                                            'name': '<?= isset($hq_data['name']) ? $hq_data['name'] : $product_name; ?>',
                                            'id': '<?= isset($hq_data['id']) ? $hq_data['id'] : $product_id; ?>',
                                            'quantity': <?php echo $product_qty; ?>,
                                            'price': '<?php echo $product_price; ?>',
                                            'category': '<?php echo strtolower($product_category); ?>',
                                            'dimension56': '<?= isset($hq_data['technology']) ? $hq_data['technology'] : 'VirtuoLine'; ?>',
                                            'brand': 'Nespresso',
                                            'dimension59': "<?= $product_price > 0 ? 'True' : 'False' ?>",
                                            'dimension57': '<?= $dimension57 . ' - ' .strtolower($product_category); ?>',
                                            'dimension44': '<?= $dimension44 ?>',
                                            'dimension53': '<?= $sku ?>',
                                            'dimension54': '<?= $product_name ?>',
                                            'dimension55': '<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : ''; ?>'
                                        },
                            <?php
                                }
                            ?>
                        ]
                    }
                });
            }
        </script>
        <script type="text/javascript">

            function checkout_step_3() {
                gtmDataObject.push({
                    'isEnvironmentProd': 'Yes',
                    'pageName': 'Order Summary | Nespresso',
                    'pageType': 'Checkout', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                    'pageCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                    'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                    'market': 'VN',
                    'version': 'VN - WooCommerce V1',
                    'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                    'segmentBusiness': 'B2C',
                    'currency': 'VND',
                    'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                    'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                    'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                    'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                    'preferredTechnology': 'Espresso',
                    'event': 'event_pageView',
                    'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                    'language': '<?= $language ?>'

                });
                gtmDataObject.push({
                    'event': 'checkout',
                    'currencyCode': 'VND',
                    'discoveryOfferIncluded': 'FALSE',
                    'ecommerce': {
                        'checkout': {
                            'actionField': {
                                'step': 1,
                                'checkoutStepName': 'Order Summary'
                            },
                        },

                         'products': [
                            <?php

                                if ( !function_exists('get_product_category_by_id') ) :
                                    function get_product_category_by_id($category_id) {
                                        $term = get_term_by('id', $category_id, 'product_cat', 'ARRAY_A');
                                        return $term['name'];
                                    }
                                endif;


                                $items = $order_items ? $order_items : $woocommerce->cart->get_cart();
                                foreach ($items as $k => $v) {

                                    if ( $order_items )
                                        $data = wc_get_product( $v['product_id'] );
                                    else
                                        $data = $v['data'];

                                    $product_variation_id = $v['variation_id'];

                                    // Check if product has variation.
                                    if ($product_variation_id) {
                                        $product = wc_get_product($v['variation_id']);
                                    } else {
                                        $product = wc_get_product($v['product_id']);
                                    }

                                    // Get SKU
                                    $sku      = $product->get_sku();
                                    $hq_data  = get_hq_data($sku);

                                    $item = $v;
                                    $product_name = $data->get_name();
                                    $product_id = $data->get_id();
                                    $product_price = $data->get_price();
                                    $product_qty = $item['quantity'];

                                    $product_categories = $data->get_category_ids();
                                    $product_category = null;
                                    foreach ($product_categories as $product_category) {
                                        $product_category = get_product_category_by_id($product_category);
                                    }
                                    if (strtolower($product_category)=== "coffee") {
                                        $product_category = "capsule";
                                    }elseif (strtolower($product_category)=== "machines") {
                                        $product_category = "machine";
                                    }elseif (strtolower($product_category)=== "accessories") {
                                        $product_category = "accessory";

                                    }

                                    $bundle = get_post_meta($product_id, 'bundle', true);
                                    if ($bundle == '1') {
                                        $dimension57 = 'Bundle Products';
                                    } else {
                                        if ($product_category == 'machine') {
                                            $dimension57 = 'Variable Products';
                                        } else {
                                            $dimension57 = 'Single Products';
                                        }
                                    }

                                    $product_category = get_post_meta($product_id, 'category', true);
                                    $dimension44 = 'False';
                                    if ($product_category == 'Discovery Offer')
                                        $dimension44 = 'True';
                            ?>
                                        {
                                            'name': '<?= isset($hq_data['name']) ? $hq_data['name'] : $product_name; ?>',
                                            'id': '<?= isset($hq_data['id']) ? $hq_data['id'] : $product_id; ?>',
                                            'quantity': <?php echo $product_qty; ?>,
                                            'price': '<?php echo $product_price; ?>',
                                            'category': '<?php echo strtolower($product_category); ?>',
                                            'dimension56': '<?= isset($hq_data['technology']) ? $hq_data['technology'] : 'VirtuoLine'; ?>',
                                            'brand': 'Nespresso',
                                            'dimension59': "<?= $product_price > 0 ? 'True' : 'False' ?>",
                                            'dimension57': '<?= $dimension57 . ' - ' .strtolower($product_category); ?>',
                                            'dimension44': '<?= $dimension44 ?>',
                                            'dimension53': '<?= $sku ?>',
                                            'dimension54': '<?= $product_name ?>',
                                            'dimension55': '<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : ''; ?>'
                                        },
                            <?php
                                }
                            ?>
                        ]
                    }
                });
            }
        </script>
        <script type="text/javascript">

            function checkout_step_4() {
                gtmDataObject.push({
                    'isEnvironmentProd': 'Yes',
                    'pageName':'Payment Setup | Nespresso',
                    'pageType': 'Checkout', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                    'pageCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                    'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                    'market': 'VN',
                    'version': 'VN - WooCommerce V1',
                    'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                    'segmentBusiness': 'B2C',
                    'currency': 'VND',
                    'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                    'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                    'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                    'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                    'preferredTechnology': 'Espresso',
                    'event': 'event_pageView',
                    'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                    'language': '<?= $language ?>'

                });
                gtmDataObject.push({
                    'event': 'checkout',
                    'currencyCode': 'VND',
                    'discoveryOfferIncluded': 'FALSE',
                    'ecommerce': {
                        'checkout': {
                            'actionField': {
                                'step': 1,
                                'checkoutStepName': 'Payment Setup'
                            },
                        },

                         'products': [
                            <?php

                                if ( !function_exists('get_product_category_by_id') ) :
                                    function get_product_category_by_id($category_id) {
                                        $term = get_term_by('id', $category_id, 'product_cat', 'ARRAY_A');
                                        return $term['name'];
                                    }
                                endif;


                                $items = $order_items ? $order_items : $woocommerce->cart->get_cart();
                                foreach ($items as $k => $v) {

                                    if ( $order_items )
                                        $data = wc_get_product( $v['product_id'] );
                                    else
                                        $data = $v['data'];

                                    $product_variation_id = $v['variation_id'];

                                    // Check if product has variation.
                                    if ($product_variation_id) {
                                        $product = wc_get_product($v['variation_id']);
                                    } else {
                                        $product = wc_get_product($v['product_id']);
                                    }

                                    // Get SKU
                                    $sku      = $product->get_sku();
                                    $hq_data  = get_hq_data($sku);

                                    $item = $v;
                                    $product_name = $data->get_name();
                                    $product_id = $data->get_id();
                                    $product_price = $data->get_price();
                                    $product_qty = $item['quantity'];

                                    $product_categories = $data->get_category_ids();
                                    $product_category = null;
                                    foreach ($product_categories as $product_category) {
                                        $product_category = get_product_category_by_id($product_category);
                                    }
                                    if (strtolower($product_category)=== "coffee") {
                                        $product_category = "capsule";
                                    }elseif (strtolower($product_category)=== "machines") {
                                        $product_category = "machine";
                                    }elseif (strtolower($product_category)=== "accessories") {
                                        $product_category = "accessory";

                                    }
                            ?>
                                        {
                                            'name': '<?= isset($hq_data['name']) ? $hq_data['name'] : $product_name; ?>',
                                            'id': '<?= isset($hq_data['id']) ? $hq_data['id'] : $product_id; ?>',
                                            'quantity': <?php echo $product_qty; ?>,
                                            'price': '<?php echo $product_price; ?>',
                                            'category': '<?php echo strtolower($product_category); ?>',
                                            'dimension56': '<?= isset($hq_data['technology']) ? $hq_data['technology'] : 'VirtuoLine'; ?>',
                                            'brand': 'Nespresso',
                                            'dimension59': 'True',
                                            'dimension57': 'Single Product - Capsule',
                                            'dimension44': 'FALSE',
                                            'dimension53': '<?= $sku ?>',
                                            'dimension54': '<?= $product_name ?>',
                                            'dimension55': '<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : ''; ?>'
                                        },
                            <?php
                                }
                            ?>
                        ]
                    }
                });
            }
        </script>
    <?php
}

function nespresso_gtm_thank_you_page() {

    global $wp, $woocommerce;

    $order = null;
    $order_data = null;
    $order_items = null;
    $discoveryOfferIncluded = 'False';
    $rebateAmount = '';
    $checkoutMainPaymentMethod = '';
    $checkoutShippingMethodID = '';
    $couponAmount = 0;
    $coupon_id = '';
    if ( isset( $wp->query_vars['order-received']) ) :

        $order_id = $wp->query_vars['order-received'];
        if ( $order_id ) :

            $order = new WC_Order( $order_id );
            $onepay = ['onepayvn', 'onepayus'];
            $order_status = $order->get_status();
            $order_method = $order->get_payment_method();

            if (in_array($order_method, $onepay) && ($order_status == 'pending' || $order_status == 'failed'))
                return false;

            $order_data =  $order->get_data();
            $rebateAmount = $order->get_subtotal() - $order->get_subtotal();
            $checkoutMainPaymentMethod = $order->get_payment_method_title();
            $checkoutShippingMethodID = $order->get_meta('delivery_option');
            foreach ( $order->get_items() as $item_id => $item ) :
                $category = get_post_meta($post->ID, 'category', true);
                if ($category == 'Discovery Offer' && $discoveryOfferIncluded == 'False')
                    $discoveryOfferIncluded = 'True';
                $order_items[] = $item->get_data();
            endforeach;
            $coupon = $order->get_used_coupons();
            // Retrieving the coupon ID
            $coupon_post_obj = get_page_by_title(@$coupon[0], OBJECT, 'shop_coupon');
            $coupon_id = $coupon_post_obj->ID;
            // Get an instance of WC_Coupon object in an array(necesary to use WC_Coupon methods)
            $coupons_obj = new WC_Coupon($coupon_id);
            $couponAmount = $coupons_obj->get_amount();
        endif;
    endif;

    $user_id = get_current_user_id();
    $states     = dhl_get_states();
    $billing_city = get_user_meta($user_id, 'billing_city', false);
    $billingCity = get_val($billing_city);
    $billing_country = get_user_meta($user_id, 'billing_country', false);
    $billingCountry = get_val($billing_country);
    $billing_postcode = get_user_meta($user_id, 'billing_postcode', false);
    $billingPostCode = get_val($billing_postcode);
    $billing_state = get_user_meta($user_id, 'billing_state', false);
    $billingState = get_val($billing_state);
    $billingState = $states[$billingState];
    $shipping_city = get_user_meta($user_id, 'shipping_city', false);
    $shippingCity = get_val($shipping_city);
    $shipping_country = get_user_meta($user_id, 'shipping_country', false);
    $shippingCountry = get_val($shipping_country);
    $shipping_postcode = get_user_meta($user_id, 'shipping_postcode', false);
    $shippingPostCode = get_val($shipping_postcode);
    $shipping_state = get_user_meta($user_id, 'shipping_state', false);
    $shippingState = get_val($shipping_state);
    $shippingState = $states[$shippingState];

    //user info
    $clubMemberID = $user_id; //get_user_meta($user_id, 'club_membership_no', true);
    $club_status = 'True'; //get_user_meta($user_id, 'has_club_membership_no', true);
    $clubMemberStatus = 'True';//$club_status > 0 ? 'True' : 'False';
    $clubMemberTitle = get_user_meta($user_id, 'title', true);
    $newsletter_subscription = get_user_meta($user_id, 'newsletter_subscription', true);
    $keepMeInformed = $newsletter_subscription == 1 ? 'True' : 'False';
    $clubMemberLoginStatus = 'True';
    $machine_owner = get_user_meta($user_id, 'has_machine_registered', true);
    $machineOwner = $machine_owner > 0 ? 'Yes' : 'No';
    $my_machines = get_user_meta($user_id, 'my_machines', false);

    $user_total_spent = wc_get_customer_total_spent($user_id);
    $newClient = 'False';
    if ($user_total_spent == 0) {
        $clubMemberLevel = 'Bronze';
        $clubMemberLevelID = '3';
        $newClient = 'True';
    } else if ($user_total_spent >= 10000000) {
        $clubMemberLevel = 'Gold';
        $clubMemberLevelID = '1';
    } else if ($user_total_spent <= 9999999) {
        $clubMemberLevel = 'Silver';
        $clubMemberLevelID = '2';
    }

    $machineOwned = '';
    $machine_counter = 0;
    foreach ($my_machines as $k => $v) {
        foreach ($v as $m) {
            if (isset($m['product_sku']) && $m['product_sku'] != '0') {
                $machine_id = wc_get_product_id_by_sku($m['product_sku']);
                $machine_data = wc_get_product( $machine_id );
                $machineOwned .= $machine_data->get_name() . '|';
                $machine_counter++;
            } else if( $m['serial_no'] != '' ) {
                if( $m['product_sku'] == '0' || $m['product_sku'] == '') {
                    $machineOwned .=  'Others|';
                    $machine_counter++;
                }
            }

        }
    }

    $machineOwned = substr($machineOwned, 0, strlen($machineOwned) - 1) != false ? substr($machineOwned, 0, strlen($machineOwned) - 1) : '';
    $machineOwner = $machine_counter > 0 ? 'Yes' : 'No';

    $cart_subtotal = $woocommerce->cart->subtotal;

    if ( $order_data ) :
        $cart_subtotal = 0;
        foreach ( $order_items as $item ) :
            $cart_subtotal += isset($item['subtotal']) ? $item['subtotal'] : 0;
        endforeach;
    endif;

    $cart_taxtotal = isset($order_data['total_tax']) ? $order_data['total_tax'] : $woocommerce->cart->tax_total;
    $cart_taxtotal = number_format((float)$cart_taxtotal, 2, '.', '');

    $cart_shipping_total = isset($order_data['shipping_total']) ? $order_data['shipping_total'] : $woocommerce->cart->shipping_total;
    $cart_shipping_total = number_format((float)$cart_shipping_total, 2, '.', '');

    $cart_grand_total = $cart_subtotal + $cart_taxtotal + $cart_shipping_total;
    $cart_grand_total = number_format((float)$cart_grand_total, 2, '.', '');

    ?>

        <script type="text/javascript">
            gtmDataObject = [{
                'isEnvironmentProd': 'Yes',
                'pageName': '<?php echo get_the_title(); ?> | Nespresso',
                'pageType': 'Checkout',
                'pageCategory': '',
                'pageSubCategory': '',
                'pageTechnology': 'Multiple',
                'pageVariant': '',
                'market': 'VN',
                'version': 'VN - WooCommerce V1',
                'landscape': 'WooCommerce-Standard_Agent',
                'segmentBusiness': 'B2C',
                'currency': 'VND',
                'clubMemberID': '<?php echo $clubMemberID; ?>',
                'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                'clubMemberLevel': '<?= $clubMemberLevel ?>',
                'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
                'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>',
                'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>',
                'preferredTechnology': 'Espresso',
            'event': 'event_pageView'
            }];
            gtmDataObject.push({
                'landscape': 'Wordpress',
                'segmentBusiness': 'B2C',
                'event': 'transaction',
                'currencyCode': 'VND',
                'productsReOrdered': '',
                'contactPreferenceSelected': 'SMS | Phone',
                'keepMeInformed': '<?= $keepMeInformed ?>',
                'transactionTotal': parseInt('<?php echo $cart_grand_total; ?>'),
                'userCreditAmountUsed': '',
                'newClient': '<?= $newClient ?>',
                'discoveryOfferIncluded': '<?= $discoveryOfferIncluded ?>',
                'ecommerce': {
                    'purchase': {
                        'actionField': {
                            'id': '<?= $order_id ?>',
                            'affiliation': 'Nespresso Online Store',
                            'revenue': '<?php echo $cart_subtotal; ?>',
                            'tax': '<?php echo $cart_taxtotal; ?>',
                            'shipping': '<?php echo $cart_shipping_total; ?>',
                            'coupon': '<?php echo @$coupon[0] ?>',
                            'clubActionTotalAmount': '<?php echo @$order_data['discount_total'] ?>',
                            'rebateAmount': '<?= $rebateAmount ?>',
                            'shippingAddressCity': '<?php echo $shippingCity; ?>',
                            'shippingAddressState': '<?php echo $shippingState; ?>',
                            'shippingAddressCountry': 'Vietnam',
                            'shippingAddressPostalCode': '<?php echo $shippingPostCode; ?>',
                            'billingAddressCity': '<?php echo $billingCity; ?>',
                            'billingAddressState': '<?php echo $billingState; ?>',
                            'billingAddressCountry': 'Vietnam',
                            'billingAddressPostalCode': '<?php echo $billingPostCode; ?>',
                            'clubActionID': '<?php echo @$coupon[0] ?>',
                            'checkoutMainPaymentMethod': '<?= $checkoutMainPaymentMethod ?>',
                            'checkoutPaymentMethods': 'cod|onepayus|onepayvn|',//'["Credit Card","Gift Card"]',
                            'checkoutShippingMethodID': '<?= $checkoutShippingMethodID ?>',
                            'deliveryOption_Priority': 'FALSE',
                            'deliveryOption_Signature': 'TRUE',
                            'deliveryOption_Recycling': 'TRUE',
                            'couponAmount': '<?= $couponAmount ?>',
                        },
                        'products': [
                            <?php

                                if ( !function_exists('get_product_category_by_id') ) :
                                    function get_product_category_by_id($category_id) {
                                        $term = get_term_by('id', $category_id, 'product_cat', 'ARRAY_A');
                                        return $term['name'];
                                    }
                                endif;


                                $items = $order_items ? $order_items : $woocommerce->cart->get_cart();
                                foreach ($items as $k => $v) {

                                    if ( $order_items )
                                        $data = wc_get_product( $v['product_id'] );
                                    else
                                        $data = $v['data'];

                                    $product_variation_id = $v['variation_id'];

                                    // Check if product has variation.
                                    if ($product_variation_id) {
                                        $product = wc_get_product($v['variation_id']);
                                    } else {
                                        $product = wc_get_product($v['product_id']);
                                    }

                                    // Get SKU
                                    $sku      = $product->get_sku();
                                    $hq_data  = get_hq_data($sku);

                                    $item = $v;
                                    $product_name = $data->get_name();
                                    $product_id = $data->get_id();
                                    $product_price = $data->get_price();
                                    $product_qty = $item['quantity'];

                                    $product_attr_str = '';
                                    $product_id = get_the_id();
                                    $product_attributes = get_post_meta($product_id);

                                    if ( isset($product_attributes['_product_attributes'] ) ) :
                                        foreach ($product_attributes['_product_attributes'] as $k => $v) {
                                            $attr = unserialize($v);
                                            if ((isset($attr['name'])) && isset($attr['color'])) {
                                                $attr_name = $attr['name']['value'];
                                                $attr_color = $attr['color']['value'];
                                                $product_attr_str = $attr_name . '|' . $attr_color;
                                            }
                                        }
                                    endif;

                                    $metric6 = 0;
                                    $metric5 = 0;
                                    $metric9 = 0;

                                    $product_categories = $data->get_category_ids();
                                    foreach ($product_categories as $product_category) {
                                        $product_category = get_product_category_by_id($product_category);

                                           if ( $product_category == 'Coffee') :
                                                $product_category = 'capsule';
                                                $metric6 = $product_qty;
                                            elseif ( $product_category == 'capsule') :
                                                $metric6 = $product_qty;
                                            elseif ( $product_category == 'Machine' ) :
                                                $metric5 = $product_qty;
                                            elseif ( $product_category == 'machine' ) :
                                                $metric5 = $product_qty;
                                            elseif ( $product_category == 'Machines' ) :
                                                $product_category = 'machine';
                                                $metric5 = $product_qty;
                                            elseif ( $product_category == 'Accessories' ) :
                                                $product_category = 'accessory';
                                                $metric9 = $product_qty;
                                            else:
                                                $metric9 = $product_qty;
                                            endif;
                                    }

                                    $bundle = get_post_meta($product_id, 'bundle', true);
                                    if ($bundle == '1') {
                                        $dimension57 = 'Bundle Products';
                                    } else {
                                        if ($product_category == 'machine') {
                                            $dimension57 = 'Variable Products';
                                        } else {
                                            $dimension57 = 'Single Products';
                                        }
                                    }

                                    $product_category = get_post_meta($product_id, 'category', true);
                                    $dimension44 = 'False';
                                    if ($product_category == 'Discovery Offer')
                                        $dimension44 = 'True';


                            ?>
                                        {
                                            'name': '<?= isset($hq_data['name']) ? $hq_data['name'] : $product_name; ?>',
                                            'id': '<?= isset($hq_data['id']) ? $hq_data['id'] : $product_id; ?>',
                                            'dimension54': '<?= $item['name'] ?>',
                                            'dimension55': '<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : $product_attr_str; ?>',
                                            'dimension53': '<?= $sku ?>',
                                            'quantity': <?php echo $product_qty; ?>,
                                            'price': '<?php echo $product_price; ?>',
                                            'category': '<?php echo strtolower($product_category); ?>',
                                            'dimension56': '<?= isset($hq_data['technology']) ? $hq_data['technology'] : 'VirtuoLine'; ?>',
                                            'brand': 'Nespresso',
                                            'metric6': '<?= $metric6 ?>',
                                            'metric5': '<?= $metric5 ?>',
                                            'metric9': '<?= $metric9 ?>',
                                            'dimension59': "<?= $product_price > 0 ? 'True' : 'False' ?>",
                                            'dimension57': '<?= $dimension57 . ' - ' .strtolower($product_category); ?>',
                                            'dimension44': '<?= $dimension44 ?>'
                                        },
                            <?php
                                }
                            ?>
                        ]
                    }
                }
            });
        </script>

    <?php
}

function nespresso_gtm_my_account() {
    global $wp, $woocommerce;

    $order = null;
    $order_data = null;
    $order_items = null;

    if ( isset( $wp->query_vars['order-received']) ) :

        $order_id = $wp->query_vars['order-received'];
        if ( $order_id ) :

            $order = new WC_Order( $order_id );
            $order_data =  $order->get_data();

            foreach ( $order->get_items() as $item_id => $item ) :
                $order_items[] = $item->get_data();
            endforeach;

        endif;
    endif;

    $user_id = get_current_user_id();
    $states     = dhl_get_states();
    $billing_city = get_user_meta($user_id, 'billing_city', false);
    $billingCity = get_val($billing_city);
    $billing_country = get_user_meta($user_id, 'billing_country', false);
    $billingCountry = get_val($billing_country);
    $billing_postcode = get_user_meta($user_id, 'billing_postcode', false);
    $billingPostCode = get_val($billing_postcode);
    $billing_state = get_user_meta($user_id, 'billing_state', false);
    $billingState = get_val($billing_state);
    $billingState = $states[$billingState];
    $shipping_city = get_user_meta($user_id, 'shipping_city', false);
    $shippingCity = get_val($shipping_city);
    $shipping_country = get_user_meta($user_id, 'shipping_country', false);
    $shippingCountry = get_val($shipping_country);
    $shipping_postcode = get_user_meta($user_id, 'shipping_postcode', false);
    $shippingPostCode = get_val($shipping_postcode);
    $shipping_state = get_user_meta($user_id, 'shipping_state', false);
    $shippingState = get_val($shipping_state);
    $shippingState = $states[$shippingState];

    //user info
    $clubMemberID = $user_id; //get_user_meta($user_id, 'club_membership_no', true);
    $club_status = 'True'; //get_user_meta($user_id, 'has_club_membership_no', true);
    $clubMemberStatus = 'True';//$club_status > 0 ? 'True' : 'False';
    $clubMemberTitle = get_user_meta($user_id, 'title', true);
    $clubMemberLoginStatus = 'True';

    $user_total_spent = wc_get_customer_total_spent($user_id);
    if ($user_total_spent == 0) {
        $clubMemberLevel = 'Bronze';
        $clubMemberLevelID = '3';
    } else if ($user_total_spent >= 10000000) {
        $clubMemberLevel = 'Gold';
        $clubMemberLevelID = '1';
    } else if ($user_total_spent <= 9999999) {
        $clubMemberLevel = 'Silver';
        $clubMemberLevelID = '2';
    }
    $persistentBasketLoaded = count(WC()->cart->get_cart()) > 0 ? 'True' : 'False';
    $language = 'EN';
    $parsed = parse_url($current_url);
    if (isset($parsed['path'])) {
        $path = $parsed['path'];
        $path_parts = explode('/', $path);
        $segment = sizeof($path_parts) > 1 ? $path_parts[1] : '';

        if ($segment == 'vi' || $segment == 'vn') {
            $language = 'VN';
        }
    }
    $my_machines = get_user_meta($user_id, 'my_machines', false);
    $machineOwned = '';
    $machine_counter = 0;

    foreach ($my_machines as $k => $v) {
        foreach ($v as $m) {
            if (isset($m['product_sku']) && $m['product_sku'] != '0') {
                $machine_id = wc_get_product_id_by_sku($m['product_sku']);
                $machine_data = wc_get_product( $machine_id );
                $machineOwned .= $machine_data->get_name() . '|';
                $machine_counter++;
            } else if( $m['serial_no'] != '' ) {
                if( $m['product_sku'] == '0' || $m['product_sku'] == '') {
                    $machineOwned .=  'Others|';
                    $machine_counter++;
                }
            }

        }
    }

    $machineOwned = substr($machineOwned, 0, strlen($machineOwned) - 1) != false ? substr($machineOwned, 0, strlen($machineOwned) - 1) : '';
    $machineOwner = $machine_counter > 0 ? 'Yes' : 'No';
    $persistentBasketLoaded = count(WC()->cart->get_cart()) > 0 ? 'True' : 'False';
?>
        <script type="text/javascript">
            window.machineOwner = "<?php echo isset($machineOwned) ? $machineOwned : ''; ?>";
            window.clubMemberID = "<?php echo isset($clubMemberID) ? $clubMemberID : ''; ?>";
            window.clubMemberStatus = "<?php echo isset($clubMemberStatus) ? $clubMemberStatus : ''; ?>";
            window.clubMemberLevel = "<?php echo isset($clubMemberLevel) ? $clubMemberLevel : ''; ?>";
            window.clubMemberLevelID = "<?php echo isset($clubMemberLevelID) ? $clubMemberLevelID : ''; ?>";
            window.clubMemberLoginStatus = "<?php echo isset($clubMemberLoginStatus) ? $clubMemberLoginStatus : ''; ?>";
            window.clubMemberTitle = "<?php echo isset($clubMemberTitle) ? $clubMemberTitle : ''; ?>";
            gtmDataObject = [{
                'isEnvironmentProd': 'Yes',
                'pageName': '<?php echo get_the_title(); ?> | Nespresso',
                'pageType': 'User Account', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                'pageCategory': 'My Account', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                'pageSubCategory': 'My Account', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                'market': 'VN',
                'version': 'VN - WooCommerce V1',
                'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                'segmentBusiness': 'B2C',
                'currency': 'VND',
                'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                'preferredTechnology': 'Espresso',
                'event': 'event_pageView',
                'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                'language': '<?= $language ?>'
            }];
            gtmDataObject.push({
                'event': 'userLogin',
                'eventCategory' : 'User Engagement',
                'eventAction' : 'User Login',
                'loginMethod' : 'Standard',
                'clubMemberID': '<?php echo $clubMemberID; ?>',
                'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                'clubMemberLevel': '<?= $clubMemberLevel ?>',
                'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
            });
        </script>

    <?php
}

function nespresso_gtm_register() {
    global $wp, $woocommerce;

    $user_id = get_current_user_id();
    $states     = dhl_get_states();
    $billing_city = get_user_meta($user_id, 'billing_city', false);
    $billingCity = get_val($billing_city);
    $billing_country = get_user_meta($user_id, 'billing_country', false);
    $billingCountry = get_val($billing_country);
    $billing_postcode = get_user_meta($user_id, 'billing_postcode', false);
    $billingPostCode = get_val($billing_postcode);
    $billing_state = get_user_meta($user_id, 'billing_state', false);
    $billingState = get_val($billing_state);
    $billingState = $states[$billingState];
    $shipping_city = get_user_meta($user_id, 'shipping_city', false);
    $shippingCity = get_val($shipping_city);
    $shipping_country = get_user_meta($user_id, 'shipping_country', false);
    $shippingCountry = get_val($shipping_country);
    $shipping_postcode = get_user_meta($user_id, 'shipping_postcode', false);
    $shippingPostCode = get_val($shipping_postcode);
    $shipping_state = get_user_meta($user_id, 'shipping_state', false);
    $shippingState = get_val($shipping_state);
    $shippingState = $states[$shippingState];

    //user info
    $clubMemberID = $user_id; //get_user_meta($user_id, 'club_membership_no', true);
    $club_status = 'True'; //get_user_meta($user_id, 'has_club_membership_no', true);
    $clubMemberStatus = 'True'; //$club_status > 0 ? 'True' : 'False';
    $clubMemberTitle = get_user_meta($user_id, 'title', true);
    $clubMemberLoginStatus = 'True';

    $user_total_spent = wc_get_customer_total_spent($user_id);
    if ($user_total_spent == 0) {
        $clubMemberLevel = 'Bronze';
        $clubMemberLevelID = '3';
    } else if ($user_total_spent >= 10000000) {
        $clubMemberLevel = 'Gold';
        $clubMemberLevelID = '1';
    } else if ($user_total_spent <= 9999999) {
        $clubMemberLevel = 'Silver';
        $clubMemberLevelID = '2';
    }
?>
        <script type="text/javascript">
            gtmDataObject = [{
                'isEnvironmentProd': 'Yes',
                'pageName': 'Personal Information | Nespresso',
                'pageType': 'User Account', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                'pageCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                'market': 'VN',
                'version': 'VN - WooCommerce V1',
                'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                'segmentBusiness': 'B2C',
                'currency': 'VND',
                'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                'preferredTechnology': 'Espresso',
                'event': 'event_pageView',
                'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                'language': '<?= $language ?>'
            }];
            gtmDataObject.push({
                'event': 'customEvent',
                'newAccountStep' : 'Personal information',
                'clubMemberID': '<?php echo $clubMemberID; ?>',
                'eventAction': 'New User Account Creation',
                'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                'clubMemberLevel': '<?= $clubMemberLevel ?>',
                'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
            });
        </script>
        <script type="text/javascript">
            function registration_step_1() {
                gtmDataObject.push({
                    'isEnvironmentProd': 'Yes',
                    'pageName': 'Personal Information | Nespresso',
                    'pageType': 'User Account', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                    'pageCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                    'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                    'market': 'VN',
                    'version': 'VN - WooCommerce V1',
                    'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                    'segmentBusiness': 'B2C',
                    'currency': 'VND',
                    'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                    'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                    'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                    'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                    'preferredTechnology': 'Espresso',
                    'event': 'event_pageView',
                    'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                    'language': '<?= $language ?>'
                });

                gtmDataObject.push({
                    'event': 'customEvent',
                    'newAccountStep' : 'Personal information',
                    'clubMemberID': '<?php echo $clubMemberID; ?>',
                    'eventAction': 'New User Account Creation',
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                    'clubMemberLevel': '<?= $clubMemberLevel ?>',
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
                });
            }

            function registration_step_2() {
                gtmDataObject.push({
                    'isEnvironmentProd': 'Yes',
                    'pageName': 'Your Address | Nespresso',
                    'pageType': 'User Account', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                    'pageCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                    'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                    'market': 'VN',
                    'version': 'VN - WooCommerce V1',
                    'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                    'segmentBusiness': 'B2C',
                    'currency': 'VND',
                    'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                    'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                    'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                    'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                    'preferredTechnology': 'Espresso',
                    'event': 'event_pageView',
                    'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                    'language': '<?= $language ?>'
                });
                gtmDataObject.push({
                    'event': 'customEvent',
                    'newAccountStep' : 'Address',
                    'eventAction': 'New User Account Creation',
                    'clubMemberID': '<?php echo $clubMemberID; ?>',
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                    'clubMemberLevel': '<?= $clubMemberLevel ?>',
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
                });
            }

            function registration_step_3() {
                gtmDataObject.push({
                    'isEnvironmentProd': 'Yes',
                    'pageName': 'Machine Info | Nespresso',
                    'pageType': 'User Account', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
                    'pageCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
                    'pageTechnology': 'Multiple', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
                    'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
                    'market': 'VN',
                    'version': 'VN - WooCommerce V1',
                    'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
                    'segmentBusiness': 'B2C',
                    'currency': 'VND',
                    'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
                    'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
                    'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
                    'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
                    'preferredTechnology': 'Espresso',
                    'event': 'event_pageView',
                    'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
                    'language': '<?= $language ?>'
                });
                var is_machine_owner = $('input[name=has_machine_registered]:checked').val();
                if(is_machine_owner == 1) {
                    var eventLabel = 'Already Own Machine';
                    var machineOwner = 'Yes';
                } else {
                    var eventLabel = 'DO NOT Already Own Machine';
                    var machineOwner = 'No';
                }
                gtmDataObject.push({
                    'event': 'customEvent',
                    'newAccountStep' : 'Machine Info',
                    'eventAction': 'New User Account Creation',
                    'clubMemberID': '<?php echo $clubMemberID; ?>',
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                    'clubMemberLevel': '<?= $clubMemberLevel ?>',
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
                    'landscape': 'WooCommerce-Standard_Agent',
                    'eventCategory': 'User Engagement',
                    'eventLabel': eventLabel, //This value is for which radio option is selected below
                    'machineOwner': machineOwner //If they select "Own a machine", set this to "Yes". Otherwise, "No"
                });
            }
            function custom_registration_step_3() {
                var is_machine_owner = $('input[name=has_machine_registered]:checked').val();
                if(is_machine_owner == 1) {
                    var eventLabel = 'Already Own Machine';
                    var machineOwner = 'Yes';
                } else {
                    var eventLabel = 'DO NOT Already Own Machine';
                    var machineOwner = 'No';
                }
                gtmDataObject.push({
                    'event': 'customEvent',
                    'newAccountStep' : 'Machine Info',
                    'eventAction': 'New User Account Creation',
                    'clubMemberID': '<?php echo $clubMemberID; ?>',
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                    'clubMemberLevel': '<?= $clubMemberLevel ?>',
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
                    'landscape': 'WooCommerce-Standard_Agent',
                    'eventCategory': 'User Engagement',
                    'eventLabel': eventLabel, //This value is for which radio option is selected below
                    'machineOwner': machineOwner //If they select "Own a machine", set this to "Yes". Otherwise, "No"
                });
            }
            function registration_step_4() {
                var is_machine_owner = $('input[name=has_machine_registered]:checked').val();
                if(is_machine_owner == 1) {
                    var eventLabel = 'Already Own Machine';
                    var machineOwner = 'Yes';
                } else {
                    var eventLabel = 'DO NOT Already Own Machine';
                    var machineOwner = 'No';
                }
                gtmDataObject.push({
                  'event': 'customEvent', //This is required and critical
                  'newAccountStep': 'Machine Info', //This is required and critical
                  'eventCategory': 'User Engagement',
                  'eventAction': 'New User Account Creation',
                  'eventLabel': eventLabel, //This value is for which radio option is selected below
                  'machineOwner': machineOwner //If they select "Own a machine", set this to "Yes". Otherwise, "No"
                });
                gtmDataObject.push({
                    'event': 'customEvent',
                    'newAccountStep' : 'Welcome to Nespresso',
                    'clubMemberID': '<?php echo $clubMemberID; ?>',
                    'clubMemberStatus': '<?php echo $clubMemberStatus; ?>',
                    'clubMemberLevel': '<?= $clubMemberLevel ?>',
                    'clubMemberTierID': '<?= $clubMemberLevelID ?>',
                    'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
                    'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>',
                });
            }
        </script>

    <?php
}

function get_gtm_products() {

    $return = ``;

    if  ( $segment == 'checkout' ) :

        $items = $woocommerce->cart->get_cart();

        foreach ($items as $k => $v) :

            $data = $v['data'];
            $item = $v;
            $product_name = $data->get_name();
            $product_id = $data->get_id();
            $product_price = $data->get_price();
            $product_qty = $item['quantity'];

            $product_categories = $data->get_category_ids();
            foreach ($product_categories as $product_category)
                $product_category = get_product_category_by_id($product_category);

                $return .= `
                    {
                        'name': '`.$product_name.`',
                        'id': '`.$product_id.`',
                        'quantity': `.$product_qty.`,
                        'price': '`.$product_price.`',
                        'category': '`.strtolower($product_category).`',
                        'dimension56' 'VirtuoLine',
                        'brand': 'Nespresso',
                        'dimension59' 'True',
                        'dimension57' 'Single Product - capsule',
                        'dimension44': 'FALSE'
                    },
                `;
            endforeach;

    else :

        global $post;
        $pageCategory = $post->post_name;

        $slugs = array(
            0 => array('category' => 'coffee', 'alias_name' => 'Capsule', 'alias_slug' => 'capsule'),
            1 => array('category' => 'machines', 'alias_name' => 'Machine', 'alias_slug' => 'machine'),
            2 => array('category' => 'accessories', 'alias_name' => 'Accessory', 'alias_slug' => 'accessory'),
        );
        $products_array = [];
        foreach ($slugs as $slug) {
            if ($categorySlug == $slug['alias_slug']) {
                $args = array('post_type' => 'product', 'product_cat' => $slug['category']);
                $loop = new WP_Query($args);
                while ($loop->have_posts()): $loop->the_post();
                    global $product;
                    if ($product->is_type('simple')) {
                        $product_type = 'Simple Product - ' . $slug['alias_name'];
                    } elseif ($product->is_type('variable')) {
                    $product_type = 'Variable Product - ' . $slug['alias_name'];
                }

                $product_attr_str = '';
                $product_id = get_the_id();
                $product_attributes = get_post_meta($product_id);
                foreach ($product_attributes['_product_attributes'] as $k => $v) {
                    $attr = unserialize($v);
                    if ((isset($attr['name'])) && isset($attr['color'])) {
                        $attr_name = $attr['name']['value'];
                        $attr_color = $attr['color']['value'];
                        $product_attr_str = $attr_name . '|' . $attr_color;
                    }
                }

                $product_array = array(
                    'name' => get_the_title(),
                    'brand' => 'Nespresso',
                    'id' => get_the_id(),
                    'price' => $product->get_price(),
                    'category' => strtolower($slug['alias_name']),
                    'quantity' => $product->get_stock_quantity(),
                    'dimension44' => 'FALSE',
                    'dimension53' => get_the_id(),
                    'dimension54' => get_the_title(),
                    'dimension55' => $product_attr_str,
                    'dimension56' => 'VirtuoLine',
                    'dimension57' => $product_type,
                );

                array_push($products_array, $product_array);
                endwhile;
                wp_reset_query();
            }
        }

        foreach ($products_array as $product) :

             $return .= `
                {
                    'name': '`.$product['name'].`',
                    'brand': '`.$product['brand'].`',
                    'id': '`.$product['id'].`',
                    'price': '`.$product['price'].`',
                    'category': '`.strtolower($product['category']).`',
                    'quantity': '`.$product['quantity'].`',
                    'dimension44': '`.$product['dimension44'].`',
                    'dimension53': '`.$product['dimension53'].`',
                    'dimension54': '`.$product['dimension54'].`',
                    'dimension55': '`.$product['dimension55'].`',
                    'dimension56': '`.$product['dimension56'].`',
                    'dimension57': '`.$product['dimension57'].`'
                }
            `;
        endforeach;

    endif;

    return $return;

} // get_gtm_products()


/**
 * chect product stock
 *
 */
add_action('wp_ajax_gtm_get_basket', 'gtm_get_basket');
add_action('wp_ajax_nopriv_gtm_get_basket', 'gtm_get_basket');
function gtm_get_basket() {
    global $woocommerce;
    $order_items = null;
    if ( !function_exists('get_product_category_by_id') ) :
        function get_product_category_by_id($category_id) {
            $term = get_term_by('id', $category_id, 'product_cat', 'ARRAY_A');
            return $term['name'];
        }
    endif;
    $items          = $woocommerce->cart->get_cart();
    $products       = [];
    $cart_item_key  = $_POST['cart_item_key'];
    $v              = $items[$cart_item_key];

    if ( $order_items )
        $data = wc_get_product( $v['product_id'] );
    else
        $data = $v['data'];

    $item = $v;
    $product_name = $data->get_name();
    $product_id = $data->get_id();
    $product_price = $data->get_price();
    $product_qty = $item['quantity'];
    $sku      = $data->get_sku();
    $hq_data  = get_hq_data($sku);

    $product_categories = $data->get_category_ids();
    $product_category = null;
    $dimension57 = 'Single Product - Accessory';
    $metric13 = 0;
    $metric14 = 0;
    $metric15 = 0;

    // foreach ($product_categories as $product_category) {
    //     $product_category = get_product_category_by_id($product_category);
    // }
    $product_category = get_post_meta($product_id, 'product_type', true);
    if (strtolower($product_category) == "coffee") {
        $product_category   = "capsule";
        $dimension57        = 'Single Product - Capsule';
        $metric13           = $product_qty;
    } elseif (strtolower($product_category) == "accessory") {
        $product_category   = "accessory";
        $dimension57        = 'Single Product - Accessory';
        $metric15           = $product_qty;

    } else {
        $product_category   = "machine";
        $dimension57        = 'Single Product - Machine';
        $metric14           = $product_qty;
    }

    $products[0]['name']           = isset($hq_data['name']) ? $hq_data['name'] : $product_name;
    $products[0]['id']             = isset($hq_data['id']) ? $hq_data['id'] : $product_id;
    $products[0]['quantity']       = $product_qty;
    $products[0]['price']          = $product_price;
    $products[0]['category']       = strtolower($product_category);
    $products[0]['dimension56']    = 'VirtuoLine';
    $products[0]['brand']          = 'Nespresso';
    // $products[0]['dimension59']    = 'True';
    $products[0]['dimension54']    = $product_name;
    $products[0]['dimension55']    = isset($hq_data['product_range']) ? $hq_data['product_range'] : $product_category;
    $products[0]['dimension53']    = $sku;
    $products[0]['dimension57']    = $dimension57;
    $products[0]['metric13']       = $metric13;
    $products[0]['metric14']       = $metric14;
    $products[0]['metric15']       = $metric15;
    $products[0]['metric17']       = 0;

    wp_send_json(['status' => true, 'products' => $products]);
}
