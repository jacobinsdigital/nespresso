<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

function custom_acf_get_data( $acf_name, $field_name ) {
    global $wpdb;
    $acf_fields = $wpdb->get_results( $wpdb->prepare( "SELECT ID,post_parent,post_name FROM $wpdb->posts WHERE post_title=%s AND post_type=%s" , $acf_name , 'acf' ) );

    if(! $acf_fields )
        return false;

    $post_meta = get_post_meta($acf_fields[0]->ID);

    foreach ($post_meta as $key => $value) {

        if(is_serialized($value[0])) {
            $unserialized_value = unserialize($value[0]);
            if (isset($unserialized_value['name'])) {
                if ($unserialized_value['name'] == $field_name)
                    return $unserialized_value;
            }
        }

    }
    return false;
}
