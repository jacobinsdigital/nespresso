<?php

/**
 * Provide a admin positive cup area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */

$raw_machines = get_machine_products();
$machines1 = [];
foreach ($raw_machines as $value) {
	$machines1[] = html_entity_decode($value->name);
}
$coffee_categories1 = [];
$coffee_categories3 = [];
$coffee_categories2 = custom_acf_get_data('Product Details', 'category');
foreach ($coffee_categories2['choices'] as $key => $value) {
	$coffee_categories3[] = $value;
}
$coffee_categories1['choices'] = $coffee_categories3;
$acce_categories1 = [];
$acce_categories3 = [];
$acce_categories2 = custom_acf_get_data('Product Details', 'collection');
foreach ($acce_categories2['choices'] as $key => $value) {
	$acce_categories3[] = $value;
}
$acce_categories1['choices'] = $acce_categories3;

$arrays = get_nespresso_product_list();
if ($arrays) {
	$coffee_categories['choices'] = $arrays['coffee'];
	$acce_categories['choices'] = $arrays['accessory'];
	$machines = $arrays['machine'];
}else{
	$machines = $machines1;
	$coffee_categories = $coffee_categories1;
	$acce_categories = $acce_categories1;
}
$current_url = home_url(add_query_arg(null, null));
$counter = 0;
$mincounter = 1;
 ?>
 <h3>Dynamic Product List</h3>
 <br>
 <h3>Machine List</h3>

<form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-best-sellers"
    >
    <input type="hidden" name="action" value="nespresso_product_list">
	<input type="hidden" name="type" value="update-or-create" id="type">
	<input type="hidden" name="redirect_url" value="<?= $current_url ?>">
	<?php foreach ($machines1 as $key => $machine) {?>
	<?php if ($counter + 1 >=sizeof($machines1)+1) {
		break;
	} ?>
	<div class="row">
		<p class="form-control">
			<?php echo $counter + 1; $mincounter = 1; ?> -

			<?php foreach ($machines1 as $value) {?>
			<?php $add = array_search($value, $machines); ?>
			<?php if ($counter === $add) {?>
			<?php $get = $add; ?>
			<?php }?>
			<?php } ?>
			<select class="machine_product_lists" data-id="machine_<?= $counter ?>" data-name="<?= trim($machines[$get]) ?>"  name="product_list_machine_product_ids[]">
				<?php foreach ($machines1 as $value) {?>
					<?php $add = array_search($value, $machines); ?>
					<?php if ($counter === $add) {?>
					<option value="<?= $value ?>" selected><?= $value ?></option>
					<?php }else{ ?>
					<option value="<?= $value ?>"><?= $value ?></option>
					<?php }$mincounter++; ?>
				<?php } ?>
			</select>
		</p>
	</div>
	<?php $counter++;} ?>
 <br>
 <h3>Coffee List</h3>
<?php $mincounter = $counter = 0; ?>

<div id="coff">
	<?php foreach ($coffee_categories1['choices'] as $cat) {?>
	<?php if ($counter + 1>=sizeof($coffee_categories1['choices'])+1) {
		break;
	} ?>
	<div class="row">
		<p class="form-control">
			<?php echo $counter + 1;$mincounter = 0; ?> -
			<?php foreach ($coffee_categories1['choices']  as $value) {?>
			<?php $add = array_search($value, $coffee_categories['choices']); ?>
			<?php if ($counter === $add) {?>
			<?php $get = $add; ?>
			<?php }?>
			<?php } ?>
			<select id="coffeelist" class="coffee_product_order" data-id="coffee_<?= $counter ?>" data-name="<?= trim($coffee_categories['choices'][$get]) ?>" name="product_list_coffe_categories[]">
				<?php foreach ($coffee_categories1['choices'] as $value2) {?>
					<?php  $key = array_search($value2, $coffee_categories['choices']); ?>
					<?php if ($counter === $key) {?>
					<option value="<?= $value2 ?>" selected><?= $value2 ?></option>
					<?php }else{ ?>
					<option value="<?= $value2 ?>"><?= $value2 ?></option>
					<?php }; ?>
				<?php } ?>
			</select>
		</p>
	</div>
	<?php $counter++;} ?>
</div>
 <br>
 <h3>Accessory List</h3>
<?php $mincounter = $counter = 0; ?>
	<?php foreach ($acce_categories1['choices'] as $cat) {?>
	<?php if ($counter + 1>=sizeof($acce_categories1['choices'])+1) {
		break;
	} ?>
	<div class="row">
		<p class="form-control">
			<?php echo $counter + 1;$mincounter = 1; ?> -
			<?php foreach ($acce_categories1['choices']  as $value) {?>
			<?php $add = array_search($value, $acce_categories['choices']); ?>
			<?php if ($counter === $add) {?>
			<?php $get = $add; ?>
			<?php }?>
			<?php } ?>
			<select class="accessory_product_list" data-id="acc_<?= $counter ?>" data-name="<?= trim($acce_categories['choices'][$get]) ?>" name="product_list_accessory_categories[]">
				<?php foreach ($acce_categories1['choices'] as $value) {?>
					<?php  $key = array_search($value, $acce_categories['choices']); ?>
					<?php if ($counter === $key) {?>
					<option value="<?= $value ?>" selected><?= $value ?></option>
					<?php }else{ ?>
					<option value="<?= $value ?>"><?= $value ?></option>
					<?php }$mincounter++; ?>
				<?php } ?>
			</select>
		</p>
	</div>
	<?php $counter++;} ?>
	<div>
		<button type="submit" class="btn btn-primary btn-flat pull-right">Submit</button>
	</div>
 </form>
