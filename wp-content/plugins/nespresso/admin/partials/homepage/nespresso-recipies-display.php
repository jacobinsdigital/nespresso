<?php

/**
 * Provide a admin recipe area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));

global $nespresso_recipe_option_name;
$recipies = get_nespresso_recipies();

?>

<div class="wrap pd-top-20 pd-bottom-20" id="recipe">

    <h3>Recipies</h3>

    <script type="text/javascript">
        $recipies = <?= $recipies ? json_encode($recipies) : 'null'; ?>;
    </script>

    <button class="btn btn-primary pull-right" id="btn-add-new-recipe">Add New Recipe</button>

    <table class="table table-striped" id="recipies-list-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($recipies ) : ?>
                <?php foreach ( $recipies as $recipe ) : ?>
                    <tr data-id="<?= $recipe->id ?>"  class="recipe-row">
                        <td class="text-center">
                            <?= $recipe->title ?>
                        </td>
                        <td class="text-center">
                            <?php if ( $recipe->image_url ) : ?>
                                <image src="<?= $recipe->image_url ?>" class="thumbnail" height="100" style="margin: auto;">
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-success btn-edit-recipe">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-danger btn-delete-recipe">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        <tbody>

    </table>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        enctype="multipart/form-data"
        role="form"
        id="form-recipe"
        class="hide"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_recipe">

                <input type="hidden" name="type" value="update-or-create" id="type">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <input type="hidden" name="id" value="" id="id-recipe">

                 <!-- title -->
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" name="title" id="title-recipe" value="" class="form-control">
                </div>

                <!-- image -->
                <div class="form-group">
                    <label for="image">Image:</label>
                    <br>
                    <img class="hide img-thumbnail btn-open-wp-modal" src="" id="image-thumbnail-recipe" data-section="recipe">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="recipe">Select Image</a>
                    <input type="hidden" name="image_url" id="image-url-recipe" value="">
                </div>

                <!-- link -->
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" name="link" id="link-recipe" value="" class="form-control">
                </div>

                 <!-- content -->
                <div class="form-group">
                    <label for="content">Content:</label>
                    <?php wp_editor( '' , 'content-recipe', $settings = array('textarea_name'=>'content') ); ?>
                </div>

                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-default btn-flat pull-left"
                        id="btn-close-recipe"
                    >
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

</div><!-- #recipe -->
