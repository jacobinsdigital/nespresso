<?php

/**
 * Provide a admin slider area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));

global $nespresso_slider_option_name;
$slides = get_nespresso_sliders();

?>

<div class="wrap pd-top-20 pd-bottom-20" id="slider">

    <h3>Slider</h3>

    <script type="text/javascript">
        $slides = <?= $slides ? json_encode($slides) : 'null'; ?>;
    </script>

    <button class="btn btn-primary pull-right" id="btn-add-new-slide">Add New Slide</button>

    <table class="table table-striped" id="slider-list-table">
        <thead>
            <tr>
                <th>Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($slides ) : ?>
                <?php foreach ( $slides as $slide ) : ?>
                    <tr data-id="<?= $slide->id ?>"  class="slide-row">
                        <td class="text-center">
                            <?php if ( $slide->image_url ) : ?>
                                <image src="<?= $slide->image_url ?>" class="thumbnail" height="100" style="margin: auto;">
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-success btn-edit">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-danger btn-delete">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        <tbody>

    </table>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        enctype="multipart/form-data"
        role="form"
        id="form-slider"
        class="hide"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_slider">

                <input type="hidden" name="action_type" value="update-or-create" id="action_type">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <input type="hidden" name="id" value="" id="id-slide">

                <!-- image -->
                <div class="form-group">
                    <label for="image">Image:</label>
                    <br>
                    <img class="hide img-thumbnail btn-open-wp-modal" src="" id="image-thumbnail-slide" data-section="slide">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="slide">Select Image</a>
                    <input type="hidden" name="image_url" id="image-url-slide" value="">
                </div>

                <!-- link -->
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" name="link" id="link-slide" value="" class="form-control">
                </div>

                <div class="form-group">
                    <label for="type">Type:</label>
                    <select name="type" class="form-control" id="type-slide">
                        <option value="call_link_slider">Link</option>
                        <option value="call_pop_up_slider">Pop Up</option>
                    </select>
                </div>

                 <!-- content -->
                <div class="form-group">
                    <label for="content">Promo Name:</label>
                    <input type="text" name="content" id="content-slide" class="form-control">
                    <?php //wp_editor( '' , 'content-slide', $settings = array('textarea_name'=>'content') ); ?>
                </div>

                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-default btn-flat pull-left"
                        id="btn-close"
                    >
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

</div><!-- #slider -->
