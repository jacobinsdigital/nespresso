<?php

/**
 * Provide a admin slider area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));
global $nespresso_slider_pop_up;
$pop_up = get_nespresso_slider_pop_up();

?>

<div class="wrap pd-top-20 pd-bottom-20" id="slider">

    <h3>Slider Pop up</h3>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-slider-pop-up-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_slider_pop_up">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4></h4>
                <div class="form-group">
                    <label for="slider_pop_up_header">Pop up Header</label>
                    <br>
                    <input type="text" class="form-control" name="slider_pop_up_header" value="<?= @$pop_up['slider_pop_up_header'] ?>">
                </div>
                <div class="form-group">
                    <label for="slider_pop_up_content">Pop up Content</label>
                    <br>
                    <textarea class="form-control" name="slider_pop_up_content" ><?= @$pop_up['slider_pop_up_content'] ?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

</div><!-- #slider -->
