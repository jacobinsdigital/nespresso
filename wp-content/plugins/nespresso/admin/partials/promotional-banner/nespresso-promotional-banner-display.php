<?php

/**
 * Provide a admin slider area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));

?>

<div class="wrap">
    <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
    <?php if ( isset($_SESSION['nespresso_alert']) && isset($_SESSION['nespresso_alert']['message'])) : ?>
        <div class="alert alert-<?= $_SESSION['nespresso_alert']['type']; ?> alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?= $_SESSION['nespresso_alert']['header']; ?></strong> <?= $_SESSION['nespresso_alert']['message']; ?>
        </div>
    <?php unset($_SESSION['nespresso_alert']); endif; ?>
</div>

<?php include_once ('nespresso-promotional-banner-pages-display.php'); ?>

<hr>

<?php include_once ('nespresso-promotional-nav-banner-pages-display.php'); ?>

