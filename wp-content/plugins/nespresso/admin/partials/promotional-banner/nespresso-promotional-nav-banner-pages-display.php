<?php

/**
 * Provide a admin positive cup area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));
$navigation_banner = get_nespresso_navigation_banner();
// dd($navigation_banner);

?>
<div class="wrap pd-top-20 pd-bottom-20" id="promotional-banner-pages">

    <h3>Promotional Navigation Banners in Pages</h3>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        enctype="multipart/form-data"
        role="form"
        id="form-promotional-navigation-banner-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_navigation_banner">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4></h4>
                <!-- Coffee -->
                <!-- image -->
                <div class="form-group">
                    <label for="image">Coffee Navigation Banner Image:</label>
                    <br>
                    <img class="<?= @$navigation_banner['coffee_navigation_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$navigation_banner['coffee_navigation_banner_image_url'] ;?>" id="image-thumbnail-coffee-navigation-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="coffee-navigation-banner">Select Image</a>
                    <input type="hidden" name="coffee_navigation_banner_image_url" value="<?= @$navigation_banner['coffee_navigation_banner_image_url'] ;?>" id="image-url-coffee-navigation-banner">
                </div>

                <!--Coffee link -->
                <div class="form-group">
                    <label for="link">Coffee Navigation Banner Title:</label>
                    <input type="text" name="coffee_navigation_banner_title" value="<?= @$navigation_banner['coffee_navigation_banner_title'] ?>" class="form-control" placeholder="e.g. Arpeggio">
                </div>

                 <!--Coffee link -->
                <div class="form-group">
                    <label for="link">Coffee Navigation Banner Link:</label>
                    <input type="text" name="coffee_navigation_banner_link" value="<?= @$navigation_banner['coffee_navigation_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>

                <!-- Machine -->
                <!-- image -->
                <div class="form-group">
                    <label for="image">Machine Navigation Banner Image:</label>
                    <br>
                    <img class="<?= @$navigation_banner['machine_navigation_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$navigation_banner['machine_navigation_banner_image_url'] ;?>" id="image-thumbnail-machine-navigation-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="machine-navigation-banner">Select Image</a>
                    <input type="hidden" name="machine_navigation_banner_image_url" value="<?= @$navigation_banner['machine_navigation_banner_image_url'] ;?>" id="image-url-machine-navigation-banner">
                </div>

                <!--Machine link -->
                <div class="form-group">
                    <label for="link">Machine Navigation Banner Title:</label>
                    <input type="text" name="machine_navigation_banner_title" value="<?= @$navigation_banner['machine_navigation_banner_title'] ?>" class="form-control" placeholder="e.g. Arpeggio">
                </div>

                 <!--Machine link -->
                <div class="form-group">
                    <label for="link">Machine Navigation Banner Link:</label>
                    <input type="text" name="machine_navigation_banner_link" value="<?= @$navigation_banner['machine_navigation_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>

                <!-- Accessory -->
                <!-- image -->
                <div class="form-group">
                    <label for="image">Accessory Navigation Banner Image:</label>
                    <br>
                    <img class="<?= @$navigation_banner['accessory_navigation_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$navigation_banner['accessory_navigation_banner_image_url'] ;?>" id="image-thumbnail-accessory-navigation-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="accessory-navigation-banner">Select Image</a>
                    <input type="hidden" name="accessory_navigation_banner_image_url" value="<?= @$navigation_banner['accessory_navigation_banner_image_url'] ;?>" id="image-url-accessory-navigation-banner">
                </div>

                <!--Accessory link -->
                <div class="form-group">
                    <label for="link">Accessory Navigation Banner Title:</label>
                    <input type="text" name="accessory_navigation_banner_title" value="<?= @$navigation_banner['accessory_navigation_banner_title'] ?>" class="form-control" placeholder="e.g. Arpeggio">
                </div>

                 <!--Accessory link -->
                <div class="form-group">
                    <label for="link">Accessory Navigation Banner Link:</label>
                    <input type="text" name="accessory_navigation_banner_link" value="<?= @$navigation_banner['accessory_navigation_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>

                <!-- Our Services -->
                <!-- image -->
                <div class="form-group">
                    <label for="image">Our Services Navigation Banner Image:</label>
                    <br>
                    <img class="<?= @$navigation_banner['our_services_navigation_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$navigation_banner['our_services_navigation_banner_image_url'] ;?>" id="image-thumbnail-our_services-navigation-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="our_services-navigation-banner">Select Image</a>
                    <input type="hidden" name="our_services_navigation_banner_image_url" value="<?= @$navigation_banner['our_services_navigation_banner_image_url'] ;?>" id="image-url-our_services-navigation-banner">
                </div>

                <!--Our Services link -->
                <div class="form-group">
                    <label for="link">Our Services Navigation Banner Title:</label>
                    <input type="text" name="our_services_navigation_banner_title" value="<?= @$navigation_banner['our_services_navigation_banner_title'] ?>" class="form-control" placeholder="e.g. Arpeggio">
                </div>

                 <!--Our Services link -->
                <div class="form-group">
                    <label for="link">Our Services Navigation Banner Link:</label>
                    <input type="text" name="our_services_navigation_banner_link" value="<?= @$navigation_banner['our_services_navigation_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>

                <!-- Contact Us -->
                <!-- image -->
                <div class="form-group">
                    <label for="image">Contact Us Navigation Banner Image:</label>
                    <br>
                    <img class="<?= @$navigation_banner['contact_us_navigation_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$navigation_banner['contact_us_navigation_banner_image_url'] ;?>" id="image-thumbnail-contact_us-navigation-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="contact_us-navigation-banner">Select Image</a>
                    <input type="hidden" name="contact_us_navigation_banner_image_url" value="<?= @$navigation_banner['contact_us_navigation_banner_image_url'] ;?>" id="image-url-contact_us-navigation-banner">
                </div>

                <!--Contact Us link -->
                <div class="form-group">
                    <label for="link">Contact Us Navigation Banner Title:</label>
                    <input type="text" name="contact_us_navigation_banner_title" value="<?= @$navigation_banner['contact_us_navigation_banner_title'] ?>" class="form-control" placeholder="e.g. Arpeggio">
                </div>

                 <!--Contact Us link -->
                <div class="form-group">
                    <label for="link">Contact Us Navigation Banner Link:</label>
                    <input type="text" name="contact_us_navigation_banner_link" value="<?= @$navigation_banner['contact_us_navigation_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>
</div><!-- #positive-cup -->
