<?php

/**
 * Provide a admin slider area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.ph/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));

$order_id = @$_POST['order_id'] ? (int)sanitize_text_field(@$_POST['order_id']) : '';

$invoice_data = null;

if ( $order_id ) {

	$order = wc_get_order( $order_id );

	if ( $order ) {

		// The Order data
		$order_data 				= $order->get_data();

		//order product details
		$order_product 				= [];

		$total_discount_amount 		= $order_data['discount_total'];
		$computed_discount_amount 	= 0;
		$counter 					= 1;
		$count 						= count($order->get_items());

		foreach ( $order->get_items() as $key => $item ) {

		    //get product details

		    if($item['variation_id']) {
		        $_product = wc_get_product( $item['variation_id'] );
		    } else {
		        $_product = wc_get_product( $item['product_id'] );
		    }


		    $discount_per_item                          = $item->get_subtotal() - $item->get_total();

		    //last loop override the last discount amount into remaining amount discount amount (due to round issue)
		    if ($counter == $count) {
		       $discount_per_item = round($total_discount_amount - $computed_discount_amount,2);
		    }

		    $computed_discount_amount					+= $discount_per_item;
		    $item_net_amount                            = $item['subtotal'] - $discount_per_item;
		    $taxAmount                                  = $item_net_amount-($item_net_amount/1.12);
		    $discount_percentage                        = ($discount_per_item / $item['subtotal']) * 100;

		    $order_product[$key]['ProductCode']         = $_product->get_sku();
		    $order_product[$key]['UnitCost']            = 0;
		    $order_product[$key]['RetailSalesPrice']    = $_product->get_price();
		    $order_product[$key]['TotalDiscount']       = round($discount_per_item,2);
		    $order_product[$key]['DiscountPercentage']  = round($discount_percentage,2);
		    $order_product[$key]['TaxAmount']           = round($taxAmount,2);
		    $order_product[$key]['TaxPercentage']       = 12; //constant
		    $order_product[$key]['ItemNetAmount']       = $item_net_amount;
		    $order_product[$key]['Quantity']            = $item['quantity'];

		    $counter++;

		}

	    $order_product_item = [];
	    foreach ($order_product as $key => $value) {
	        $order_product_item[] = $value;
	    }

	    $paymentMode = 'Cash';
	    if( $order->get_payment_method() == 'ppg') {
	    	$paymentMode = 'Visa';
	    } else if ($order->get_payment_method() == 'bacs') {
	    	$paymentMode = 'Bank Transfer';
	    }

	    //date
	    $date_created = $order_data['date_created'];
	    $date_created->modify('-8 hour');

	    //tax amount
	    $net_sale_amount = $order->get_subtotal() - $order_data['discount_total'];
	    $taxAmount = $net_sale_amount-($net_sale_amount/1.12);
		$invoice_data = json_encode([
		   "InvoiceDate" 				=> $date_created->format('Y-m-d\TH:i:s'),
		   "CustomerCode" 				=> $order_data['customer_id'],
		   "TotalDiscountAmount" 		=> $order_data['discount_total'],
		   "InvoiceNumber" 				=> $order_id,
		   "InvoiceType" 				=> "SALES",
		   "Location" 					=> "WECOM01",
		   "NetSalesAmount" 			=> $net_sale_amount,
		   // "RoundOffAmount" 			=> 0,
		   "CashierCode" 				=>  "admin",
		   "Status" 					=> 1,
		   "TaxAmount" 					=> round($taxAmount,2),
		   "TaxPercentage" 				=> 12, //12% constant
		   // "VoidBy" 					=> null,
		   // "VoidDate" 				=> null,
		   // "VoidReason" 				=> null,
		   // "WholeReceiptDiscount" 	=> 0,
		   "Terminal" 					=> 'EC001',
		   "SalesmanCode" 				=> 'admin',
		   "Remark" 					=> $order->get_customer_note(),
		   'DeliveryName'				=> $order_data['shipping']['first_name'] . ' ' . $order_data['shipping']['last_name'],
		   "DeliveryAddress" 			=> $order_data['shipping']['address_1'],
		   'DeliveryAddress2'			=> $order_data['shipping']['address_2'],
		   'DeliveryCity'				=> $order_data['shipping']['city'],
		   'DeliveryState'				=> $order_data['shipping']['state'],
		   'DeliveryCountry'			=> $order_data['shipping']['country'],
		   'DeliveryPostalCode'			=> @$order_data['shipping']['shipping_postcode'],
		   'OrderStatus'				=> 1,
		   // "TotalNoOfSalesman" 			=> 1,
		   // "PointsEarned" 				=> 0,
		   "CurrencyCode" 				=> $order_data['currency'],
		   "IsTaxExclusive" 			=> 0,
		   "ShippingCost" 				=> $order_data['shipping_total'],
		   // "PaymentPromoDiscount" 	=> null,
		   "CustomerName" 				=> $order_data['billing']['first_name'] . ' ' . $order_data['billing']['last_name'],
		   "CustomerEmail" 				=> $order_data['billing']['email'],
		   "CustomerPhone" 				=> $order_data['billing']['phone'],
		   "CustomerAddress" 			=> $order_data['billing']['address_1'] . ' ' . $order_data['billing']['address_2'] . ' ' . $order_data['billing']['city'],
		   // "OrginalInvoiceNumber" 	=> " ",
		   // "OrginalSalesOrderNumber" => " ",

		   "InvoiceItems" 				=> $order_product_item, //array
		   "PaymentItems" => [
		      [
		         "Amount" 				=> $order->get_total(),
		         "PaymentDate" 			=> $date_created->format('Y-m-d\TH:i:s'),
		         "PaymentMode" 			=> $paymentMode,
		         "Remark"				=> $order->get_payment_method_title(),
		         "CurrencyCode" 		=> $order_data['currency'],
		         // "CustomerPoint" 		=> 0,
		         // "DocumentNumber" 		=> null
		      ]
		   ]

		]);
	} // endif

} // endif

?>
<br>
<form action="<?php echo $current_url; ?>" method="post">
	<label for="order_id">Order ID:</label>
	<input type="text" name="order_id" value="<?php echo $order_id; ?>">
	<input type="submit" value="submit">
</form>
<br>
<?php if (!$invoice_data && $order_id ) { ?>
	<div>
		No order found
	</div>
<?php } // endif ?>

<?php if ($invoice_data) { ?>
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
	<div style="word-break: break-all;">
		<?php echo $invoice_data; ?>
	</div>

<?php } // endif ?>
