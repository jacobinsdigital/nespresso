<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://minionsolutions.ph/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin
 * @author     Glenn Espejo <glenn.espejo0112@gmail.com>
 */
class Nespresso_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		//  handle the homepage slider update or create form
		add_action( 'admin_post_nespresso_slider', [$this, 'post_nespresso_slider'] );

		//  handle the homepage positive cup update or create form
		add_action( 'admin_post_nespresso_positive_cup', [$this, 'post_nespresso_positive_cup'] );

		//  handle the homepage recipe update or create form
		add_action( 'admin_post_nespresso_recipe', [$this, 'post_nespresso_recipe'] );

		//  handle the homepage best sellers update or create form
		add_action( 'admin_post_nespresso_best_sellers', [$this, 'post_nespresso_best_sellers'] );

		//  handle the homepage product list order
		add_action( 'admin_post_nespresso_product_list', [$this, 'post_nespresso_product_list'] );

		//handle the homepage category banner
		add_action( 'admin_post_nespresso_category_banner', [$this, 'post_nespresso_category_banner'] );

		//handle the homepage navigation banner
		add_action( 'admin_post_nespresso_navigation_banner', [$this, 'post_nespresso_navigation_banner'] );

		//  handle the homepage mobile slider update or create form
		add_action( 'admin_post_nespresso_mobile_slider', [$this, 'post_nespresso_mobile_slider'] );

		//  handle the homepage dynamic banner slider update or create form
		add_action( 'admin_post_nespresso_left_dynamic_banner', [$this, 'post_nespresso_left_dynamic_banner'] );
		add_action( 'admin_post_nespresso_right_dynamic_banner', [$this, 'post_nespresso_right_dynamic_banner'] );

		//handle the other settings
		add_action( 'admin_post_nespresso_other_settings', [$this, 'post_nespresso_other_settings'] );

		add_action( 'admin_post_nespresso_slider_pop_up', [$this, 'post_nespresso_slider_pop_up'] );

		add_action( 'admin_post_nespresso_coys_promo', [$this, 'post_nespresso_coys_promo'] );

		add_action( 'admin_post_nespresso_coys_discount', [$this, 'post_nespresso_coys_discount'] );

		//handle the email
        add_action( 'admin_post_nespresso_email_header', [$this, 'post_nespresso_email_header'] );
        add_action( 'admin_post_nespresso_email_on_hold_order', [$this, 'post_nespresso_email_on_hold_order'] );
        add_action( 'admin_post_nespresso_email_processing_order', [$this, 'post_nespresso_email_processing_order'] );
        add_action( 'admin_post_nespresso_email_completed_order', [$this, 'post_nespresso_email_completed_order'] );
        add_action( 'admin_post_nespresso_email_reset_password', [$this, 'post_nespresso_email_reset_password'] );
        add_action( 'admin_post_nespresso_email_new_account', [$this, 'post_nespresso_email_new_account'] );

        //pickup location
        add_action( 'admin_post_nespresso_pickup_location', [$this, 'post_nespresso_pickup_location'] );

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nespresso_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nespresso_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if ( isset($_GET['page']) && preg_match("~\bnespresso\b~", $_GET['page']) ) :
			wp_enqueue_style( $this->plugin_name . '-bootstrap', plugin_dir_url( __FILE__ ) . '../vendors/bootstrap/css/bootstrap.min.css', array(), $this->version, 'all' );
		endif;

		wp_enqueue_style( $this->plugin_name .'-font-awesome', plugin_dir_url( __FILE__ ) . '../vendors/font-awesome/css/font-awesome.min.css', array(), $this->version, 'all' );

		wp_enqueue_style( $this->plugin_name .'-select2', plugin_dir_url( __FILE__ ) . '../vendors/select2/css/select2.min.css', array(), $this->version, 'all' );

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nespresso-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nespresso_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nespresso_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script('jQuery');

		wp_enqueue_script( $this->plugin_name . '-bootstrap', plugin_dir_url( __FILE__ ) . '../vendors/bootstrap/js/bootstrap.min.js', array(), $this->version, false );

		wp_enqueue_script( $this->plugin_name . '-select2', plugin_dir_url( __FILE__ ) . '../vendors/select2/js/select2.min.js', array(), $this->version, false );

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nespresso-admin.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( $this->plugin_name . '-homepage', plugin_dir_url( __FILE__ ) . 'js/nespresso-homepage.js', array(), $this->version, false );

		// for a customized meta box for product color variants, specifically for machines
		wp_enqueue_script( $this->plugin_name . '-product-color-variants', plugin_dir_url( __FILE__ ) . 'js/nespresso-product-color-variants.js', array(), $this->version, false );

		wp_localize_script( $this->plugin_name , 'updateOrCreateUrl', array(
		    'ajax_url' => admin_url( 'admin-ajax.php' )
		));

	}

	public function add_plugin_admin_menu() {

	    /*
	     * Add a settings page for this plugin to the Settings menu.
	     *
	     * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
	     *
	     *        Administration Menus: http://codex.wordpress.org/Administration_Menus
	     *
	     */


	    add_menu_page( 'Nespresso Setup', 'Nespresso', '', $this->plugin_name);

    	add_submenu_page( $this->plugin_name, 'Nespresso Homepage Configuration', 'Homepage', 'manage_options', $this->plugin_name.'-homepage', array($this, 'display_plugin_homepage_setup_page') );

    	add_submenu_page( $this->plugin_name, 'Nespresso Promotional Banner', 'Promotional Banner', 'manage_options', $this->plugin_name.'-promotional-banner', array($this, 'display_plugin_promotional_banner_setup_page') );

    	add_submenu_page( $this->plugin_name, 'Nespresso Email Settings', 'Email Settings', 'manage_options', $this->plugin_name.'-email-settings', array($this, 'display_plugin_email_settings_setup_page') );

    	add_submenu_page( $this->plugin_name, 'Nespresso Promotion Settings', 'Promotion Settings', 'manage_options', $this->plugin_name.'-other-settings', array($this, 'display_plugin_other_settings_setup_page') );

    	add_submenu_page( $this->plugin_name, 'Nespresso Pickup Location Settings', 'Pickup Location', 'manage_options', $this->plugin_name.'-pickup-location-settings', array($this, 'display_plugin_pickup_location_settings_setup_page') );
    	// hack
    	// only for developers
    	$user = wp_get_current_user();
    	if ( $user->data->user_login == 'glenn.espejo0112@gmail.com' ) {
    		// add_submenu_page( $this->plugin_name, 'Get Order Manual', 'Get Order Manual', 'manage_options', $this->plugin_name.'-get-order-manual', array($this, 'display_plugin_get_order_manual_page') );
		}
		$page = @$_GET['page'];
		$page = strtolower($page);
		if (strpos($page, 'nespresso') !== false) {
			wp_enqueue_media();
		}

    	remove_filter( 'the_content', 'wpautop' );

    	add_action( 'wp_ajax_post_update_or_create', [$this, 'post_update_or_create'] );
	}

	 /**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */

	public function add_action_links( $links ) {
	    /*
	    *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
	    */
	   $settings_link = array(
	    '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __('Settings', $this->plugin_name) . '</a>',
	   );
	   return array_merge(  $settings_link, $links );

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_setup_page() {
	    include_once( 'partials/nespresso-homepage-display.php' );
	}

	/**
	 * Render the settings hompage page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_homepage_setup_page() {
	    include_once( 'partials/homepage/nespresso-homepage-display.php' );
	}

	/**
	 * Render for promotional banner
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_promotional_banner_setup_page() {
	    include_once( 'partials/promotional-banner/nespresso-promotional-banner-display.php' );
	}

	/**
	 * Render for promotional banner
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_other_settings_setup_page() {
	    include_once( 'partials/settings/nespresso-settings-display.php' );
	}

	/**
	 * Render for get order manual
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_get_order_manual_page() {
	    include_once( 'partials/dev/nespresso-get-order-manual.php' );
	}


	/**
     * Render for promotional banner
     *
     * @since    1.0.0
     */
    public function display_plugin_email_settings_setup_page() {
        include_once( 'partials/email/nespresso-email-display.php' );
    }

    /**
     * Render for promotional banner
     *
     * @since    1.0.0
     */
    public function display_plugin_pickup_location_settings_setup_page() {
        include_once( 'partials/settings/nespresso-pickup-location-display.php' );
    }

	public function add_meta_host_url_on_head() {
		echo '<meta name="host" value="">';
	}

	/**
	 * handle the slider update or create form
	 *
	 */
	public function post_nespresso_slider() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$type = sanitize_text_field( isset($_POST['action_type']) ? $_POST['action_type'] : null);

		if ($type == 'delete')
			$nespresso_slider = delete_nespresso_slider($_POST);
		else
			$nespresso_slider = save_nespresso_slider($_POST);

		if ( $nespresso_slider )
			$query_param = "?nespresso-slider-update=success";
		else
			$query_param = "?nespresso-slider-update=fail";

		wp_redirect( $redirect_url );
		exit;

	} // post_nespresso_homepage()

	/**
	 * handle the slider update or create form
	 *
	 */
	public function post_nespresso_positive_cup() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$positive_cup = save_nespresso_positive_cup($_POST);

		if ( $positive_cup )
			$query_param = "?nespresso-positive-cup-update=success";
		else
			$query_param = "?nespresso-positive-cup-update=fail";

		wp_redirect( $redirect_url );
		return;

	} // post_nespresso_homepage()

	/**
	 * handle the recipe update or create form
	 *
	 */
	public function post_nespresso_recipe() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$type = sanitize_text_field( isset($_POST['type']) ? $_POST['type'] : null);

		if ($type == 'delete')
			$nespresso_recipe = delete_nespresso_recipe($_POST);
		else
			$nespresso_recipe = save_nespresso_recipe($_POST);

		if ( $nespresso_recipe )
			$query_param = "?nespresso-recipe-update=success";
		else
			$query_param = "?nespresso-recipe-update=fail";

		wp_redirect( $redirect_url );
		exit;

	} // post_nespresso_recipe()

	/**
	 * handle the slider update or create form
	 *
	 */
	public function post_nespresso_best_sellers() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$nespresso_recipe = save_nespresso_best_sellers($_POST);

		if ( $best_seller )
			$query_param = "?nespresso-best-seller-update=success";
		else
			$query_param = "?nespresso-best-seller-update=fail";

		wp_redirect( $redirect_url );
		return;

	} // post_nespresso_best_seller()


	/**
	 * handle the slider update or create form
	 *
	 */
	public function post_nespresso_product_list() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$product_list = save_nespresso_product_list($_POST);

		if ( $product_list )
			$query_param = "?nespresso-product-list-update=success";
		else
			$query_param = "?nespresso-product-list-update=fail";

		wp_redirect( $redirect_url );
		return;

	} // post_nespresso_product_list()


	/**
	 * handle the slider update or create form
	 *
	 */
	public function post_nespresso_category_banner() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$nespresso_category_banner = save_nespresso_category_banner($_POST);

		if ( $nespresso_category_banner )
			$query_param = "?nespresso-category-banner-update=success";
		else
			$query_param = "?nespresso-category-banner-update=fail";

		wp_redirect( $redirect_url );
		return;

	} // post_nespresso_category_banner()


	/**
	 * handle the slider update or create form
	 *
	 */
	public function post_nespresso_navigation_banner() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$nespresso_navigation_banner = save_nespresso_navigation_banner($_POST);

		if ( $nespresso_navigation_banner )
			$query_param = "?nespresso-navigation-banner-update=success";
		else
			$query_param = "?nespresso-navigation-banner-update=fail";

		wp_redirect( $redirect_url );
		return;

	} // post_nespresso_navigation_banner()

	public function post_nespresso_mobile_slider() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$type = sanitize_text_field( isset($_POST['action_type']) ? $_POST['action_type'] : null);

		if ($type == 'delete')
			$nespresso_mobile_slider = delete_nespresso_mobile_slider($_POST);
		else
			$nespresso_mobile_slider = save_nespresso_mobile_slider($_POST);

		if ( $nespresso_mobile_slider )
			$query_param = "?nespresso-mobile-slider-update=success";
		else
			$query_param = "?nespresso-mobile-slider-update=fail";

		wp_redirect( $redirect_url );
		exit;

	} // post_nespresso_homepage()



	/**
	 * handle the other settings update or create form
	 *
	 */
	public function post_nespresso_other_settings() {

		$nespresso_other_settings = save_nespresso_other_settings($_POST);

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		if ( $nespresso_other_settings )
			$query_param = "?nespresso-other-settings-update=success";
		else
			$query_param = "?nespresso-other-settings-update=fail";

		wp_redirect( $redirect_url );
		return;

	} // post_nespresso_navigation_banner()

	public function post_nespresso_left_dynamic_banner() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$type = sanitize_text_field( isset($_POST['type']) ? $_POST['type'] : null);

		if ($type == 'delete')
			$nespresso_left_dynamic_banner = delete_nespresso_left_dynamic_banner($_POST);
		else
			$nespresso_left_dynamic_banner = save_nespresso_left_dynamic_banner($_POST);

		if ( $nespresso_left_dynamic_banner )
			$query_param = "?nespresso-left-dynamic-banner-update=success";
		else
			$query_param = "?nespresso-left-dynamic-banner-update=fail";

		wp_redirect( $redirect_url );
		exit;

	} // post_nespresso_homepage()

	public function post_nespresso_right_dynamic_banner() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$type = sanitize_text_field( isset($_POST['type']) ? $_POST['type'] : null);

		if ($type == 'delete')
			$nespresso_right_dynamic_banner = delete_nespresso_right_dynamic_banner($_POST);
		else
			$nespresso_right_dynamic_banner = save_nespresso_right_dynamic_banner($_POST);

		if ( $nespresso_right_dynamic_banner )
			$query_param = "?nespresso-right-dynamic-banner-update=success";
		else
			$query_param = "?nespresso-right-dynamic-banner-update=fail";

		wp_redirect( $redirect_url );
		exit;

	} // post_nespresso_homepage()

	public function post_nespresso_slider_pop_up() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$nespresso_slider_pop_up = save_nespresso_slider_pop_up($_POST);

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		if ( $nespresso_slider_pop_up )
			$query_param = "?nespresso-other-settings-update=success";
		else
			$query_param = "?nespresso-other-settings-update=fail";

		wp_redirect( $redirect_url );
		exit;

	} // post_nespresso_homepage()

	public function post_nespresso_coys_promo() {
		$nespresso_coys_promo = save_nespresso_coys_promo($_POST);

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		session_start();
		if ( $nespresso_coys_promo ) {
			$query_param = "?nespresso-coy-promo-update=success";
			$_SESSION['nespresso_alert'] = ['message'=> 'COYS promo save successful.', 'type' => 'success', 'header' => 'Well done!'];
		} else {
			$query_param = "?nespresso-coy-promo-update=fail";
			$_SESSION['nespresso_alert'] = ['message'=> 'Something went wrong. Please try again later.', 'type' => 'danger', 'header' => 'Oh snap!'];
		}

		wp_redirect( $redirect_url );

		return;

	} // post_nespresso_coys_promo()

	public function post_nespresso_coys_discount() {
		$nespresso_coys_discount = save_nespresso_coys_discount($_POST);

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		session_start();
		if ( $nespresso_coys_discount ) {
			$query_param = "?nespresso-coy-discount-update=success";
			$_SESSION['nespresso_alert'] = ['message'=> 'COYS discount successful.', 'type' => 'success', 'header' => 'Well done!'];
		} else {
			$query_param = "?nespresso-coy-discount-update=fail";
			$_SESSION['nespresso_alert'] = ['message'=> 'Something went wrong. Please try again later.', 'type' => 'danger', 'header' => 'Oh snap!'];
		}

		wp_redirect( $redirect_url );

		return;

	} // post_nespresso_coys_discount()

	public function post_nespresso_email_header() {

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        $nespresso_email_header = save_nespresso_email_header($_POST);

        $redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

        if ( $nespresso_email_header )
            $query_param = "?nespresso-email-header-update=success";
        else
            $query_param = "?nespresso-email-header-update=fail";

        wp_redirect( $redirect_url );
        exit;

    } // post_nespresso_email_header()

    public function post_nespresso_email_on_hold_order() {

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        $nespresso_email_on_hold_order = save_nespresso_email_on_hold_order($_POST);

        $redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

        if ( $nespresso_email_on_hold_order )
            $query_param = "?nespresso-email-on-hold-order-update=success";
        else
            $query_param = "?nespresso-email-on-hold-order-update=fail";

        wp_redirect( $redirect_url );
        exit;

    } // post_nespresso_email_header()

    public function post_nespresso_email_processing_order() {

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        $nespresso_email_processing_order = save_nespresso_email_processing_order($_POST);

        $redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

        if ( $nespresso_email_processing_order )
            $query_param = "?nespresso-email-processing-order-update=success";
        else
            $query_param = "?nespresso-email-processing-order-update=fail";

        wp_redirect( $redirect_url );
        exit;

    } // post_nespresso_email_processing_order()

    public function post_nespresso_email_completed_order() {

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        $nespresso_email_completed_order = save_nespresso_email_completed_order($_POST);

        $redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

        if ( $nespresso_email_completed_order )
            $query_param = "?nespresso-email-completed-order-update=success";
        else
            $query_param = "?nespresso-email-completed-order-update=fail";

        wp_redirect( $redirect_url );
        exit;

    } // post_nespresso_email_completed_order()

    public function post_nespresso_email_reset_password() {

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        $nespresso_email_reset_password = save_nespresso_email_reset_password($_POST);

        $redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

        if ( $nespresso_email_reset_password )
            $query_param = "?nespresso-email-reset-password-update=success";
        else
            $query_param = "?nespresso-email-reset-password-update=fail";

        wp_redirect( $redirect_url );
        exit;

    } // post_nespresso_email_reset_password()

    public function post_nespresso_email_new_account() {

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        $nespresso_email_new_account = save_nespresso_email_new_acount($_POST);

        $redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

        if ( $nespresso_email_new_account )
            $query_param = "?nespresso-email-reset-password-update=success";
        else
            $query_param = "?nespresso-email-reset-password-update=fail";

        wp_redirect( $redirect_url );
        exit;

    } // post_nespresso_email_new_account()


    public function post_nespresso_pickup_location() {
		$nespresso_pickup_location = save_nespresso_pickup_location($_POST);

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		session_start();
		if ( $nespresso_pickup_location ) {
			$query_param = "?nespresso-pickup-location-update=success";
			$_SESSION['nespresso_alert'] = ['message'=> 'Pickup location save successful.', 'type' => 'success', 'header' => 'Well done!'];
		} else {
			$query_param = "?nespresso-pickup-location-update=fail";
			$_SESSION['nespresso_alert'] = ['message'=> 'Something went wrong. Please try again later.', 'type' => 'danger', 'header' => 'Oh snap!'];
		}

		wp_redirect( $redirect_url );

		return;

	}// post_nespresso_pickup_location()

} // class Nespresso_Admin
