
/**
 * product-color-variants.js
 */

(function($) {

	$(document).ready( function() {

		// this selector is for the product type and should be looked closely
		$('#acf-field-product_type').showHideProductColorVariant();

		// this selector is for the product type and should be looked closely
		$('#acf-field-product_type').on('change load ready', function() {
			$(this).showHideProductColorVariant();
		});

	});

	/**
	 * show or hide product color variants
	 */
	$.fn.showHideProductColorVariant = function() {

		var self = $(this);

		// reference in class-nesspresso-admin.php product color variant metabox id
		var container_product_color_variant = $('#container-product-color-variant');

		if ( self.val() == 'Machine' )
			container_product_color_variant.show();
		else
			container_product_color_variant.hide()

	};

})(jQuery);