<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://minionsolutions.com
 * @since      1.0.0
 *
 * @package    Axapta_Api
 * @subpackage Axapta_Api/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Axapta_Api
 * @subpackage Axapta_Api/includes
 * @author     Glenn Espejo <glenn@espejo0112@gmail.com>
 */
class Axapta_Api_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
