<?php
// only for developers
$user = wp_get_current_user();
    global $wpdb;
    $table_name = "axapta_logs";
    if (isset($_GET['order_id']) && $_GET['order_id']) {
        $order_id = $_GET['order_id'];
        $logs = $wpdb->get_results( "SELECT * FROM $table_name WHERE test_key LIKE '%$order_id%'  ORDER BY date_created DESC LIMIT 0, 100" );
    } else {
        $logs = $wpdb->get_results( "SELECT * FROM $table_name ORDER BY date_created DESC LIMIT 0, 100" );
    }
?>
    <br><br>
    <form>
        <input type="hidden" name="page" value="<?=$_GET['page']?>">
        <div class="form-group">
            <label for="order_id">Filter by Order#</span></label>
            <input type="number" name="order_id">
            <button>Submit</button>
        </div>
    </form>
    <br>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <div>
    <table id="orders">
        <thead>
            <th>Created Date</th>
            <th>Action</th>
            <th>Data</th>
        </thead>
        <tbody>
            <?php foreach ($logs as $log): ?>
                <tr>
                    <td><?=$log->date_created?></td>
                    <td><?=$log->test_key?></td>
                    <td><?=htmlentities ($log->test_value)?></td>
                </tr>
            <?php endforeach?>
        </tbody>
    </table>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#orders').DataTable();
    } );
</script>
