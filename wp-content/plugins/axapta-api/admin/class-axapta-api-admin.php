<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://minionsolutions.com
 * @since      1.0.0
 *
 * @package    Axapta_Api
 * @subpackage Axapta_Api/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Axapta_Api
 * @subpackage Axapta_Api/admin
 * @author     Glenn Espejo <glenn@espejo0112@gmail.com>
 */
class Axapta_Api_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		add_action( 'admin_post_axapta_settings', [$this, 'post_axapta_settings'] );

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Axapta_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Axapta_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if (isset($_GET['page']) && $_GET['page'] == 'axapta-api-settings') {
			wp_enqueue_style($this->plugin_name . '-bootstrap', plugin_dir_url(__FILE__) . '../node_modules/bootstrap/dist/css/bootstrap.min.css', array(), $this->version, 'all');
		}

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/axapta-api-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Axapta_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Axapta_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script('jQuery');

		wp_enqueue_script($this->plugin_name . '-bootstrap', plugin_dir_url(__FILE__) . '../node_modules/bootstrap/dist/js/bootstrap.min.js', array(), $this->version, false);

		wp_enqueue_script($this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/axapta-api-admin.js', array( 'jquery' ), $this->version, false );

		wp_localize_script($this->plugin_name, 'updateOrCreateUrl', array(
           'ajax_url' => admin_url('admin-ajax.php'),
       ));

	}

	public function add_plugin_admin_menu() {

		add_menu_page( 'AXAPTA API SETUP', 'AXAPTA', '', $this->plugin_name);
		add_submenu_page( $this->plugin_name, 'AXAPTA', 'Setup', 'manage_options', $this->plugin_name.'-settings', array($this, 'display_plugin_setup_page') );
    	add_submenu_page( $this->plugin_name, 'Logs', 'Logs', 'manage_options', $this->plugin_name.'-get-logs', array($this, 'display_plugin_log_page') );
	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */

	public function add_action_links( $links ) {
	    /*
	    *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
	    */
	   $settings_link = array(
	    '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __('Settings', $this->plugin_name) . '</a>',
	   );
	   return array_merge(  $settings_link, $links );

	}

	public function display_plugin_setup_page() {
	    include_once( 'partials/setup/setup-page.php' );
	}

	public function display_plugin_log_page() {
	    include_once( 'partials/setup/log-page.php' );
	}

	/**
	 * handle the slider update or create form
	 *
	 */
	public function post_axapta_settings() {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$redirect_url = sanitize_text_field( isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '');

		$axapta_settings = save_axapta_settings($_POST);
		session_start();
		if ( $axapta_settings ) {
			$query_param = "?axapta-settings-update=success";
			$_SESSION['axapta_alert'] = ['message'=> 'Settings save successful.', 'type' => 'success', 'header' => 'Well done!'];
		} else {
			$query_param = "?axapta-settings-update=fail";
			$_SESSION['axapta_alert'] = ['message'=> 'Something went wrong. Please try again later.', 'type' => 'danger', 'header' => 'Oh snap!'];
		}

		wp_redirect( $redirect_url );
		return;

	} // post_axapta_settings()

}
